package com.xyxy.platform.admin.system.service;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.admin.system.entity.Icon;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by VirtueCai on 15/11/24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class IconServiceImplTest {

    @Autowired
    IconService iconService;

    @Test
    public void testFindAll() throws Exception {
        List<Icon> iconList = iconService.findAll();
        System.out.println(iconList);
    }

    @Test
    public void testFindByCategory() throws Exception {
        List<Icon> iconList = iconService.findByCategory(0l);
        System.out.println(iconList);
    }

    @Test
    public void testPage() throws Exception {
        Icon icon = new Icon();
        icon.setCategory(1l);
        PageInfo<Icon> page = iconService.page(icon, 1, 20);
        System.out.println(page);
    }
}