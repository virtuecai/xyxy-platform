package com.xyxy.platform.admin.system.repository;

import com.github.pagehelper.PageHelper;
import com.xyxy.platform.admin.system.entity.Resource;
import com.xyxy.platform.admin.system.repository.mybatis.ResourceMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by VirtueCai on 15/11/19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-config.xml"})
public class ResourceMapperTest {

    @Autowired
    ResourceMapper mapper;

    @Test
    public void testCreateResource() {
        Resource r = new Resource();
        r.setAvailable(true);
        r.setName("会画二级");
        r.setParentId(51l);
        r.setParentIds("0/1/2/51/");
        r.setPermission("system:session:two");
        r.setType(Resource.ResourceType.menu);
        mapper.createResource(r);
        System.out.println(r);
    }

    @Test
    public void testDeleteResourceByParentIds() {
        int rows = mapper.deleteResourceByParentIds("号%");
        System.out.println(rows);
    }

    @Test
    public void findAll() {
        //PageHelper.startPage(1, 2);
        List list1 = mapper.findAll();
        System.out.println(list1);
    }

    @Test
    public void testFindByPermission() throws Exception {

        PageHelper.startPage(1, 2);
        PageHelper.orderBy("parent_id");

//        List list1 = mapper.findByPermission("%view%");
//
//        System.out.println(list1.size());
//        Page page1 = (Page)list1;
//        System.out.println("list1.size:" +list1.size());
//        System.out.println("page1.gettotal:" + +page1.getTotal());
//        System.out.println("page1.getorderby:" + page1.getOrderBy());
//        for(Object r : list1) {
//            System.out.println(r);
//        }
//        System.out.println("------------- page1 end------------");
//
//        PageHelper.startPage(2, 2);
//        PageHelper.orderBy("parent_id");
//        List list2 = mapper.findByPermission("%view%");
//        System.out.println("list2.size:" +list1.size());
//        System.out.println("page2.gettotal:" + +page1.getTotal());
//        System.out.println("page2.getorderby:" + page1.getOrderBy());
//        for(Object r : list2) {
//            System.out.println(r);
//        }
//        System.out.println("------------- page2 end------------");
    }
}