package com.xyxy.platform.admin.system.repository;

import com.xyxy.platform.admin.system.entity.User;
import com.xyxy.platform.admin.system.repository.mybatis.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by VirtueCai on 15/11/18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class UserMapperTest {

    @Autowired
    UserMapper userMapper;

    @Test
    public void testFindOne() throws Exception {
        User user = userMapper.findOne(1l);
        System.out.println(user);
    }

    @Test
    public void findAll() throws Exception {
        List<User> userList = userMapper.findAll();
        Assert.assertNotNull(userList);
    }

    @Test
    public void findByUsername() throws Exception {
        User user = userMapper.findByUsername("admin");
        System.out.println(user);
    }

}