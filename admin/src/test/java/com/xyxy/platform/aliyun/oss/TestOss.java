package com.xyxy.platform.aliyun.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.UUID;

/**
 * Created by VirtueCai on 15/11/27.
 */
public class TestOss {

    static String accessKeyId = "btrTikBH47M18k8g";
    static String accessKeySecret = "keLpUsRlvExLWT79Dp1OFMhAYxLeQP";
    //static String endpoint = "http://oss-cn-hangzhou-internal.aliyuncs.com";//ECS内网访问
    static String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";//外网访问

    OSSClient client = null;

    @Before
    public void before(){
        client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
    }

    private static final String bucketName = "testcreatebucket";

    /**
     * 新建bucket
     * Bucket是OSS全局命名空间，相当于数据的容器，可以存储若干Object。您可以按照下面的代码新建一个Bucket：
     */
    //@Test
    public void testCreateBucket() {
        // 新建一个Bucket, bucketName
        Bucket bucket = client.createBucket(bucketName);
        System.out.println(bucket);
    }

    //@Test
    public void fileUpload() throws Exception {
        String filePath = "/Users/czd/Documents/sys_icon.sql";
        PutObjectResult result = putObject(bucketName, "level1/level2/" + generateFileName(filePath), filePath);
        System.out.println(result);
    }

    public String generateFileName(String filePath) {
        return UUID.randomUUID().toString() + "." + getFilePostfix(filePath);
    }

    public String getFilePostfix(String filePath) {
        if(StringUtils.isEmpty(filePath)) {
            return null;
        }
        return filePath.substring(filePath.lastIndexOf(".") + 1);
    }

    private PutObjectResult putObject(String bucketName, String key, String filePath) throws FileNotFoundException {
        // 初始化OSSClient
        //OSSClient client = new OSSClient(endpoint,accessKeyId, accessKeySecret);
        // 获取指定文件的输入流
        File file = new File(filePath);
        InputStream content = new FileInputStream(file);
        // 创建上传Object的Metadata
        ObjectMetadata meta = new ObjectMetadata();
        // 必须设置ContentLength
        meta.setContentLength(file.length());
        // 上传Object.
        PutObjectResult result = client.putObject(bucketName, key, content, meta);
        // 打印ETag
        //System.out.println(result.getETag());
        return result;
    }

    //@Test
    public void testListObjects() {
        this.listObjects(bucketName);
    }

    /**
     * 当您完成一系列上传后，可能需要查看某个Bucket中有哪些Object，可以通过下面的程序实现：
     * @param bucketName
     */
    public void listObjects(String bucketName) {

        // 获取指定bucket下的所有Object信息
        ObjectListing listing = client.listObjects(bucketName);

        // 遍历所有Object
        for (OSSObjectSummary objectSummary : listing.getObjectSummaries()) {
            System.out.println(objectSummary.getKey());
        }
    }

    //@Test
    public void testGetObject() throws IOException {
        String key = "level1/level2/cff45fcf-b166-463c-ad3d-ad1254db9ae5.sql";
        getObject(bucketName, key);
    }

    public void getObject(String bucketName, String key) throws IOException {
        // 获取Object，返回结果为OSSObject对象
        OSSObject object = client.getObject(bucketName, key);
        // 获取Object的输入流
        InputStream objectContent = object.getObjectContent();
        // 处理Object
        //...
        // 关闭流
        objectContent.close();
    }


}
