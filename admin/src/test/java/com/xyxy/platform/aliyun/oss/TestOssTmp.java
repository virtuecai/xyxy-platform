package com.xyxy.platform.aliyun.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.*;
import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by VirtueCai on 15/11/17.
 */
public class TestOssTmp {

    static String accessKeyId = "btrTikBH47M18k8g";
    static String accessKeySecret = "keLpUsRlvExLWT79Dp1OFMhAYxLeQP";
    //static String endpoint = "http://oss-cn-hangzhou-internal.aliyuncs.com";//ECS内网访问
    static String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";//外网访问
    OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);

    /**
     * todo 上传文件到阿里云
     * @param filePath 文件目录,是个数组
     * @param bucketName 文件夹名 可单个可多个 对应文件就行
     * @param objectName 上传到阿里云服务器的保存的文件名
     * @return 上传结果
     * @throws FileNotFoundException
     */
    public Object uploadToAliyun(String[] filePath,String[] bucketName,String[] objectName) throws FileNotFoundException {
        //初始化一个存放上传结果的map k=文件名 v=结果
        Map<Object,Object> returnmap=new HashMap<Object,Object>();

        // 初始化一个OSSClient
        OSSClient client = new OSSClient(endpoint,accessKeyId, accessKeySecret);

        if(filePath.length==1){ //单个上传
            // 获取指定文件的输入流
            File f=new File(filePath[0]);
            InputStream content = new FileInputStream(f);
            // 创建上传Object的Metadata
            ObjectMetadata meta = new ObjectMetadata();
            // 必须设置ContentLength
            meta.setContentLength(f.length());

            // 上传Object
            PutObjectResult result = client.putObject(bucketName[0],  objectName[0] , content, meta );

            // 打印ETag
            System.out.println(result.getETag());
            returnmap.put(objectName[0], result.getETag());
            return returnmap;
        }else if(filePath.length>1){ //多个上传
            for(int i=0;i<filePath.length;i++){
                // 获取指定文件的输入流
                File f=new File(filePath[i]);
                InputStream content = new FileInputStream(f);
                // 创建上传Object的Metadata
                ObjectMetadata meta = new ObjectMetadata();
                // 必须设置ContentLength
                meta.setContentLength(f.length());

                // 上传Object
                PutObjectResult result = client.putObject(bucketName[i],  objectName[i] , content, meta );

                // 打印ETag
                //System.out.println(result.getETag());
                returnmap.put(objectName[i], result.getETag());
            }
            return returnmap;
        }
        return "上传失败,服务器未响应,请重试！";
    }



    /**
     * 列出文件夹里所有文件
     * @param bucketName 文件夹名
     */
    public static void listObjects(String bucketName) {

        // 初始化OSSClient
        OSSClient client = new OSSClient(endpoint,accessKeyId, accessKeySecret);

        // 获取指定bucket下的所有Object信息
        ObjectListing listing = client.listObjects(bucketName);

        // 遍历所有Object
        for (OSSObjectSummary objectSummary : listing.getObjectSummaries()) {
            System.out.println(objectSummary.getKey());
        }
    }

    /**
     * 获取指定object
     * @param bucketName 文件夹名
     * @param key
     * @throws IOException
     */
    public InputStream getObject(String bucketName, String key) throws IOException {

        // 初始化OSSClient
        OSSClient client = new OSSClient(endpoint,accessKeyId, accessKeySecret);
        // 获取Object，返回结果为OSSObject对象
        OSSObject object = client.getObject(bucketName, key);
        // 获取Object的输入流
        InputStream objectContent = object.getObjectContent();
        if(objectContent!=null)
            return objectContent;
        return null;
    }

}