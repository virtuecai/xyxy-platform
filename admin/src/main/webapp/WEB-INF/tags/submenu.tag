<%@tag pageEncoding="UTF-8" description="构建子菜单" %>
<%@ attribute name="menu" type="com.xyxy.platform.modules.core.vo.system.Menu" required="true" description="当前菜单" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="admin" tagdir="/WEB-INF/tags" %>
<c:choose>
    <c:when test="${fn:length(menu.childMenuList) == 0}">
        <li><a href="${menu.url}">${menu.name}</a></li>
    </c:when>
    <c:otherwise>
        <li>
            <a href="#">${menu.name}</a>
            <ul>
                <c:forEach items="${menu.childMenuList}" var="childMenu">
                    <admin:submenu menu="${childMenu}"/>
                </c:forEach>
            </ul>
        </li>
    </c:otherwise>
</c:choose>

<%!
    private static String menuUrl(HttpServletRequest request, String url) {
        if(url.startsWith("http")) {
            return url;
        }
        String ctx = request.getContextPath();

        if(url.startsWith(ctx) || url.startsWith("/" + ctx  )) {
            return url;
        }

        if(!url.startsWith("/")) {
            url = url + "/";
        }
        return ctx + url;

    }
%>

