<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglibs.jspf"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/js/plugin/zTree_v3-3.5.18/css/zTreeStyle/zTreeStyle.css">
</head>
<body>


<div class="row">
    <div class="col-md-4">
        <a class="btn btn-success btn-xs tree-reload" href="javascript:void(0);">刷新</a>
        <ul id="tree" class="ztree">
            <li>loading...</li>
        </ul>
    </div>
    <div class="col-md-7">
        <div>
            <form action="" id="tree-form" class="smart-form" style="display: none;background: #fff;">
                <input type="hidden" name="id"/>
                <input type="hidden" name="parentId"/>
                <input type="hidden" name="parentIds"/>
                <input type="hidden" name="available"/>
                <header>节点内容编辑</header>
                <fieldset>
                    <section>
                        <label class="input"> <i class="icon-append fa fa-tags"></i>
                            <input type="text" name="name">
                            <b class="tooltip tooltip-bottom-right">请正确输入内容</b> </label>
                    </section>
                </fieldset>
                <footer>
                    <shiro:hasPermission name="system:organization:update">
                        <button type="submit" class="btn btn btn-primary save">保存</button>
                    </shiro:hasPermission>
                    <shiro:hasPermission name="system:organization:delete">
                        <button type="button" class="btn btn-danger delete">删除</button>
                    </shiro:hasPermission>
                    <shiro:hasPermission name="system:organization:create">
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#appendChildModal">添加子节点</button>
                    </shiro:hasPermission>
                </footer>
            </form>
        </div>
    </div>
</div>

<jsp:include page="append.child.modal.jsp"/>

<script>
    //标识是否拥有 更新权限, 用于节点移动判断
    var hasUpdateNodePermission = false;
    <shiro:hasPermission name="system:organization:update">
    hasUpdateNodePermission = true;
    </shiro:hasPermission>
</script>
<script src="${pageContext.request.contextPath}/static/js/plugin/zTree_v3-3.5.18/js/jquery.ztree.all-3.5.js"></script>
<script src="${ctx}/static/js/admin/system/organization.js"></script>
</body>
</html>