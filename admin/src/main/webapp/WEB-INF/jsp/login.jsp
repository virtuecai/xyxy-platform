<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh-CN" id="extr-page">
<head>
    <meta charset="utf-8">
    <title> SmartAdmin</title>
    <meta name="description" content="xyxy-platform-admin">
    <meta name="author" content="xyxy-team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-rtl.min.css">

    <!-- We recommend you use "your_style.css" to override SmartAdmin
         specific styles this will also ensure you retrain your customization with each SmartAdmin update.
    <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/demo.min.css">

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="${ctx}/static/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="${ctx}/static/img/favicon/favicon.ico" type="image/x-icon">

    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="${ctx}/static/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="${ctx}/static/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="${ctx}/static/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="${ctx}/static/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

</head>

<body class="animated fadeInDown" style="display: none;">

<header id="header">

    <div id="logo-group">
        <span id="logo"> <img src="${ctx}/static/img/logo.png" alt="SmartAdmin"> </span>
    </div>


</header>

<div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">

        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" style="<shiro:user>display: none;</shiro:user>">
                <c:if test="${not empty error}">
                <div class="alert alert-danger fade in">
                    <button class="close" data-dismiss="alert"> × </button> ${error}
                </div>
                </c:if>
                <div class="well no-padding">
                    <form id="login-form" action="" method="post" class="smart-form client-form" >
                        <header>登录</header>
                        <fieldset>

                            <section>
                                <label class="label">用户名</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" name="username">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> 请输入登录用户名!</b></label>
                            </section>

                            <section>
                                <label class="label">密码</label>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> 请输入登录密码!</b> </label>
                                <div class="note">
                                    <a class="forgot-password" href="javascript:void(0);">忘记了密码?</a>
                                </div>
                            </section>

                            <section>
                                <label class="checkbox">
                                    <input type="checkbox" name="rememberMe">
                                    <i></i>记住密码 (自动登录)</label>
                            </section>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                登录
                            </button>
                        </footer>
                    </form>
                </div>
            </div>
            <shiro:user>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">

                            </button>
                            <h4 class="modal-title">温馨提示</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                您已登录, 请直接进入首页!
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="${ctx}/" class="btn btn-primary">进入首页</a>
                        </div>
                    </div><!-- /.modal-content -->
                </div>
            </shiro:user>
        </div>
    </div>

</div>

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script src="${ctx}/static/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="${ctx}/static/js/libs/jquery-2.1.1.min.js"></script>

<script src="${ctx}/static/js/libs/jquery-ui-1.10.3.min.js"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="${ctx}/static/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="${ctx}/static/js/bootstrap/bootstrap.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="${ctx}/static/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<script src="${ctx}/static/js/plugin/jquery-validate/jquery.validate.messages_zh.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="${ctx}/static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- Jquery layer -->
<script src="${ctx}/static/js/plugin/layer/layer.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<h1>您的浏览器以及非常的古老, 推荐使用 Firefox/Chrome 浏览器.</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="${ctx}/static/js/app.js"></script>

<script type="text/javascript">

    //防止被强退后登录造成 iframe 无限嵌套 begin
    if (window != top) {
        top.location.reload();
    } else {
        //优化显示效果.
        $('body').removeAttrs("style")
    }
    //防止被强退后登录造成 iframe 无限嵌套 begin

    runAllForms();

    $(function() {
        // Validation
        $("#login-form").validate({
            // Rules for form validation
            rules : {
                username : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                }
            },

            // Messages for form validation
            messages : {
                username : {
                },
                password : {
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });

        $('.forgot-password').click(function () {
            layer.tips('请咨询系统管理员!', $(this));
        });

        //$('[name=username]').focus();
    });
</script>

</body>
</html>