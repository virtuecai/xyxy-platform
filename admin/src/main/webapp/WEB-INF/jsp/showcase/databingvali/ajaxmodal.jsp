<%--
  Created by IntelliJ IDEA.
  User: czd
  Date: 15/11/25
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>welcome showcae-ajaxmodel data bind</h1>

<div>
    <button class="btn btn-default" id="btn1">User GET</button>
    <button class="btn btn-default" id="btn2">User GET 2</button>
</div>
<hr/>
<div>
    <button class="btn btn-default" id="btn3">User POST</button>
    <button class="btn btn-default" id="btn4">User POST 2</button>
    <button class="btn btn-default" id="btn5">User POST 3</button>
</div>

<hr/>
<div>
    <button class="btn btn-default" id="btn6">User And Teacher</button>
    <button class="btn btn-default" id="btn7">User And Teacher2</button>
</div>

<h1>data bind and @Vali</h1>
<div>
    <button class="btn btn-default" id="btn11">User @Vali</button>
    <button class="btn btn-default" id="btn12">User @RequestBody @Vali</button>
    <button class="btn btn-default" id="btn13">User And Teacher @RequestBody @Vali</button>
</div>

<script>
    $(function () {
        $('#btn1').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/ajaxmodal/user-get',
                type: 'get',
                dataType: 'json',
                data: {username: 'czd'},
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        $('#btn2').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/ajaxmodal/user-get2',
                type: 'get',
                dataType: 'json',
                data: {username: 'czd', password: "密码 mima"},
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        //@RequestBody
        $('#btn3').click(function () {
            var user =
                    $.ajax({
                        url: window['ctx'] + '/showcase/ajaxmodal/user-post',
                        type: 'post',
                        dataType: 'json',
                        contentType: "application/json",
                        data: JSON.stringify({username: 'czd', password: '密码 mima'}),
                        success: function (res) {
                            console.log(res)
                        },
                        error: function (res) {
                            console.log(res)
                        }
                    });
        });

        $('#btn4').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/ajaxmodal/user-post2',
                type: 'post',
                dataType: 'json',
                data: {username: 'czd', password: '密码 mima'},
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        $('#btn5').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/ajaxmodal/user-post3',
                type: 'post',
                dataType: 'json',
                data: {username: 'czd', password: '密码 mima'},
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        $('#btn6').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/ajaxmodal//user-and-teacher',
                type: 'post',
                dataType: 'json',
                data: {teacherName: 'teacherName', password: 'password 都一样', username: 'username'},
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        $('#btn7').click(function () {
            var data = {
                user: {password: '1'},
                teacher: {teacherName: 'teacherName', password: '2'}
            };
            $.ajax({
                url: window['ctx'] + '/showcase/ajaxmodal//user-and-teacher2',
                type: 'post',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        //----------------------------------------------------------------------------------------
        $('#btn11').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/datavali/simepleBindVali',
                type: 'get',
                dataType: 'json',
                data: {username: null, password: "密码 mima"},
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        $('#btn12').click(function () {
            $.ajax({
                url: window['ctx'] + '/showcase/datavali/simepleBindVali2',
                type: 'post',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify({username: '', password: "密码 mima"}),
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

        $('#btn13').click(function () {
            var data = {
                user: {
                    username: null, password: "密码 mima",
                    teacherModel: {teacherName: 'teacherName', password: 'teacher password'}
                },
                teacher: {
                    teacherName: 'teacherName2', password: 'teacher password2',
                    userModelList: [
                        { username: '2222', password: "密码 mima2222"},
                        { username: '3333', password: "密码 mima3333"},
                    ]
                }
            };
            $.ajax({
                url: window['ctx'] + '/showcase/datavali/user-and-teacher',
                type: 'post',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (res) {
                    console.log(res)
                },
                error: function (res) {
                    console.log(res)
                }
            });
        });

    });
</script>
</body>
</html>
