<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglibs.jspf"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en-us">

<head>
  <meta charset="utf-8">
  <title> SmartAdmin (Index)</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <!-- #CSS Links -->
  <!-- Basic Styles -->
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/font-awesome.min.css">
  <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-production-plugins.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-production.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-skins.min.css">
  <!-- SmartAdmin RTL Support -->
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-rtl.min.css">
  <!-- We recommend you use "your_style.css" to override SmartAdmin
           specific styles this will also ensure you retrain your customization with each SmartAdmin update.
      <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->
  <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/your_style.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/demo.min.css">
  <!-- #FAVICONS -->
  <link rel="shortcut icon" href="${ctx}/static/img/favicon/favicon.ico" type="image/x-icon">
  <link rel="icon" href="${ctx}/static/img/favicon/favicon.ico" type="image/x-icon">
  <!-- #GOOGLE FONT -->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
  <!-- #APP SCREEN / ICONS -->
  <!-- Specifying a Webpage Icon for Web Clip
           Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
  <link rel="apple-touch-icon" href="${ctx}/static/img/splash/sptouch-icon-iphone.png">
  <link rel="apple-touch-icon" sizes="76x76" href="${ctx}/static/img/splash/touch-icon-ipad.png">
  <link rel="apple-touch-icon" sizes="120x120" href="${ctx}/static/img/splash/touch-icon-iphone-retina.png">
  <link rel="apple-touch-icon" sizes="152x152" href="${ctx}/static/img/splash/touch-icon-ipad-retina.png">
  <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <!-- Startup image for web apps -->
  <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
  <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
  <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
</head>

<body class="">

<!-- #HEADER -->
<%@include file="header.jsp"%>
<!-- END HEADER -->

<!-- #NAVIGATION -->
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS/SASS variables -->
<%@include file="left.jsp"%>
<!-- END NAVIGATION -->

<!-- #MAIN PANEL -->
<div id="main" role="main">
  <!-- RIBBON -->
  <div id="ribbon">
            <span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true" data-reset-msg="Would you like to RESET all your saved widgets and clear LocalStorage?"><i class="fa fa-refresh"></i></span>
            </span>
    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <!-- This is auto generated -->
    </ol>
    <!-- end breadcrumb -->
    <!-- You can also add more buttons to the
        ribbon for further usability

        Example below:

        <span class="ribbon-button-alignment pull-right" style="margin-right:25px">
            <a href="#" id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa fa-grid"></i> Change Grid</a>
            <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa fa-plus"></i> Add</span>
            <button id="search" class="btn btn-ribbon" data-title="search"><i class="fa fa-search"></i> <span class="hidden-mobile">Search</span></button>
        </span> -->
  </div>
  <!-- END RIBBON -->
  <!-- #MAIN CONTENT -->
  <div id="content">
      <div class="page-loading">
          <h1 class="ajax-loading-animation"><i class="fa fa-cog fa-spin"></i> Loading...</h1>
      </div>
      <!-- uplodate smartAdmin ajax html to iframe -->
      <iframe frameborder="0" scrolling="auto" style="width: 100%;" scrolling="no"></iframe>
  </div>
  <!-- END #MAIN CONTENT -->
</div>
<!-- END #MAIN PANEL -->
<!-- #PAGE FOOTER -->
<%@include file="footer.jsp"%>
<!-- END FOOTER -->

<!-- #SHORTCUT AREA : With large tiles (activated via clicking user name tag)
         Note: These tiles are completely responsive, you can add as many as you like -->
<%--<%@include file="shortcut.jsp"%>--%>
<!-- END SHORTCUT AREA -->

<script>
  window['ctx'] = '${ctx}';
</script>

<!--================================================== -->
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
    <script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->
<!-- #PLUGINS -->
<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="${ctx}/static/js/libs/jquery-2.1.1.min.js"></script>
<script src="${ctx}/static/js/libs/jquery-ui-1.10.3.min.js"></script>
<!-- IMPORTANT: APP CONFIG -->
<script src="${ctx}/static/js/app.config.js"></script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="${ctx}/static/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="${ctx}/static/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="${ctx}/static/js/notification/SmartNotification.min.js"></script>
<!-- JARVIS WIDGETS -->
<script src="${ctx}/static/js/smartwidgets/jarvis.widget.min.js"></script>
<!-- EASY PIE CHARTS -->
<script src="${ctx}/static/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<!-- SPARKLINES -->
<script src="${ctx}/static/js/plugin/sparkline/jquery.sparkline.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="${ctx}/static/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="${ctx}/static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="${ctx}/static/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="${ctx}/static/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="${ctx}/static/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices: you can disable this in app.js -->
<script src="${ctx}/static/js/plugin/fastclick/fastclick.min.js"></script>
<!--[if IE 8]>
<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<![endif]-->
<!-- Demo purpose only -->
<script src="${ctx}/static/js/demo.js"></script>
<!-- MAIN APP JS FILE -->
<script src="${ctx}/static/js/app.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="${ctx}/static/js/speech/voicecommand.min.js"></script>
<!-- SmartChat UI : plugin -->
<script src="${ctx}/static/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="${ctx}/static/js/smart-chat-ui/smart.chat.manager.min.js"></script>
</body>

</html>


