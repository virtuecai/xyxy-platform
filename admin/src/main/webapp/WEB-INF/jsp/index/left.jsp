<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="admin" tagdir="/WEB-INF/tags" %>
<aside id="left-panel">
    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is -->
            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <img src="${ctx}/static/img/avatars/sunny.png" alt="me" class="online"/>
                <span><shiro:principal/></span>
                <i class="fa fa-angle-down"></i>
            </a>
        </span>
    </div>

    <nav>
        <ul>
            <li class="">
                <a href="/welcome" title="Dashboard">
                    <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">首页</span>
                </a>
            </li>
            <c:forEach items="${menuList}" var="menu">
                <li>
                    <c:choose>
                        <c:when test="${fn:length(menu.childMenuList) == 0}">
                            <a href="${menu.url}">
                                <i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">${menu.name}</span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a href="#">
                                <i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">${menu.name}</span>
                            </a>
                            <ul>
                                <c:forEach items="${menu.childMenuList}" var="childMenu">
                                    <admin:submenu menu="${childMenu}"/>
                                </c:forEach>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                </li>
            </c:forEach>
        </ul>
    </nav>
    <%--<a href="#ajax/difver.html" class="btn btn-primary nav-demo-btn">AngularJS, PHP and .Net Versions</a>--%>
    <%--<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>--%>
</aside>
