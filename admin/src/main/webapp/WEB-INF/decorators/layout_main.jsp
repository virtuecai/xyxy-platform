<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglibs.jspf"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en-us">

<head>
  <meta charset="utf-8">
  <title> SmartAdmin <sitemesh:write property='title'/></title>
  <meta name="description" content="xyxy">
  <meta name="author" content="xyxy">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-production-plugins.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-production.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-skins.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/smartadmin-rtl.min.css">
  <!-- We recommend you use "your_style.css" to override SmartAdmin
           specific styles this will also ensure you retrain your customization with each SmartAdmin update.-->
      <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/your_style.css">
  <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
  <link rel="stylesheet" type="text/css" media="screen" href="${ctx}/static/css/demo.min.css">
  <link rel="shortcut icon" href="${ctx}/static/img/favicon/favicon.ico" type="image/x-icon">
  <link rel="icon" href="${ctx}/static/img/favicon/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
  <link rel="apple-touch-icon" href="${ctx}/static/img/splash/sptouch-icon-iphone.png">
  <link rel="apple-touch-icon" sizes="76x76" href="${ctx}/static/img/splash/touch-icon-ipad.png">
  <link rel="apple-touch-icon" sizes="120x120" href="${ctx}/static/img/splash/touch-icon-iphone-retina.png">
  <link rel="apple-touch-icon" sizes="152x152" href="${ctx}/static/img/splash/touch-icon-ipad-retina.png">
  <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <!-- Startup image for web apps -->
  <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
  <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
  <link rel="apple-touch-startup-image" href="${ctx}/static/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

  <script src="${ctx}/static/js/libs/jquery-2.1.1.min.js"></script>
  <script src="${ctx}/static/js/libs/jquery-ui-1.10.3.min.js"></script>
  <!-- IMPORTANT: APP CONFIG -->
  <script src="${ctx}/static/js/app.config.js"></script>
  <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
  <script src="${ctx}/static/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
  <!-- BOOTSTRAP JS -->
  <script src="${ctx}/static/js/bootstrap/bootstrap.min.js"></script>
  <!-- CUSTOM NOTIFICATION -->
  <script src="${ctx}/static/js/notification/SmartNotification.min.js"></script>
  <!-- JARVIS WIDGETS -->
  <script src="${ctx}/static/js/smartwidgets/jarvis.widget.min.js"></script>
  <!-- EASY PIE CHARTS -->
  <script src="${ctx}/static/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
  <!-- SPARKLINES -->
  <script src="${ctx}/static/js/plugin/sparkline/jquery.sparkline.min.js"></script>
  <!-- JQUERY VALIDATE -->
  <script src="${ctx}/static/js/plugin/jquery-validate/jquery.validate.min.js"></script>
  <script src="${ctx}/static/js/plugin/jquery-validate/jquery.validate.messages_zh.js"></script>
  <!-- JQUERY MASKED INPUT -->
  <script src="${ctx}/static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
  <!-- JQUERY SELECT2 INPUT -->
  <script src="${ctx}/static/js/plugin/select2/select2.min.js"></script>
  <!-- JQUERY UI + Bootstrap Slider -->
  <script src="${ctx}/static/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
  <!-- browser msie issue fix -->
  <script src="${ctx}/static/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
  <!-- FastClick: For mobile devices: you can disable this in app.js -->
  <script src="${ctx}/static/js/plugin/fastclick/fastclick.min.js"></script>
  <!--[if IE 8]>
  <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
  <![endif]-->
  <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
  <!-- Voice command : plugin -->
  <script src="${ctx}/static/js/speech/voicecommand.min.js"></script>
  <!-- SmartChat UI : plugin -->
  <script src="${ctx}/static/js/smart-chat-ui/smart.chat.ui.min.js"></script>
  <script src="${ctx}/static/js/smart-chat-ui/smart.chat.manager.min.js"></script>

  <script src="${ctx}/static/js/plugin/bootstrap-tags/bootstrap-tagsinput.js"></script>
  <script src="${ctx}/static/js/plugin/jquery-form/jquery-form.min.js"></script>
  <script src="${ctx}/static/js/libs/underscore/underscore-min.js"></script>
  <script src="${ctx}/static/js/plugin/layer/layer.js"></script>
  <script src="${ctx}/static/js/libs/moment/moment.js"></script>
  <!-- MAIN APP JS FILE -->
  <script src="${ctx}/static/js/app.js"></script>

  <script>
    window['ctx'] = '${ctx}';
  </script>
  <sitemesh:write property='head'/>
</head>

<body style="overflow-x:hidden;overflow-y:hidden;padding: 10px 14px;">

<sitemesh:write property='body'/>

</body>

</html>

