$(function(){
    var list= new List();
})

var List =function(){
    this.init();
}
List.prototype={
        init : function(){
            var that = this;
            //提交表单验证字段,
            $("#btn1").on('click',function(){
                $("#title").val("");
                $("#price").val("");
            })

            $("#sub1").on('click',function(){
                var r = /^\d+(\.\d+)?$/; //价格验证
                var price=$("#price").val();
                var title=$("#title").val();
                if(title==""||title.length==0||title==null||title.trim()==""){
                    layer.tips('服务名不能为空', '#title', {
                        tips: [2, '#cccccc'],
                        time: 2000
                    });
                    return false;
                }else if(!r.test(price)){
                    //小tips
                    layer.tips('请输入正确的价格', '#price', {
                        tips: [2, '#cccccc'],
                        time: 2000
                    });
                    return false;
                }
            });
            //------------分割线--------------
            //1级分类和2级分类级联
            $("#cls1").on('change',function(){
                $.ajax({
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    type: "GET",
                    url: "goods/getclassify",
                    data: {parentid:$("#cls1").val()},
                    dataType: "json",
                    success: function(data){
                        $('#cls2').empty();   //清空resText里面的所有内容
                        var str = '';
                        for ( var x = 0; x < data.length; x++) {
                            str += "<option value='" + data[x].id + "'>"
                                + data[x].title + "</option>";
                        }
                        $('#cls2').html(str);
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "goods/getclassify",
                    data: {parentid:$("#cls2").val()},
                    dataType: "json",
                    success: function(data){
                        $('#cls3').empty();   //清空resText里面的所有内容
                        var str = '';
                        for ( var x = 0; x < data.length; x++) {
                            str += "<option value='" + data[x].id + "'>"
                                + data[x].title + "</option>";
                        }
                        $('#cls3').html(str);
                    }
                });
                $("#cls2").trigger("change");
            });
            //------------分割线--------------
            //2级分类和3级分来级联
            $("#cls2").on('change',function(){
                $.ajax({
                    type: "GET",
                    url: "goods/getclassify",
                    data: {parentid:$("#cls2").val()},
                    dataType: "json",
                    success: function(data){
                        $('#cls3').empty();   //清空resText里面的所有内容
                        var str = '';
                        for ( var x = 0; x < data.length; x++) {
                            str += "<option value='" + data[x].id + "'>"
                                + data[x].title + "</option>";
                        }
                        $('#cls3').html(str);
                    }
                });
            });

        }
}

function getLocalTime(nS) {
    var date=new Date(nS);
    return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
}




