package com.xyxy.platform.admin.showcase.databingvali.web.entity;

import java.util.List;

/**
 * Created by VirtueCai on 15/11/25.
 */
public class TeacherModel {
    private String password;
    private String teacherName;

    private List<UserModel> userModelList;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserModel> getUserModelList() {
        return userModelList;
    }

    public void setUserModelList(List<UserModel> userModelList) {
        this.userModelList = userModelList;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
}
