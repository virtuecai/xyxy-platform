package com.xyxy.platform.admin.system.service;

import com.xyxy.platform.admin.system.entity.Resource;
import com.xyxy.platform.modules.core.vo.system.Menu;

import java.util.List;
import java.util.Set;

public interface ResourceService {


    Resource createResource(Resource resource);
    Resource updateResource(Resource resource);
    void deleteResource(Long resourceId);

    Resource findOne(Long resourceId);
    List<Resource> findAll();

    /**
     * 得到资源对应的权限字符串
     * @param resourceIds
     * @return
     */
    Set<String> findPermissions(Set<Long> resourceIds);

    /**
     * 根据用户权限得到菜单
     * @param permissions
     * @return
     */
    List<Resource> findMenus(Set<String> permissions);

    /**
     * 根据查询到得所有菜单资源组装数据成 Menu 用于页面树状菜单显示使用
     * @param permissions 权限
     * @return
     */
    List<Menu> findMenusByPermissions(Set<String> permissions);

}
