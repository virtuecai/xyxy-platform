package com.xyxy.platform.admin.system.web.controller;

import com.google.common.collect.ImmutableMap;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.admin.system.entity.Organization;
import com.xyxy.platform.admin.system.service.OrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 组织机构管理.
 * 采用zTree实现
 * 可作为树状数据的编码参照
 * @Author: 蔡正大
 */
@Controller
@RequestMapping("/system/organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;

    @RequiresPermissions("system:organization:view")
    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "system/organization/index";
    }

    @RequiresPermissions("system:organization:view")
    @ResponseBody
    @RequestMapping(value = "/tree/data", method = RequestMethod.GET)
    public ResponseMessage<?> getTreeData() {
        List<Organization> organizationList = organizationService.findAll();
        return ResponseUtils.jsonSuccess(organizationList);
    }

    @RequiresPermissions("system:organization:create")
    @ResponseBody
    @RequestMapping(value = "appendChild", method = RequestMethod.POST)
    public ResponseMessage<?> appendChild(Organization organization) {
        if(!StringUtils.isNotEmpty(organization.getName()))  {
            return ResponseUtils.jsonFail(null, "请输入内容后在添加!");
        }
        List<Organization> organizationList = organizationService.appendChild(organization);
        return ResponseUtils.jsonSuccess(ImmutableMap.of("organizationList", organizationList), "添加子节点成功!");
    }


    @ResponseBody
    @RequiresPermissions("system:organization:update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseMessage<?> update(Organization organization) {
        organizationService.updateOrganization(organization);
        return ResponseUtils.jsonSuccess("更新成功!");
    }

    @ResponseBody
    @RequiresPermissions("system:organization:delete")
    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public ResponseMessage<?> delete(@PathVariable("id") Long id) {
        organizationService.deleteOrganization(id);
        return ResponseUtils.jsonSuccess("删除节点成功!");
    }

    /**
     * 节点移动, 以及移动后的排序
     * @param sourceId 源节点
     * @param targetId 目标节点
     * @param moveType "inner"：成为子节点，"prev"：成为同级前一个节点，"next"：成为同级后一个节点
     * @return
     */
    @ResponseBody
    @RequiresPermissions("system:organization:update")
    @RequestMapping(value = "move", method = RequestMethod.POST)
    public ResponseMessage<?> moveExt(
            @RequestParam("sourceId") Long sourceId,
            @RequestParam("targetId") Long targetId,
            @RequestParam("moveType") String moveType) {

        Organization source = organizationService.findOne(sourceId);
        Organization target = organizationService.findOne(targetId);
        organizationService.move(source, target, moveType);
        return ResponseUtils.jsonSuccess(null, "节点移动成功!");
    }

}
