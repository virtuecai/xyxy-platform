package com.xyxy.platform.admin.system.service;


import com.xyxy.platform.admin.system.entity.Organization;

import java.util.List;

public interface OrganizationService {


    Organization createOrganization(Organization organization);

    /**
     * organization.name 为可进行逗号分隔的多个 node name
     * @param organization
     * @return
     */
    List<Organization> appendChild(Organization organization);
    Organization updateOrganization(Organization organization);
    void deleteOrganization(Long organizationId);

    Organization findOne(Long organizationId);
    List<Organization> findAll();

    Object findAllWithExclude(Organization excludeOraganization);

    void move(Organization source, Organization target);

    void move(Organization source, Organization target, String moveType);
}
