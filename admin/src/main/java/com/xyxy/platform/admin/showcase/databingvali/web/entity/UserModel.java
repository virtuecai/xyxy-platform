package com.xyxy.platform.admin.showcase.databingvali.web.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by VirtueCai on 15/11/25.
 */
public class UserModel {
    @NotEmpty
    @Size(max = 10, min = 3)
    private String username;
    private String password;

    private TeacherModel teacherModel;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TeacherModel getTeacherModel() {
        return teacherModel;
    }

    public void setTeacherModel(TeacherModel teacherModel) {
        this.teacherModel = teacherModel;
    }
}
