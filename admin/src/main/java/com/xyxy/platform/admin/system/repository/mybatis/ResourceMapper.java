package com.xyxy.platform.admin.system.repository.mybatis;

import com.xyxy.platform.admin.system.entity.Resource;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;

import java.util.List;

@MyBatisRepository
public interface ResourceMapper {

   int createResource(Resource resource);
   int updateResource(Resource resource);
   int deleteResource(Long resourceId);
   int deleteResourceByParentIds(String parentIds);

   Resource findOne(Long resourceId);
   List<Resource> findAll();

}
