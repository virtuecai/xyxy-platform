package com.xyxy.platform.admin.system.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.admin.system.entity.Icon;
import com.xyxy.platform.admin.system.repository.mybatis.IconMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class IconServiceImpl implements IconService {

    @Autowired
    private IconMapper mapper;

    @Override
    public List<Icon> findAll() {
        return mapper.selectAll();
    }

    @Override
    public List<Icon> findByCategory(Long category) {
        return mapper.selectByCategory(category);
    }

    @Override
    public PageInfo<Icon> page(Icon params, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Icon> iconList = mapper.dynamicSelect(params);
        PageInfo<Icon> pageInfo = new PageInfo<>(iconList);
        return pageInfo;
    }
}
