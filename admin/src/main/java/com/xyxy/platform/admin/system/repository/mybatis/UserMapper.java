package com.xyxy.platform.admin.system.repository.mybatis;

import com.xyxy.platform.admin.system.entity.User;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import java.util.List;

@MyBatisRepository
public interface UserMapper {

    int createUser(User user);

    int updateUser(User user);

    int deleteUser(Long userId);

    User findOne(Long userId);

    List<User> findAll();

    User findByUsername(String username);


}
