package com.xyxy.platform.admin.system.service;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.admin.system.entity.Icon;

import java.util.List;

/**
 * 系统图片管理服务
 */
public interface IconService {

    List<Icon> findAll();

    List<Icon> findByCategory(Long category);

    PageInfo<Icon> page(Icon params, Integer pageNo, Integer pageSize);

}
