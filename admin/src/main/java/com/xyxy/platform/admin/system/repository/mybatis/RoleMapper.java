package com.xyxy.platform.admin.system.repository.mybatis;

import com.xyxy.platform.admin.system.entity.Role;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;

import java.util.List;

@MyBatisRepository
public interface RoleMapper {
    int createRole(Role role);
    int updateRole(Role role);
    int deleteRole(Long roleId);

    Role findOne(Long roleId);
    List<Role> findAll();
}