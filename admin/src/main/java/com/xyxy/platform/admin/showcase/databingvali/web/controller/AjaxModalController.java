package com.xyxy.platform.admin.showcase.databingvali.web.controller;

import com.google.common.collect.ImmutableMap;
import com.xyxy.platform.admin.showcase.databingvali.web.entity.TeacherModel;
import com.xyxy.platform.admin.showcase.databingvali.web.entity.UserAndTeacherVo;
import com.xyxy.platform.admin.showcase.databingvali.web.entity.UserModel;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by VirtueCai on 15/11/25.
 */
@Controller
@RequestMapping("/showcase/ajaxmodal")
public class AjaxModalController {

    @RequestMapping()
    public String page() {
        return "showcase/databingvali/ajaxmodal";
    }

    @ResponseBody
    @RequestMapping(value = "/user-get", method = RequestMethod.GET)
    public ResponseMessage<?> userGet(UserModel user) {
        System.out.println(user);
        return ResponseUtils.jsonSuccess(user);
    }

    @ResponseBody
    @RequestMapping(value = "/user-get2", method = RequestMethod.GET)
    public ResponseMessage<?> userGet2(String username, String password) {
        return ResponseUtils.jsonSuccess(ImmutableMap.of("username", username, "password", password));
    }

    @ResponseBody
    @RequestMapping(value = "/user-post", method = RequestMethod.POST)
    public ResponseMessage<?> userPost(@RequestBody UserModel user) {
        System.out.println(user);
        return ResponseUtils.jsonSuccess(user);
    }

    @ResponseBody
    @RequestMapping(value = "/user-post2", method = RequestMethod.POST)
    public ResponseMessage<?> userPost2(String username, String password) {
        return ResponseUtils.jsonSuccess(ImmutableMap.of("username", username, "password", password));
    }

    @ResponseBody
    @RequestMapping(value = "/user-post3", method = RequestMethod.POST)
    public ResponseMessage<?> userPost3(UserModel user) {
        System.out.println(user);
        return ResponseUtils.jsonSuccess(user);
    }

    /**
     * data: {username: 'czd', teacherName:"teacherName", password: '密码 mima'}, 这样的ajax 数据, username 和 teachername 是可以正确绑定的.
     * 但是两个对象中都有的 password 是没办法精确绑定. 需要采用 userAndTeacher2 方式.
     * @param user
     * @param teacher
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/user-and-teacher")
    public ResponseMessage<?> userAndTeacher(UserModel user,TeacherModel teacher) {
        System.out.println(user);
        return ResponseUtils.jsonSuccess(ImmutableMap.of("user", user, "teacher", teacher));
    }

    @ResponseBody
    @RequestMapping(value = "/user-and-teacher2")
    public ResponseMessage<?> userAndTeacher2(@RequestBody UserAndTeacherVo userAndTeacherVo) {
        System.out.println(userAndTeacherVo);
        return ResponseUtils.jsonSuccess(userAndTeacherVo);
    }

}
