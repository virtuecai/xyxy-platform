package com.xyxy.platform.admin.system.web.controller;

import com.xyxy.platform.modules.core.vo.system.Menu;
import com.xyxy.platform.modules.core.web.bind.annotation.CurrentUser;
import com.xyxy.platform.admin.system.entity.User;
import com.xyxy.platform.admin.system.service.ResourceService;
import com.xyxy.platform.admin.system.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Set;

@Controller
public class IndexController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ResourceService resourceService;
    @Autowired
    private UserService userService;

    /**
     * smartAdmin index page
     * @param loginUser
     * @param model
     * @return
     */
    @RequestMapping("/index")
    public String index2(@CurrentUser User loginUser, Model model) {
        Set<String> permissions = userService.findPermissions(loginUser.getUsername());
        List<Menu> menuList = resourceService.findMenusByPermissions(permissions);
        model.addAttribute("menuList", menuList);
        return "index/index";
    }

    @RequestMapping("/welcome")
    public String welcome() {
        return "welcome";
    }


}
