package com.xyxy.platform.admin.showcase.databingvali.web.controller;

import com.xyxy.platform.admin.showcase.databingvali.web.entity.UserAndTeacherVo;
import com.xyxy.platform.admin.showcase.databingvali.web.entity.UserModel;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * Created by VirtueCai on 15/11/24.
 */
@Controller
@RequestMapping("/showcase/datavali")
public class DataBindAndValiController {


    @ResponseBody
    @RequestMapping(value = "simepleBindVali")
    public ResponseMessage<?> simepleBindVali(@Valid UserModel user, BindingResult result) {
        System.out.println(user);
        return ResponseUtils.jsonSuccess(user);
    }

    @ResponseBody
    @RequestMapping(value = "simepleBindVali2")
    public ResponseMessage<?> simepleBindVali2(@RequestBody @Valid UserModel user, BindingResult result) {
        System.out.println(user);
        return ResponseUtils.jsonSuccess(user);
    }

    @ResponseBody
    @RequestMapping(value = "/user-and-teacher")
    public ResponseMessage<?> userAndTeacher(@RequestBody @Valid UserAndTeacherVo userAndTeacherVo, BindingResult result) {
        System.out.println(userAndTeacherVo);
        return ResponseUtils.jsonSuccess(userAndTeacherVo);
    }


}

