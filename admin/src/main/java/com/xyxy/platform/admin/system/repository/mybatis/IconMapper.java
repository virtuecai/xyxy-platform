package com.xyxy.platform.admin.system.repository.mybatis;


import com.xyxy.platform.admin.system.entity.Icon;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;

import java.util.List;

@MyBatisRepository
public interface IconMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Icon record);

    int insertSelective(Icon record);

    Icon selectByPrimaryKey(Long id);

    List<Icon> selectAll();

    List<Icon> selectByCategory(Long category);

    List<Icon> dynamicSelect(Icon icon);

    int updateByPrimaryKeySelective(Icon record);

    int updateByPrimaryKey(Icon record);
}