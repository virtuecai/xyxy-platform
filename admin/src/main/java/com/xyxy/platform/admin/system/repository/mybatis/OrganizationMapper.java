package com.xyxy.platform.admin.system.repository.mybatis;

import com.xyxy.platform.admin.system.entity.Organization;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@MyBatisRepository
public interface OrganizationMapper {

    int createOrganization(Organization organization);

    int updateOrganization(Organization organization);

    void deleteOrganization(Long organizationId);

    void deleteOrganizationByParentIds(String parentIds);

    Organization findOne(Long organizationId);

    List<Organization> findAll();

    List<Organization> findAllWithExclude(Organization organization);

    void move(@Param("source") Organization source, @Param("target") Organization target);

    void moveChild(@Param("source") Organization source, @Param("target") Organization target);

    /**
     * 获得该父节点下一级节点中最大idx
     *
     * @param parent
     * @return
     */
    Integer findChildMaxIdx(Organization parent);

    /**
     * 更新下标 idx,
     *
     * @param id  主键id
     * @param idx 下标
     * @return
     */
    Integer updateIdx(@Param("id") Long id, @Param("idx") Integer idx);

    Integer updateIdxWithPrev(@Param("source") Organization source, @Param("target") Organization target);

    Integer updateIdxWithNext(@Param("source") Organization source, @Param("target") Organization target);
}