package com.xyxy.platform.admin.system.entity;

/**
 * Created by VirtueCai on 15/11/24.
 */
public class Icon {

    private Long id;
    private String iconClass;
    private String name;
    private Long category;

    public Icon() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Icon{" +
                "id=" + id +
                ", iconClass='" + iconClass + '\'' +
                ", name='" + name + '\'' +
                ", category=" + category +
                '}';
    }
}
