package com.xyxy.platform.admin.showcase.databingvali.web.entity;

import javax.validation.Valid;

/**
 * Created by VirtueCai on 15/11/25.
 */
public class UserAndTeacherVo {

    @Valid //嵌套对象jsr验证, 需要在对象上加this annotation
    private UserModel user;
    private TeacherModel teacher;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public TeacherModel getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherModel teacher) {
        this.teacher = teacher;
    }
}
