package com.xyxy.platform.admin.system.service;

import com.xyxy.platform.admin.system.entity.Organization;
import com.xyxy.platform.admin.system.repository.mybatis.OrganizationMapper;
import com.xyxy.platform.modules.core.utils.BeanUtilsExt;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private OrganizationMapper organizationMapper;

    @Override
    public Organization createOrganization(Organization organization) {
        organizationMapper.createOrganization(organization);
        return organization;
    }

    @Override
    public List<Organization> appendChild(Organization organization) {
        List<Organization> result = new ArrayList<>();
        String[] nameArr = organization.getName().split(",");
        Organization parent = organizationMapper.findOne(organization.getParentId());
        Integer maxIdx = organizationMapper.findChildMaxIdx(parent);
        for (String name : nameArr) {
            Organization o = new Organization();
            o.setName(name);
            o.setAvailable(Boolean.TRUE);
            o.setIdx(maxIdx++);//最大idx + 1
            o.setParentId(parent.getId());
            o.setParentIds(parent.getMakeSelfAsParentIds());
            organizationMapper.createOrganization(o);
            result.add(o);
        }
        return result;
    }

    @Override
    public Organization updateOrganization(Organization organization) {
        if (null == organization) return null;
        Organization entity = findOne(organization.getId());
        if (null == entity) return null;
        BeanUtils.copyProperties(organization, entity, BeanUtilsExt.getNullPropertyNames(organization));
        organizationMapper.updateOrganization(entity);
        return entity;
    }

    @Override
    public void deleteOrganization(Long organizationId) {
        Organization organization = organizationMapper.findOne(organizationId);
        organizationMapper.deleteOrganization(organizationId);
        organizationMapper.deleteOrganizationByParentIds(organization.getMakeSelfAsParentIds() + "%");
    }

    @Override
    public Organization findOne(Long organizationId) {
        return organizationMapper.findOne(organizationId);
    }

    @Override
    public List<Organization> findAll() {
        return organizationMapper.findAll();
    }

    @Override
    public List<Organization> findAllWithExclude(Organization excludeOraganization) {
        return organizationMapper.findAllWithExclude(excludeOraganization);
    }

    @Override
    public void move(Organization source, Organization target) {
        organizationMapper.move(source, target);
        organizationMapper.moveChild(source, target);
    }

    @Override
    public void move(Organization source, Organization target, String moveType) {
        //TODO 迭代: 需要更新父级节点, 以及缩短更新 idx 范围 待实现
        // 目标节点的父节点
        Organization targetParent = findOne(target.getParentId());
        switch (moveType) {
            case "inner":
                // 更新 parentId, parentIds
                organizationMapper.move(source, target);
                // 更新子集 parentId, parentIds
                organizationMapper.moveChild(source, target);
                // 更新 idx, 最底部
                organizationMapper.updateIdx(source.getId(), organizationMapper.findChildMaxIdx(target) + 1);
                break;
            case "prev":
                // 更新 parentId, parentIds
                organizationMapper.move(source, targetParent);
                // 更新子集 parentId, parentIds
                organizationMapper.moveChild(source, targetParent);
                organizationMapper.updateIdxWithPrev(source, target);
                organizationMapper.updateIdx(source.getId(), target.getIdx());
                break;
            case "next":
                // 更新 parentId, parentIds
                organizationMapper.move(source, targetParent);
                // 更新子集 parentId, parentIds
                organizationMapper.moveChild(source, targetParent);
                organizationMapper.updateIdxWithNext(source, target);
                organizationMapper.updateIdx(source.getId(), target.getIdx() + 1);
                break;
        }
    }
}
