package com.xyxy.platform.openapi.sns.v1.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.sns.MemberCollection;
import com.xyxy.platform.modules.service.sns.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 获取会员服务收藏列表、添加或取消收藏
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */
@RestController
@RequestMapping(value = "/app/v1/collection", method = RequestMethod.GET)
public class CollectionController {

    /*自动注入 服务收藏业务*/
    @Autowired
    private CollectionService collectionService;

    /**
     * 获取会员服务收藏列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping("/collection_list")
    public ResponseMessage<?> collectionList(@RequestParam Long memberId, @RequestParam Integer pageNumber, @RequestParam Integer pageSize){
        PageInfo pageResult = collectionService.getCollectionPage(memberId, pageNumber, pageSize);
        return ResponseUtils.jsonSuccess(pageResult);
    }

    /**
     * 添加商品收藏
     * @param memberCollection 会员收藏夹实体[memberId 会员ID,goodsid 商品ID]
     * @return
     */
    @RequestMapping("/create_collection")
    public ResponseMessage<?> createCollection(MemberCollection memberCollection){
        memberCollection.setCreateTime(new Date());
        int result = collectionService.createCollection(memberCollection);
        return ResponseUtils.jsonSuccess(result);
    }

    /**
     * 取消商品收藏
     * @param collectionId 收藏夹ID
     * @param memberId 会员ID
     * @return
     */
    @RequestMapping("/delete_collection")
    public ResponseMessage<?> deleteCollection(@RequestParam Long collectionId, @RequestParam Long memberId){
        int result = collectionService.deleteCollection(collectionId, memberId);
        return ResponseUtils.jsonSuccess(result);
    }
}
