package com.xyxy.platform.openapi.member.v1.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.entity.member.MemberSpecificTag;
import com.xyxy.platform.modules.entity.member.SpecificTag;
import com.xyxy.platform.modules.service.common.address.AddressService;
import com.xyxy.platform.modules.service.member.MemberService;
import com.xyxy.platform.modules.service.member.MemberSpecificTagService;
import com.xyxy.platform.modules.service.member.SpecificTagService;
import com.xyxy.platform.modules.service.member.vo.MemberDetail;
import com.xyxy.platform.modules.service.member.vo.SimpleMember;
import com.xyxy.platform.modules.service.sns.FollowerService;
import com.xyxy.platform.openapi.common.security.RequiresPermissions;
import com.xyxy.platform.openapi.common.web.bind.annotation.CurrentLoginMember;

/**
 * 会员 member
 * Created by VirtueCai on 15/12/18.
 */
@RestController
@RequestMapping("/member/v1")
@RequiresPermissions
public class MemberController extends BaseController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;
    
    @Autowired
    private FollowerService  followerService;
    
    @Autowired
    private AddressService addressService;
    
    @Autowired
    private SpecificTagService specificTagService;
    
    @Autowired
    private MemberSpecificTagService memberSpecificTagService;

    /**
     * 简介: 根据会员id获取会员信息
     * @author zhaowei
     * @Date 2016年1月7日 下午5:24:23
     * @version 1.0
     *
     * @return
     */
    @RequestMapping(value = "member_detail", method = RequestMethod.GET)
    public ResponseMessage<?> memberDetail(
    		@CurrentLoginMember SimpleMember simpleMember
    ){
    	MemberDetail memberDetail = memberService.findMemberDetailById(simpleMember.getId());
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	//基本资料
    	if(memberDetail!=null){
    		
    		//根据地区id获取地区数组
    		String address = memberDetail.getMember().getAddress();
    		if(!StringUtils.isEmpty(address)){
    			List<SysArea> sysAreaList = addressService.getPathArea(Integer.parseInt(address));
    			if(sysAreaList == null){
    				sysAreaList = new ArrayList<SysArea>(0);
    			}
    			memberDetail.setAddress(sysAreaList);
    		}
    		
    		if(memberDetail.getImageItemList()==null){
    			memberDetail.setImageItemList(new ArrayList<ImageItem>(0));
    		}
    		if(memberDetail.getTagList() == null){
    			memberDetail.setTagList(new ArrayList<SpecificTag>(0));
    		}
    		
    		//获取我的所有标签
    		List<MemberSpecificTag> memberSpecificTagList = memberSpecificTagService.findByMemberId(simpleMember.getId());
            List<Long> specificIdList = Collections3.extractToList(memberSpecificTagList, "specificId");
            List<SpecificTag> tagList  =  null ;
            if(specificIdList != null){
            	tagList = specificTagService.findByIdList(specificIdList);
            }
            if(tagList==null){
            	tagList = new ArrayList<SpecificTag>();
            }
            memberDetail.setTagList(tagList);
    		
    		//我的粉丝总数
    		int fansCount = followerService.countFollower(simpleMember.getId());
        	//今日新增粉丝数
    		int dayFansCount = followerService.fansNewCount(simpleMember.getId());
    		resultMap.put("fansCount",fansCount);
    		resultMap.put("dayFansCount", dayFansCount);
    	}
    	
    	//TODO:计算资料完整度
    	int completeNo = 20 ;
    	memberDetail.setCompleteNo(completeNo);
    	resultMap.put("memberDetail", memberDetail);
    	
    	return ResponseUtils.jsonSuccess(memberDetail, "搜索会员成功!");
	}
    
    
    /**
     * 搜索推荐 星会员
     * @return
     */
    @RequestMapping(value = "member_list_new", method = RequestMethod.GET)
    public ResponseMessage<?> memberListNew(
            @RequestParam(required=false) Integer categoryId,
            @RequestParam(required=false) Integer cityId,
            @RequestParam(defaultValue = "1",required=false) Integer page,
            @RequestParam(defaultValue = "10",required=false) Integer rows
    ) {
        PageInfo<MemberDetail> result = null;
        
        if(rows > 100){
        	rows = 100;
        }
        Map<String, Object> parMap = new HashMap<String, Object>();
        parMap.put("categoryId", categoryId);
        parMap.put("cityId", cityId);
        result = memberService.findNewest(null, page, rows,parMap);
        System.out.println(result.getList().size());
        List<MemberDetail> memberList = result.getList();
        if(memberList!=null&&memberList.size()>0){
        	memberList = cleanMemberDetail(memberList);
        }
        return ResponseUtils.jsonSuccess(memberList, "搜索会员成功!");
    }
    
    /**
     * 搜索推荐 星会员
     * @return
     */
    @RequestMapping(value = "member_list", method = RequestMethod.GET)
    public ResponseMessage<?> memberList(
            @RequestParam(required=false) Integer categoryId,
            @RequestParam(required=false) Integer cityId,
            @RequestParam(defaultValue = "1",required=false) Integer page,
            @RequestParam(defaultValue = "10",required=false) Integer rows
    ) {
        PageInfo<MemberDetail> result = null;
        
        if(rows > 100){
        	rows = 100;
        }
        Map<String, Object> parMap = new HashMap<String, Object>();
        parMap.put("categoryId", categoryId);
        parMap.put("cityId", cityId);
        result = memberService.findRecommend(null, page, rows , parMap);
        List<MemberDetail> memberList = result.getList();
        if(memberList!=null&&memberList.size()>0){
        	memberList = cleanMemberDetail(memberList);
        }
        return ResponseUtils.jsonSuccess(memberList, "搜索会员成功!");
    }

    private List<MemberDetail> cleanMemberDetail(List<MemberDetail> memberList){
    	MemberDetail memberDetail = null ;
    	Member member = null ;
    	for(int i=0;i<memberList.size();i++){
    		memberDetail = memberList.get(i);
    		member =  memberDetail.getMember();
    		if(member != null){
    			member.setCreateTime(null);
    			member.setUpdateTime(null);
    			member.setFrozenMoney(null);
    			member.setImagePkgId(null);
    			member.setIsDel(null);
    			member.setRankPoints(null);
    			member.setUserMoney(null);
    			member.setUserStatus(null);
    			memberDetail.setMember(member);
    		}
    		List<ImageItem> imageItemList = memberDetail.getImageItemList();
    		if(imageItemList!=null&&imageItemList.size()>0){
    			ImageItem imageItem = null ;
    			for(int y=0;y<imageItemList.size();y++){
    				imageItem = imageItemList.get(y);
    				imageItem.setId(null);
    				imageItem.setImagePkgId(null);
    				imageItemList.set(y, imageItem);
    			}
    			memberDetail.setImageItemList(imageItemList);
    		}
    		memberList.set(i, memberDetail);
    	}
    	return memberList;
    }
    
    // 推荐, 根据推荐是否推荐字段
    // 最新 根据创建时间
    // 分页排序

}
