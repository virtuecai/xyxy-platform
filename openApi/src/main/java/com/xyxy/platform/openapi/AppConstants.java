package com.xyxy.platform.openapi;

public class AppConstants {

    public static final String ENCODING = "UTF-8";

    /**
     * 约定 请求 header token key, 响应 response token key
     *
     */
    public static final String REQUEST_TOKEN_KEY = "token";

}
