package com.xyxy.platform.openapi.trade.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xyxy.platform.modules.core.utils.DateUtil;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.service.trade.ServerTimeService;
import com.xyxy.platform.openapi.common.security.RequiresPermissions;
/**
 * 服务时段控制器
 * @author zhaowei
 */
@RequiresPermissions
@RestController
@RequestMapping("/servertime/v1")
public class ServerTimeController extends BaseController {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	@Autowired
	private ServerTimeService  serverTimeService;
	
	/**
	* 简介: 获取可用服务时段
	* @author zhaowei
	* @Date 2016年1月10日 上午10:48:05
	* @version 1.0
	* @param goodsSn
	* @param startTime
	* @param endTime
	* @return  下标串      例： 0,5,10,20
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ResponseMessage<?> memberList(
				@RequestParam(required=true) String goodsSn,
				@RequestParam(required=true) String startTime,
	            @RequestParam(required=true) String endTime
			){
		ResponseMessage<?> result = null;
		
		Date startDate = null ;
		Date endDate = null ;
		try{
			startDate = sdf.parse(startTime);
			endDate = sdf.parse(endTime);
			if(startDate.getTime()> endDate.getTime()){
				throw new RuntimeException();
			}
		}catch(Exception e){
			result = ResponseUtils.jsonResult(null, false, 500, "时间格式出错");
			return result;
		}
		String serverTimeResult = serverTimeService.selectAvailable(goodsSn, startDate, endDate);
		result = ResponseUtils.jsonResult(serverTimeResult,true) ;
		return result;
	}
	
}
