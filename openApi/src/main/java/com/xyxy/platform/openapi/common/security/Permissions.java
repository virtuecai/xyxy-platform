package com.xyxy.platform.openapi.common.security;

/**
 * 权限枚举
 *
 * @author Caizhenegda
 * @version 1.0
 * @since 2015-12-32 00:29
 */
public enum Permissions {

    /**
     * 登录权限, token
     */
    LOGIN,
    /**
     * 银行卡绑
     */
    BANK_CARD

}
