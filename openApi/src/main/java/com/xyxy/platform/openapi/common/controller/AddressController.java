package com.xyxy.platform.openapi.common.controller;

import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.service.common.address.AddressService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 获取省市区地址
 * @author liushun
 * @version 1.0
 * @Date 2015-12-30
 */
@RestController
@RequestMapping(value = "/common",method = RequestMethod.POST)
public class AddressController {

    private AddressService addressService;
    /**
     * 获取所有的省
     * @return
     */
    @RequestMapping("/province_list")
    public ResponseMessage<?> getListProvince(){
        List list = addressService.getListProvince();
        return ResponseUtils.jsonSuccess(list);
    }

    /**
     * 根据省份ID获取城市列表
     * @param provinceId 省份ID
     * @return
     */
    @RequestMapping("/city_list")
    public ResponseMessage<?> getCityProvince(@RequestParam Integer provinceId){
        List list = addressService.getListCity(provinceId);
        return ResponseUtils.jsonSuccess(list);
    }
    /**
     * 根据城市ID获取城市列表
     * @param cityId 城市ID
     * @return
     */
    @RequestMapping("/area_list")
    public ResponseMessage<?> getAreaProvince(@RequestParam Integer cityId){
        List list = addressService.getListArea(cityId);
        return ResponseUtils.jsonSuccess(list);
    }

}
