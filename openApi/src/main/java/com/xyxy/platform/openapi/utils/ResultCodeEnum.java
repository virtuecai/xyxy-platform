package com.xyxy.platform.openapi.utils;
/** 
 * Json 返回代码枚举
 * @author vic E-mail: 251361939@qq.com
 * @version 创建时间：2015年12月19日 下午6:34:12 
 */
public enum ResultCodeEnum {

	ERROR(0,"失败"),
	SUCCESS(1,"成功"),
	TOKEN_OUTTIME(2,"token失效"),
	LIMIT(3,"系统忙，请稍后再试"),
	PARD_BREAK(4,"参数错误"),
	USER_EXIST(5,"用户已存在");
	
	private int value;
	private String msg;
	
	private ResultCodeEnum(int vlaue,String msg){
		this.value = vlaue;
		this.msg = msg;
	}

	
	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}


	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
