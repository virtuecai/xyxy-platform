package com.xyxy.platform.openapi.common.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于权限拦截
 * 扩展权限枚举即可, 暂登录拦截
 *
 * @Author: Caizhengda
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresPermissions {

    /**
     * 默认 验证登录权限
     *
     * @return
     */
    Permissions[] permissions() default {Permissions.LOGIN};

}
