package com.xyxy.platform.openapi.sns.v1.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.sns.MemberFollow;
import com.xyxy.platform.modules.service.sns.FollowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 查询粉丝列表、我的关注列表、添加或取消关注
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */
@RestController
@RequestMapping(value=("/follower/v1"), method = RequestMethod.GET)
public class FollowerController extends BaseController {

    /*自动注入 关注、粉丝业务类*/
    @Autowired
    private FollowerService followerService;

    /**
     * 查询粉丝列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping("/follower_list")
    public ResponseMessage<?> followerList(@RequestParam long memberId, @RequestParam Integer pageNumber, @RequestParam Integer pageSize){
        PageInfo pageResult = followerService.getFollowerPage(memberId, pageNumber, pageSize);
        return ResponseUtils.jsonSuccess(pageResult);
    }

    /**
     * 查询我关注的人列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping("/with_list")
    public ResponseMessage<?> withList(@RequestParam long memberId, @RequestParam Integer pageNumber, @RequestParam Integer pageSize){
        PageInfo pageResult = followerService.getWithPage(memberId, pageNumber, pageSize);
        return ResponseUtils.jsonSuccess(pageResult);
    }

    /**
     * 添加关注
     * @param memberFollow 实体[memberId 会员ID,toMemberId 被关注ID]
     * @return
     */
    @RequestMapping("/create_with")
    public ResponseMessage<?> addWith(MemberFollow memberFollow){
        memberFollow.setCreateTime(new Date());
        int result = followerService.addWith(memberFollow);
        if(result>0)
            return ResponseUtils.jsonSuccess(result);
        else
            return ResponseUtils.jsonFail("不能重复关注!");
    }

    /**
     * 取消关注
     * @param followId 关注ID
     * @param memberId 会员ID
     * @return
     */
    @RequestMapping("/delete_with")
    public ResponseMessage<?> delWith(@RequestParam long followId, @RequestParam long memberId){
        int result = followerService.delWith(followId, memberId);
        return ResponseUtils.jsonSuccess(result);
    }
    
    /**
     * 粉丝总数
     * @param memberId 会员ID
     * @return
     */
    @RequestMapping("/fans_count")
    public ResponseMessage<?> fansCount(@RequestParam long memberId){
        int result = followerService.countFollower(memberId);
        return ResponseUtils.jsonSuccess(result);
    }
    
    /**
     * 简介: 今日新增粉丝数量
     * @author zhaowei
     * @Date 2016年1月7日 下午4:44:25
     * @version 1.0
     *
     * @param memberId
     * @return
     */
    @RequestMapping("/fans_new_count")
    public ResponseMessage<?> fansNewCount(@RequestParam long memberId){
        int result = followerService.fansNewCount(memberId);
        return ResponseUtils.jsonSuccess(result);
    }
    
    
}
