package com.xyxy.platform.openapi.common.web.controller;

import java.io.Serializable;

import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xyxy.platform.modules.core.beanvalidator.BeanValidators;
import com.xyxy.platform.modules.core.beanvalidator.group.A;
import com.xyxy.platform.modules.core.beanvalidator.group.B;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.service.common.sms.SmsService;
import com.xyxy.platform.modules.service.common.sms.vo.SmsTemplateType;

/**
 * 简介: 短信验证码获取接口
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 13:43
 */
@RestController
@RequestMapping("/common/sms/v1")
public class SmsController extends BaseController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Validator validator;

    @Autowired
    private SmsService smsService;

    /**
     * 获取 短信验证码
     *
     * @param form 数据绑定表单, A 组(手机号码)
     * @return
     */
    @RequestMapping(value = "code", method = RequestMethod.GET)
    public ResponseMessage<?> generateSmsCode(SmsForm form) {

        BeanValidators.validateWithException(validator, form, A.class);

        //判断是否在60秒内已生成
        if (!smsService.exist(form.getMobile())) {

            //随机生成六位短信验证码
            Long code = smsService.generateSmsCode(form.getMobile(),SmsTemplateType.REG_MSG);

            logger.debug("生成短信验证码: [{}], 并写入缓存, 60秒内有效!", code);

            return ResponseUtils.jsonSuccess(code, "获取短信验证码成功, 有效时长60秒!");
        } else {
            return ResponseUtils.jsonFail(null, "60秒内不得重复发送短信验证码!");
        }
    }

    /**
     * 验证 短信验证码
     *
     * @param form 绑定表单, B 组(手机号码, 短信验证码)
     * @return
     */
    @RequestMapping(value = "verify_code", method = RequestMethod.POST)
    public ResponseMessage<?> verifyCode(SmsForm form) {

        BeanValidators.validateWithException(validator, form, B.class);
        //是否正确
        boolean success = smsService.verifyCode(form.getMobile(), form.getSmsCode());
        if (success) {
            return ResponseUtils.jsonSuccess(true, "验证码正确!");
        } else {
            return ResponseUtils.jsonFail(false, "验证码错误!");
        }
    }

}

class SmsForm implements Serializable {

    //手机号码
    @NotNull(groups = {A.class, B.class})
    @Pattern(regexp = "^1\\d{10}$", groups = {A.class, B.class}, message = "{validator.mobile.number}")
    private String mobile;

    //短信验证码
    @NotNull(groups = {B.class})
    @Length(min = 6, max = 6, groups = {B.class})
    private Long smsCode;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(Long smsCode) {
        this.smsCode = smsCode;
    }

}
