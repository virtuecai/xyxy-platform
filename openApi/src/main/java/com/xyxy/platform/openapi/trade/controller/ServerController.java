package com.xyxy.platform.openapi.trade.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Maps;
import com.xyxy.platform.modules.core.file.SystemFileService;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.entity.trade.Goods;
import com.xyxy.platform.modules.service.member.vo.SimpleMember;
import com.xyxy.platform.modules.service.trade.GoodsService;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;
import com.xyxy.platform.openapi.common.security.RequiresPermissions;
import com.xyxy.platform.openapi.common.web.bind.annotation.CurrentLoginMember;

/** 
 * 订单控制器
 * @author vic E-mail: 251361939@qq.com
 * @version 创建时间：2015年12月19日 下午6:10:20 
 */
@RequiresPermissions
@RestController
@RequestMapping("/server/v1")
public class ServerController extends BaseController{
	
	Logger logger = LoggerFactory.getLogger(ServerController.class);
	
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private SystemFileService systemFileService;
	
	
	/**
	 * 简介:  获取【最新】服务列表
	 * @author zhaowei
	 * @Date 2016年1月5日 下午5:24:39
	 * @version 1.0
	 *
	 * @return
	 */
	@RequestMapping(value = "list/new", method = RequestMethod.GET)
	public ResponseMessage<?> listNew(){
		ResponseMessage<?> result = null;
		String categoryId = request.getParameter("categoryId");
		String cityId = request.getParameter("cityId");
		String serverType = request.getParameter("serverType");
		String pageStr = request.getParameter("page");
		String rowsStr = request.getParameter("rows");
		
		int page = 1;
		int rows = 10;
		if(StringUtils.isEmpty(pageStr)){
			try{
				page = Integer.parseInt(pageStr);
				if(page < 1 ){
					page  = 1;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		if(StringUtils.isEmpty(rowsStr)){
			try{
				rows = Integer.parseInt(rowsStr);
				if(rows < 1){
					rows = 1;
				}
				if(rows > 100){
					rows = 100;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		Map<String, Object> parMap = Maps.newHashMap();
		parMap.put("categoryId", categoryId);
		parMap.put("cityId", cityId);
		parMap.put("serverType", serverType);
		parMap.put("pageNum", page);
		parMap.put("pageSize", rows);
		List<GoodsDto> goodsList = goodsService.selectNewGoodsByMap(parMap).getList();
		result = ResponseUtils.jsonSuccess(goodsList);
		return result;
	}
	
	/**
	 * 简介:  获取【推荐】服务列表
	 * @author zhaowei
	 * @Date 2016年1月5日 下午5:24:39
	 * @version 1.0
	 *
	 * @return
	 */
	@RequestMapping(value = "list/best", method = RequestMethod.GET)
	public ResponseMessage<?> list(){
		ResponseMessage<?> result = null;
		String categoryId = request.getParameter("categoryId");
		String cityId = request.getParameter("cityId");
		String serverType = request.getParameter("serverType");
		String pageStr = request.getParameter("page");
		String rowsStr = request.getParameter("rows");
		
		int page = 1;
		int rows = 10;
		if(StringUtils.isEmpty(pageStr)){
			try{
				page = Integer.parseInt(pageStr);
				if(page < 1 ){
					page  = 1;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		if(StringUtils.isEmpty(rowsStr)){
			try{
				rows = Integer.parseInt(rowsStr);
				if(rows < 1){
					rows = 1;
				}
				if(rows > 100){
					rows = 100;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		Map<String, Object> parMap = Maps.newHashMap();
		parMap.put("categoryId", categoryId);
		parMap.put("cityId", cityId);
		parMap.put("serverType", serverType);
		parMap.put("pageNum", page);
		parMap.put("pageSize", rows);
		List<GoodsDto> goodsList = goodsService.selectBestGoodsByMap(parMap).getList();
		result = ResponseUtils.jsonSuccess(goodsList);
		return result;
	}
	
	/**
	 * 发布服务
	 * @return
	 */
	@RequestMapping(value = "release_service", method = RequestMethod.POST)
	public  ResponseMessage<?>  releaseService(@CurrentLoginMember SimpleMember simpleMember,GoodsDto goodsDto ){
		ResponseMessage<?> result;
		
		Category category = null ;
		
		if(StringUtils.isEmpty(goodsDto.getGoodsName())){
			result = ResponseUtils.jsonFail(false, "标题不能为空！");
			return result;
		}
		if(StringUtils.isEmpty(goodsDto.getContent())){
			result = ResponseUtils.jsonFail(false, "描述不能为空！");
			return result;
		}
		
		if(StringUtils.isEmpty(goodsDto.getGoodPic())){
			result = ResponseUtils.jsonFail(false, "封面不能为空！");
			return result;
		}else{
			//处理图片封面
			String goodPic = goodsDto.getGoodPic();
			if(!StringUtils.isEmpty(goodPic)){
				String[] goodPics = StringUtils.split(goodPic, ",");
				String fileName = null;
				for(int i=0;i<goodPics.length;i++){
					fileName = goodPics[i];
					if(fileName.indexOf("http://") != -1){
						fileName = fileName.substring(fileName.lastIndexOf("/")+1, fileName.length());
					}
					systemFileService.changeFileStatus(fileName);
					goodPics[i] = systemFileService.getFileUrl()+fileName;
				}
				goodsDto.setGoodPics(goodPics);
			}
		}
		
		if(goodsDto.getGoodPrice() == 0){
			result = ResponseUtils.jsonFail(false, "价格不能为空！");
			return result;
		}
		if(StringUtils.isEmpty(goodsDto.getGoodUnit())){
			result = ResponseUtils.jsonFail(false, "单位不能为空！");
			return result;
		}
		if( !( !StringUtils.isEmpty(goodsDto.getTime()) || !StringUtils.isEmpty(goodsDto.getTimeNote()) ) ){
			result = ResponseUtils.jsonFail(false, "服务时间不能为空！");
			return result;
		}
		if(goodsDto.getType() == 0){
			result = ResponseUtils.jsonFail(false, "服务方式不能为空！");
			return result;
		}else if( goodsDto.getType() < 1 || goodsDto.getType() > 4){
			result = ResponseUtils.jsonFail(false, "服务方式参数错误！");
			return result;
		}
		
		if(goodsDto.getCateId() == 0){
			result = ResponseUtils.jsonFail(false, "服务分类不能为空！");
			return result;
		}else{
			//判断服务分类是否存在
			if( (category = goodsService.selectCategoryById(goodsDto.getCateId())) == null){
				result = ResponseUtils.jsonFail(false, "服务分类不存在！");
				return result;
			}
		}
		goodsDto.setCategory(category);
		goodsDto.setMemberId(simpleMember.getId());
		goodsDto.setMemberName(simpleMember.getName());
		boolean flag = goodsService.saveGoods(goodsDto);
		if(flag){
			result = ResponseUtils.jsonSuccess(flag, "发布成功 ！");
		}else{
			result = ResponseUtils.jsonFail(flag, "发布失败 ！");
		}
		return result;
	}
	
	/**
	* 简介: 获取服务详情
	* @author zhaowei
	* @Date 2016年1月9日 下午12:50:25
	* @version 1.0
	* @param simpleMember
	* @param goodsSn	服务编号
	* @return
	 */
	@RequestMapping(value = "detail", method = RequestMethod.GET)
	public  ResponseMessage<?>  detail(
				@CurrentLoginMember SimpleMember simpleMember,
				@RequestParam(required=true) String goodsSn
			){
		ResponseMessage<?> result = null;
		GoodsDto goodsDto = goodsService.selectGoodsDtoInfoBySn(goodsSn);
		return ResponseUtils.jsonResult(goodsDto,true) ;
	} 
	
	
	/**
	* 简介: 服务  投诉
	* @author zhaowei
	* @Date 2016年1月9日 下午2:57:13
	* @version 1.0
	* @return
	 */
	@RequestMapping(value = "complaint", method = RequestMethod.POST)
	public  ResponseMessage<?>  complaint(
				@RequestParam(required=true) String msg
			){
		ResponseMessage<?> result = null;
		logger.info("服务投诉内容："+msg);
		return ResponseUtils.jsonResult(true,true) ;
	}
	
	/**
	* 简介: 会员服务列表
	* @author zhaowei
	* @Date 2016年1月10日 上午9:48:05
	* @version 1.0
	* @return
	 */
	@RequestMapping(value = "member", method = RequestMethod.GET)
	public ResponseMessage<?> memberList(
				@RequestParam(required=true) long memberId,
				@RequestParam(defaultValue = "1",required=false) Integer page,
	            @RequestParam(defaultValue = "10",required=false) Integer rows
			){
		ResponseMessage<?> result = null;
		Map<String, Object> parMap = new HashMap<String, Object>();
		parMap.put("pageNum", page);
		parMap.put("pageSize", rows);
		parMap.put("memberId", memberId);
		List<GoodsDto> goodsDtoList =  goodsService.selectNewGoodsByMap(parMap).getList();
		result = ResponseUtils.jsonResult(goodsDtoList,true) ;
		return result;
	}
	
}
