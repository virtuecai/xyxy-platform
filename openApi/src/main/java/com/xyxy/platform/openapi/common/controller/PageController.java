package com.xyxy.platform.openapi.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * App静态页面 帮助页面、关于页面、注册协议
 * @author liushun
 * @version 1.0
 * @Date 2015-12-30
 */
@Controller
@RequestMapping(value = "/app/v1/common/page",method = RequestMethod.GET)
public class PageController {

    @RequestMapping("/{name}")
    public String pageName(@PathVariable String name){
        //TODO放入页面地址
        return "";
    }
}
