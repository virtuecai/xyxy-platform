package com.xyxy.platform.openapi.member.v1.web.controller;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.beanvalidator.BeanValidators;
import com.xyxy.platform.modules.core.utils.BeanUtilsExt;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.member.MemberSpecificTag;
import com.xyxy.platform.modules.entity.member.SpecificTag;
import com.xyxy.platform.modules.service.member.MemberSpecificTagService;
import com.xyxy.platform.modules.service.member.SpecificTagService;
import com.xyxy.platform.modules.service.member.vo.SimpleMember;
import com.xyxy.platform.openapi.common.security.RequiresPermissions;
import com.xyxy.platform.openapi.common.web.bind.annotation.CurrentLoginMember;
import com.xyxy.platform.openapi.member.v1.web.vo.SpecificTagSimple;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.Validator;
import java.io.Serializable;
import java.util.List;

/**
 * 简介: 会员个性标签
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-27 21:19
 */
@RestController
@RequiresPermissions
@RequestMapping("/member/v1/specific_tag")
public class MemberSpecificTagController {

    @Autowired
    private Validator validator;

    @Autowired
    private MemberSpecificTagService memberSpecificTagService;

    @Autowired
    private SpecificTagService specificTagService;

    /**
     * 查询会员的个性标签
     *
     * @param simpleMember 根据登录信息注入的简单用户信息
     * @return
     */
    @RequiresPermissions
    @RequestMapping(method = RequestMethod.GET)
    public ResponseMessage<?> search(@CurrentLoginMember SimpleMember simpleMember) {
        List<MemberSpecificTag> memberSpecificTagList = memberSpecificTagService.findByMemberId(simpleMember.getId());
        List<Long> specificIdList = Collections3.extractToList(memberSpecificTagList, "specificId");
        List<SpecificTag> tagList = specificTagService.findByIdList(specificIdList);
        return ResponseUtils.jsonSuccess(tagList, "查询会员个性标签成功!");
    }

    /**
     * 更新会员个性标签数据
     * 先删除当前用户个性标签数据, 然后新增
     * @param simpleMember 当前挡路用户信息
     * @param specificTagForm 个性标签表单数据
     * @return
     */
    @RequiresPermissions
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseMessage<?> update(@CurrentLoginMember SimpleMember simpleMember, SpecificTagForm specificTagForm) {
        BeanValidators.validateWithException(validator, specificTagForm);

        List<SpecificTag> tagList = specificTagService.checkNotInDb(this.convert(specificTagForm.getTagList()));

        memberSpecificTagService.update(tagList, simpleMember.getId());

        return ResponseUtils.jsonSuccess(true, "更新会员个性标签成功!");
    }

    /**
     * convert SpecificTagSimple to SpecificTag
     *
     * @param specificTagSimpleList 参数绑定用到的简单 SpecificTag
     * @return
     */
    private List<SpecificTag> convert(List<SpecificTagSimple> specificTagSimpleList) {
        List<SpecificTag> specificTagList = Lists.newArrayList();
        for (SpecificTagSimple tagSimple : specificTagSimpleList) {
            SpecificTag tag = new SpecificTag();
            BeanUtils.copyProperties(tagSimple, tag, BeanUtilsExt.getNullPropertyNames(tagSimple));
            specificTagList.add(tag);
        }
        return specificTagList;
    }

}

/**
 * 标签参数绑定 form
 */
class SpecificTagForm implements Serializable {
    //注解标注jsr验证list中得对象
    @Valid
    private List<SpecificTagSimple> tagList;

    public List<SpecificTagSimple> getTagList() {
        return tagList;
    }

    public void setTagList(List<SpecificTagSimple> tagList) {
        this.tagList = tagList;
    }
}


