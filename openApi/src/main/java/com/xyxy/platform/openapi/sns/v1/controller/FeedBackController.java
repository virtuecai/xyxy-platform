package com.xyxy.platform.openapi.sns.v1.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.sns.FeedBack;
import com.xyxy.platform.modules.service.sns.FeedBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 提交留言、删除留言、获取留言列表
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */
@RestController
@RequestMapping(value=("/app/v1/feedback"), method = RequestMethod.GET)
public class FeedBackController {

    /*自动注入 留言业务类*/
    @Autowired
    private FeedBackService feedBackService;

    /**
     * 提交留言
     * @param feedBack 留言实体类
     * @return
     */
    @RequestMapping("/submit_feedback")
    public ResponseMessage<?> submitFeedback(FeedBack feedBack){
        int result = feedBackService.subimtFeedBack(feedBack);
        return ResponseUtils.jsonSuccess(result);
    }

    /**
     * 删除留言
     * @param msgId 留言消息ID
     * @return
     */
    @RequestMapping("/delete_feedback")
    public ResponseMessage<?> deleteFeedBack(@RequestParam long msgId){
        int result = feedBackService.deleteFeedBack(msgId);
        return ResponseUtils.jsonSuccess(result);
    }

    /**
     * 获取留言列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping("/feedback_list")
    public ResponseMessage<?> feedbackList(@RequestParam long memberId, @RequestParam Integer pageNumber, @RequestParam Integer pageSize){
        PageInfo pageResult = feedBackService.getFeedbackList(memberId, pageNumber, pageSize);
        return ResponseUtils.jsonSuccess(pageResult);
    }
}
