package com.xyxy.platform.openapi.test.v1.service;

import com.xyxy.platform.openapi.test.v1.vo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @Author: VirtueCai
 */
@Service
@Transactional
public class UserService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Async
    @Transactional(readOnly = true)
    public User findById(Long id) {
        User user = new User();
        user.setId(id);
        user.setName("caizhengda");
        logger.info("into UserService.findById");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }

}
