package com.xyxy.platform.openapi.member.v1.web.vo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-27 22:39
 */
public class SpecificTagSimple implements Serializable {

    private Long id;

    @NotNull
    @Length(min = 1, max = 100)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SpecificTagSimple{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}