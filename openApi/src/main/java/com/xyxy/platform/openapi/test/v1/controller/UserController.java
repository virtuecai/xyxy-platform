package com.xyxy.platform.openapi.test.v1.controller;


import com.xyxy.platform.modules.core.web.bind.annotation.FormModel;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.openapi.common.security.Permissions;
import com.xyxy.platform.openapi.common.security.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.openapi.test.v1.vo.User;
import com.xyxy.platform.openapi.test.v1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

/**
 * 简单实现了 controller 异步, 以及非controller外, service 异步
 * 可通过日志, 了解过程.
 * 一个纯http restful 并不一定需要webapp项目结构的. 考虑到可能会需要openApi提供部分详情页.
 * @Author: VirtueCai
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;


    @RequiresPermissions(permissions = {Permissions.BANK_CARD})
    @RequestMapping(value = "findicon", method = RequestMethod.GET)
    public Callable<ResponseMessage<?>> findIcon() {
        logger.info("into controller");
        Callable<ResponseMessage<?>> asyncTask = new Callable<ResponseMessage<?>>() {
            @Override
            public ResponseMessage call() throws Exception {
                Thread.sleep(2000);
                logger.info("into Callable");
                return ResponseUtils.jsonSuccess("success");
            }
        };
        logger.info("after Callable");
        return asyncTask;
    }

    @RequestMapping(value = "controllerasync/{id}", method = RequestMethod.GET)
    public Callable<ResponseMessage<?>> controllerAsync(@PathVariable("id") final Long id) {
        logger.info("into controller");

        Callable<ResponseMessage<?>> asyncTask = new Callable<ResponseMessage<?>>() {
            @Override
            public ResponseMessage call() throws Exception {
                Thread.sleep(2000);
                logger.info("info Callable");
                return ResponseUtils.jsonSuccess(new User(id, "czd"));
            }
        };
        logger.info("after Callable");
        return asyncTask;
    }

    @RequestMapping(value = "serviceasync/{id}", method = RequestMethod.GET)
    public ResponseMessage<?> serviceAsync(@PathVariable("id") final Long id) {
        logger.info("into controller");
        userService.findById(id);
        logger.info("after userService.findById method");
        return ResponseUtils.jsonSuccess(new User(id, "czd"));
    }

    @RequestMapping(value = "currentuser", method = RequestMethod.GET)
    public ResponseMessage<?> currentUser(@FormModel("user") User user) {
        logger.info("123");
        return ResponseUtils.jsonSuccess(user);
    }

}
