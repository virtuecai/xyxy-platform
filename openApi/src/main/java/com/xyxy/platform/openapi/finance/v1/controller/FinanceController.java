package com.xyxy.platform.openapi.finance.v1.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.beanvalidator.BeanValidators;
import com.xyxy.platform.modules.core.beanvalidator.group.A;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.finance.UserAccount;
import com.xyxy.platform.modules.service.finance.FinanceService;
import com.xyxy.platform.openapi.finance.v1.controller.finance.v1.group.B;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * FinanceController
 *
 * @author yangdianjun
 * @date 2015/12/18 0018
 */
@RestController
@RequestMapping("/finance")
public class FinanceController {

    @Autowired
    FinanceService financeService;
    @Autowired
    private Validator validator;


    @ResponseBody
    @RequestMapping(value = "/get_pay_log",method = RequestMethod.GET)
    public ResponseMessage<?> getIncomeOrPaylog( @RequestParam Long memberId, Integer action ,
                                                 Integer isPaid,@RequestParam Integer pageNumber,@RequestParam Integer pageSize){
        PageInfo page=financeService.getIncomeOrPaylog(memberId,action,isPaid,pageNumber,pageSize);
        return ResponseUtils.jsonSuccess(page);
    }

    @ResponseBody
    @RequestMapping(value = "/get_myaccount",method = RequestMethod.GET)
    public ResponseMessage<?> getMyAccount(validateForm validateform){
        BeanValidators.validateWithException(validator, validateform,A.class);
        if(financeService.getMyAccount(validateform.getMemberId())==null){
            return ResponseUtils.jsonResult(null,true,"暂无数据");
        }
        return ResponseUtils.jsonResult(financeService.getMyAccount(validateform.getMemberId()), true, "");
    }

    @ResponseBody
    @RequestMapping(value="/get_payment",method = RequestMethod.GET)
    public ResponseMessage<?> getPayment(){
        return ResponseUtils.jsonSuccess(financeService.getPayment());
    }

    @ResponseBody
    @RequestMapping(value="/insert_useraccount" , method = RequestMethod.GET)
    public ResponseMessage<?> insertUserAccount(validateForm validateform){
        BeanValidators.validateWithException(validator, validateform);
        UserAccount userAccount=new UserAccount();
        userAccount.setIsYue(validateform.getIsYue());
        userAccount.setAmount(validateform.getAmount());
        userAccount.setAction(validateform.getAction());
        userAccount.setPaymentId(validateform.getPaymentId());
        userAccount.setMemberId(validateform.getMemberId());
        userAccount.setIsPaid(validateform.getIsPaid());
        if(financeService.insertUseraccount(userAccount)==null){
            return ResponseUtils.jsonFail(userAccount,"错误操作!请仔细核对金额");
        }
        return ResponseUtils.jsonResult(financeService.insertUseraccount(userAccount),true,"添加成功");
    }

    @ResponseBody
    @RequestMapping(value="/update_useraccount" , method = RequestMethod.GET)
    public ResponseMessage<?> updateUserAccount(UserAccount userAccount){
        UserAccount entity = financeService.updateUseraccount(userAccount);
        if(userAccount.getMemberId()==null ||userAccount.getAccountId()==null){
            return ResponseUtils.jsonFail(null,"缺少用户id或提现记录id");
        }
        if(entity==null){
            return ResponseUtils.jsonFail(null,"暂无数据！请核对提交内容");
        }
        return ResponseUtils.jsonResult(entity,true,"更新成功");
    }

}

//用于插入支出收入数据时的表单验证
class validateForm implements Serializable{


    @NotNull
    @Range(min=1,max=1)
    private int action;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private Integer paymentId;

    @NotNull(groups = A.class)
    private Long memberId;

    @NotNull
    @Range(min=1,max=1)
    private int isPaid;

    @NotNull
    @Range(min=1,max=1)
    private int isYue;

    public int getIsYue() {
        return isYue;
    }
    public void setIsYue(int isYue) {
        this.isYue = isYue;
    }
    public Integer getPaymentId() {
        return paymentId;
    }
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }
    public Long getMemberId() {
        return memberId;
    }
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }
    public int getAction() {
        return action;
    }
    public void setAction(int action) {
        this.action = action;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public int getIsPaid() {
        return isPaid;
    }
    public void setIsPaid(int isPaid) {
        this.isPaid = isPaid;
    }
}
