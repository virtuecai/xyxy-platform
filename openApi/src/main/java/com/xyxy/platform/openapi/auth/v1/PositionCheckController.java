package com.xyxy.platform.openapi.auth.v1;

import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.service.auth.PositionCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 简介: 职业认证
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-25 15:09
 */
@RestController
@RequestMapping("/auth/v1/position_check")
public class PositionCheckController extends BaseController {

    @Autowired
    private PositionCheckService positionCheckService;

}
