package com.xyxy.platform.openapi.common.web.bind.vo;

import java.io.Serializable;

/**
 * Created by VirtueCai on 15/12/21.
 */
public class CurrentMember implements Serializable {

    private Long id;

    private String name;

    private String token;

    public CurrentMember() {
    }

    public CurrentMember(Long id, String name, String token) {
        this.id = id;
        this.name = name;
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
