package com.xyxy.platform.openapi.trade.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.trade.UserAddress;
import com.xyxy.platform.modules.service.member.vo.SimpleMember;
import com.xyxy.platform.modules.service.trade.UserAddressService;
import com.xyxy.platform.openapi.common.security.RequiresPermissions;
import com.xyxy.platform.openapi.common.web.bind.annotation.CurrentLoginMember;
/** 
 * 联系人控制器
 * @author vic E-mail: 251361939@qq.com
 * @version 创建时间：2015年12月19日 下午6:10:20 
 */
@RestController
@RequestMapping("/contacts/v1")
@RequiresPermissions
public class ContactsController extends BaseController {
	
	@Autowired
	private UserAddressService userAddressService;
	
	/**
	* 简介: 获取我的联系人列表 
	* @author zhaowei
	* @Date 2016年1月10日 下午9:00:36
	* @version 1.0
	* @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public  ResponseMessage<?>  list(
				@CurrentLoginMember SimpleMember simpleMember
			){
		List<UserAddress> userAddressList = userAddressService.selectCartById(simpleMember.getId());
		return ResponseUtils.jsonSuccess(userAddressList);
	}
	
	/**
	* 简介: 修改联系人信息 
	* @author zhaowei
	* @Date 2016年1月10日 下午9:17:50
	* @version 1.0
	* @param simpleMember
	* @return
	 */
	@RequestMapping(value = "edit", method = RequestMethod.POST)
	public  ResponseMessage<?>  edit(
				@CurrentLoginMember SimpleMember simpleMember,
				UserAddress userAddress
			){
		ResponseMessage<?> result = null;
		boolean success = userAddressService.editUserAddress(userAddress);
		if(success){
			result = ResponseUtils.jsonSuccess(true);
		}else{
			result = ResponseUtils.jsonFail(false);
		}
		return result;
	}
	
	/**
	* 简介: 删除【我的】联系人 
	* @author zhaowei
	* @Date 2016年1月10日 下午9:27:09
	* @version 1.0
	* @param simpleMember
	* @param contactId
	* @return
	 */
	@RequestMapping(value = "remove", method = RequestMethod.POST)
	public  ResponseMessage<?>  remove(
				@CurrentLoginMember SimpleMember simpleMember,
				@RequestParam(required=true) long addressId
			){
		ResponseMessage<?> result = null;
		
		boolean success = userAddressService.removeUserAddress(addressId,simpleMember.getId());
		if(success){
			result = ResponseUtils.jsonSuccess(true);
		}else{
			result = ResponseUtils.jsonFail(false);
		}
		return result;
	}
	
	
}
