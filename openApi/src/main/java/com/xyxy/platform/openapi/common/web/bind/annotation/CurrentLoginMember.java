package com.xyxy.platform.openapi.common.web.bind.annotation;


import java.lang.annotation.*;

/**
 * 用于根据请求header中得token绑定用户对象</p>
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentLoginMember {

}
