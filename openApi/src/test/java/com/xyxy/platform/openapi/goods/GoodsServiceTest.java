package com.xyxy.platform.openapi.goods;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.xyxy.platform.modules.service.trade.GoodsService;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class GoodsServiceTest {
	
	@Autowired
	private GoodsService goodService;
	
	//@Test
	public void testFindNew(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("page", 1);
		map.put("rows", 10);
		List<GoodsDto> goodsList  = goodService.selectNewGoodsByMap(map).getList();
		if(goodsList!=null&&goodsList.size()>0){
			for(GoodsDto dto : goodsList){
				System.out.println(dto);
			}
		}
	}
	
	//@Test
	public void testFindHot(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("page", 1);
		map.put("rows", 10);
		List<GoodsDto> goodsList  = goodService.selectHotGoodsByMap(map).getList();
		if(goodsList!=null&&goodsList.size()>0){
			for(GoodsDto dto : goodsList){
				System.out.println(dto);
			}
		}
	}
	
	//@Test
	public void testFindBest(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("page", 1);
		map.put("rows", 10);
		List<GoodsDto> goodsList  = goodService.selectBestGoodsByMap(map).getList();
		if(goodsList!=null&&goodsList.size()>0){
			for(GoodsDto dto : goodsList){
				System.out.println(dto);
			}
		}
	}
	
	//@Test
	public void testSjsGoods(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("categoryId", "12");
		List<GoodsDto> goodsList  = goodService.selectNewGoodsByMap(map).getList();
		if(goodsList!=null&&goodsList.size()>0){
			for(GoodsDto dto : goodsList){
				System.out.println(dto);
			}
		}
	}
	
}
