package com.xyxy.platform.openapi.common;

import com.xyxy.platform.modules.core.file.SystemFileService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 简介: 系统文件处理类
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-29 14:16
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class SystemFileTest {

    @Autowired
    SystemFileService systemFile;

    String htmlContent = "<img src=\"http://localhost:8080/files/tmp/111111111-sss.gif\"/>\n" +
            "\t<br/>\n" +
            "\t<div>\n" +
            "\t\t<span>1</span>\t\n" +
            "\t</div>\n" +
            "\t<img src=\"http://localhost:8080/files/tmp/22222222-sss.gif\"/>\n" +
            "\t<img src=\"http://localhost:8080/files/tmp/33333333333-sss.gif\"/>";

    @Test
    public void testSaveTmpFile() throws Exception {

    }

    @Test
    public void testChangeFileStatus() throws Exception {

    }

    @Test
    public void testChangeImageFileStatusFromHtml() throws Exception {
        Document document = Jsoup.parse(htmlContent);

        Elements images = document.select("img[src]");
        for (Element element : images) {
            String imgUrl = element.attr("src");
            String fileName = imgUrl.substring(imgUrl.lastIndexOf("/"));
            if (StringUtils.isNotEmpty(fileName)) {
                imgUrl = systemFile.getFileUrl() + fileName;
                element.attr("src", imgUrl);
            }
        }

        System.out.println(document.select("body").html().toString());
    }

    @Test
    public void testGetFileName() throws Exception {
        String url = "http://localhost:8888/files/sdfsfsafwerwerwer.gif";
        systemFile.getFileNameFromUrl(url);
    }
}