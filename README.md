<a href="http://www.xyxy.com/" target="_blank">星语心愿</a>

##介绍
* xyxy-platform 属于星语心愿平台项目模块集

### 模块接收
* modules: 通用底层实现, 数据库映射entity, 数据访问, 业务支撑, 核心core.
* admin: 企业内部后台管理系统.
* website: PC Web 端
* openApi: ios, android http 接口


### 技术选型

#### 管理
* maven(3.3.3)依赖和项目管理
* git版本控制

#### 后端
* IoC容器 spring
* web框架 springmvc
* orm框架 mybatis + spring jdbc
* 安全框架 shiro, 自定义简单权限验证(openApi)
* 验证框架 hibernate validator
* 任务调度框架 quartz
* 缓存 ehcache, memberCache
* 数据源 druid
* 日志 slf4j+logback, log4jdbc
* Json 阿里 fastjson
* servlet 3.0(需要支持servlet3的servlet容器，如tomcat7)
* jcaptcha 验证码
* jsp 模板视图

#### 前端
* jquery js框架
* jquery-ui-bootstrap界面框架
* font-wesome 字体/图标框架
* 日期操作 moment
* jquery Validation Engine 验证框架（配合spring的验证框架，页面验证简化不少）
* UEditor 百度富文本编辑器
* nicescroll 漂亮的滚动条
* zTree 树框架
* 基于bootstrap实现的SmartAdmin
* jquery-fileupload 文件上传
* bootstrap-datatimepicker 日历选择

#### 数据库
 * mysql

#### JDK
 * 1.7

####
 * 最新版的选型技术

#### 开发工具
 * IntellijIdea
 * Eclipse

###支持的浏览器
 * chrome
 * firefox（目前使用firefox 19.0.2测试）
 * opera 12
 * ie7及以上（建议ie9以上，获取更好的体验）
 * 其他浏览器暂时未测试