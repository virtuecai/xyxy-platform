/*index.js  20151202 author xiegang*/

/***********************************全局commonjs开始**********************************/

//顶部工具栏展开
$('#tool_right>ul>li').on('mouseenter mouseleave',function(){
	$(this).find('i').toggleClass('icon_up');
	$(this).find('ul').stop().slideToggle();
});

//头部二级服务菜单显示
$('#nav_left_menu>ul>li').hover(function(){
	$(this).find('ul').show();
},function(){
	$(this).find('ul').hide();
});

/*//搜索条件工具栏
 $('#search_classify').hover(function(){
 $(this).find('ul').fadeIn(200);
 $(this).find('i').addClass('icon_up');
 },function(){
 $(this).find('ul').hide();
 $(this).find('i').removeClass('icon_up');
 });

 //搜索条件工具栏选择
 $('#search_classify>ul>li').on('click',function(){
 var classifyId   = $(this).attr('classifyid');
 var classifyText = $(this).html();
 $(this).parent('ul').siblings('input').val(classifyId);
 $(this).parent('ul').siblings('.selected').html(classifyText);
 $(this).parent('ul').stop().slideUp();
 });*/

//返回顶部
$("#nav_back_top").on("click", function(){
	$('html,body').animate({scrollTop: 0}, 500);
});

/***********************************首页JS开始**********************************/
//banner轮播
$("#banner").slide({ titCell:".hd>ul", mainCell:".bd>ul", effect:"fade",  autoPlay:true, autoPage:true, trigger:"mouseover" });

//楼层滚动
initFloor();//初始化楼层定位
//自适应窗口楼层定位
$(window).resize(function() {
	initFloor();//初始化楼层定位
});
function initFloor(){
	var floor_one_top  = $('#floor_first').offset().top + 20;
	var floor_one_left = (($(window).width()-$('#floor_first_bottom').width())/2)-60;
	$('#floor_fixed_nav').css({left:floor_one_left,top:floor_one_top});
}

//执行楼层函数
$("#floor_fixed_nav a").highlight("hover");

//点击单独楼层
$('#floor_fixed_nav a').on('click',function(){
	var floorNums = $(this).attr('href');
	$('html,body').animate({scrollTop: $(floorNums).offset().top}, 500);
});

//滚动窗口-执行数字滚动函数
$(window).scroll(function(){// @ 给窗口加滚动条事件
	var scrollH = $('#data_show').offset().top-$(window).height()-$(document).scrollTop();
	if(scrollH>0){
		//执行数字滚动
		numRuns();
	}
});
//数字滚动函数
$.fn.countTo = function (options) {
	options = options || {};

	return $(this).each(function () {
		// set options for current element
		var settings = $.extend({}, $.fn.countTo.defaults, {
			from:            $(this).data('from'),
			to:              $(this).data('to'),
			speed:           $(this).data('speed'),
			refreshInterval: $(this).data('refresh-interval'),
			decimals:        $(this).data('decimals')
		}, options);

		// how many times to update the value, and how much to increment the value on each update
		var loops = Math.ceil(settings.speed / settings.refreshInterval),
			increment = (settings.to - settings.from) / loops;

		// references & variables that will change with each update
		var self = this,
			$self = $(this),
			loopCount = 0,
			value = settings.from,
			data = $self.data('countTo') || {};

		$self.data('countTo', data);

		// if an existing interval can be found, clear it first
		if (data.interval) {
			clearInterval(data.interval);
		}
		data.interval = setInterval(updateTimer, settings.refreshInterval);

		// initialize the element with the starting value
		render(value);

		function updateTimer() {
			value += increment;
			loopCount++;

			render(value);

			if (typeof(settings.onUpdate) == 'function') {
				settings.onUpdate.call(self, value);
			}

			if (loopCount >= loops) {
				// remove the interval
				$self.removeData('countTo');
				clearInterval(data.interval);
				value = settings.to;

				if (typeof(settings.onComplete) == 'function') {
					settings.onComplete.call(self, value);
				}
			}
		}

		function render(value) {
			var formattedValue = settings.formatter.call(self, value, settings);
			$self.html(formattedValue);
		}
	});
};

$.fn.countTo.defaults = {
	from: 0,               // the number the element should start at
	to: 0,                 // the number the element should end at
	speed: 1000,           // how long it should take to count between the target numbers
	refreshInterval: 100,  // how often the element should be updated
	decimals: 0,           // the number of decimal places to show
	formatter: formatter,  // handler for formatting the value before rendering
	onUpdate: null,        // callback method for every time the element is updated
	onComplete: null       // callback method for when the element finishes updating
};

function formatter(value, settings) {
	return value.toFixed(settings.decimals);
}

//执行函数
function numRuns(){
	// custom formatting example
	$('#data_show .timer').data('countToOptions', {
		formatter: function (value, options) {
			return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
		}
	});
	// start all the timers
	$('#data_show .timer').each(count);
}

function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
}
