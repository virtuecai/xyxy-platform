$(function () {
    new ServiceIndex();
});

var ServiceIndex = function () {
    this.init();
};
ServiceIndex.prototype = {
    $categoryListContainer: $('.category-list'),
    $serviceList: $('.service_list'),
    $page: null,
    init: function () {
        var that = this;

        // 加载分类信息
        that.queryCategoryAjax();

        // 服务列表分页查询
        that.initGoodsPage();

        //服务筛选
        $('#service').on('change', function () {
            that.$page.pagination('setParams', that.getSearchParams());
            that.$page.pagination('remote');
        });

        // 最新 or 热门点击切换
        $('.service_filter .status a').on('click', function () {
            var $this = $(this);
            $this.siblings('a').removeClass('hover');
            $this.addClass('hover');
            $this.siblings('input:hidden').val($this.data('val'));

            that.$page.pagination('setParams', that.getSearchParams());
            that.$page.pagination('remote');
        });

        return that;
    },
    getSearchParams: function () {
        var searchType = $('[name=searchType]').val();
        var serverType = $('#service').val();
        var categoryId = $('[name=parentCategoryId]').val();
        return {
            searchType: searchType ? Number(searchType) : null,
            categoryId: categoryId ? Number(categoryId): null,
            serverType: serverType ? Number(serverType) : null
        }
    },
    initGoodsPage: function () {
        var that = this;
        that.$page = $("#goods-pagination").pagination({
            pageSize: 15,
            total: 100,
            debug: false,
            showInfo: true,
            showJump: true,
            showPageSizes: false,
            prevBtnText: '<i><</i> 上一页',
            nextBtnText: '下一页 <i>></i>',
            jumpBtnText: '确定',
            infoFormat: '共 {total} 条',
            noInfoText: '暂无数据',
            pageElementSort: ['$page', '$size', '$jump', '$info'],
            remote: {
                url: window['ctx'] + '/service/goods/list.json',
                params: that.getSearchParams(),
                totalName: 'result.total',
                pageIndexName: 'pageNum',
                pageSizeName: 'pageSize',
                success: function (data) {
                    _.templateRender(that.$serviceList, data.result.list);
                }
            }
        });
    },
    // 加载 服务 分类 列表
    queryCategoryAjax: function () {
        var that = this;
        var categoryParentId = $('[name=parentCategoryId]').val();
        $.ajax({
            url: window['ctx'] + '/service/category/list.json',
            type: 'get',
            dataType: 'json',
            data: {
                categoryParentId: categoryParentId ? categoryParentId : 0
            }
        }).done(function (data) {
            if (!data.success) {
                return;
            }
            _.templateRender(that.$categoryListContainer, data.result);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            layer.msg('网络繁忙, 查询分类信息失败, 请稍后再试!', {icon: 2});
        })
    }
};