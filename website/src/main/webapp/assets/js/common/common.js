/*common.js  20151202 author xiegang*/

//顶部工具栏展开
$('#tool_right>ul>li').on('mouseenter mouseleave',function(){
	$(this).find('i').toggleClass('icon_up');
	$(this).find('ul').stop().slideToggle();
});

//服务菜单显示
if($('#header_nav_left').attr("slideDisabled") != 'true') {
	//头部服务菜单展开
	$('#header_nav_left').hover(function () {
		$(this).find('.nav_left_menu').show();
	}, function () {
		$(this).find('.nav_left_menu').hide();
	});
}

//头部二级服务菜单显示
$('#nav_left_menu>ul>li').hover(function(){
	$(this).find('ul').show();
},function(){
	$(this).find('ul').hide();
});

/*//搜索条件工具栏
 $('#search_classify').hover(function(){
 $(this).find('ul').fadeIn(200);
 $(this).find('i').addClass('icon_up');
 },function(){
 $(this).find('ul').hide();
 $(this).find('i').removeClass('icon_up');
 });

 //搜索条件工具栏选择
 $('#search_classify>ul>li').on('click',function(){
 var classifyId   = $(this).attr('classifyid');
 var classifyText = $(this).html();
 $(this).parent('ul').siblings('input').val(classifyId);
 $(this).parent('ul').siblings('.selected').html(classifyText);
 $(this).parent('ul').stop().slideUp();
 });*/

//返回顶部
$("#nav_back_top").on("click", function(){
	$('html,body').animate({scrollTop: 0}, 500);
});

//浏览器版本检测