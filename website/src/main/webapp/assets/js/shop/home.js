$(function(){
	var shopHome = new ShopHome();
});

var ShopHome = function(){
	this.init();
}
ShopHome.prototype = {
	init:function(){
		//获取头像
		$.ajax({
			url: ctx+"/shop/"+imgid+"/home/member_img",
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				if(data.success) {
					$("#photo").attr("src",ctx + "/files/" +data.result);
				}else
					alert(data.message);
			}
		});

		//获取个人介绍
		$.ajax({
			url: ctx+"/shop/"+aid+"/home/member_desc",
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				if(data.success) {
					$(".intro_detail").html(data.result);
				}else
					alert(data.message);
			}
		});
		////分页留言信息
		//$.ajax({
		//	url: ctx+"/shop/"+mid+"/home/messages_page",
		//	type: 'GET',
		//	data:{pageNum:1, pageSize:10},
		//	dataType: 'json',
		//	success: function (data) {
		//		alert(JSON.stringify(data));
		//		if(data.success) {
		//			$(".comment_list").html(data.result.list);
		//		}else
		//			alert(data.message);
		//	}
		//});
		//个人主页推荐服务
		$.ajax({
			url: ctx+"/shop/"+mid+"/home/goods_list",
			type: 'GET',
			dataType: 'json',
			success: function (data) {
			//	layer.alert(JSON.stringify(data.result));
				if(data.success) {
					_.templateRender('#goods-template', data.result);
				}else
					alert(data.message);
			}
		});
		//提交留言
		$("#submit_msg").on("click",function(){
			var content = $(".message").val()
			$.ajax({
				url: ctx+"/shop/"+mid+"/home/submit_messages",
				type: 'POST',
				data: {content:content},
				dataType: 'json',
				success: function (data) {
					if(data.success) {
						alert("留言提交成功!");
					}else
						alert(data.message);
				}
			});
		});

		//个人主页统计粉丝和我关注的人
		$.ajax({
			url: ctx+"/shop/"+mid+"/home/with_count",
			type: "GET",
			data: {toMemberId: "123354"},
			dataType: 'json',
			success: function (data) {
				if(data.success) {
				//	alert(JSON.stringify(data.result));
					//$("#is_gz").html(data.result.isGz);
					$("#fs_count").text(data.result.fsCount);
					$("#gz_count").text(data.result.gzCount);
				}else
					alert(data.message);
			}
		});
		//个人主页获取职业标签和个人标签列表信息
		$.ajax({
			url: ctx+"/shop/"+mid+"/home/tag_list",
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				if(data.success) {
					//迭代个人标签
					var grtag = ""
					$.each(data.result.grTag,function(i, obj){
						grtag += '<span class="clearfix hover"> <i class="icon_shop icon_header"></i> <i class="icon_shop icon_footer">'+obj.tagName+'</i> </span>'
					});
					$("#grtag").html(grtag);

					//迭代职业标签
					var zytag = "";
					$.each(data.result.zyTag,function(i, obj){
						zytag += '<span class="clearfix"><i class="icon_shop icon_header"></i> <i class="icon_shop icon_footer">'+obj.tagName+'</i> </span>';
					});
					$("#zytag").html(zytag)
				}else
					alert(data.message);
			}
		});
		//个人主页获取我关注的人中 有那些人人也关注了他
		$.ajax({
			url: ctx+"/shop/"+mid+"/home/with_list",
			type: 'GET',
			data: {toMemberId:"153465434"},
			dataType: 'json',
			success: function (data) {
				if(data.success) {
				//	alert(JSON.stringify(data.result));
				}else
					alert(data.message);
			}
		});
	}
}