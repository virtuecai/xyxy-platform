var Application = function () {
    this.init();
};
Application.prototype = {
    init: function () {
        var that = this;

        that._jqueryAjax();

        that._underscoreSettings();

        that._extUnderscoreTemplate();

        that._synchronousServerTime();

        that._extNativeJs();

        return that;
    },
    /**
     * 全局 jquery ajax
     * jquery ajax 时候, 使用layer加载提示, 请求完成隐藏.
     * @private 私有 不可访问
     */
    _jqueryAjax: function () {
        var layerLoadingIdx;

        $(document).ajaxSend(function (evt, request, settings) {
            // 请求开始
            if (settings.loading != false) {
                layerLoadingIdx = layer.load(0, {shade: false});
            }
            // 当前页面url
            var url = window.location.href;
            var requestMapper = url.substring(url.indexOf(ctx) + ctx.length + 1);
            request.setRequestHeader("login_success_redirect_url", requestMapper);
        }).ajaxComplete(function (event, request, settings, xhr) {
            // 请求完成
            setTimeout(function () {
                layer.close(layerLoadingIdx);
            }, 200);
            if (request.status == 403) {
                layer.confirm('登录超时, 跳转至登录页面登录?', {
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    window.location.href = window['ctx'] + '/login';
                }, function () {
                    // 取消
                });
                return false;
            }
        }).ajaxError(function (event, request, settings) {
            // 请求异常
            //服务停止, 返回 request readyState: 0, responseJSON: undefined, status: 0, statusText: "error"
        });
    },
    /**
     * 定制 underscore 模版解析语法
     * @type {{evaluate: RegExp, interpolate: RegExp, escape: RegExp}}
     * @private 私有 不可访问
     */
    _underscoreSettings: function () {
        if (!window['_']) return;
        _.templateSettings = {
            evaluate: /\{\{(.+?)\}\}/g,
            interpolate: /\{\{=(.+?)\}\}/g,
            escape: /\{\{-(.+?)\}\}/g
        };
    },
    /**
     * 扩展 underscore 模版渲染
     * <img data-src='' default-src='' />
     * @private 私有 不可访问
     */
    _extUnderscoreTemplate: function () {
        var that = this;
        if (!window['_']) return;
        /**
         * 在需要使用underscore模版渲染的父级元素上加入 class underscore-template 更好地交互体验 display: none;
         * 构造模版解析, 无需使用 each 操作!
         * @param container 模版内容所在元素parent, jquery 选择器string 或 jquery 对象
         * @param data 模版解析中使用到得数据
         * @param beforeCallback 处理之前回调函数
         * @param afterCallback 处理完成回调函数, 并带有 遍历获得的 jquery 对象数组
         *
         * 调用示例: _.templateRender('.className' | $('.className'), {} || [{},{}], function(){} ,function($elArray){});
         */
        _.templateRender = function (container, data, beforeCallback, afterCallback) {
            beforeCallback && beforeCallback();
            var $container = $(container);
            var templateContent = $container.data('template');
            if (!templateContent) {
                templateContent = $container.find('.underscore-template').first().prop('outerHTML');
            }

            //templateContent 内容转义 防止 js 代码中出现 &gt 等等
            templateContent = templateContent.replace(new RegExp("&lt;","g"), '<').replace(new RegExp("&gt;","g"), '>');

            if (
                _.templateSettings.evaluate.test(templateContent) ||
                _.templateSettings.interpolate.test(templateContent) ||
                _.templateSettings.escape.test(templateContent)) {
                $container.data('template', templateContent);
            } else {
                throw "请确定模版语法:( {{=}} {{}} {{-}}), 以及容器选择器!";
            }

            var $el = [];
            // 兼容 对象 or 数据
            if (jQuery.type(data) === "array") {
                $.each(data, function (idx, item) {
                    var $item = $(_.template(templateContent)(item)).data('item', item).removeClass('underscore-template').addClass('underscore-template-rendered');
                    $item.find('img').each(function () {
                        var dataSrc = $(this).data('src');
                        if (dataSrc) $(this).attr('src', dataSrc);
                    });
                    $el.push($item);
                });
            } else if (jQuery.type(data) === "object") {
                var $item = $(_.template(templateContent)(data)).data('item', data).removeClass('underscore-template').addClass('underscore-template-rendered');
                $item.find('img').each(function () {
                    var dataSrc = $(this).data('src');
                    if (dataSrc) $(this).attr('src', dataSrc);
                });
                $el.push($item);
            }
            $container.find('.underscore-template, .underscore-template-rendered').remove();
            $container.append($el);
            // 当图片url加载时候, 采用默认图片, 需要在图片元素上加上 src-default url;
            that.loadDefaultImgOnError($container);
            //完成后回调
            afterCallback && afterCallback($el);
        };
        _.templateResolve = function (templateContent, data) {
            if (
                _.templateSettings.evaluate.test(templateContent) ||
                _.templateSettings.interpolate.test(templateContent) ||
                _.templateSettings.escape.test(templateContent)) {
            } else {
                throw "请确定模版语法:( {{=}} {{}} {{-}}), 以及容器选择器!";
            }

            var $el = [];
            // 兼容 对象 or 数据
            if (jQuery.type(data) === "array") {
                $.each(data, function (idx, item) {
                    var $item = $(_.template(templateContent)(item)).data('item', item).removeClass('underscore-template').addClass('underscore-template-rendered');
                    $el.push($item);
                });
            } else if (jQuery.type(data) === "object") {
                var $item = $(_.template(templateContent)(data)).data('item', data).removeClass('underscore-template').addClass('underscore-template-rendered');
                $el.push($item);
            }
            return $el;
        };
    },
    /**
     * 当图片url加载时候, 采用默认图片, 需要在图片元素上加上 src-default url;
     * @param $container 该元素容器下得所有img, 可以为jquery对象, 或者jquery选择器字符串
     */
    loadDefaultImgOnError: function ($container) {
        // 当图片url加载时候, 采用默认图片, 需要在图片元素上加上 src-default url;
        $("img", $($container)).one("error", function () {
            var $this = $(this);
            var srcDefault = $.trim($this.attr('src-default'));
            if (srcDefault && srcDefault != '') $this.attr("src", srcDefault);
        }).filter(function () {  //check to see if an image was loaded from the cache as broken (or if onerror fired before set)
            return (this.complete && (this.naturalWidth === 0 || this.naturalWidth === undefined));
        }).trigger("error");
    },
    /**
     * 面包屑内容加载
     * 示例: ['管理中心','我的服务','发布服务']
     */
    initPagePosition: function (names, hoverName) {
        var $positionUl = $('.position ul');
        var line = '<li class="line">&gt;</li>';
        var _html = [];
        if (names && names.length > 0) {
            $.each(names, function (idx, name) {
                _html.push('<li><a href="javascript:void(0);" title="' + name + '">' + name + '</a></li>');
            });
        }
        $positionUl.html(_html.join(line));
        if (hoverName) $positionUl.find('a:last').attr('hoverName', hoverName);
    },
    /**
     * 前端同步服务器时间
     * 通过装饰器jsp获得当前服务器时间戳, 前端每一秒更新服务器获得的时间
     * @private 私有 不可访问
     */
    _synchronousServerTime: function () {
        if (!window['serverTime']) window['serverTime'] = new Date().getTime();
        setInterval(function () {
            window['serverTime'] = window['serverTime'] + 1000;
        }, 1000);
    },
    /**
     * 扩展浏览器查一下导致部分函数兼容性问题
     * @private
     */
    _extNativeJs: function () {
        //-----------------------数组操作相关方法-------------------------
        Array.indexOf = function (arr, item) {
            return Array.indexOf(arr, item, 0);
        };
        Array.indexOf = function (arr, item, fromIndex) {
            for (var i = (fromIndex >= 0 ? fromIndex : 0), j = arr.length; i < j; i++) if (arr[i] == item) return i;
            return -1;
        };
        Array.lastIndexOf = function (arr, item, fromIndex) {
            for (var i = arr.length - 1; i >= (fromIndex >= 0 ? fromIndex : 0); i--) if (arr[i] == item) return i;
            return -1;
        };
        Array.contains = function (arr, item) {
            return Array.indexOf(arr, item) > -1;
        };
        Array.remove = function (arr, item) {
            var i = Array.indexOf(arr, item);
            if (i > -1) {
                arr.splice(i, 1);
                return true;
            }
            return false;
        };
        Array.removeAt = function (arr, index) {
            arr.splice(index, 1);
        };
        Array.insertAt = function (arr, index, obj) {
            arr.splice(index, 0, obj);
        };
        Array.clear = function (arr) {
            arr.splice(0, arr.length);
        };

        //for ie6
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (val) {
                var value = this;
                for (var i = 0; i < value.length; i++) {
                    if (value[i] == val) return i;
                }
                return -1;
            };
        }
        ;

        String.prototype.replaceAll = function (s1, s2) {
            return this.replace(new RegExp(s1, "gm"), s2);
        }

        //解决IE8下不支持Object.keys
        if (!Object.keys) {
            Object.keys = function (obj) {
                var keys = [];
                for (var i in obj) {
                    if (obj.hasOwnProperty(i)) {
                        keys.push(i);
                    }
                }
                return keys;
            };
        }
        ;

        if (!window.console) {
            window.console = {
                log: function () {
                }
            };
        }
    }
};

var Utils = {};

var Constants = {
    goodsServerTimeDefine: {
        1: "09:00-12:00",
        2: "12:00-15:00",
        3: "15:00-17:00",
        4: "17:00-21:00",
        5: "21:00-24:00"
    }
}

$(function () {
    window['application'] = new Application().init();
});

