/**
 * Created by Administrator on 2016/1/11 0011.
 */
$(function () {
    application.initPagePosition(['管理中心', '基本资料', '个人信息']);
    var info = new Info();
})

var Info = function () {
    this.init();
}

Info.prototype = {
    init: function () {

        // 图片上传
        //上传服务标题图 浮动效果.
        var that = this;
        $('#upload_thumb').on('mouseenter mouseleave', function () {
            $(this).siblings('#upload_btn').toggleClass('hover');
        });
        that.uploadHeadpic();

        //删除上传的服务标题图
        $(document).on('click', '#form_imglsit .icon_delete', function () {
            var objClick = $(this);
            var _layerId = layer.confirm('确认删除该图？', {
                btn: ['确认', '再考虑下'], //按钮
                title: '温馨提示',
                icon: 0
            }, function () {
                objClick.parent('a').remove();
                layer.close(_layerId);
            }, function () {
                //layer.msg('多谢手下留情！',{icon:6,time:1000});
            });
        });


        var mcityid = $("#loadcityid").val();
        var mareaid = $("#loadareaid").val();
        $.get(ctx + "/car/getCityByProvinceId/" + $("#province").val(), function (data) {
            if (data.success) {
                var result = "";
                $.each(data.result, function (n, value) {
                    result += "<option value='" + value.cityId + "'>" + value.cityName + "</option>";
                });
                $("#city").html('');
                $("#city").append(result);
            }
        }, "json");

        $.get(ctx + "/car/getAreaByCityId/" + mcityid, function (data) {
            if (data.success) {
                var result = "";
                $.each(data.result, function (n, value) {
                    if (value.areaId == mareaid) {
                        return true;
                    } else {
                        result += "<option value='" + value.areaId + "'>" + value.areaName + "</option>";
                    }
                });
                //$("#area").html("");
                $("#area").append(result);
            }
        }, "json");

        $("#province").change(function () {
            $.get(ctx + "/car/getCityByProvinceId/" + $("#province").val(), function (data) {
                if (data.success) {
                    var result = "";
                    $.each(data.result, function (n, value) {
                        result += "<option value='" + value.cityId + "'>" + value.cityName + "</option>";
                    });
                    $("#city").html('');
                    $("#city").append(result);
                    $.get(ctx + "/car/getAreaByCityId/" + $("#city").val(), function (data) {
                        if (data.success) {
                            var result = "";
                            $.each(data.result, function (n, value) {
                                result += "<option value='" + value.areaId + "'>" + value.areaName + "</option>";
                            });
                            $("#area").html('');
                            $("#area").append(result);
                        }
                    }, "json");
                }
            }, "json");
        });

        $("#city").change(function () {
            $.get(ctx + "/car/getAreaByCityId/" + $("#city").val(), function (data) {
                if (data.success) {
                    var result = "";
                    $.each(data.result, function (n, value) {
                        result += "<option value='" + value.areaId + "'>" + value.areaName + "</option>";
                    });
                    $("#area").html('');
                    $("#area").append(result);
                }
            }, "json");
        });

    },

    //图片上传
    uploadHeadpic: function () {
        var that = this;
        var $formImglist = $('#form_imglsit');
        $('#upload_thumb').fileupload({
            url: window['ctx'] + '/common/fileupload/tmp.json',
            dataType: 'json',
            add: function (e, data) {
                //文件过滤

                //判断所选文件格式，根据后缀名
                for (var i = 0; i < data.originalFiles.length; i++) {
                    var fileItem = data.originalFiles[i];
                    var fileName = fileItem.name.toUpperCase();
                    if (fileName.lastIndexOf('JPG') == -1 && fileName.lastIndexOf('PNG') == -1 && fileName.lastIndexOf('GIF') == -1) {
                        layer.alert('所选图片格式必须为 gif/jpg/png !', {
                            icon: 0,
                            skin: 'layer-ext-moon' //该皮肤由layer.seaning.com友情扩展。关于皮肤的扩展规则，去这里查阅
                        });
                        return;
                    }
                    if (fileItem.size > 3000000) {
                        layer.alert('图片大小不能超过3兆,请压缩图片后再上传!', {
                            icon: 0,
                            skin: 'layer-ext-moon' //该皮肤由layer.seaning.com友情扩展。关于皮肤的扩展规则，去这里查阅
                        });
                        return;
                    }
                }
                if (e.isDefaultPrevented()) {
                    return;
                }
                if (data.autoUpload || (data.autoUpload !== false && $(this).fileupload('option', 'autoUpload'))) {
                    data.process().done(function () {
                        data.submit();
                    });
                }
            },
            done: function (e, data) {
                setTimeout(function () {
                    $("#headImage").attr("src", data.result.result[0].url);
                }, 10);
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }
}