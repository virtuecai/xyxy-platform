/**
 * Created by Administrator on 2016/1/15 0015.
 */
$(function(){
  var password =new Password();
})

var Password = function(){
    this.init();
}

Password.prototype={
    init:function(){
        application.initPagePosition(['管理中心','基本资料', '密码修改']);
        var falg=false;
        //表单变量
        var	 moduleBox  = '#password_form_box',
            moduleForm = '#password_form';
        //表单失去焦点
        $(moduleBox+' .text_common[isrequired]').on('blur',function(){
            validateForm($(this));
        });
        //重置表单
        $('#form_reset').on('click',function(){
            window.history.back();
        });
        //表单信息验证
        function validateForm(obj){
            var flag	  	 	= false;
            var color		 	= '#FF7800';
            var uploadBtn 	 	= obj.siblings('.upload_btn');
            var newPassword		= $('#new_password');
            var replyPassword	= $('#reply_password');
            var pwd =$("#password");
            //表单值是否为空验证
            if(!obj.val()){
                colorChange(1,obj,uploadBtn);
                layer.tips(obj.attr('null'), obj, {tips: [2, color]});
                flag = false;
            }else if(pwd.val()==newPassword.val()){
                //新密码与重复密码不一致
                colorChange(1,newPassword,uploadBtn);
                layer.tips(newPassword.attr('same'), newPassword, {tips: [2, color]});
                flag = false;
            }else if(newPassword.val()!=replyPassword.val()){
                //新密码与重复密码不一致
                colorChange(1,replyPassword,uploadBtn);
                layer.tips(replyPassword.attr('same'), replyPassword, {tips: [2, color]});
                flag = false;
            }else{
                colorChange(2,obj,uploadBtn);
                layer.closeAll('tips');
                flag = true;
            }
            return flag;
        }

        //边框颜色变化
        function colorChange(isok,obj,uploadBtn) {
            if (isok == 1) {
                if (obj.attr('type') == 'file') {
                    uploadBtn.addClass('error_border');
                } else if (!obj.hasClass('error_border')) {
                    obj.addClass('error_border');
                }
            } else {
                if (obj.attr('type') == 'file') {
                    uploadBtn.removeClass('error_border');
                } else {
                    obj.removeClass('error_border');
                }
            }
            application.initPagePosition(['管理中心', '基本资料', '密码修改']);
        }

            $("#form_submit").on('click',function(){
            var allFlag   = false;
            var checkItem = $(moduleBox+' .text_common[isrequired]');
            var reg=/^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~\s]{6,22}$/;
            var password=$("#password").val();
            checkItem.each(function(){
                var obj = $(this);
                if(validateForm(obj)){
                    allFlag = true;
                }else{
                    allFlag = false;
                    $('html,body').animate({scrollTop: 0}, 500);
                    return false;
                }
            });
            if(allFlag){
                layer.confirm('确认保存密码设置？', {
                    btn: ['确认', '再考虑下'], //按钮
                    title: '温馨提示',
                    icon: 0
                }, function () {
                    if(!falg){
                        update();
                    }
                });
            }

            function update(){
            if(reg.test(password)){
                falg=true;
                $.ajax({
                    url:ctx+'/member/password',
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{
                        password:$("#password").val(),
                        newpassword:$("#new_password").val()
                    },
                    type:'post',
                    dataType:'json',
                    beforeSend:function(){
                        $("#form_submit").attr("disabled","disabled");
                    },
                    success:function(data){
                        console.dir(data);
                        if(data.success) {
                            layer.msg("修改成功，请重新登录");
                            setTimeout(function(){location.href=ctx+'/logout/'},2000);
                            falg=false;
                        } else {
                            falg=false;
                            layer.msg(data.message);
                        }
                    },
                    complete:function(){
                        $("#form_submit").removeAttr("disabled");
                    }
                });
            }else{
                layer.meg("密码长度为6-22位，不包含空格");
                return false;
            }
            }

        });
    }
}
