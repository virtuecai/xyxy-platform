$(function () {
    application.initPagePosition(['管理中心', '我的服务', '管理服务']);
    serverManage = new ServiceManage();
});

var ServiceManage = function () {
    this.init();
};
ServiceManage.prototype = {
    $page: null, //分页控件对象
    serverType: {
        1: '上门服务',
        2: '约定地点服务',
        3: '线上服务',
        4: '邮寄地址'
    },
    init: function () {
        var that = this;

        // 服务状体切换于查询
        $('.service_status li').on('click', function () {
            var $this = $(this);
            $this.addClass('hover').siblings().removeClass('hover');
            var goodsStatus = $.trim($this.data('goods_status'));
            that.$page.pagination('setParams', {isSale: goodsStatus != '' ? Number(goodsStatus) : null});
            that.$page.pagination('remote');
        });

        //删除服务
        $(document).on('click', '#service_list .delete', function () {
            var $this = $(this);
            layer.confirm("确认删除该服务？", {
                btn: ["确认", "再考虑下"], //按钮
                title: "温馨提示",
                icon: 0
            }, function () {
                var goodsId = Number($this.parents('.service_list_box').data('item')['goodsId']);
                that.deleteGoodsAjax(goodsId, function () {
                    $this.parents('.service_list_box').fadeOut("slow", function () {
                        layer.msg("删除成功！", {icon: 1, time: 1000});
                    });
                });
            }, function () {
                layer.msg("多谢手下留情！", {icon: 6, time: 1000});
            });
        });

        //开启|关闭服务
        $(document).on('click', '.update-is-sale', function () {
            var $this = $(this);
            var item = $this.parents('.service_list_box').data('item');
            var goodsId = item['goodsId'];
            var isSale = $this.hasClass('open') && item['isSale'] == 0 ? 1 : 0;
            var layerId = layer.confirm("确认" + (isSale ? '开启' : '关闭') + "该服务？", {
                btn: ["确认", "再考虑下"], //按钮
                title: "温馨提示",
                icon: 0
            }, function () {
                that.updateGoodsIsSaleAjax(goodsId, isSale, function () {
                    item['isSale'] = isSale;
                    $this.text(isSale ? '关闭服务' : '开启服务');
                    $this.attr('class', (isSale ? 'red close' : 'green open') + ' update-is-sale');
                    that.$page.pagination('remote');
                    layer.msg((isSale ? '开启' : '关闭') + "服务成功！", {icon: 1, time: 1000}, function () {
                        layer.close(layerId);
                    });
                });
            }, function () {
                //layer.msg("多谢手下留情！",{icon:6,time:1000});
            });
        });

        that.query();

        return that;
    },
    /**
     * 分页查询
     */
    query: function () {
        var that = this;
        that.$page = $("#goods-pagination").pagination({
            debug: false,
            showInfo: false,
            showJump: true,
            showPageSizes: false,
            prevBtnText: '<i><</i> 上一页',
            nextBtnText: '下一页 <i>></i>',
            jumpBtnText: '确定',
            infoFormat: '共 {total} 条',
            noInfoText: '暂无数据',
            pageElementSort: ['$page', '$size', '$jump', '$info'],
            remote: {
                url: window['ctx'] + '/member/goods/query.json',
                params: {
                    //isSale: null
                },
                totalName: 'result.total',
                pageIndexName: 'pageNum',
                pageSizeName: 'pageSize',
                success: function (data) {
                    _.templateRender('#service_list', data.result.list, null, function ($elArray) {
                        $('.service_none')[$elArray.length == 0 ? 'show' : 'hide']();
                    });
                }
            }
        });
    },
    /**
     * 删除服务
     * @param goodsId 服务ID
     * @param callback 删除成功后回调
     */
    deleteGoodsAjax: function (goodsId, callback) {
        var that = this;
        $.ajax({
            url: window['ctx'] + '/member/service/delete.json',
            type: 'post',
            dataType: 'json',
            loading: false, //禁用 loading动画效果
            data: {
                goodsId: goodsId
            },
        }).done(function (data) {
            if (!data.success) {
                layer.msg(data.message, {icon: 2});
            } else {
                callback && callback(data.result);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            layer.msg('网络繁忙, 删除服务失败, 请稍后再试!', {icon: 2});
        })
    },
    /**
     * 开启|关闭服务
     * @param goodsId 服务ID
     * @param isSale 0关闭,1开启
     * @param callback 开启|关闭成功后回调
     */
    updateGoodsIsSaleAjax: function (goodsId, isSale, callback) {
        var that = this;
        $.ajax({
            url: window['ctx'] + '/member/service/is_sale.json',
            type: 'post',
            dataType: 'json',
            loading: false, //禁用 loading动画效果
            data: {
                goodsId: goodsId,
                isSale: isSale
            },
        }).done(function (data) {
            if (!data.success) {
                layer.msg(data.message, {icon: 2});
            } else {
                callback && callback(data.result);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            layer.msg('网络繁忙, 删除服务失败, 请稍后再试!', {icon: 2});
        })
    }
};