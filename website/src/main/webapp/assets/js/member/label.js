/**
 * Created by Administrator on 2016/1/19 0019.
 */
$(function(){
    var label = new Label();
})

var Label = function (){
    this.init();
}

Label.prototype={

    init:function() {
        application.initPagePosition(['管理中心','基本资料', '个人标签']);
        var color		= '#FF7800';
        //初始化
        addListenInput($('#label_own_box #label'));
        labelNums();

        //删除个人标签
        $('body').on('click','#label_own .icon_delete',function(){
            var objClick = $(this);
            var objId    = objClick.siblings('.specific_id').attr('attrid');
            alert(objId);
            layer.confirm('确认删除该标签？', {
                btn  :['确认','再考虑下'], //按钮
                title:'温馨提示',
                icon :0
            },function(){
                $.ajax({
                    url:ctx+'/member/deletespecifitag',
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{
                        id:$("#mysprcifictag"+objId).val(),
                        memberSpecificTagId:$("#spcMemberId").val()
                    },
                    type:'post',
                    dataType:'json',
                    success:function(data){
                        console.dir(data);
                        if(data.success) {
                            layer.msg('删除成功！',{icon:1,time:1000},function(){
                                objClick.parent('span').remove();
                                labelNums();
                                //更多操作
                            });
                        } else {
                            layer.msg(data.message);
                        }
                    },
                });
            },function(){
                //layer.msg('多谢手下留情！',{icon:6,time:1000});
            });
        });

        //删除职业标签
        $('body').on('click','#label_job .icon_delete',function(){
            var objClick = $(this);
            var objId    = objClick.siblings('.profession').attr('attrid');
            alert(objId);
            layer.confirm('确认删除该标签？', {
                btn  :['确认','再考虑下'], //按钮
                title:'温馨提示',
                icon :0
            },function(){
                $.ajax({
                    url:ctx+'/member/deleteprofession',
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{
                        id:$("#myprofession"+objId).val(),
                        professionMemberId:$("#proMemberId").val()
                    },
                    type:'post',
                    dataType:'json',
                    success:function(data){
                        console.dir(data);
                        if(data.success) {
                            layer.msg('删除成功！',{icon:1,time:1000},function(){
                                objClick.parent('span').remove();
                                labelNums();
                                //更多操作
                            });
                        } else {
                            layer.msg(data.message);
                        }
                    },
                });

            },function(){
                //layer.msg('多谢手下留情！',{icon:6,time:1000});
            });
        });

        //添加职业标签
        $('body').on('click','#label_job .add',function(){
            layer.open({
                type: 1,
                title: '新增职业标签',
                shadeClose: true,
                shade: 0.5,
                closeBtn: 1,
                maxmin: false, //开启最大化最小化按钮
                area: ['630px', '200px'],
                content: $('#label_add_box')
            });
        });

        //添加个人标签
        $('body').on('click','#label_own .add',function(){
            layer.open({
                type: 1,
                title: '新增个人标签',
                shadeClose: true,
                shade: 0.5,
                closeBtn: 1,
                maxmin: false, //开启最大化最小化按钮
                area: ['550px', '450px'],
                content: $('#label_own_box')
            });
        });

        //输入标签--监听输入框变化
        $("body").on('input propertychange','#label_own_box #label',function(){
            addListenInput($(this));
        });

        //个人标签弹窗添加个人标签
        /*$("body").on('click','#label_own_box #label_btn',function(){
            var obj 	  = $(this),
                label 	  = obj.siblings('span').find('#label').val(),
                labelHtml = '';
            if(labelNums(label)){
                if(label){
                    labelHtml = '<span class="clearfix"><i class="icon_member icon_header"></i><i class="icon_footer">'+label+'</i><i class="icon_delete">x</i></span>'
                }else{
                    layer.tips(obj.attr('null'), $(this), {tips: [2, color]});
                }
                $('#label_own_box .label_list').append(labelHtml);
                labelNums(label);
            }
        });*/

        //新增标签
        function addLabel(thisObj){
            var labelHtml = '';
            labelHtml = '<span class="clearfix"><i class="icon_member icon_header"></i><i class="icon_footer">'+label+'</i><i class="icon_delete">x</i></span>'
            thisObj.append(labelHtml);
        }


        //提交职业标签
        $('body').on('click','#label_job .form_submit',function(){
            $.ajax({
                url:ctx+'/member/addprofession',
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                data:{
                    tagName:$("#protag").val(),
                },
                type:'post',
                dataType:'json',
                beforeSend:function(){
                    $("#label_job .form_submit").attr("disabled","disabled");
                },
                success:function(data){
                    console.dir(data);
                    if(data.success) {
                        layer.msg("添加成功");
                        setTimeout(function(){location.href=ctx+'/member/label'},3000);
                    } else {
                        layer.msg(data.message);
                    }
                },
                complete:function(){
                    $("#label_job .form_submit").removeAttr("disabled");
                }
            });
        });

        //提交个人标签
        $('body').on('click','#label_own .form_submit',function(){
            var tagName=$("#label").val();
            if(tagName==""){
                layer.msg("标签不能为空");
                return false;
            }

            $.ajax({
                url:ctx+'/member/addspecifi',
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                data:{
                    tagName:tagName
                },
                type:'post',
                dataType:'json',
                beforeSend:function(){
                    $("#label_job .form_submit").attr("disabled","disabled");
                },
                success:function(data){
                    if(data.success) {
                        layer.msg("添加成功");
                        setTimeout(function(){location.href=ctx+'/member/label'},3000);
                    } else {
                        layer.msg(data.message);
                    }
                },
                complete:function(){
                    $("#label_job .form_submit").removeAttr("disabled");
                }
            });
        });

        //重置表单
        $('body').on('click','#container_label .form_reset',function(){
            layer.closeAll();
        });

//计算标签字符函数
        function labelNums(label){
            var	flag       = true,
                nums       = $( '#label_own .label_list span').size(),
                maxNums    = $( '#label_own_box .total_num').text(),
                allowNums  = $( '#label_own_box .allow_num'),
                maxLength  = $( '#label_own_box .max_length').text();

            if(nums>=maxNums){
                allowNums.text(0);
                layer.msg('最多添加'+maxNums+'个标签',{icon: 2});
                flag = false;
            }else if(label && label.length>maxLength){
                layer.msg('单个标签只能添加'+maxLength+'个字符',{icon: 2});
                flag = false;
            }else{
                allowNums.text(maxNums-nums);
            }
            return flag;
        }

        //监听input事件
        function addListenInput(obj){
            var valComment = obj.val();
            if(valComment == ''){//输入框值是否为空
                obj.parent('span').siblings('.label_btn').removeClass('label_can');
            }else{
                obj.parent('span').siblings('.label_btn').addClass('label_can');
            }
        }
    }

}