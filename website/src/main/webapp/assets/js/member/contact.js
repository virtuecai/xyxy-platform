/**
 * Created by Administrator on 2016/1/15 0015.
 */
$(function(){
  var contact =new Contact();
})

var Contact = function(){
    this.init();
}

Contact.prototype={
    init:function() {

        application.initPagePosition(['管理中心','基本资料', '联系方式']);

        $(function(){
            //表单变量
            var	 contactBox  = '#contact_form_box',
                contactForm = '#contact_form',
                mobileBox   = '#mobile_form_box',
                mobileForm  = '#mobile_form';

            //提交表单--联系方式
            $(contactBox+' .form_submit').on('click',function(){
                var reg=/^[0-9]{6,10}$/;
                var qq=$("#qq_number").val();
                if(!reg.test(qq)){
                    layer.msg("请输入正确的QQ号");
                    return false;
                }
                $.ajax({
                    url:ctx+'/member/contact',
                    loading:true,
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{
                        qq:qq
                    },
                    type:'post',
                    dataType:'json',
                    beforeSend:function(){
                        $("#qq_form_btn").attr("disabled","disabled");
                    },
                    success:function(data){
                        console.dir(data);
                        if(data.success) {
                            layer.msg("修改成功");
                            setTimeout(function(){location.href=ctx+'/member/contact/'},3000);
                        } else {
                            layer.msg(data.result);
                        }
                    },
                    complete:function(){
                        $("#form_submit").removeAttr("disabled");
                    }
                });
            });

            //修改手机号码触发器--联系方式
            $(contactBox+' .form_edit').on('click',function(){
                layer.open({
                    type: 1,
                    title: '修改手机号码',
                    shadeClose: true,
                    shade: 0.5,
                    closeBtn: 1,
                    maxmin: false, //开启最大化最小化按钮
                    area: ['350px', '220px'],
                    content: $(mobileBox),
                    end:function(){
                        layer.closeAll();
                    }
                });
            });

            //重置表单--联系方式
            $(contactBox+' .form_reset').on('click',function(){
                window.history.back();
            });

            //提交表单--手机号码
            $('body').on('click',mobileBox+' .form_submit',function(){
                var allFlag   = false;
                var checkItem = $(mobileBox+' .text_common[isrequired]');
                checkItem.each(function(){
                    var obj = $(this);
                    if(validateForm(obj)){
                        allFlag = true;
                    }else{
                        allFlag = false;
                        return false;
                    }
                });
                if(allFlag) {

                    $.ajax({
                        url: ctx + '/member/contact',
                        loading: true,
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        data: {
                            mobile: $("#mobile").val()
                        },
                        type: 'post',
                        dataType: 'json',
                        beforeSend: function () {
                            $("#mobile_submit_btn").attr("disabled", "disabled");
                        },
                        success: function (data) {
                            console.dir(data);
                            if (data.success) {
                                layer.msg("修改成功");
                                setTimeout(function () {
                                    location.href = ctx + '/member/contact/'
                                }, 3000);
                            } else {
                                layer.msg(data.result);
                            }
                        },
                        complete: function () {
                            $("#mobile_submit_btn").removeAttr("disabled");
                        }
                    });
                }
            });

            //表单失去焦点--手机号码
            $('body').on('blur',mobileBox+' .text_common[isrequired]',function(){
                validateForm($(this));
            });

            //重置表单--手机号码
            $('body').on('click',mobileBox+' .form_reset',function(){
                layer.closeAll();
            });

        });

        //表单信息验证
        function validateForm(obj){
            var flag	  	= false;
            var color		= '#FF7800';
            //表单值是否为空验证
            var reg=/^[0-9]{11}$/;
            if(!reg.test(obj)){
                layer.tips(obj.attr('error'), obj, {tips: [2, color]});
                flag = false;
            }
            if(!obj.val()){
                if(!obj.hasClass('error_border')){
                    obj.addClass('error_border');
                }
                layer.tips(obj.attr('null'), obj, {tips: [2, color]});
                flag = false;
            }else{
                obj.removeClass('error_border');
                layer.closeAll('tips');
                flag = true;
            }
            return flag;
        }

    }
}
