/**
 * js 代码编写模版
 */

$(function () {
    var pagination = new Pagination();
});

var Pagination = function () {
    this.init();
};
Pagination.prototype = {
    $categoryName: $('[name=categoryName]'),
    $dataTable: $('table').first(),
    $searchBtn: $('#searchBtn'),
    dataItemTemplate: $('#data_item_template').html(),
    $page: null,
    init: function () {
        var that = this;

        /**
         * 加载分页插件
         */
        that.initPagination();

        /**
         * 搜索点击时间
         */
        $('#searchBtn').on('click', function () {
            that.$page.pagination('setParams', {
                categoryName: $.trim(that.$categoryName.val())
            });
            that.$page.pagination('remote');
        });

        return that;
    },
    initPagination: function () {
        var that = this;
        that.$page = $(".mricode-pagination").pagination({
            pageSize: 2, // 指定 一页多少天数据
            debug: false,
            showInfo: false,
            showJump: true,
            showPageSizes: false,
            prevBtnText: '<i><</i> 上一页',
            nextBtnText: '下一页 <i>></i>',
            jumpBtnText: '确定',
            infoFormat: '共 {total} 条',
            noInfoText: '暂无数据',
            pageElementSort: ['$page', '$size', '$jump', '$info'],
            remote: {
                url: window['ctx'] + '/showcase/pagination/data.json',
                params: {
                    categoryName: $.trim(that.$categoryName.val())
                },
                totalName: 'result.total',
                pageIndexName: 'pageNum',
                pageSizeName: 'pageSize',
                success: function (data) {
                    if(data.success) {
                        _.templateRender('table tbody', data.result.list);
                    } else {
                        layer.msg('请求失败!');
                    }
                }
            }
        });
    }
};