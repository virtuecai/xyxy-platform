/**
 * Created by Administrator on 2016/1/6 0006.
 */
$(function(){
    var login =new Login();
})

var Login = function(){
    this.init();
}

Login.prototype={
    init : function (){
        //登录
        $("#form_submit").on('click',function (){
            loginon();
        });
        var boo = true;
        document.onkeydown = function(e) {
            if ((e.keyCode || e.which) == 13 && boo) {
                loginon();
            }
        };

        //登录具体方法
        function loginon(){
            var realname = $('#realname'),
                pwd      = $('#password'),
                yzm      = $('#yzm'),
                color    = '#FF7800';
            if(pwd.val().length<6){
                layer.tips(pwd.attr('error'), pwd, {tips: [1, color]});
                return false;
            }
            if(realname.val().length<6){
                layer.tips(realname.attr('error'), realname, {tips: [1, color]});
                return false;
            }
            if(!realname.val()){
                layer.tips(realname.attr('null'), realname, {tips: [1, color]});
                return ;
            }
            if(!pwd.val()){
                layer.tips(pwd.attr('null'), pwd, {tips: [1, color]});
                return ;
            }
            if(!yzm.val()){
                layer.tips(yzm.attr('null'), yzm, {tips: [1, color]});
                return ;
            }

            $.ajax({
                url:ctx+'/loginnovalidata',
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                data:{
                    moblie:realname.val(),
                    password:pwd.val(),
                    jcaptcha:yzm.val()
                },
                type:'post',
                dataType:'json',
                beforeSend:function(){
                    boo=false;
                    $("#form_submit").attr("disabled","disabled");
                },
                success:function(data){
                    if(data.success) {
                        layer.msg("登陆成功，欢迎回来");
                        location.href=ctx+data.result.redirectUrl;
                    } else {
                        layer.msg(data.result);
                        $("#code").attr("src",ctx+"/jcaptcha.jpg?d="+new Date().getTime());
                        boo=true;
                    }
                },
                complete:function(){
                    $("#form_submit").removeAttr("disabled");
                }
            });
        }

    }
}