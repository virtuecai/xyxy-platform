/**
 * Created by Administrator on 2016/1/6 0006.
 */
$(function () {
    var register = new Register();
});

var Register = function () {
    this.init();
};
Register.prototype = {


    init: function () {
        //获取验证码
        $("#getcode").click(function () {
            var InterValObj; //timer变量，控制时间
            var count = 60; //间隔函数，1秒执行
            var curCount;//当前剩余秒数
            var str = "";
            var number = $("#realname").val();
            if (number == null || number.trim() == "") {
                return;
            }
            var rel = /^(13[0-9]|14[0-9]|15[0-9]|18[0-9]|17[0-9])\d{8}$/;
            if(!rel.test(number)){
                layer.msg("请输入正确的手机号码");
                return;
            }
            $.ajax({
                url: ctx + '/biewanlecode',
                data: {
                    moblie: $("#realname").val()
                },
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        console.log(data);
                        $("#getcode").hide();
                    }else{
                        layer.msg(data.result);
                    }
                }
            });
            //设置button效果，开始计时
            curCount = count;
            $("#getcode").hide();
            $("#codestr").show();
            $("#codetime").text(curCount);
            InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
            //timer处理函数
            function SetRemainTime() {
                if (curCount == 0) {
                    window.clearInterval(InterValObj);//停止计时器
                    $("#getcode").html("重新发送验证码");
                    $("#getcode").show();
                    $("#codestr").hide();
                }
                else {
                    curCount--;
                    $("#codetime").text(curCount);
                }
            }
        });

        //提交注册
        $("#form_submit").on('click', function () {
           registeron();
        });

        document.onkeydown = function(e) {
            if ((e.keyCode || e.which) == 13) {
                registeron();
            }
        };

        //注册具体实现方法
        function registeron(){
            var realname = $('#realname'),
                pwd = $('#password'),
                rPwd = $('#r_password'),
                yzm = $('#yzm'),
                color = "#FF7800";
            var rel = /^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/;
            if (!realname.val()) {
                layer.tips(realname.attr('null'), realname, {tips: [1, color]});
                return false;
            }
            if (!rel.test(realname.val())) {
                layer.tips(realname.attr('error'), realname, {tips: [1, color]});
                return false;
            }
            if (!pwd.val()) {
                layer.tips(pwd.attr('null'), pwd, {tips: [1, color]});
                return false;
            }
            if (pwd.val().length < 6) {
                layer.tips(pwd.attr('error'), pwd, {tips: [1, color]});
                return false;
            }
            if (!rPwd.val()) {
                layer.tips(rPwd.attr('null'), rPwd, {tips: [1, color]});
                return false;
            }
            if (pwd.val() != rPwd.val()) {
                layer.tips(rPwd.attr('same'), rPwd, {tips: [1, color]});
                return false;
            }
            if (!yzm.val()) {
                layer.tips(yzm.attr('null'), yzm, {tips: [1, color]});
                return false;
            }

            $.ajax({
                url: ctx + '/register',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                data: {
                    mobile: realname.val(),
                    password: pwd.val(),
                    vcode: yzm.val()
                },
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    $("#form_submit").attr("disabled", "disabled");
                },
                success: function (data) {
                    if (data.success) {
                        layer.msg("注册成功，请完善个人资料~");
                        setTimeout(function(){location.href=ctx+"/member/"},2000);
                    } else {
                        layer.msg(data.result);
                    }
                },
                complete: function () {
                    $("#form_submit").removeAttr("disabled");
                }
            });
        }


    },
    formValidate: function () {
        var that = this;
        return true;
    }

};

