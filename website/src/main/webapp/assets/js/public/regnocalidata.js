/**
 * Created by Administrator on 2016/1/6 0006.
 */
$(function () {
    var register = new Register();
});

var Register = function () {
    this.init();
};
Register.prototype = {


    init: function () {

        //提交注册
        $("#form_submit").on('click', function () {
           registeron();
        });

        document.onkeydown = function(e) {
            if ((e.keyCode || e.which) == 13) {
                registeron();
            }
        };

        //注册具体实现方法
        function registeron(){
            var realname = $('#realname'),
                pwd = $('#password'),
                rPwd = $('#r_password'),
                color = "#FF7800";
            if (!realname.val()) {
                layer.tips(realname.attr('null'), realname, {tips: [1, color]});
                return false;
            }
            if (realname.val().length<6) {
                layer.tips(realname.attr('error'), realname, {tips: [1, color]});
                return false;
            }
            if (!pwd.val()) {
                layer.tips(pwd.attr('null'), pwd, {tips: [1, color]});
                return false;
            }
            if (pwd.val().length < 6) {
                layer.tips(pwd.attr('error'), pwd, {tips: [1, color]});
                return false;
            }
            if (!rPwd.val()) {
                layer.tips(rPwd.attr('null'), rPwd, {tips: [1, color]});
                return false;
            }
            if (pwd.val() != rPwd.val()) {
                layer.tips(rPwd.attr('same'), rPwd, {tips: [1, color]});
                return false;
            }
            $.ajax({
                url: ctx + '/regnovalidata',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                data: {
                    mobile: realname.val(),
                    password: pwd.val(),
                },
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    $("#form_submit").attr("disabled", "disabled");
                },
                success: function (data) {
                    if (data.success) {
                        layer.msg("注册成功，2秒之后跳转到登录页面");
                        setTimeout(function(){location.href=ctx+"/loginnovalidata"},2000);
                    } else {
                        layer.msg(data);
                    }
                },
                complete: function () {
                    $("#form_submit").removeAttr("disabled");
                }
            });
        }


    },
    formValidate: function () {
        var that = this;
        return true;
    }

};

