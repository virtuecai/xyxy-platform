
var $image_resouce = $('.image-container > img');  //裁切区域图片元素



$(document).ready(function() {
	
	var cropper_option = {
    		 aspectRatio: 1/1,
		     preview: '.img-preview'
	}

	$("#head-image-container").bind('click', function() {
		    var _this = this; 
			layer.open({
			    type: 1,
			    title: false,
			    closeBtn: true,
			    area: ['1200px', '600px'],
			    skin: 'layui-layer-nobg', //没有背景色
			    shadeClose: false,
			    content: $('#cropper-layer'),
			    success: function(layero, index){
			       var  $org_headImge =  $(_this).attr("src");  //获取现有头像图片 
			       $image_resouce.attr("src",$org_headImge); //将图片赋值给切片组件图片容器
	    		   $image_resouce.cropper(cropper_option);  //初始化插件
	    		   var $inputImage = $('#inputImage'),
	   				   URL = window.URL || window.webkitURL,
        			   blobURL;
	      	       if (URL) {
				      $inputImage.change(function () {
				        var files = this.files,
				            file;
				
				        if (files && files.length) {
				          file = files[0];
				          if (/^image\/\w+$/.test(file.type)) {
				            blobURL = URL.createObjectURL(file);
				            $image_resouce.one('built.cropper', function () {
				              URL.revokeObjectURL(blobURL); // Revoke when load complete
				            }).cropper('reset').cropper('replace', blobURL);
				            $inputImage.val('');
				          } else {
				            showMessage('Please choose an image file.');
				          }
				        }
				      });
				    } else {
				      $inputImage.parent().remove();
				    }
				},
			   cancel:function(index){
			   	  $image_resouce.cropper('destroy');
			   }
			});
      });


});