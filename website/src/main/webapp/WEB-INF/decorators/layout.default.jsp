<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title><sitemesh:write property='title'/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta id="wxKeywords" name="Keywords" content="星语星愿"/>
    <meta id="wxDescription" name="Description" content="星语星愿"/>
    <meta name="baidu-site-verification" content="Iq4H8yD1Pc" />

    <link rel="shortcut icon" href="${ctx}/assets/images/icon.png" />

    <!--全局resetCss样式开始-->
    <link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css"/>
    <!--全局resetCss样式结束-->

    <!--公用commonCss样式开始-->
    <link href="${ctx}/assets/css/common/common.css" rel="stylesheet" type="text/css"/>
    <!--公用commonCss样式结束-->

    <!--公用面包屑positionCss样式开始-->
    <link href="${ctx}/assets/css/common/position.css" rel="stylesheet" type="text/css"/>
    <!--公用面包屑positionCss样式结束-->

    <!--主题Css样式开始-->
    <link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css"/>
    <!--主题Css样式结束-->

    <!--公用分页pageCss样式开始-->
    <link href="${ctx}/assets/css/common/page.css" rel="stylesheet" type="text/css"/>
    <!--公用分页pageCss样式j结束-->

    <!-- 分页插件样式(待修改)-->
    <link href="${ctx}/assets/plugins/Mricode.Pagination/mricode.pagination.css" rel="stylesheet" type="text/css"/>

    <!-- 全局公共样式 -->
    <link href="${ctx}/assets/css/common/application.css" rel="stylesheet" type="text/css"/>

    <sitemesh:write property='head'/>
</head>
<body>

<jsp:include page="/WEB-INF/views/common/header.jsp"/>

<sitemesh:write property='body'/>

<jsp:include page="/WEB-INF/views/common/footer.jsp"/>

<script>
    window['ctx'] = '${ctx}';
    window['serverTime'] = <%=System.currentTimeMillis()%>;
</script>

<!--Jquery库开始-->
<script type="text/javascript" src="${ctx}/assets/js/lib/jquery-1.8.3.min.js"></script>
<!--Jquery库结束-->

<!--javascript 工具集-->
<script type="text/javascript" src="${ctx}/assets/plugins/underscore/underscore-min.js"></script>

<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->

<!-- 时间日期操作 -->
<script type="text/javascript" src="${ctx}/assets/plugins/moment/moment.min.js"></script>

<!-- 分页插件 -->
<script type="text/javascript" src="${ctx}/assets/plugins/Mricode.Pagination/mricode.pagination.js"></script>

<!-- 全局公共js -->
<script type="text/javascript" src="${ctx}/assets/js/application.js"></script>

<!--公用cmmonJS开始-->
<script type="text/javascript" src="${ctx}/assets/js/common/common.min.js"></script>
<!--公用cmmonJS结束-->

<sitemesh:write property='javascript-list'/>

<script>
    /**
     * 导航菜单高亮
     */
    $(function () {
        var moduleName = $('.position a').first().text();
        if($.trim(moduleName) != '') {
            $('.nav_common.nav_right span.title').filter(function () {
                var $this = $(this);
                return $.trim($this.text()) == $.trim(moduleName);
            }).addClass('orange');
        } else {
            $('.nav_common.nav_right span.title').first().addClass('orange');
        }
    });
</script>

</body>
</html>