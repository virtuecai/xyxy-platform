<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>核对订单</title>
	<!--全局resetCss样式开始-->
	<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
	<!--全局resetCss样式结束-->

	<!--订单公用commonCss样式开始-->
	<link href="${ctx}/assets/css/order/common.css" rel="stylesheet" type="text/css" />
	<!--订单公用commonCss样式结束-->

	<!--公用formCss样式开始-->
	<link href="${ctx}/assets/css/common/form.css" rel="stylesheet" type="text/css" />
	<!--公用formCss样式结束-->

	<!--页面Css样式开始-->
	<link href="${ctx}/assets/css/order/confirm.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

	<!--主题themeCss样式开始-->
	<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
	<!--主题themeCss样式结束-->
</head>
<body>
<input type="hidden" id="parm_ids" value="${ids}"/>
<input type="hidden" id="serverTimeIdx" value="${serverTimeIdx}"/>
<input type="hidden" id="serverDate" value="${serverDate}"/>
<input type="hidden" id="addressId1" value="${addressId1}"/>

<div class="space_hx">&nbsp;</div>

<!--头部中间内容开始-->
<div class="header_content clearfix">

	<!--头部logo开始-->
	<div class="header_logo">
		<a href="../${ctx}" title>
			<img src="${ctx}/assets/images/logo.jpg" alt="" />
		</a>
	</div>
	<!--头部logo结束-->

	<!--页面提示开始-->
	<div class="header_tip">核对订单</div>
	<!--页面提示结束-->

	<!--订单流程开始-->
	<div class="header_process clearfix">
		<!--注意：hover为当前步奏，passed为已完成步骤-->
		<!--购物车入口开始-->
		<ul class="shopcar_process clearfix">
			<li class="hover">
				<span class="num">1</span>
				<span class="title">填写核对订单信息</span>
			</li>
			<li>
				<span class="num">2</span>
				<span class="title">确认支付</span>
			</li>
			<li>
				<span class="num">3</span>
				<span class="title">支付结果</span>
			</li>
		</ul>
		<!--购物车入口结束-->
		<!--立即购买入口开始-->
		<!--<ul class="buy_process clearfix">
            <li class="hover">
                <span class="num">1</span>
                <span class="title">填写核对订单信息</span>
            </li>
            <li>
                <span class="num">2</span>
                <span class="title">确认支付</span>
            </li>
            <li>
                <span class="num">3</span>
                <span class="title">支付结果</span>
            </li>
        </ul>-->
		<!--立即购买入口结束-->
	</div>
	<!--订单流程结束-->
</div>
<!--头部中间内容结束-->


<!--身体开始-->
<div class="layer clearfix" id="layer">

	<!--核对订单表单开始-->
	<form class="confirm_form" name="confirm_form" id="confirm_form" action="" method="post">

		<!--订单地址开始-->
		<div class="confirm_common confirm_address">
			<!--约见地址开始-->
			<div class="common_title clearfix">
				<span class="left">约见地址</span>
				<span class="right" id="address_add">新增地址</span>
			</div>
			<!--约见地址结束-->

			<!--上门地址开始-->
			<!--<div class="common_title clearfix">
				<span class="left">上门地址</span>
				<span class="right" id="address_add">新增地址</span>
			</div>-->
			<!--上门地址结束-->

			<!--邮寄地址开始-->
			<!--<div class="common_title clearfix">
				<span class="left">邮寄地址</span>
				<span class="right" id="address_add">新增地址</span>
			</div>-->
			<!--邮寄地址结束-->

			<!--线上交易开始-->
			<!--<div class="common_title clearfix">
				<span class="left">线上交易</span>
			</div>-->
			<!--线上交易结束-->

			<!--约见、上门、邮寄方式地址列表开始-->
				<div class="address_list" id="address_list">
					<ul>
			<c:forEach  items="${addresslst}" var="address">
						<!--默认‘默认地址’高亮hover样式-->
						<c:choose>
						<c:when test="${address.isDefault==1}">
						<li class="clearfix hover" attrid="${address.addressId}">
							</c:when>
							<c:otherwise>
						<li class="clearfix" attrid="${address.addressId}">
							</c:otherwise>
							</c:choose>
							<span class="province">
								<em>${address.provinceName}</em> <em>${address.cityName}</em>
								<i class="icon_order icon_hover"></i>
							</span>
							<span class="name">${address.consignee}</span>
							<span class="detail">${address.districtName}${address.address}</span>
							<span class="tel">${address.mobile}</span>
							<span class="deal delete" attrid="${address.addressId}">删除</span>
							<span class="deal edit" attrid="${address.addressId}">编辑</span>
							<span class="deal default" attrid="${address.addressId}">默认地址</span>
						</li>
			</c:forEach>
					</ul>
				</div>
			<div class="address_hide" id="address_hide">
				<span>更多地址</span>
				<i class="icon_order icon_show"></i>
			</div>
			<!--约见、上门、邮寄方式地址列表结束-->

			<!--线上交易地址开始-->
			<!--<div class="address_list" id="address_list">
				下单完成后与卖家协商服务地址或事宜
			</div>-->
			<!--线上交易地址结束-->

			<!--地址栏为空时开始-->
			<!--<div class="address_list" id="address_list">
				您的地址为空，请先添加地址！
			</div>-->
			<!--地址栏为空时结束-->
		</div>
		<!--订单地址结束-->

		<!--订购清单开始-->
		<div class="confirm_common confirm_product">
			<div class="common_title clearfix">
				<span class="left">预定订单</span>
				<!--直接购买入口时没有’修改购物车‘
				<a href="" class="right" >修改心愿车</a>-->
			</div>
			<div class="product_list">

				<!--商品清单遍历开始-->
				<div class="product_list_box">
						<!--星店信息开始-->
						<div class="shop_common shop_info">
							<ul class="clearfix">
								<li class="name clearfix">
									<a href="" title="">${shopper}</a>
									<a href="" title="" class="icon clearfix">
										<i class="icon_order icon_zheng"></i>
										<i class="icon_order icon_v_member"></i>
									</a>
								</li>
								<li class="desc">服务名称</li>
								<li class="time">服务时间</li>
								<li class="num">数量</li>
								<li class="money">金额 ( 元 )</li>
							</ul>
						</div>
						<!--星店信息结束-->

						<!--星店服务列表开始-->

						<div class="shop_common shop_service" id="shop_service">
							<!--服务遍历开始-->
							<ul class="clearfix">
								<li class="name">
									<a href="" title="">
										<img src="${carDetail.img}" alt=""  />
									</a>
								</li>
								<li class="desc">
									<span class="title">${keyword}</span>
									<span class="intro">${goodsDesc}</span>
								</li>
								<li class="time">${serviceTime} 时</li>
								<li class="num">x <em>${num}</em></li>
								<li class="money">￥${price}</li>
								<!--给卖家留言开始-->
								<li class="message clearfix">
									<span>给卖家留言:</span>
									<input type="text" name="message" class="message" value="" placeholder="还有什么要求跟卖家说？" />
								</li>
								<!--给卖家留言结束-->
							</ul>
							<!--服务遍历结束-->

						</div>
					<!--星店服务列表结束-->

				</div>
				<!--商品清单遍历结束-->
				<!--商品清单遍历结束-->

			</div>
		</div>
		<!--订购清单结束-->

		<!--核对订单工具开始-->
		<div class="confirm_tool clearfix">
			<div class="confirm_all">
				总订单金额：<span>￥</span>
				<span class="total_price" id="total_price">${price}</span>
			</div>
			<div class="confirm_settle">
				<span class="btn submit_order" id="submit_order">提交订单</span>
			</div>
		</div>
		<!--核对订单工具结束-->

	</form>
	<!--核对订单表单结束-->

	<!--添加地址表单开始-->
	<form class="module_form address_add_form" name="address_add_form" id="address_add_form" method="post" >
		<div class="module_form_box confirm_address_add" id="confirm_address_add">
			<ul>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">约见人姓名</span>
					<span class="form_text">
						<input type="text" class="text_common text_input" id="realname" name="consignee" value="" placeholder="请填写约见人姓名" null="约见人姓名不能为空" />
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">所在地区</span>
					<span class="form_text">
						<select name="province" id="provinceId" class="text_common text_select" error="请选择省份">
							<c:if test="${!empty provincelist}">
								<c:forEach items="${provincelist}" var="province">
									<option value="${province.provinId}">${province.provinceName}</option>
								</c:forEach>
							</c:if>
						</select>
						<select name="city" id="cityId" class="text_common text_select" error="请选择所属市">
							<option value="0">选择城市</option>
						</select>
						<select name="district" id="areaId" class="text_common text_select" error="请选择所属区">
							<option value="0">请选择所属区</option>
						</select>
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">详细地址</span>
					<span class="form_text">
						<textarea class="text_common text_textarea" id="detail" name="address" value="" placeholder="请填写约见的详细地址" null="详细地址不能为空"></textarea>
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">联系方式</span>
					<span class="form_text">
						<input type="text" class="text_common text_input" id="mobile" name="mobile" value="" placeholder="请填写约见的联系方式" null="联系方式不能为空" />
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">&nbsp;</span>
					<span class="form_title">&nbsp;</span>
					<span class="form_text">
						默认地址:&nbsp;&nbsp;<input type="radio" class="text_radio" name="isdefault" id="isdefault" value="1" />是&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" class="text_radio" name="isdefault" id="isdefault" value="0" /> 否
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">&nbsp;</span>
					<span class="form_title">&nbsp;</span>
					<span class="form_btn">
						<a class="btn btn_common form_submit">保存</a>
						<a class="btn btn_common form_reset">取消</a>
					</span>
				</li>
			</ul>
		</div>
	</form>
	<!--添加地址表单结束-->

	<!--修改地址表单开始-->
	<form class="module_form address_edit_form" name="address_edit_form" id="address_edit_form" method="post" >
		<div class="module_form_box confirm_address_edit" id="confirm_address_edit">
		<input type="hidden" name="parm_address" id="parm_address"/>
			<ul>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">约见人姓名</span>
					<span class="form_text">
						<input type="text" class="text_common text_input" id="consignee" name="consignee" value="" placeholder="请填写约见人姓名" null="约见人姓名不能为空" />
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">所在地区</span>
					<span class="form_text">
						<select name="province" id="provinceId_update" class="text_common text_select" error="请选择省份">
							<c:if test="${!empty provincelist}">
								<c:forEach items="${provincelist}" var="province">
									<option value="${province.provinId}">${province.provinceName}</option>
								</c:forEach>
							</c:if>
						</select>
						<select name="city" id="cityId_update" class="text_common text_select" error="请选择所属市">
							<option value="0">选择城市</option>
						</select>
						<select name="district" id="areaId_update" class="text_common text_select" error="请选择所属区">
							<option value="0">请选择所属区</option>
						</select>
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">详细地址</span>
					<span class="form_text">
						<textarea class="text_common text_textarea" id="address" name="address" value="" placeholder="请填写约见的详细地址" null="详细地址不能为空"></textarea>
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">*</span>
					<span class="form_title">联系方式</span>
					<span class="form_text">
						<input type="text" class="text_common text_input" id="mobile" name="mobile" value="" placeholder="请填写约见的联系方式" null="联系方式不能为空" />
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">&nbsp;</span>
					<span class="form_title">&nbsp;</span>
					<span class="form_text">
						默认地址:&nbsp;&nbsp;<input type="radio" class="text_radio" name="isdefault" id="isdefault" value="1" />是&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" class="text_radio" name="isdefault" id="isdefault" value="0" /> 否
					</span>
				</li>
				<li class="clearfix">
					<span class="form_point red">&nbsp;</span>
					<span class="form_title">&nbsp;</span>
					<span class="form_btn">
						<a class="btn btn_common form_submit">保存</a>
						<a class="btn btn_common form_reset">取消</a>
					</span>
				</li>
			</ul>
		</div>
	</form>
	<!--修改地址表单结束-->

</div>
<!--身体结束-->

<javascript-list>
<script type="text/javascript">
	$(function(){
		$.get("car/getCityByProvinceId/"+$("#provinceId").val(),function(data){
			if(data.success){
				var result = "";
				$.each(data.result,function(n,value){
					result +="<option value='"+value.cityId+"'>"+value.cityName+"</option>";
				});
				$("#cityId").html('');
				$("#cityId").append(result);
				$.get("car/getAreaByCityId/"+$("#cityId").val(),function(data){
					if(data.success){
						var result = "";
						$.each(data.result,function(n,value){
							result +="<option value='"+value.areaId+"'>"+value.areaName+"</option>";
						});
						$("#areaId").html('');
						$("#areaId").append(result);
					}
				},"json");
			}
		},"json");

		

		//初始化计算订单总价
		//calcTotalFee();

		//初始化订单地址数目
		addressNums();

		//地址列表收缩
		$('#address_hide').on('click',function(){
			var obj        = $(this).find('i');
			var iconHide   = 'icon_hide';
			var listLiHide = $('#address_list>ul>li:gt(1)');
			var hideHtml   = '收起地址',
					showHtml   = '更多地址';
			if(obj.hasClass(iconHide)){
				obj.removeClass(iconHide).siblings('span').text(showHtml);
				listLiHide.hide();
			}else{
				obj.addClass(iconHide).siblings('span').text(hideHtml);
				listLiHide.show();
			}
		});

		var addr; 
		//选择订单地址
		$('#address_list>ul>li').on('click',function(){
			$(this).addClass('hover').siblings().removeClass('hover');
			var objId    = $(this).attr('attrid');
			addr=objId;
		});

		//新增订单地址
		$('#address_add').on('click',function(){
			layer.open({
				type: 1,
				title: '新增地址',
				shadeClose: true,
				shade: 0.5,
				closeBtn: 1,
				maxmin: false, //开启最大化最小化按钮
				area: ['520px', '420px'],
				content: $('#confirm_address_add')
			});
		});

		//编辑订单地址
		$('body').on('click','#address_list>ul>li>span.edit',function(){
			layer.open({
				type: 1,
				title: '编辑地址',
				shadeClose: true,
				shade: 0.5,
				closeBtn: 1,
				maxmin: false, //开启最大化最小化按钮
				area: ['520px', '420px'],
				content: $('#confirm_address_edit')
			});

			$.get("car/getAddressById/"+$(this).attr('attrid'),function(data){
				if(data.success){
					var jStr = "{ ";
					for(var item in data.result){
						jStr += "'address':'"+data.result[item].address+"',";
						jStr += "'consignee':'"+data.result[item].consignee+"',";
						jStr += "'mobile':'"+data.result[item].mobile+"',";
						jStr += "'isDefault':'"+data.result[item].isdefault+"',";
						jStr += "'province':'"+data.result[item].province+"',";
						jStr += "'city':'"+data.result[item].city+"',";
						jStr += "'parm_address':'"+data.result[item].addressId+"',";						
						jStr += "'district':'"+data.result[item].district+"'";
					}
					jStr += " }";
					$('#address_edit_form').formEdit(strToJson(jStr));
					
					$.get("car/getCityByProvinceId/"+$("#provinceId_update").val(),function(data){
						if(data.success){
							var result = "";
							$.each(data.result,function(n,value){
								result +="<option value='"+value.cityId+"'>"+value.cityName+"</option>";
							});
							$("#cityId_update").html('');
							$("#cityId_update").append(result);
							$.get("car/getAreaByCityId/"+$("#cityId_update").val(),function(data){
								if(data.success){
									var result = "";
									$.each(data.result,function(n,value){
										result +="<option value='"+value.areaId+"'>"+value.areaName+"</option>";
									});
									$("#areaId_update").html('');
									$("#areaId_update").append(result);
								}
							},"json");
						}
					},"json");
				}
			},"json");
			
		});


		function strToJson(str){
			var json = eval('(' + str + ')');
			return json;
		}

		$.fn.formEdit = function(data){
			console.log(data['address']);
			return this.each(function(){
				var input, name;
				if(data == null){this.reset(); return; }
				for(var i = 0; i < this.length; i++){
					input = this.elements[i];
					//checkbox的name可能是name[]数组形式
					name = (input.type == "checkbox")? input.name.replace(/(.+)\[\]$/, "$1") : input.name;
					if(data[name] == undefined) continue;
					switch(input.type){
						case "checkbox":
							if(data[name] == ""){
								input.checked = false;
							}else{
								//数组查找元素
								if(data[name].indexOf(input.value) > -1){
									input.checked = true;
								}else{
									input.checked = false;
								}
							}
							break;
						case "radio":
							if(data[name] == ""){
								input.checked = false;
							}else if(input.value == data[name]){
								input.checked = true;
							}
							break;
						case "button": break;
						default: input.value = data[name];
					}
				}
			});
		};

		//删除订单地址
		$('body').on('click','#address_list>ul>li>span.delete',function(){
			var objClick = $(this);
			var objId    = $(this).attr('attrid');
			layer.confirm('确认删除该地址？', {
				btn  :['确认','再考虑下'], //按钮
				title:'温馨提示',
				icon :0
			},function(){
				layer.msg('删除成功！',{icon:1,time:1000},function(){
					objClick.parents('li').remove();
					//重新计算订单地址个数
					addressNums();
					var ids = $("#parm_ids").val();

					$.ajax({  
			            type: "post",  
			            url: "${ctx}/car/delete?ids="+ids+"&id="+objId, 
			            success: function(data){  			               
			                    window.location.href = window.location.href;  
	    
			            }                         
			        })
					
				});
			},function(){
				layer.msg('多谢手下留情！',{icon:6,time:1000});
			});
		});

		function deteteAddress(){
			var ids = $("#parm_ids").val();
			var addressId = $("#edit_id").val();
			  $.ajax({  
		            type: "post",  
		            url: "${ctx}/buy_delete?ids="+ids+"&id="+addressId+"&serverTimeIdx="+serverTimeIdx+"&serverDate="+serverDate, 
		            data: datas,            
		            success: function(data){  
		                    window.location.href = window.location.href;  
		            }                         
		        })
		}
		//提交表单--新增订单地址
		$('body').on('click','#address_add_form .form_submit',function(){
			var ids = $("#parm_ids").val();
			var serverTimeIdx = $("#serverTimeIdx").val();
			var serverDate = $("#serverDate").val();
			$('#address_add_form').attr("action", "buy_saveAddress?ids="+ids+"&serverTimeIdx="+serverTimeIdx+"&serverDate="+serverDate);
			if(validateAddr($(this))){
				$('#address_add_form').submit();
			}
		});

		//提交表单--编辑订单地址
		$('body').on('click','#address_edit_form .form_submit',function(){
			var ids = $("#parm_ids").val();
			var addressId = $("#parm_address").val();
			var serverTimeIdx = $("#serverTimeIdx").val();
			var serverDate = $("#serverDate").val();
			$('#address_edit_form').attr("action", "buy_saveAddress?ids="+ids+"&addressId="+addressId+"&serverTimeIdx="+serverTimeIdx+"&serverDate="+serverDate);
			$('#address_edit_form').submit();
		});

		//点击取消-关闭遮罩
		$('body').on('click','.module_form_box .form_reset',function(){
			layer.closeAll();
		});

		//提交表单--核对订单
		$('#submit_order').on('click',function(){
			var ids = $("#parm_ids").val();
			var message = $("#message").val();
			if(message== undefined){
				message=null
			}
			if(addr== undefined){
				addr=$("#addressId1").val();
			}
			if(addr==""){
				layer.msg('约见地址不能为空，请增加约见地址，谢谢',{icon:6,time:1000});
				return;
			}
			$('#confirm_form').attr("action", "toPayment?ids="+ids+"&addressId="+addr+"&message="+message);
			$('#confirm_form').submit();
		});
	});

	//计算订单地址数目
	function addressNums(){
		var maxNums  = 2;//默认显示2个地址
		var allNums  = $("#address_list>ul>li").size();
		var addrMore = $('#address_hide');
		var addrLi   = "#address_list>ul>li";
		var addrList = $("#address_list");
		if(allNums>maxNums){
			$(addrLi+":gt(1)").hide();
			addrMore.show();
		}else if(allNums==maxNums){
			$(addrLi).show();
			addrMore.hide();
		}else if(allNums==0){
			addrList.html('您的地址为空，请先添加地址！');
			addrMore.hide();
		}else{
			addrMore.hide();
		}
	}


	$("#provinceId").change(function () {
		$.get("car/getCityByProvinceId/"+$("#provinceId").val(),function(data){
			if(data.success){
				var result = "";
				$.each(data.result,function(n,value){
					result +="<option value='"+value.cityId+"'>"+value.cityName+"</option>";
				});
				$("#cityId").html('');
				$("#cityId").append(result);
				$.get("car/getAreaByCityId/"+$("#cityId").val(),function(data){
					if(data.success){
						var result = "";
						$.each(data.result,function(n,value){
							result +="<option value='"+value.areaId+"'>"+value.areaName+"</option>";
						});
						$("#areaId").html('');
						$("#areaId").append(result);
					}
				},"json");
			}
		},"json");
	});

	


	$("#provinceId_update").change(function () {
		$.get("car/getCityByProvinceId/"+$("#provinceId_update").val(),function(data){
			if(data.success){
				var result = "";
				$.each(data.result,function(n,value){
					result +="<option value='"+value.cityId+"'>"+value.cityName+"</option>";
				});
				$("#cityId_update").html('');
				$("#cityId_update").append(result);
				$.get("car/getAreaByCityId/"+$("#cityId_update").val(),function(data){
					if(data.success){
						var result = "";
						$.each(data.result,function(n,value){
							result +="<option value='"+value.areaId+"'>"+value.areaName+"</option>";
						});
						$("#areaId_update").html('');
						$("#areaId_update").append(result);
						$.get("car/getAreaByCityId/"+$("#cityId_update").val(),function(data){
							if(data.success){
								var result = "";
								$.each(data.result,function(n,value){
									result +="<option value='"+value.areaId+"'>"+value.areaName+"</option>";
								});
								$("#areaId_update").html('');
								$("#areaId_update").append(result);
							}
						},"json");
					}
				},"json");
			}
		},"json");
	});

	//订单地址验证
	function validateAddr(thisObj){
		var thisForm = $(thisObj).parents('.module_form_box'),
				mobile   = $(thisForm).find('#mobile'),
				realname = $(thisForm).find('#realname'),
				detail   = $(thisForm).find('#detail'),
				color    = '#FF7800';

		if(!realname.val()){
			layer.tips(realname.attr('null'), realname, {tips: [1, color]});
			return false;
		}
		if(!detail.val()){
			layer.tips(detail.attr('null'), detail, {tips: [1, color]});
			return false;
		}
		if(!mobile.val()){
			layer.tips(mobile.attr('null'), mobile, {tips: [1, color]});
			return false;
		}
		return true;
	}

	//计算总价
	function calcTotalFee(){
	 var totalPrice = 0;
	 var moneyLi    = $('#shop_service li.money');
	 moneyLi.each(function(){
	 var obj = $(this);
	 if(obj.attr('price')){
	 var num   = obj.siblings('.num').find('em').text();
	 totalPrice += parseFloat(obj.attr('price'))*parseInt(num);
	 }
	 });
	 if(totalPrice<0){
	 totalPrice = 0;
	 }
	 $("#total_price").text(totalPrice.toFixed(2));
	 }
</script>

</javascript-list>

</body>
</html>