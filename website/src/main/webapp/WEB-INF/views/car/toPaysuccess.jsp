<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>支付结果_星语星愿</title>
	<!--全局resetCss样式开始-->
	<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
	<!--全局resetCss样式结束-->

	<!--订单公用commonCss样式开始-->
	<link href="${ctx}/assets/css/order/common.css" rel="stylesheet" type="text/css" />
	<!--订单公用commonCss样式结束-->

	<!--页面Css样式开始-->
	<link href="${ctx}/assets/css/order/paysuccess.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

	<!--主题themeCss样式开始-->
	<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
	<!--主题themeCss样式结束-->
</head>
<body>
<div class="space_hx">&nbsp;</div>

<!--头部中间内容开始-->
<div class="header_content clearfix">

	<!--头部logo开始-->
	<div class="header_logo">
		<a href="../${ctx}" title>
			<img src="${ctx}/assets/images/logo.jpg" alt="" />
		</a>
	</div>
	<!--头部logo结束-->

	<!--页面提示开始-->
	<div class="header_tip">支付成功</div>
	<!--页面提示结束-->

	<!--订单流程开始-->
	<div class="header_process clearfix">
		<!--注意：hover为当前步奏，passed为已完成步骤-->
		<!--购物车入口开始-->
		<ul class="shopcar_process clearfix">
			<li class="passed">
				<span class="num">1</span>
				<span class="title">填写核对订单信息</span>
			</li>
			<li class="passed">
				<span class="num">2</span>
				<span class="title">确认支付</span>
			</li>
			<li class="hover">
				<span class="num">3</span>
				<span class="title">支付结果</span>
			</li>
		</ul>
		<!--购物车入口结束-->
		<!--立即购买入口开始-->
		<!--<ul class="buy_process clearfix">
            <li class="hover">
                <span class="num">1</span>
                <span class="title">填写核对订单信息</span>
            </li>
            <li>
                <span class="num">2</span>
                <span class="title">确认支付</span>
            </li>
            <li>
                <span class="num">3</span>
                <span class="title">支付结果</span>
            </li>
        </ul>-->
		<!--立即购买入口结束-->
	</div>
	<!--订单流程结束-->
</div>
<!--头部中间内容结束-->

<!--身体开始-->
<div class="layer clearfix" id="layer">

	<!--支付结果表单开始-->
	<form class="paysuccess_form" name="paysuccess_form" id="paysuccess_form" method="">

		<!--订单信息开始-->
		<div class="paysuccess_common paysuccess_order">
			<ul>
				<li class="order_tips">
					<i class="icon_order icon_order_msg"></i>
					<span>订单支付成功！ </span>
				</li>
				<li class="order_time">
					请尽快联系服务商确认服务事宜，如需有疑问请拨打24小时咨询热线400-1314-666。
				</li>
				<li class="order_msg">
					<span>订单号：${orderSN}</span>
					<span id="order_money" price='1000'>应付总额： <em class="price orange">￥${totalPrice}</em></span>
					<span>
						<a href="" title="">订单详情 <i class="icon_order icon_down"></i></a>
					</span>
				</li>
				<li class="order_address">
					<span>约见人姓名：${consignee}</span>
					<span>收货地址：${prov} ${city} ${district} ${address} </span>
					<span>联系方式：${mobile}</span>
				</li>
				<li class="order_deal">
					<a href="" class="continue">继续购买此服务</a>
					<a href="${ctx}/">返回首页</a>
				</li>
			</ul>
		</div>
		<!--订单信息结束-->

	</form>
	<!--支付结果表单结束-->

</div>
<!--身体结束-->


<!--Jquery库开始-->
<script type="text/javascript" src="${ctx}/assets/js/lib/jquery-1.8.3.min.js"></script>
<!--Jquery库结束-->
<!--公用cmmonJS开始-->
<script type="text/javascript" src="${ctx}/assets/js/common/common.min.js"></script>
<!--公用cmmonJS结束-->

</body>
</html>