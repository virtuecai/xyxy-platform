<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>确认支付_星语星愿</title>
	<!--全局resetCss样式开始-->
	<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
	<!--全局resetCss样式结束-->

	<!--订单公用commonCss样式开始-->
	<link href="${ctx}/assets/css/order/common.css" rel="stylesheet" type="text/css" />
	<!--订单公用commonCss样式结束-->

	<!--页面Css样式开始-->
	<link href="${ctx}/assets/css/order/payment.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

	<!--主题themeCss样式开始-->
	<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
	<!--主题themeCss样式结束-->
</head>
<body>
<div class="space_hx">&nbsp;</div>
<input type="hidden" id="parm_ids" value="${ids}"/>
<input type="hidden" id="ordersn" value="${orderSN}"/>
<input type="hidden" id="addressId" value="${addressId}"/>

<!--头部中间内容开始-->
<div class="header_content clearfix">

	<!--头部logo开始-->
	<div class="header_logo">
		<a href="../../${ctx}" title>
			<img src="${ctx}/assets/images/logo.jpg" alt="" />
		</a>
	</div>
	<!--头部logo结束-->

	<!--页面提示开始-->
	<div class="header_tip">购物车</div>
	<!--页面提示结束-->

	<!--订单流程开始-->
	<div class="header_process clearfix">
		<!--注意：hover为当前步奏，passed为已完成步骤-->
		<!--购物车入口开始-->
		<ul class="shopcar_process clearfix">
			<li class="passed">
				<span class="num">1</span>
				<span class="title">我的购物车</span>
			</li>
			<li class="passed">
				<span class="num">2</span>
				<span class="title">填写核对订单信息</span>
			</li>
			<li class="hover">
				<span class="num">3</span>
				<span class="title">确认支付</span>
			</li>
			<li>
				<span class="num">4</span>
				<span class="title">支付结果</span>
			</li>
		</ul>
		<!--购物车入口结束-->
		<!--立即购买入口开始-->
		<!--<ul class="buy_process clearfix">
            <li class="hover">
                <span class="num">1</span>
                <span class="title">填写核对订单信息</span>
            </li>
            <li>
                <span class="num">2</span>
                <span class="title">确认支付</span>
            </li>
            <li>
                <span class="num">3</span>
                <span class="title">支付结果</span>
            </li>
        </ul>-->
		<!--立即购买入口结束-->
	</div>
	<!--订单流程结束-->
</div>
<!--头部中间内容结束-->

<div class="layer clearfix" id="layer">

	<!--确认支付表单开始-->
	<form class="payment_form" name="payment_form" id="payment_form"  action="" method="post">

		<!--订单信息开始-->
		<div class="payment_common payment_order">
			<ul>
				<li class="order_tips">
					<i class="icon_order icon_order_msg"></i>
					<span>订单提交成功，请您尽快付款！ </span>
				</li>
				<li class="order_time">
					请您在提交订单后<em>45分钟</em>内完成支付，否则订单会自动取消。
				</li>
				<li class="order_msg">
					<span>订单号：${orderSN}</span>
					<span id="order_money" price='1000'>已付总额： <em class="price orange">￥${totalPrice}</em></span>
					<span>
						<a href="" title="">订单详情 <i class="icon_order icon_down"></i></a>
					</span>
				</li>
				<li class="order_address">
					<span>约见人姓名：${consignee}</span>
					<span>收货地址：${prov} ${city} ${district} ${address} </span>
					<span>联系方式：${mobile}</span>
				</li>
				<li class="order_service">服务名称：和车库咖啡老板苏菂，面对面聊创业！</li>
			</ul>
		</div>
		<!--订单信息结束-->

		<!--支付方式开始-->
		<div class="payment_common payment_way" id="payment_way">
			<h2>支付方式</h2>
			<ul class="clearfix">
				<li class="way_wechat"><!--高亮样式hover-->
					<i class="icon_order icon_wechat"></i>
					<i class="icon_order icon_hover"></i>
					<span class="name">微信支付</span>
				</li>
				<li class="way_alipay">
					<i class="icon_order icon_alipay"></i>
					<i class="icon_order icon_hover"></i>
					<span class="name">支付宝</span>
				</li>
				<!--注意：余额不足时隐藏钱包支付-->
				<li class="way_wallet">
					<i class="icon_order icon_wallet"></i>
					<i class="icon_order icon_hover"></i>
					<span class="name">钱包支付</span>
					<span class="wallet">( 余额：￥<em class="orange" id="wallet_money">12000.00</em> )</span>
				</li>
			</ul>
		</div>
		<!--支付方式结束-->

		<!--微信扫码支付开始-->
		<div class="payment_wechat clearfix" id="payment_wechat">
			<div class="wechat_code">
				<ul>
					<li class="code_img">
						<!--支付二维码开始-->
						<img src="${ctx}/assets/images/code.jpg"  alt="" />
						<!--支付二维码结束-->
					</li>
					<li class="code_tip cmain_bg_color clearfix">
						<span>
							<i  class="icon_order icon_sweep"></i>
							<em class="tip">
								请使用微信扫一扫<br/>扫描二维码支付
							</em>
						</span>
					</li>
				</ul>
			</div>
			<div class="wechat_sweep">
				<img src="${ctx}/assets/images/wechat.jpg" alt=""  />
			</div>
		</div>
		<!--微信扫码支付结束-->

		<!--确认支付工具开始-->
		<div class="payment_tool clearfix">
			<span class="btn payment_order" id="payment_order">立即支付</span>
		</div>
		<!--确认支付工具结束-->

	</form>
	<!--确认支付表单结束-->

</div>
<!--身体结束-->
<!--Jquery库开始-->
<script type="text/javascript" src="${ctx}/assets/js/lib/jquery-1.8.3.min.js"></script>
<!--Jquery库结束-->
<!--公用cmmonJS开始-->
<script type="text/javascript" src="${ctx}/assets/js/common/common.min.js"></script>
<!--公用cmmonJS结束-->
<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->
<script type="text/javascript">
	$(function(){
		//选择支付方式
		$('#payment_way>ul>li').on('click',function(){
			$(this).addClass('hover').siblings().removeClass('hover');
		});

		//微信支付--扫描支付
		$('#payment_way>ul>li.way_wechat').on('click',function(){
			var obj = $(this);
			layer.open({
				type: 1,
				title: '微信扫码支付',
				shadeClose: true,
				shade: 0.5,
				closeBtn: 1,
				maxmin: false, //开启最大化最小化按钮
				area: ['650px', '450px'],
				content: $('#payment_wechat'),
				end:function(index){//取消和关闭按钮触发的回调
					obj.removeClass('hover');
				}
			});
		});

		//提交表单--确认支付
		$('#payment_order').on('click',function(){
			var ids = $("#parm_ids").val();
			var ordersn = $("#ordersn").val();
			var addressId = $("#addressId").val();
			$('#payment_form').attr("action", "paysuccess?ids="+ids+"&addressId="+addressId+"&ordersn="+ordersn);
			$('#payment_form').submit();
		});
	});
</script>

</body>
</html>