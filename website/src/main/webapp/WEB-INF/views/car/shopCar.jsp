<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>购物车</title>
	<!--全局resetCss样式开始-->
	<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
	<!--全局resetCss样式结束-->

	<!--订单公用commonCss样式开始-->
	<link href="${ctx}/assets/css/order/common.css" rel="stylesheet" type="text/css" />
	<!--订单公用commonCss样式结束-->

	<!--公用分页pageCss样式开始-->
	<link href="${ctx}/assets/css/common/page.css" rel="stylesheet" type="text/css" />
	<!--公用分页pageCss样式结束-->

	<!--页面Css样式开始-->
	<link href="${ctx}/assets/css/order/shopcar.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

	<!--主题themeCss样式开始-->
	<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="space_hx">&nbsp;</div>

<!--头部中间内容开始-->
<div class="header_content clearfix">

	<!--头部logo开始-->
	<div class="header_logo">
		<a href="../../${ctx}" title>
			<img src="${ctx}/assets/images/logo.jpg" alt="" />
		</a>
	</div>
	<!--头部logo结束-->

	<!--页面提示开始-->
	<div class="header_tip">购物车</div>
	<!--页面提示结束-->

	<!--订单流程开始-->
	<div class="header_process clearfix">
		<!--注意：hover为当前步奏，passed为已完成步骤-->
		<!--购物车入口开始-->
		<ul class="shopcar_process clearfix">
			<li class="hover">
				<span class="num">1</span>
				<span class="title">我的购物车</span>
			</li>
			<li>
				<span class="num">2</span>
				<span class="title">填写核对订单信息</span>
			</li>
			<li>
				<span class="num">3</span>
				<span class="title">确认支付</span>
			</li>
			<li>
				<span class="num">4</span>
				<span class="title">支付结果</span>
			</li>
		</ul>
		<!--购物车入口结束-->
		<!--立即购买入口开始-->
		<!--<ul class="buy_process clearfix">
            <li class="hover">
                <span class="num">1</span>
                <span class="title">填写核对订单信息</span>
            </li>
            <li>
                <span class="num">2</span>
                <span class="title">确认支付</span>
            </li>
            <li>
                <span class="num">3</span>
                <span class="title">支付结果</span>
            </li>
        </ul>-->
		<!--立即购买入口结束-->
	</div>
	<!--订单流程结束-->
</div>
<!--头部中间内容结束-->

<!--身体开始-->
<div class="layer clearfix" id="layer">

	<!--购物车工具开始-->
	<div class="shopcar_tool clearfix">
		<div class="shopcar_all">
			全部商品
			<span class="total_num" id="total_num">10</span>
		</div>
		<div class="shopcar_settle">
			<span class="settle_money">
				已选商品总计
				<em>￥</em><em class="total_price"></em>
			</span>
			<span class="btn submit_ok submit_btn">结算</span>
		</div>
	</div>
	<!--购物车工具结束-->

	<!--购物车列表标题开始-->
	<div class="common_form shopcar_title">
		<ul class="clearfix">
			<li class="checkbox">
				<input type="checkbox" name="check_all" class="check_all" checked />
				<span>全选</span>
			</li>
			<li class="product">商品信息</li>
			<li class="price">单价(元)</li>
			<li class="num">数量</li>
			<li class="total">金额(元)</li>
			<li class="deal">操作</li>
		</ul>
	</div>
	<!--购物车列表标题结束-->

	<!--购物车列表开始-->
	<div class="shopcar_list">
		<c:if test="${carlist}==null">
		<!--没有商品的情况开始-->
			<div class="shopcar_list_none" id="shopcar_list_none">
				抱歉，您的购物车暂无任何商品~
			</div>
		<!--没有商品的情况结束-->
		</c:if>
		<form action="" method="post" class="cart_form" id="cart_form">
			<c:forEach  items="${carlist}" var="carDetail">
			<!--购物车商品盒子遍历开始-->
			<div class="shopcar_list_box">
				<!--星店信息开始-->
				<div class="shop_info">
					<ul class="clearfix">
						<li class="checkbox">
							<input type="checkbox" name="sid[]" class="check_item check_sid" checked />
						</li>
						<li class="title">
							<a href="" title="">${carDetail.username}</a>
						</li>
						<li class="icon">
							<i class="icon_order icon_zheng"></i>
							<i class="icon_order icon_v_member"></i>
						</li>
					</ul>
				</div>
				<!--星店信息结束-->
				<!--星店服务开始-->
				<div class="common_form shop_service">
					<!--服务列表遍历开始-->
					<ul class="clearfix">
						<li class="checkbox">
							<input type="checkbox" name="pid[]" class="check_item check_pid" price="${carDetail.price}" value="${carDetail.id}" checked />
								<%-- <input type="text" name="id"  value="${carDetail.id}" /> --%>
						</li>
						<li class="product">
							<a href="" title="" class="thumb">
								<img src="${carDetail.img}" alt=""  />
							</a>
							<a href="" title="" class="desc">
								<span class="title">${carDetail.goodInfo}</span>
								<span class="intro">聊聊创业，关于你的项目，或者其他创业者的故事</span>
							</a>
						</li>
						<li class="price">￥${carDetail.price}</li>
						<li class="num">x <em>${carDetail.num}</em></li>
						<li class="total">￥${carDetail.totalPrice}</li>
						<li class="deal">
							<span class="delete_this">删除</span>
							<span class="add_collect">加入收藏夹</span>
						</li>
					</ul>
				</div>

				<!--星店信息结束-->
			</div>
			<tbody></tbody>
			<!--购物车商品盒子遍历结束-->
							</c:forEach>
			
		</form>

		<!--分页结构开始-->
		<div class="web_page clearfix" id="web_page">
			<ul>
				<div class="mricode-pagination"></div>

			</ul>
		</div>
		<!--分页结构结束-->

	</div>
	<!--购物车列表结束-->

	<!--购物车工具开始-->
	<div class="shopcar_tool shopcar_bottom clearfix">
		<div class="shopcar_all">
			<input type="checkbox" name="check_all" class="check_all" checked="" />
			<span>全选</span>
			<span class="delete_this" more='yes'>删除</span>
			<span class="add_collect" more='yes'>加入收藏夹</span>
		</div>
		<div class="shopcar_settle">
			<span class="settle_money">
				合计（不含运费、手续费、税等费用）：
				<em>￥</em><em class="total_price" id="total_price"></em>
			</span>
			<span class="btn submit_ok submit_btn">结算</span>
		</div>
	</div>
	<!--购物车工具结束-->

</div>


<!--Jquery库开始-->
<script type="text/javascript" src="${ctx}/assets/js/lib/jquery-1.8.3.min.js"></script>
<!--Jquery库结束-->
<!--公用cmmonJS开始-->
<script type="text/javascript" src="${ctx}/assets/js/common/common.js"></script>
<!--公用cmmonJS结束-->
<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->
<script type="text/javascript">
	$(function(){
		var layerId = '#layer ';//全局身体变量layer
		calcTotal(layerId);//初始化计算总价、总量

		//全选所有星店商品
		$(layerId+'.check_all').on('click',function(){
			var checkAll = $(layerId+'.check_all');
			if($(this).prop('checked')){
				$('.check_item').prop('checked',true);
				checkAll.prop('checked',true);
			}else{
				$('.check_item').prop('checked',false);
				checkAll.prop('checked',false);
			}
			calcTotal(layerId);
		});

		//全选单个星店的商品
		$(layerId+'.check_sid').on('click',function(){
			var checkPid  = $(this).parents('.shop_info').siblings('.shop_service').find('.check_pid');
			var checkPids = $(this).parents('.shopcar_list_box').siblings().find('.check_pid');
			if($(this).prop('checked')){
				checkPid.prop('checked',true);
			}else{
				checkPid.prop('checked',false);
			}
			checkAllStatus(layerId,checkPid,checkPids);
			calcTotal(layerId);
		});

		//单选星店中的某个商品
		$(layerId+'.check_pid').each(function(){
			var thisPid   = $(this);
			var otherPid  = thisPid.parents('ul').siblings('ul').find('.check_pid');
			var checkSid  = thisPid.parents('.shop_service').siblings('.shop_info').find('.check_sid');
			var checkSids = thisPid.parents('.shopcar_list_box').siblings().find('.check_sid');
			$(this).on('click',function(){
				//星店中商品全选中，则星店选中
				if(thisPid.prop('checked') == true && (otherPid.prop('checked') == true || otherPid.prop('checked') == undefined)){
					checkSid.prop('checked',true);
				}else{
					checkSid.prop('checked',false);
				};
				checkAllStatus(layerId,checkSid,checkSids);
				calcTotal(layerId);
			});
		});

		//点击删除商品
		$(layerId+'.delete_this').on('click',function(){
			var obj = $(this);
			var deleteMore = obj.attr('more');
			var checkItem  = $('.check_item');
			var ids 	   = '';
			checkItem.each(function(){
				var obj = $(this);
				if(obj.prop('checked')){
					if(obj.val()){
						if(!isNaN(obj.val())){
							ids += obj.val()+"_";
						}
					}
				}
			})
			layer.confirm('确认删除所选商品？', {
				btn  :['确认','再考虑下'], //按钮
				title:'温馨提示',
				icon :0
			},function(){
				layer.msg('删除成功！',{icon:1,time:1000},function(){
					if(deleteMore){
						//删除多个商品
						$(layerId+'.check_pid').each(function(){
							if($(this).prop('checked')){
								$(this).parents('ul').remove();
							}
						});
					}else{
						//单个删除商品
						obj.parents('ul').remove();

					}
					calcTotal(layerId,'delete')
					$('#cart_form').attr("action", "deleteCart?id="+ids);
					$('#cart_form').submit();

				});
			},function(){
				layer.msg('多谢手下留情！',{icon:6,time:1000});
			});
		});

		//点击加入收藏夹
		$(layerId+'.add_collect').on('click',function(){
			layer.confirm('确认加入收藏夹？', {
				btn  :['确认','再考虑下'], //按钮
				title:'温馨提示',
				icon :0
			},function(){
				layer.msg('加入收藏夹成功！',{icon:1,time:1000});
			},function(){
				layer.msg('加入收藏夹失败！',{icon:2,time:1000});
			});
		});

		//提交表单
		$(layerId+'.submit_ok').on('click',function(){
			var checkItem  = $('.check_item');
			var ids 	   = '';
			if($(this).hasClass('submit_ok')){
				checkItem.each(function(){
					var obj = $(this);
					if(obj.prop('checked')){
						if(!isNaN(obj.val())){
							ids += obj.val()+"_";
						}
					}
				});
				$('#cart_form').attr("action", "confirm?ids="+ids);
				$('#cart_form').submit();
			}else{
				layer.alert('请先选择至少一件商品！',{icon:0})
			}
		});
	});

	//全选状态筛选
	function checkAllStatus(layerId,obj,objSiblings){
		var checkAll  = $(layerId+'.check_all');
		if(obj.prop('checked') == true && objSiblings.prop('checked') == true){
			checkAll.prop('checked',true);
		}else{
			checkAll.prop('checked',false);
		}
	}

	//计算商品总价格
	function calcTotal(layerId,isDelete){
		var price      = 0;
		var total      = 0;
		var totalNum   = $('#total_num');
		var totalPrice = $(layerId+'.total_price');
		var checkItem  = $(layerId+'.check_item');
		var listNone   = $('#shopcar_list_none');
		checkItem.each(function(){
			var obj = $(this);
			if(obj.prop('checked')){
				if(obj.attr('price')){
					var num = obj.parent('.checkbox').siblings('.num').find('em').text();
					price += parseFloat(obj.attr('price'))*parseInt(num);
					total += 1
				}
			}
		});
		//商品总价格
		totalPrice.text(price.toFixed(2));
		if(price > 0 ){
			submitBtn(layerId,true);
		}else{
			submitBtn(layerId,false);
		}
		//商品总数量
		totalNum.text(parseInt(total));
		if(total==0 && isDelete){
			listNone.show().siblings().hide();
		}
	}

	//设置确定按钮样式
	function submitBtn(layerId,flag){
		if(flag){
			$(layerId+'.submit_btn').removeClass('submit_no').addClass('submit_ok');
		}else{
			$(layerId+'.submit_btn').addClass('submit_no').removeClass('submit_ok');
		}
	}


	/**
	 * js 代码编写模版
	 */

	$(function () {
		var pagination = new Pagination();
	});

	var Pagination = function () {
		this.init();
	};
	Pagination.prototype = {
		$categoryName: $('[name=categoryName]'),
		$dataTable: $('table').first(),
		$searchBtn: $('#searchBtn'),
		dataItemTemplate: $('#black_white_list_template').html(),
		$page: null,
		init: function () {
			var that = this;

			/**
			 * 加载分页插件
			 */
			that.initPagination();

			/**
			 * 搜索点击时间
			 */
			$('#searchBtn').on('click', function () {
				that.$page.pagination('setParams', {
					categoryName: $.trim(that.$categoryName.val())
				});
				that.$page.pagination('remote');
			});

			return that;
		},
		initPagination: function () {
			var that = this;
			that.$page = $(".mricode-pagination").pagination({
				pageSize: 1, // 指定 一页多少天数据
				debug: false,
				showInfo: false,
				showJump: true,
				showPageSizes: false,
				prevBtnText: '<i><</i> 上一页',
				nextBtnText: '下一页 <i>></i>',
				jumpBtnText: '确定',
				infoFormat: '共 {total} 条',
				noInfoText: '暂无数据',
				pageElementSort: ['$page', '$size', '$jump', '$info'],
				remote: {
					url: window['ctx'] + '/car/pagination/data.json',
					totalName: 'result.total',
					pageIndexName: 'pageNum',
					pageSizeName: 'pageSize',
					success: function (data) {
						if(data.success) {
							var renderHtml = that.renderHtml(data.result.list);
							that.$dataTable.find('tbody').empty().append(renderHtml);
						} else {
							layer.msg('请求失败!');
						}
					}
				}
			});
		},
		renderHtml: function (list) {
			var that = this;
			var renderHtml = '';
			$.each(list, function (idx, item) {
				renderHtml += _.template(that.dataItemTemplate, {items: item});
			});
			return renderHtml;
		}
	};


	function confirm(){
		var price      = 0;
		var total      = 0;
		var checkItem  = $('.check_item');
		var ids 	   = '';
		checkItem.each(function(){
			var obj = $(this);
			if(obj.prop('checked')){
				if(obj.val()){
					ids += obj.val()+"_";
				}
			}
		});
	}
</script>
</body>
</html>