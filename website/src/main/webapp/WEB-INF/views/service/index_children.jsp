<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
    <title>子级分类_服务广场_星语星愿</title>

    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/service/index_children.css" rel="stylesheet" type="text/css"/>
    <!--页面Css样式结束-->

</head>

<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

    <!--头部面包屑开始-->
    <div class="position">
        <ul class="clearfix">
            <li>
                <a href="#" title="">服务广场</a>
            </li>
            <li class="line">></li>
            <li>
                <a href="#" title="">分类</a>
            </li>
            <li class="line">></li>
            <li>
                <a href="#" title="">子级分类</a>
            </li>
        </ul>
    </div>
    <!--头部面包屑结束-->

    <!--服务内容开始-->
    <div class="service_container">

        <!--一级分类推荐开始-->
        <div class="service_recom">
            <a href="javascript:void(0);" class="present">${parentCategory.categoryType}</a>
            <c:choose>
                <c:when test="${parentCategory.pid == 0}">
                    <a href="${ctx}/service/index" class="go_back">返回上一级↑</a>
                </c:when>
                <c:otherwise>
                    <a href="${ctx}/service/category/${parentCategory.pid}" class="go_back">返回上一级↑</a>
                </c:otherwise>
            </c:choose>
            <input type="hidden" name="parentCategoryId" value="${parentCategory.categoryId}"/>
        </div>
        <!--一级分类推荐结束-->

        <!--服务分类选择开始-->
        <div class="service_selector">
            <ul class="clearfix category-list">
                <li class="underscore-template"><!--高亮样式 hover-->
                    <a href="{{=window['ctx']}}/service/category/{{=categoryId}}" title="{{=categoryType}}">{{=categoryType}}</a>
                </li>
            </ul>
        </div>
        <!--服务分类选择结束-->

        <!--服务内容主体开始-->
        <div class="service_main clearfix">
            <!--条件筛选开始-->
            <form name="service_form" id="service_form">
                <div class="service_filter clearfix">
                    <div class="status clearfix">
                        <a href="javascript:void(0);" title="个性推荐服务" class="hover" data-val="0">推荐</a>
                        <a href="javascript:void(0);" title="最新发布服务" data-val="1">最新</a>
                        <input type="hidden" name="searchType" value="0"/>
                    </div>
                    <div class="other clearfix">
                        <div class="other_common other_service">
                            <select name="service" id="service" class="text_select">
                                <option value="">全部服务方式</option>
                                <option value="1">上门服务</option>
                                <option value="2">约定地点服务</option>
                                <option value="3">线上服务</option>
                                <option value="4">邮寄地址</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <!--条件筛选结束-->

            <!--筛选内容列表开始-->
            <div class="service_list clearfix">
                <dl class="underscore-template">
                    <dt>
                        <a href="{{=window['ctx']}}/service/detail/{{=goodsId}}" title="">
                            <img src-default="${ctx}/assets/images/content/2f_01.jpg" data-src="{{=coverImage}}" alt=""/>
                            <span class="nums" title="已关注人数">{{=collectionCount}}</span>
                            <span class="description" style="display: none;">
                                <em>[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
                            </span>
                        </a>
                    </dt>
                    <dd class="title clearfix" title="{{=goodsName}}">
                        <a href="{{=window['ctx']}}/service/detail/{{=goodsId}}" title="">{{=goodsName?goodsName: '--'}}</a>
                    </dd>
                    <dd class="price clearfix">
                        <em>￥{{=marketPrice}}</em>元/{{=goodsCompanyNote}}
                    </dd>
                    <dd class="label clearfix">
                        {{ var tagNameArray = _.filter(serverTag.slice(0,2), function(tagName){return tagName != '';}); }}
                        {{ _.each(tagNameArray, function(tagName){ }}
                            <span class="hover" title="{{=tagName}}"><!--高亮样式hover serverTags-->
                                <i class="icon_service icon_header"></i>
                                <i class="icon_footer">{{=tagName}}</i>
                            </span>
                        {{ }) }}
                        {{ if(!tagNameArray || tagNameArray.length == 0) { }}
                            <span>
                                <i class="icon_service icon_header"></i>
                                <i class="icon_footer">暂无标签</i>
                            </span>
                        {{ } }}
                    </dd>
                    <dd class="member clearfix">
                        <a class="headpic clearfix">
                            <img src-default="${ctx}/assets/images/content/headpic_01.jpg" src="{{=memberHeadImage}}" alt=""/>
                            <em>{{=memberName}}</em>
                        </a>
                        <a class="address">
                            {{ _.each(address ? address.slice(0,2) : [], function(obj){ }}
                            {{var addressItem = $.parseJSON(obj)}}
                            {{= addressItem.shortname}}
                            {{ }) }}
                            {{ if(!address || address.length == 0) { }}
                            --
                            {{ } }}
                        </a>
                    </dd>
                </dl>
            </div>
            <!--筛选内容列表结束-->

            <!-- 分页 -->
            <div class="mricode-pagination" id="goods-pagination"></div>

            <!--分页结构开始-->
            <div class="web_page clearfix" style="display: none;">
                <ul>
                    <li class="link prev">
                        <a href="" title=""><em><</em> 上一页</a>
                    </li>
                    <li class="link current"><!--当前页样式current-->
                        <a href="" title="">1</a>
                    </li>
                    <li class="link">
                        <a href="" title="">2</a>
                    </li>
                    <li class="link">
                        <a href="" title="">3</a>
                    </li>
                    <li class="link">
                        <a href="" title="">4</a>
                    </li>
                    <li class="link">
                        <a href="" title="">5</a>
                    </li>
                    <li class="link">
                        <a href="" title="">6</a>
                    </li>
                    <li class="more">
                        ...
                    </li>
                    <li class="link next">
                        <a href="" title="">下一页 <em>></em></a>
                    </li>
                    <li class="total">
                        共<em>50</em>页
                    </li>
                    <li class="jump">
                        到第<input type="text" value="1" name="pagenum"/>页
                        <span class="submit_page">确定</span>
                    </li>
                </ul>
            </div>
            <!--分页结构结束-->

        </div>
        <!--服务内容主体开始-->

    </div>
    <!--服务内容结束-->

</div>
<!--身体结束-->

<javascript-list>
    <script type="text/javascript" src="${ctx}/assets/js/service/index.js"></script>
</javascript-list>
</body>
</html>
