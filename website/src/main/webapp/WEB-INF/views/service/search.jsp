<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
<title>搜索服务结果_服务广场_星语星愿</title>

	<!--页面Css样式开始-->
	<link href="../assets/css/service/search.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

</head>

<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>


<!--身体开始-->
<div class="layer clearfix">
	
	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">首页</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">搜索服务结果</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->
	
	<!--服务内容开始-->
	<div class="service_container">
		
		<!--一级分类推荐开始-->
		<div class="service_search">
			<span class="keywords">家居保姆</span>
			相关服务 <span class="nums">1239</span> 条
		</div>
		<!--一级分类推荐结束-->
			
		<!--服务分类选择开始-->
		<div class="service_selector">
			<dl class="clearfix">
				<dt>热门地区：</dt>
				<dd class="all"><a href="" title="">全部地区</a></dd>
				<dd><a href="" title="">长沙</a></dd>
				<dd><a href="" title="">深圳</a></dd>
				<dd><a href="" title="">北京</a></dd>
				<dd><a href="" title="">上海</a></dd>
			</dl>
			<dl class="clearfix">
				<dt>服务方式：</dt>
				<dd class="all"><a href="" title="">全部方式</a></dd>
				<dd><a href="" title="">上门服务</a></dd>
				<dd><a href="" title="">约定地点服务</a></dd>
				<dd><a href="" title="">线上服务</a></dd>
				<dd><a href="" title="">邮寄</a></dd>
			</dl>
		</div>
		<!--服务分类选择结束-->
		
		<!--服务内容主体开始-->
		<div class="service_main clearfix">
			<!--条件筛选开始-->
			<form name="service_form" id="service_form">
				<div class="service_filter clearfix">
					<div class="status clearfix">
						<a href="" title="" class="hover">推荐</a>
						<a href="" title="" class="">最新</a>
					</div>
					<!--<div class="other clearfix">
						<div class="other_common other_address">
							<div class="selected">全部地区</div>
							<div class="address_list list_common" id="address_list">
								<ul>
									<li attrid="1">华东地区</li>
									<li attrid="2">华中地区</li>
									<li attrid="3">华北地区</li>
								</ul>
							</div>
							<i class="icon_service icon_down"></i>
							<input name='address' value="" class="address" type="hidden" />
						</div>
						<div class="other_common other_service">
							<div class="selected">全部服务方式</div>
							<div class="way_list list_common" id="way_list">
								<ul>
									<li attrid="1">项目咨询</li>
									<li attrid="2">天使投资</li>
								</ul>
							</div>
							<i class="icon_service icon_down"></i>
							<input name='service' value="" class="service" type="hidden" />
						</div>
					</div>-->
				</div>
			</form>
			<!--条件筛选结束-->
			
			<!--筛选内容列表开始-->
			<div class="service_list clearfix">
				<dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_01.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span class="hover"><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_02.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_03.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_04.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_05.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_02.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_07.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_08.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_01.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_03.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_03.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_03.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_02.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_03.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="${ctx}/service/detail/1" title="">
		                <dt>
		                    <img src="../assets/images/content/2f_04.jpg" alt="" />
		                    <span class="nums">112</span>
		                    <span class="description">
		                    	<em>
		                    		[线上服务] 互联网项目投资，平均投资金额 100万 - 1500万元。
		                    	</em>
		                    </span>
		                </dt>
		                <dd class="title clearfix">互联网项目天使投资咨询</dd>
		                <dd class="price clearfix">
		                	<em>￥1000</em>元/个
		                </dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                <dd class="member clearfix">
		                	<span class="headpic clearfix">
		                		<img src="../assets/images/content/headpic_01.jpg" alt=""  />
		                		<em>韩梅梅</em>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		            </a>
	            </dl>
			</div>
			<!--筛选内容列表结束-->
			
			<!--分页结构开始-->
			<div class="web_page clearfix">
				<ul>
					<li class="link prev">
						<a href="" title=""><em><</em> 上一页</a>
					</li>
					<li class="link current"><!--当前页样式current-->
						<a href="" title="">1</a>
					</li>
					<li class="link">
						<a href="" title="">2</a>
					</li>
					<li class="link">
						<a href="" title="">3</a>
					</li>
					<li class="link">
						<a href="" title="">4</a>
					</li>
					<li class="link">
						<a href="" title="">5</a>
					</li>
					<li class="link">
						<a href="" title="">6</a>
					</li>
					<li class="more">
						...
					</li>
					<li class="link next">
						<a href="" title="">下一页 <em>></em></a>
					</li>
					<li class="total">
						共<em>50</em>页
					</li>
					<li class="jump">
						到第<input type="text" value="1" name="pagenum" />页
						<span class="submit_page">确定</span>
					</li>
				</ul>
			</div>
			<!--分页结构结束-->
			
		</div>
		<!--服务内容主体开始-->
		
	</div>
	<!--服务内容结束-->
	
</div>
<!--身体结束-->


<javascript-list>

	<!--<script type="text/javascript">
	$(function(){
		//地区、服务筛选
		$('#service_form .other_common').on('mouseenter mouseleave',function(){
			$(this).find('i').toggleClass('icon_up');
			$(this).find('.list_common').stop().slideToggle()
		});
		$('#service_form .list_common>ul>li').on('click',function(){
			var attrid  = $(this).attr('attrid');
			var attrtxt = $(this).html();
			$(this).parents('.list_common').siblings('input').val(attrid);
			$(this).parents('.list_common').siblings('.selected').html(attrtxt);
			$(this).parents('.list_common').slideUp();
		})
	})
</script>-->

</javascript-list>

</body>
</html>
