<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
    <title>服务详情_服务广场_星语星愿</title>

    <!--图片etalage缩放开始-->
    <link href="${ctx}/assets/plugins/etalage/css/etalage.css" rel="stylesheet" type="text/css"/>
    <!--图片etalage缩放结束-->
    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/service/detail.css" rel="stylesheet" type="text/css"/>
    <!--页面Css样式结束-->
</head>

<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">
    <input type="hidden" value="${goodsId}" name="goodsId"/>
    <input type="hidden" value="${memberId}" name="goodsMemberId"/>
    <!--头部面包屑开始-->
    <div class="position">
        <ul class="clearfix">
            <li>
                <a href="#" title="">首页</a>
            </li>
            <li class="line">></li>
            <li>
                <a href="#" title="">服务广场</a>
            </li>
            <li class="line">></li>
            <li>
                <a href="#" title="">服务详情</a>
            </li>
        </ul>
    </div>
    <!--头部面包屑结束-->

    <!--服务详情内容开始-->
    <div class="service_container clearfix">

        <!--左侧服务内容开始-->
        <div class="container_left">

            <!--预定服务开始-->
            <div class="service_reserve clearfix">

                <!--滚动图片展示开始-->
                <div class="service_photo">
                    <ul id="etalage">
                        <li class="default">
                            <img class="etalage_thumb_image" data-src="{{=uri}}" src-default="${ctx}/assets/images/default_content.png"/>
                            <img class="etalage_source_image"  data-src="{{=uri}}" src-default="${ctx}/assets/images/default_content.png" />
                            <!--第二张图放大图: title="互联网天使投资咨询" -->
                        </li>
                        <li class="underscore-template">
                            <img class="etalage_thumb_image" data-src="{{=uri}}" src-default="${ctx}/assets/images/default_content.png"/>
                            <img class="etalage_source_image"  data-src="{{=uri}}" src-default="${ctx}/assets/images/default_content.png" />
                            <!--第二张图放大图: title="互联网天使投资咨询" -->
                        </li>
                    </ul>
                </div>
                <!--滚动图片展示结束-->

                <!--服务介绍开始-->
                <div class="service_introduce" id="service_introduce">
                    <form action="" name="service_form" id="service_form" method="">
                        <ul>
                            <li class="title" id="goodsName">
                                ...
                            </li>
                            <li class="label server-tag-list">
                                <%--<span class="hover"><!--高亮样式hover-->
                                    <i class="icon_service icon_header"></i>
                                    <i class="icon_footer">环游世界</i>
                                </span>--%>
                            </li>
                            <li class="cost">
                                <em>费用：</em>
                                <span>￥<em id="shopPrice">...</em></span>元/<span id="goodsCompanyNote">...</span>
                            </li>
                            <li class="common_li time">
                                <em>服务时间：</em>
                                <i class="icon_service icon_down">&nbsp;</i>
                                <input name="service_time" type="text" id="service_time" placeholder="请选择购买服务时间" />
                            </li>
                            <li class="common_li time_block clearfix" id="time_block">
                                <em>具体时间：</em>
                                <span class="clearfix">
                                    <i class="btn none" data-idx="1" data-end="12:00">09:00-12:00</i>
                                    <i class="btn none" data-idx="2" data-end="15:00">12:00-15:00</i>
                                    <i class="btn none" data-idx="3" data-end="17:00">15:00-17:00</i>
                                    <i class="btn none" data-idx="4" data-end="21:00">17:00-21:00</i>
                                    <i class="btn none" data-idx="5" data-end="23:59">21:00-24:00</i>
                                </span>
                                <!-- 注意样式说明: hover 选中, none 不能选, can能选  -->
                            </li>
                            <li class="common_li ways clearfix" id="ways">
                                <em>服务方式：</em>
                                <span class="clearfix" id="serverType"><!-- hover -->
                                    <i class="btn" data-id="1">上门服务</i>
                                    <i class="btn" data-id="2">约定地点服务</i>
                                    <i class="btn" data-id="3">线上服务</i>
                                    <i class="btn" data-id="4">邮寄</i>
                                </span>
                                <!--注意：不同的服务方式会对应订单的不同地址-->
                            </li>
                            <li class="handle" style="display: none;">
                                <span class="btn subscribe" id="subscribe">立即购买</span>
                                <span class="btn addcart" id="addcart">加入心愿车</span>
                            </li>
                            <li class="tools clearfix" style="display: none;">
                                <span id="collect" class="clearfix">
                                    <i class="icon_service icon_collect"></i>
                                    <em>收藏</em>
                                </span>
                                <span id="share" class="share bdsharebuttonbox clearfix">
                                    <a class="bds_more" data-cmd="more"></a>
                                    <em>分享</em>
                                </span>
                                <span id="comment" class="comment clearfix" style="display: none;">
                                    <i class="icon_service icon_comment"></i>
                                    <em>评论</em>
                                    <em class="num">4</em>
                                </span>
                                <span id="report" class="report clearfix">
                                    <!--<i class="icon_service icon_report"></i>-->
                                    <em>举报</em>
                                </span>
                            </li>
                        </ul>
                    </form>
                </div>
                <!--服务介绍开始-->

            </div>
            <!--预定服务开始-->

            <!--服务详情、评价、案例开始-->
            <div class="service_detail">

                <!--头部选项开始-->
                <div class="service_detail_header" id="service_detail_header">
                    <ul class="clearfix">
                        <li attrid="detail_content" class="hover">
                            <i class="icon_service icon_down"></i>
                            服务详情
                        </li>
                        <li attrid="detail_comment" class="" style="display: none;">
                            <i class="icon_service icon_down"></i>
                            顾客评价
                            <em class="comments_num">12</em>
                        </li>
                        <li attrid="detail_case" class="" style="display: none;">
                            <i class="icon_service icon_down"></i>
                            我的案例
                        </li>
                    </ul>
                </div>
                <!--头部选项结束-->

                <!--底部显示开始-->
                <div class="service_detail_footer">

                    <!--详情内容开始-->
                    <div class="detail_common detail_content" id="detail_content">
                        <!--以下为测试内容，做动态处理时可删除--开始-->
                        <%--<h2>约会主题</h2>

                        <p>1. 和刘友常面对面深度交谈，聊聊产品，聊聊商业模式，关于你的项目，或者其他创业者的故事。</p>

                        <p>2. 分享12308的创业经验和观点，及产品管理经验，相信有他的独到之处。</p>

                        <p>3. 分享人生经历，认识靠谱的朋友。</p>

                        <h2>为什么要和我约会</h2>

                        <p>关注互联网/电商/移动互联。12308商旅电商平台创始人，目前已完成C轮的融资。现创办星语心愿社交电商平台，同时为为创业团队提供麓谷办公场地，并为创业团队提供技术支持。</p>

                        <p>
                            <img src="${ctx}/assets/images/content/content_01.jpg" alt=""/>
                            <img src="${ctx}/assets/images/content/content_02.jpg" alt=""/>
                            <img src="${ctx}/assets/images/content/content_03.jpg" alt=""/>
                        </p>

                        <p class="center">12308深圳我行我速电子商务有限公司 第2排左1</p>

                        <p>
                            在11月19日下午，2013年黑马大赛年度50强震撼发布。这是黑马大赛历经一年的分赛历程之后，层层选拔出的优质黑马企业，它们之中将产生最后的年度总冠军。 <br/>
                            12308全国公路客运预订平台，深圳我行我速电子商务有限公司首次参赛并进入50强。而黑马大赛作为中国第一投融资平台，已经为数千家黑马企业提供综合创业服务，而黑马大赛成立三年来，已经累积完成融资超过110亿元。
                        </p>

                        <p>
                            2013年度黑马大赛50强的发布，是对中国2013年创新创业的一次系统性的盘点，也是为未来的中国商业寻找最具创新活力的企业。总体而言，本届黑马大赛50强呈现出以下几大特点。
                        </p>--%>
                        <!--以上为测试内容，做动态处理时可删除--结束-->
                    </div>
                    <!--详情内容结束-->

                    <!--评价内容开始-->
                    <div class="detail_common detail_comment" id="detail_comment">
                        <!--没有评论情况开始-->
                        <!--<div class="comment_none">
                            暂时没有任何评论！快去抢沙发~
                        </div>-->
                        <!--没有评论情况结束-->

                        <!--单条评论开始--单条评论遍历-->
                        <div class="comment_list clearfix">
                            <!--左边-评论人头像开始-->
                            <div class="comment_headpic">
                                <a href="" title="">
                                    <img src="${ctx}/assets/images/content/comment_small_01.jpg" alt=""/>
                                </a>
                            </div>
                            <!--左边-评论人头像结束-->

                            <!--右边-评论信息开始-->
                            <div class="comment_msg">
                                <!--评论人信息开始-->
                                <div class="msg_people">
                                    <ul class="clearfix">
                                        <li class="left name">巧克力</li>
                                        <li class="left address ">
                                            <em>30岁</em>,<em>湖南 长沙</em>
                                        </li>
                                        <li class="right more">
                                            <i class="icon_service icon_down"></i>
                                            <ul>
                                                <li><a href="">举报</a></li>
                                            </ul>
                                        </li>
                                        <li class="right time">2015-11-24 15：52</li>
                                    </ul>
                                </div>
                                <!--评论人信息结束-->

                                <!--服务印象开始-->
                                <div class="msg_service">
                                    <ul class="clearfix">
                                        <li class="title">
                                            服务印象：
                                        </li>
                                        <li class="clearfix">
                                            <i class="icon_service icon_header"></i>
                                            <i class="icon_service icon_footer">专业</i>
                                        </li>
                                        <li class="clearfix">
                                            <i class="icon_service icon_header"></i>
                                            <i class="icon_service icon_footer">耐心</i>
                                        </li>
                                    </ul>
                                </div>
                                <!--服务印象结束-->

                                <!--评论描述开始-->
                                <div class="msg_desc">
                                    拉伸热身、身体开度、形体训练、体能训练、身体控制、爵士舞基本功、
                                    舞蹈教学技巧、舞蹈表演技巧、爵士舞编舞技巧拉伸热身身体开度、形体训练、体能训练、身体控制、爵士舞基本功、舞蹈教学技巧舞蹈表演技巧爵士舞编舞技巧
                                </div>
                                <!--评论描述结束-->

                                <!--评论附属图片开始-->
                                <div class="msg_imglist" id="msg_imglist">
                                    <!-- layer-src表示大图  layer-pid表示图片id  src表示缩略图-->
                                    <ul class="clearfix">
                                        <li><img layer-src="${ctx}/assets/images/content/content_01.jpg"
                                                 src="${ctx}/assets/images/content/content_01.jpg" alt="附属图片1"
                                                 layer-pid=""/></li>
                                        <li><img layer-src="${ctx}/assets/images/content/content_02.jpg"
                                                 src="${ctx}/assets/images/content/content_02.jpg" alt="附属图片2"
                                                 layer-pid=""/></li>
                                        <li><img layer-src="${ctx}/assets/images/content/content_03.jpg"
                                                 src="${ctx}/assets/images/content/content_03.jpg" alt="附属图片3"
                                                 layer-pid=""/></li>
                                    </ul>
                                </div>
                                <!--评论附属图片结束-->
                            </div>
                            <!--右边-评论信息结束-->
                        </div>
                        <!--单条评论结束--单条评论遍历-->

                        <!--分页结构开始-->
                        <div class="web_page clearfix" id="web_page">
                            <ul>
                                <li class="link prev">
                                    <a href="" title=""><em><</em> 上一页</a>
                                </li>
                                <li class="link current"><!--当前页样式current-->
                                    <a href="" title="">1</a>
                                </li>
                                <li class="link">
                                    <a href="" title="">2</a>
                                </li>
                                <li class="link">
                                    <a href="" title="">3</a>
                                </li>
                                <li class="link">
                                    <a href="" title="">4</a>
                                </li>
                                <li class="link">
                                    <a href="" title="">5</a>
                                </li>
                                <li class="link">
                                    <a href="" title="">6</a>
                                </li>
                                <li class="more">
                                    ...
                                </li>
                                <li class="link next">
                                    <a href="" title="">下一页 <em>></em></a>
                                </li>
                                <li class="total">
                                    共<em>50</em>页
                                </li>
                                <li class="jump">
                                    到第<input type="text" value="1" name="pagenum"/>页
                                    <span class="submit_page">确定</span>
                                </li>
                            </ul>
                        </div>
                        <!--分页结构结束-->

                    </div>
                    <!--评价内容结束-->

                    <!--我的案例开始-->
                    <div class="detail_common detail_content detail_case" id="detail_case">
                        <!--以下为测试内容，做动态处理时可删除--开始-->
                        <p>
                            1、2012年投资12308汽车票，12308是由中国道路运输协会和全国站场工作委员会合作共建的全国公路客运互联网信息服务平台。
                            广大旅客通过手机端（客户端app或者微信公众号）或者PC端均可直接登录平台查询出行所需的车站、车次、线路、票价等信息，并购买出行汽车票。
                            12308通过网站及客户端的全平台覆盖，从而为广大旅客提供更为优化便捷的出行解决方案。
                        </p>

                        <p>
                            <img src="${ctx}/assets/images/content/case_01.jpg" alt=""/>
                            <img src="${ctx}/assets/images/content/case_02.jpg" alt=""/>
                        </p>

                        <p>视频链接地址：http://tv.people.com.cn/n/2015/0213/c39805-26560811.html</p>

                        <p>
                            2、2015年创办鼎晖投资，鼎晖投资的和美医疗集团是国内第一家登录资本巿场的妇产专科连锁医疗集团，上市后更多的社会资本的注入以及医疗改革的推进、
                            大力扶持私立医疗机构发展、全面放开二胎等一系列利好政策的推动下，凭借其品牌定位、资本、规模，为和美医疗终将成为中国第一高端妇儿医疗集团提供了可能。根据弗若斯特沙利文的行业研究报告数据，
                            2009年至2013年，我国妇幼保健院复合年增长率为2.0%，而私立妇产专科医院复合年增长率为18.0%，增长率远高于公立妇产医院。
                        </p>

                        <p>
                            <img src="${ctx}/assets/images/content/case_03.jpg" alt=""/>
                        </p>
                        <!--以上为测试内容，做动态处理时可删除--结束-->
                    </div>
                    <!--我的案例结束-->

                </div>
                <!--底部显示结束-->

            </div>
            <!--服务详情、评价、案例结束-->

        </div>
        <!--左侧服务内容结束-->

        <!--右侧人物介绍开始-->
        <div class="container_right" style="display: none;">
            <div class="people_focus clearfix">
                <!--主体居中开始-->
                <div class="people_focus_main">
                    <div class="focus_img">
                        <a href="" title=""><img src="${ctx}/assets/images/content/detail_people.png" src-default="${ctx}/assets/images/default_headpic.jpg" alt=""/></a>
                    </div>
                    <div class="focus_intro">
                        <span class="name">刘友常</span>
						<span class="label">
							<i class="icon_service icon_zheng"></i>
	                    	<i class="icon_service icon_v_member"></i>
						</span>
						<span class="focus_on">
							<!--未关注-->
							<em class="btn onfocus cmain_bg_color">+ 关注</em>
							<!--已关注-->
							<!--<em class="btn offfocus bg_gray">已关注</em>-->
						</span>
                    </div>
                </div>
                <!--主体居中结束-->
            </div>
            <div class="people_intro">
                <ul>
                    <li class="label clearfix" style="display: none;">
						<span class="clearfix underscore-template hover"><!--高亮样式hover-->
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">{{=name}}</i>
						</span>
                    </li>
                    <li class="description">
                        关注互联网/电商/移动互联。12308商旅电商平台创始人，目前已完成C轮的融资。现创办星语心愿社交电商平台，同时为为创业团队提供麓谷办公场地，并为创业团队提供技术支持。
                    </li>
                    <li class="resfuse">
                        <h3>关于退款</h3>
                        订单取消时，已冻结金额将退至您的余额中。您可以选择下次继续预约，或申请退款至您的支付宝账户。
                    </li>
                </ul>
            </div>
        </div>
        <!--右侧人物介绍结束-->

    </div>
    <!--服务详情内容结束-->

</div>
<!--身体结束-->


<javascript-list>
    <!--etalage图片缩放插件开始-->
    <script type="text/javascript" src="${ctx}/assets/plugins/etalage/js/jquery.etalage.min.js"></script>
    <!--etalage图片缩放插件结束-->
    <!--日期插件datepicker开始-->
    <script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
    <!--日期插件datepicker结束-->
    <script type="text/javascript" src="${ctx}/assets/js/service/detail.js"></script>
</javascript-list>
</body>
</html>
