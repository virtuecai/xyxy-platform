<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title>星语星愿-首页</title>

    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/index/index.css" rel="stylesheet" type="text/css" />
    <!--页面Css样式结束-->

</head>
<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp">
    <jsp:param name="slideDisabled" value="true"/>
</jsp:include>

    <!--身体开始-->
<div class="layer clearfix">

    <!--banner轮播开始-->
    <div class="banner" id="banner">
        <div class="bd">
            <ul>
                <li style="background:url(${ctx}/assets/images/content/banner_02.jpg) #fff center 0 repeat;">
                    <div class="banner_info">
                        <!--banner信息列表开始-->
                        <div class="banner_info_main">

                            <div class="info_common info_list">
                                <h2 class="clearfix">
                                    <span><em>刘友常</em>的星店</span>
                                    <a href="" class="more">更多&nbsp;>></a>
                                </h2>
                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">投资咨询</span>
                                        <span class="right"><em class="orange">免费咨询</em></span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">投资行业入行指导，职业发展咨询，创业导师及投资方面的帮助.......</span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">天使投资</span>
                                        <span class="right"><em class="orange">免费咨询</em></span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">咖啡馆的一小时，与我面对面，谈谈我们都感兴趣的这个世界。</span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">巴菲特午餐</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">咖啡馆的一小时，与我面对面，谈谈我们都感兴趣的这个世界。</span>
                                        <span class="right">起拍价<em class="orange">800</em> 元/次</span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                            </div>

                            <div class="info_common info_link">
                                <a href="" title="">我要开星店 >></a>
                            </div>
                        </div>
                        <!--banner信息列表结束-->
                    </div>
                </li>
                <li style="background:url(${ctx}/assets/images/content/banner_01.jpg) #fff center 0 repeat;">
                    <div class="banner_info">
                        <!--banner信息列表开始-->
                        <div class="banner_info_main">

                            <div class="info_common info_list">
                                <h2 class="clearfix">
                                    <span><em>章子怡</em>的星店</span>
                                    <a href="" class="more">更多&nbsp;>></a>
                                </h2>

                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">金融证券股票咨询</span>
                                        <span class="right"><em class="orange">500</em> 元/次</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">聊聊当前证券、股票咨询行业走势。分享我个人在金融业的经验和观点，注意事项，我会将给你最优的方案。股票开户。</span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">瑜伽教练</span>
                                        <span class="right"><em class="orange">500</em> 元/一次</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">帮助你了解你平时生活的习惯和工作情况，帮你做身体的测试诊断，比如脚跟、骨盆、胸腔等等......</span>
                                        <a href="" class="right detail">去预约</a>
                                    </p>
                                </div>
                            </div>

                            <div class="info_common info_link">
                                <a href="" title="">我要开星店 >></a>
                            </div>
                        </div>
                        <!--banner信息列表结束-->
                    </div>
                </li>
                <li style="background:url(${ctx}/assets/images/content/banner_03.jpg) #fff center 0 repeat;">
                    <div class="banner_info">
                        <!--banner信息列表开始-->
                        <div class="banner_info_main">

                            <div class="info_common info_list">
                                <h2 class="clearfix">
                                    <span><em>周浪</em>的星店</span>
                                    <a href="" class="more">更多&nbsp;>></a>
                                </h2>

                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">台球助教</span>
                                        <span class="right"><em class="orange">200</em> 元/小时</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">面对面深度沟通台球知识，聊聊关于台球技巧，比赛经验。现场指导台球技术，使你的球技短期内得到快速提升。分享台球大赛的经历，谈论比赛当中的细节、心态。  </span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                                <%--<div class="info_list_function">--%>
                                    <%--<p class="clearfix head">--%>
                                        <%--<span class="left title">企业标志设计</span>--%>
                                        <%--<span class="right"><em class="orange">1000</em> 元/小时</span>--%>
                                    <%--</p>--%>
                                    <%--<p class="clearfix bottom">--%>
                                        <%--<span class="left">咖啡馆的一小时，与我面对面，谈谈我们都感兴趣的这个世界。</span>--%>
                                        <%--<a href="" class="right detail">去预约</a>--%>
                                    <%--</p>--%>
                                <%--</div>--%>
                            </div>

                            <div class="info_common info_link">
                                <a href="" title="">我要开星店 >></a>
                            </div>
                        </div>
                        <!--banner信息列表结束-->
                    </div>
                </li>
                <li style="background:url(${ctx}/assets/images/content/banner_04.jpg) #fff center 0 repeat;">
                    <div class="banner_info">
                        <!--banner信息列表开始-->
                        <div class="banner_info_main">

                            <div class="info_common info_list">
                                <h2 class="clearfix">
                                    <span><em>凌兵</em>的星店</span>
                                    <a href="" class="more">更多&nbsp;>></a>
                                </h2>

                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">怎样成为一名合格的产品经理</span>
                                        <span class="right"><em class="orange">500</em> 元/一次</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">竞品分析、需求分析、原型设计、写需求文档和上线运营等方面的咨询，聊一聊个人的定位和如何才能成为产品经理。</span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">怎样成为一名自由摄影师</span>
                                        <span class="right"><em class="orange">500</em> 元/一次</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">面对面探讨摄影师这个行业的真实情况，帮你评估你的能力是否已经达到技术要求，帮你找准摄影发展的领域，以及一些职业化建议......</span>
                                        <a href="" class="right detail">去预约</a>
                                    </p>
                                </div>
                            </div>

                            <div class="info_common info_link">
                                <a href="" title="">我要开星店 >></a>
                            </div>
                        </div>
                        <!--banner信息列表结束-->
                    </div>
                </li>
                <li style="background:url(${ctx}/assets/images/content/banner_05.jpg) #fff center 0 repeat;">
                    <div class="banner_info">
                        <!--banner信息列表开始-->
                        <div class="banner_info_main">

                            <div class="info_common info_list">
                                <h2 class="clearfix">
                                    <span><em>唐祥飞</em>的星店</span>
                                    <a href="" class="more">更多&nbsp;>></a>
                                </h2>

                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">定制化人像摄影</span>
                                        <span class="right"><em class="orange">1000</em> 元/小时</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">面对面的聊一聊摄影的那些年，咨询摄影过程中的问题。也可以为你量身定做适合你的艺术照，让你做百变达人。</span>
                                        <a href="" class="right detail">加入心愿车</a>
                                    </p>
                                </div>
                                <div class="info_list_function">
                                    <p class="clearfix head">
                                        <span class="left title">企业标志设计</span>
                                        <span class="right"><em class="orange">1000</em> 元/小时</span>
                                    </p>
                                    <p class="clearfix bottom">
                                        <span class="left">最可靠的企业标志摄影，LOGO的摄影处理。想要最专业的企业标志摄影就来找我吧。</span>
                                        <a href="" class="right detail">去预约</a>
                                    </p>
                                </div>
                            </div>

                            <div class="info_common info_link">
                                <a href="" title="">我要开星店 >></a>
                            </div>
                        </div>
                        <!--banner信息列表结束-->
                    </div>
                </li>
            </ul>
        </div>
        <div class="hd"><ul></ul></div>
    </div>
    <!--banner轮播结束-->

    <!--提供的服务、保障开始-->
    <div class="service index_service">
        <ul class="clearfix">
            <li>
                <i class="icon_index icon_five5"></i>
                <span>五大服务保障</span>
            </li>
            <li>
                <i class="icon_index icon_smrz"></i>
                <span>实名认证</span>
            </li>
            <li>
                <i class="icon_index icon_zzrz"></i>
                <span>资质认证</span>
            </li>
            <li>
                <i class="icon_index icon_zfaq"></i>
                <span>支付安全</span>
            </li>
            <li>
                <i class="icon_index icon_zyfw"></i>
                <span>专业服务</span>
            </li>
            <li>
                <i class="icon_index icon_shwy"></i>
                <span>售后无忧</span>
            </li>
        </ul>
    </div>
    <!--提供的服务、保障结束-->

    <div class="space_hx">&nbsp;</div>

    <!--1F开始-->
    <div class="floor_common floor_first" id="floor_first">

        <!--楼层头部开始-->
        <div class="floor_common_head floor_first_head clearfix">
            <div class="floor_head_box floor_title">
                <i class="icon_index icon_f">1F</i>
                <span>热门星会员</span>
            </div>
            <div class="floor_head_box floor_tips">
                已推荐优秀星会员<em class="orange">${recommend_member}</em>人！目前已发布服务的星会员<em class="orange">${goods_member}</em>人！———— 你加入我们了吗？
            </div>
            <div class="floor_head_box floor_handle">
            	<span class="refresh" id="hot_member_change">
            		<i class="icon_index icon_refresh"></i> 换一批
            	</span>
                <a class="orange" href="${ctx}/shop/index" >更多&nbsp;>></a>
            </div>
        </div>
        <!--楼层头部结束-->

        <!--楼层底部开始-->
        <div class="floor_common_bottom floor_first_bottom" id="floor_first_bottom">
            <div class="floor_common_bottom_main clearfix" id="hot_member_list">
                <dl class="clearfix underscore-template">
                    <dt>
                        <a href="${ctx}/shop/{{=member.id}}/home" title="">
                            <img data-src="{{= imageItemList.length > 0 ? imageItemList[0].uri : 'error.xxx'}}" src-default="${ctx}/assets/images/default_headpic.jpg">
                        </a>
                    </dt>
                    <dd class="name clearfix">
                        <em>{{=member.realName?member.realName:member.alias}}</em>
                        <i class="icon_index icon_zheng"></i>
                        <i class="icon_index icon_v_member"></i>
                    </dd>
                    <dd class="label clearfix">
                        {{ _.each(tagList, function(item){ }}
                        <span class="clearfix">
		                    <i class="icon_index icon_header"></i>
		                    <i class="icon_footer">{{=item.name}}</i>
	                    </span>
                        {{ }) }}
                        {{ if(!tagList || tagList.length == 0) { }}
                             <span class="clearfix">
                                <i class="icon_index icon_header"></i>
                                <i class="icon_footer">暂无标签</i>
                            </span>
                        {{ } }}
                    </dd>
                    <dd class="intro">
                        {{=member.articleContent!=''?member.articleContent:'暂无介绍'}}
                    </dd>

                    <!--遮罩显示内容开始-->
                    <dd class="member_service" id="member_service" style="{{=goods.length <= 0 ? 'display:none;': ''}}">
                        <div class="service_list">
                            {{ _.each(goods, function(item){ }}
                            <p class="clearfix">
                                <span class="left"><em>&gt;</em>{{=item.goodsName}}</span>
                                <span class="right orange">￥{{=item.goodPrice}}元</span>
                            </p>
                            {{ }) }}
                        </div>
                        <a href="${ctx}/shop/{{=member.id}}/home" class="cmain_bg_color subscribe">预约</a>
                    </dd>
                    <!--遮罩显示内容结束-->
                </dl>
            </div>
        </div>
        <!--楼层底部结束-->
    </div>
    <!--1F结束-->

    <div class="space_hx">&nbsp;</div>

    <!--2F开始-->
    <div class="floor_common floor_second floor_common_list" id="floor_second">

        <!--楼层头部开始-->
        <div class="floor_common_head floor_second_head clearfix">
            <div class="floor_head_box floor_title">
                <i class="icon_index icon_f">2F</i>
                <span>最新服务</span>
            </div>
            <div class="floor_head_box floor_tips">
                大家在星语星愿已累计发布服务<em class="orange">${publish_goods}</em>人/次！快来看看今日被大家约单、推荐的服务吧！
            </div>
            <div class="floor_head_box floor_handle">
            	<span class="refresh" id="new_goods_change">
            		<i class="icon_index icon_refresh"></i> 换一批
            	</span>
                <a class="orange" href="${ctx}/service/index" >更多&nbsp;>></a>
            </div>
        </div>
        <!--楼层头部结束-->

        <!--楼层底部开始-->
        <div class="floor_common_bottom floor_second_botttom">
            <div class="floor_common_bottom_main clearfix" id="new_goods_list">
                <dl class="clearfix underscore-template">
                    <dt>
                        <a href="${ctx}/service/detail/{{=goodId}}">
                        <img data-src="{{='http://static.xyxy.com/system_files/files/' + goodPic.substr(goodPic.lastIndexOf('/') + 1)}}" src-default="${ctx}/assets/images/default_content.png" />
                        </a>
                    </dt>
                    <dd class="price clearfix">
                        <span><i>￥</i><em>{{=goodPrice}}</em>元/{{=goodUnit}}</span>
                        <span class="linkurl"><a href="${ctx}/service/detail/{{=goodId}}" title="" class="cmain_bg_color">预约</a></span>
                    </dd>
                    <dd class="title clearfix"><a href="${ctx}/service/detail/{{=goodId}}">{{=goodsName}}</a></dd>
                    <dd class="member clearfix">
                        <a class="headpic clearfix" href="${ctx}/shop/{{=memberId?memberId:0}}/home" title="">
                            <img src="${ctx}/assets/images/default_headpic.jpg" alt=""  />
                            <em>{{=nickName}}</em>
                        </a>
	                	<a class="address">
                            {{ _.each(address.slice(0,2), function(item){ }}
                                {{=item.shortname}}
                            {{ }) }}
	                	</a>
                    </dd>
                </dl>
            </div>
        </div>
        <!--楼层头部结束-->

    </div>
    <!--2F结束-->

    <div class="space_hx">&nbsp;</div>

    <!--3F开始-->
    <div class="floor_common floor_third floor_common_list" id="floor_third">

        <!--楼层头部开始-->
        <div class="floor_common_head floor_third_head clearfix">
            <div class="floor_head_box floor_title">
                <i class="icon_index icon_f">3F</i>
                <span>互联网</span>
            </div>
            <div class="floor_head_box floor_tips">
                大家在星语星愿已累计发布服务<em class="orange">${publish_goods}</em>人/次！快来看看今日被大家约单、推荐的服务吧！
            </div>
            <div class="floor_head_box floor_handle">
            	<span class="refresh" id="key1_goods_change">
            		<i class="icon_index icon_refresh"></i> 换一批
            	</span>
                <a class="orange" href="${ctx}/service/index" >更多&nbsp;>></a>
            </div>
        </div>
        <!--楼层头部结束-->

        <!--楼层底部开始-->
        <div class="floor_common_bottom floor_third_botttom">
            <div class="floor_common_bottom_main clearfix" id="key1_goods_list">
                <dl class="clearfix underscore-template">
                    <dt>
                        <a href="${ctx}/service/detail/{{=goodId}}">
                            <img data-src="{{='http://static.xyxy.com/system_files/files/' + goodPic.substr(goodPic.lastIndexOf('/') + 1)}}"  src-default="${ctx}/assets/images/default_content.png" />
                        </a>
                    </dt>
                    <dd class="price clearfix">
                        <span><i>￥</i><em>{{=goodPrice}}</em>元/{{=goodUnit}}</span>
                        <span class="linkurl"><a href="${ctx}/service/detail/{{=goodId}}" title="" class="cmain_bg_color">预约</a></span>
                    </dd>
                    <dd class="title clearfix"><a href="${ctx}/service/detail/{{=goodId}}">{{=goodsName}}</a></dd>
                    <dd class="member clearfix">
                        <a class="headpic clearfix" href="${ctx}/shop/{{=memberId?memberId:0}}/home" title="">
	                		 <img src="${ctx}/assets/images/default_headpic.jpg" alt=""  />
	                		<em>{{=nickName}}</em>
                        </a>
	                	<a class="address">
	                		{{ _.each(address.slice(0,2), function(item){ }}
                                {{=item.shortname}}
                            {{ }) }}
	                	</a>
                    </dd>
                </dl>
            </div>
        </div>
        <!--楼层头部结束-->

    </div>
    <!--3F结束-->

    <!--4F开始-->
    <div class="floor_common floor_four floor_common_list" id="floor_four">

        <!--楼层头部开始-->
        <div class="floor_common_head floor_third_head clearfix">
            <div class="floor_head_box floor_title">
                <i class="icon_index icon_f">4F</i>
                <span>运动健身</span>
            </div>
            <div class="floor_head_box floor_tips">
                大家在星语星愿已累计发布服务<em class="orange">${publish_goods}</em>人/次！快来看看今日被大家约单、推荐的服务吧！
            </div>
            <div class="floor_head_box floor_handle">
            	<span class="refresh" id="key2_goods_change">
            		<i class="icon_index icon_refresh"></i> 换一批
            	</span>
                <a class="orange" href="${ctx}/service/index" >更多&nbsp;>></a>
            </div>
        </div>
        <!--楼层头部结束-->

        <!--楼层底部开始-->
        <div class="floor_common_bottom floor_third_botttom">
            <div class="floor_common_bottom_main clearfix" id="key2_goods_list">
                <dl class="clearfix underscore-template">
                    <dt>
                        <a href="${ctx}/service/detail/{{=goodId}}">
                            <img data-src="{{=goodPic}}"  src-default="${ctx}/assets/images/default_content.png" />
                        </a>
                    </dt>
                    <dd class="price clearfix">
                        <span><i>￥</i><em>{{=goodPrice}}</em>元/{{=goodUnit}}</span>
                        <span class="linkurl"><a href="${ctx}/service/detail/{{=goodId}}" title="" class="cmain_bg_color">预约</a></span>
                    </dd>
                    <dd class="title clearfix"><a href="${ctx}/service/detail/{{=goodId}}">{{=goodsName}}</a></dd>
                    <dd class="member clearfix">
                        <a class="headpic clearfix" href="${ctx}/shop/{{=memberId?memberId:0}}/home" title="">
                            <img src="${ctx}/assets/images/default_headpic.jpg" alt=""  />
                            <em>{{=nickName}}</em>
                        </a>
                        <a class="address">
                            {{ _.each(address.slice(0,2), function(item){ }}
                            {{=item.shortname}}
                            {{ }) }}
                        </a>
                    </dd>
                </dl>
            </div>
        </div>
        <!--楼层头部结束-->

    </div>
    <!--4F结束-->

    <!--5F开始-->
    <div class="floor_common floor_five floor_common_list" id="floor_five">

        <!--楼层头部开始-->
        <div class="floor_common_head floor_third_head clearfix">
            <div class="floor_head_box floor_title">
                <i class="icon_index icon_f">5F</i>
                <span>颜值变现</span>
            </div>
            <div class="floor_head_box floor_tips">
                大家在星语星愿已累计发布服务<em class="orange">${publish_goods}</em>人/次！快来看看今日被大家约单、推荐的服务吧！
            </div>
            <div class="floor_head_box floor_handle">
            	<span class="refresh" id="key3_goods_change">
            		<i class="icon_index icon_refresh"></i> 换一批
            	</span>
                <a class="orange" href="${ctx}/service/index" >更多&nbsp;>></a>
            </div>
        </div>
        <!--楼层头部结束-->

        <!--楼层底部开始-->
        <div class="floor_common_bottom floor_third_botttom">
            <div class="floor_common_bottom_main clearfix" id="key3_goods_list">
                <dl class="clearfix underscore-template">
                    <dt>
                        <a href="${ctx}/service/detail/{{=goodId}}">
                            <img data-src="{{='http://static.xyxy.com/system_files/files/' + goodPic.substr(goodPic.lastIndexOf('/') + 1)}}"  src-default="${ctx}/assets/images/default_content.png" />
                        </a>
                    </dt>
                    <dd class="price clearfix">
                        <span><i>￥</i><em>{{=goodPrice}}</em>元/{{=goodUnit}}</span>
                        <span class="linkurl"><a href="${ctx}/service/detail/{{=goodId}}" title="" class="cmain_bg_color">预约</a></span>
                    </dd>
                    <dd class="title clearfix"><a href="${ctx}/service/detail/{{=goodId}}">{{=goodsName}}</a></dd>
                    <dd class="member clearfix">
                        <a class="headpic clearfix" href="${ctx}/shop/{{=memberId?memberId:0}}/home" title="">
                            <img src="${ctx}/assets/images/default_headpic.jpg" alt=""  />
                            <em>{{=nickName}}</em>
                        </a>
                        <a class="address">
                            {{ _.each(address.slice(0,2), function(item){ }}
                            {{=item.shortname}}
                            {{ }) }}
                        </a>
                    </dd>
                </dl>
            </div>
        </div>
        <!--楼层头部结束-->

    </div>
    <!--5F结束-->

    <div class="space_hx">&nbsp;</div>

    <!--交易数据展示开始-->
    <div class="data_show"  id="data_show">
        <div class="data_show_common title clearfix">
            <span class="words"><em>中国领先的众包服务平台</em></span>
        </div>
        <ul class="data_show_common clearfix">
            <li>
                <span class="icon_index icon_fws"></span>
                <span class="nums">
                	<em class="timer" data-to="13360125" data-speed="2500">13360125</em>&nbsp;人
                    <em class="info">服务商</em>
                </span>
            </li>
            <li>
            	<span class="icon_index icon_jyl">
                </span>
                <span class="nums">
                	<em class="timer" data-to="13360125" data-speed="2500">13360125</em>&nbsp;个
                    <em class="info">交易量</em>
                </span>
            </li>
            <li>
            	<span class="icon_index icon_jyje">
                </span>
                <span class="nums">
                	<em class="timer" data-to="13360125" data-speed="2500">13360125</em>&nbsp;元
                    <em class="info">交易金额</em>
                </span>
            </li>
        </ul>
    </div>
    <!--交易数据展示结束-->

</div>
<!--身体结束-->

<!--身体左边楼层导航开始-->
<div class="floor_fixed_nav" id="floor_fixed_nav">
    <a href="#floor_first" title="">
        <span class="num">1F</span>
        <span class="word">热门新会员</span>
    </a>
    <a href="#floor_second" title="">
        <span class="num">2F</span>
        <span class="word">最新服务</span>
    </a>
    <a href="#floor_third" title="">
        <span class="num">3F</span>
        <span class="word">互联网</span>
    </a>
    <a href="#floor_four" title="">
        <span class="num">4F</span>
        <span class="word">运动健身</span>
    </a> 
        <a href="#floor_five" title="">
        <span class="num">5F</span>
        <span class="word">颜值变现</span>
    </a>    
</div>
<script type="text/javascript" >
    indexFlag = true;
</script>
<!--身体左边楼层导航结束-->

<javascript-list>
    <!--superslide插件开始-->
    <script type="text/javascript" src="${ctx}/assets/plugins/superslide/jquery.superslide.2.1.1.js"></script>
    <!--superslide插件结束-->
    <!--页面JS开始-->
    <script type="text/javascript" src="${ctx}/assets/js/index/jquery.highlight.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/index/index.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/application.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/index/pageination.js"></script>
    <!--页面JS结束-->

</javascript-list>
</body>
</html>


