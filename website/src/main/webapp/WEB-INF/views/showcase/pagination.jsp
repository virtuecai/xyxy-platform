<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
	<title>分页插件使用示例</title>
	<style>
		table {
			margin-left: 200px;;
		}
		table,tr,th,td {
			border: grey solid 1px;
		}

	</style>
</head>

<body>

<div class="position">
	<ul class="clearfix">
		<li>
			<a href="#" title="">首页</a>
		</li>
		<li class="line">></li>
		<li>
			<a href="#" title="">示例</a>
		</li>
		<li class="line">></li>
		<li>
			<a href="#" title="">分页插件</a>
		</li>
	</ul>
</div>

<div class="layer clearfix" style="margin: 20px 100px;">

	<div class="service_container clearfix">
		
		<div class="container_left">
			分类名like: <input type="text" name="categoryName" value="创"/>
			<button id="searchBtn">搜索</button>
			<table style="margin-bottom: 50px;">
				<thead>
					<tr>
						<th style="width: 100px;">ID</th>
						<th style="width: 100px;">名称</th>
						<th style="width: 100px;">创建时间</th>
					</tr>
				</thead>
				<tbody>
				<tr class="underscore-template">
					<td>{{=categoryId}}</td>
					<td>{{=categoryType}}</td>
					<td>{{=moment(createTime).format("YYYY-MM-DD")}}</td>
				</tr>
				</tbody>
			</table>

			<!-- 分页元素 -->
			<div class="mricode-pagination"></div>

		</div>

	</div>

</div>

<!-- 简单示例 -->
<script id="data_item_template" type="text/template">
	{{ _.each(items, function(item) { }}
	<tr>
		<td>{{= item.categoryId }}</td>
		<td>{{= item.categoryType }}</td>
		<td>{{= moment(item.createTime).format("YYYY-MM-DD") }}</td>
	</tr>
	{{ }) }}
</script>


<javascript-list>
	<script type="text/javascript" src="${ctx}/assets/js/showcase/pagination.js"></script>
</javascript-list>



</body>
</html>
