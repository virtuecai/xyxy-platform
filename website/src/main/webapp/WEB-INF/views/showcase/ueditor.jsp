<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
	<title>百度UEditor</title>
</head>

<body>

<div class="position">
	<ul class="clearfix">
		<li>
			<a href="#" title="">首页</a>
		</li>
		<li class="line">></li>
		<li>
			<a href="#" title="">示例</a>
		</li>
		<li class="line">></li>
		<li>
			<a href="#" title="">百度UEditor</a>
		</li>
	</ul>
</div>
<div style="width: 500px; margin: auto auto;">
	<!-- 加载编辑器的容器 -->
	<script id="container" name="content" type="text/plain">
        这里写你的初始化内容
	</script>
</div>


<javascript-list>
	<script type="text/javascript" src="${ctx}/assets/plugins/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="${ctx}/assets/plugins/ueditor/ueditor.all.xyxy.js"></script>
	<!-- 实例化编辑器 -->
	<script type="text/javascript">
		var ue = UE.getEditor('container');
	</script>
</javascript-list>



</body>
</html>
