<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
    <title>管理服务_我的服务_星语星愿</title>

    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/member/service_manage.css" rel="stylesheet" type="text/css"/>
    <!--页面Css样式结束-->
</head>

<body>

<body-child-1>
    <!--头部面包屑开始-->
    <div class="position">
        <ul class="clearfix">
            <li>
                <a href="#" title="">管理中心</a>
            </li>
            <li class="line">></li>
            <li>
                <a href="#" title="">我的服务</a>
            </li>
            <li class="line">></li>
            <li>
                <a href="#" title="">管理服务</a>
            </li>
        </ul>
    </div>
    <!--头部面包屑结束-->
</body-child-1>

<!--右侧内容部分开始-->
<div class="container_right">
    <h2>管理服务</h2>

    <div class="container_service">
        <!--服务状态开始-->
        <div class="service_status">
            <ul class="clearfix">
                <li class="hover" data-goods_status="">
                    <a href="javascript:void(0);">全部服务</a>
                </li>
                <li data-goods_status="1">
                    <a href="javascript:void(0);">开启的服务</a>
                </li>
                <li data-goods_status="0">
                    <a href="javascript:void(0);">关闭的服务</a>
                </li>
               <%-- <li>
                    <a href="javascript:void(0);" data-goods_status="">草稿箱</a>
                </li>--%>
            </ul>
        </div>
        <!--服务状态结束-->

        <!--服务列表开始-->
        <div class="service_list" id="service_list">

            <!--服务列表为空时开始-->
            <div class="service_none" style="display: none;">
                您的服务为空，快去挑选您喜欢的服务吧~
            </div>
            <!--服务列表为空时结束-->

            <!--服务列表遍历开始-->
            <div class="service_list_box underscore-template">
                <div class="list_head">
                    <ul class="clearfix">
                        <li class="time">发起时间：{{=updateTime?moment(updateTime).format('YYYY-MM-DD'):'--'}}</li>
                        <li class="status">
                            服务状态：<em class="gray{{= isSale==1?'gray':'green'}}">{{= isSale==1?'已开启': '已关闭' }}</em>
                            <!--<em class="">草稿</em>
                            <em class="green">已开启</em>-->
                        </li>
                    </ul>
                </div>
                <div class="list_content">
                    <ul class="clearfix">
                        <li class="thumb">
                            <a href="${ctx}/service/detail/{{=goodsId}}" target="_blank" > <img src-default="${ctx}/assets/images/default_content.png" data-src="{{=imageUrl?imageUrl:'errorToDefault'}}"/></a>
                        </li>
                        <li class="desc">
                            <a title="{{=goodsName}}" href="${ctx}/service/detail/{{=goodsId}}" target="_blank" class="title">{{=goodsName}}</a>
                            <a  href="${ctx}/service/detail/{{=goodsId}}" target="_blank"  class="intro"><<{{=serverManage.serverType[serverType]}}>></a>
                        </li>
                        <li class="deal">
                            <a title="删除服务" class="delete">删除服务</a>
                            <%--<a title="服务详情" href="${ctx}/service/detail/{{=goodsId}}" target="_blank">服务详情</a>--%>
                            <a title="" class="{{=isSale?'red close':'green open'}} update-is-sale">{{=isSale?'关闭服务':'开启服务'}}</a>
                            <a title="服务编辑" href="${ctx}/member/service/edit/{{=goodsId}}" class="blue edit">修改内容</a>
                            <!--<a title="" class="green open">开启服务</a>
                           <a title="" class="red close">关闭服务</a>-->
                        </li>
                        <li class="money"><span><em>{{=marketPrice.toFixed(2)}} </em>元</span>/每{{=goodsCompanyNote}}</li>
                    </ul>
                </div>
            </div>
            <!--服务列表遍历结束-->

        </div>
        <!--服务列表结束-->
        <!-- 分页 -->
        <div class="mricode-pagination" id="goods-pagination" style="margin-top: 15px;"></div>
    </div>
</div>
<!--右侧内容部分结束-->

<javascript-list>
    <script src="${ctx}/assets/js/member/service.manage.js"></script>
</javascript-list>
</body>
</html>
