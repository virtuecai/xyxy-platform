<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/15 0015
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta id="wxKeywords" name="Keywords" content="星语星愿" />
  <meta id="wxDescription" name="Description" content="星语星愿" />
  <title>头像设置_基本资料_管理中心_星语星愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/headpic.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->

  <!--头像上传插件Css开始-->
  <link href="${ctx}/assets/plugins/headcropper/css/cropper.min.css" rel="stylesheet" type="text/css" />
  <link href="${ctx}/assets/plugins/headcropper/css/main.css" rel="stylesheet" type="text/css" />
  <!--头像上传插件Css结束-->
</head>

<body>
<!--身体开始-->
<div class="layer clearfix">
  <!--个人资料内容开始-->
  <div class="member_container clearfix">

    <!--右侧内容部分开始-->
    <div class="container_right">
      <h2>头像设置</h2>
      <div class="headpic_form_box" id="headpic_form_box">
        <form class="headpic_form" name="headpic_form" method="" id="headpic_form">

          <!-- 上传头像内容开始 -->
          <div class="form_content">

            <div class="content_upload clearfix" id="content_upload">
              <div class="img-container">
                <img src="${ctx}/assets/images/content/2f_01.jpg" alt="个人头像">
              </div>
              <div class="docs-preview clearfix">
                <div class="img-preview preview-lg"></div>
                <div class="img-tpis">180px*180px</div>
                <div class="img-preview preview-md"></div>
                <div class="img-tpis">120px*120px</div>
                <div class="img-preview preview-sm"></div>
                <div class="img-tpis img-tpis-sm">64px*64px</div>
                <!--<div class="img-preview preview-xs"></div>
                <div class="img-previewtpis">50px*50px</div>-->
              </div>
            </div>

            <div class="content_del" id="content_del">
              <ul class="clearfix">
                <li class="btn upload_li" title="上传头像">
                  <input class="upload_thumb" id="inputImage" name="file" type="file" accept="image/*" />
                  重新上传
                </li>
                <!--<li class="btn" data-method="setDragMode" data-option="move" title="移动背景图">
                    移动背景图
                </li>
                <li class="btn" data-method="setDragMode" data-option="crop" title="Crop">
                    移动选框
                </li>-->
                <%--<li class="btn" data-method="zoom" data-option="0.1" title="放大">--%>
                  <%--放大--%>
                <%--</li>--%>
                <%--<li class="btn" data-method="zoom" data-option="-0.1" title="缩小">--%>
                  <%--放小--%>
                <%--</li>--%>
                <!--<li class="btn" data-method="disable" title="锁定">
                    锁定
                </li>
                <li class="btn" data-method="enable" title="解锁">
                    解锁
                </li>-->
                <%--<li class="btn" data-method="rotate" data-option="-45" title="向左旋转45度">--%>
                  <%--向左旋转45度--%>
                <%--</li>--%>
                <%--<li class="btn" data-method="rotate" data-option="45" title="向右旋转45度">--%>
                  <%--向右旋转45度--%>
                <%--</li>--%>
                <!--<li class="btn" data-method="reset" title="恢复默认">
                    恢复默认
                </li>-->
                <li class="btn form_submit" title="确定" id="form_submit">
                  提交设置
                </li>
              </ul>
            </div>

          </div>
          <!-- 上传头像内容结束 -->

        </form>
      </div>
    </div>
    <!--右侧内容部分结束-->

  </div>
  <!--个人资料内容结束-->

</div>
<!--身体结束-->



<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->

<javascript-list>
<!--头像上传插件headcropper开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/headcropper/js/cropper.min.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/member/headpic.js"></script>
<!--头像上传插件headcropper结束-->
<!--[if IE]>
<script type="text/javascript" src="http://libs.useso.com/js/html5shiv/3.7/html5shiv.min.js"></script>
<![endif]-->
<script type="text/javascript">
  $(function(){
    application.initPagePosition(['管理中心','基本资料', '头像设置']);
    //表单变量
    var	 moduleBox  = '#headpic_form_box',
            moduleForm = '#headpic_form';

    //提交表单
    $('#form_submit').on('click',function(){
      $(moduleForm).submit();
    });

    //选取头像工具
    $('#content_del>ul>li').on('click',function(){
      $(this).addClass('hover').siblings().removeClass('hover');
    });
  });
</script>
  </javascript-list>
</body>
</html>

