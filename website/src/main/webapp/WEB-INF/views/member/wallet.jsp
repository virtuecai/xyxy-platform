<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/13 0013
  Time: 11:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta id="wxKeywords" name="Keywords" content="星语星愿" />
  <meta id="wxDescription" name="Description" content="星语星愿" />
  <title>我的钱包_管理中心_星语星愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/wallet.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->
</head>

<body>
<!--身体开始-->
<div class="layer clearfix">

  <!--我的钱包内容开始-->
  <div class="member_container clearfix">
    <!--右侧内容部分开始-->
    <div class="container_right">
      <h2>我的钱包</h2>
      <div class="container_wallet">
        <c:choose>
          <c:when test="${empty myaccount || myaccount eq null}">
            <h3>账户余额：<em class="orange">0</em>元</h3>
          </c:when>
          <c:otherwise>
            <h3>账户余额：<em class="orange">${myaccount.userMoney}</em>元</h3>
          </c:otherwise>
        </c:choose>

        <!--钱包状态筛选开始-->
        <div class="wallet_filter">
          <ul class="clearfix">
            <li>选择时间：</li>
            <li class="time">
              <input type="text" name="start_time" id="start_time" vlaue="" onFocus="WdatePicker({minDate:'%y-%M-{%d}'})" placeholder="请选择开始时间" />
              <i class="icon_member icon_down">&nbsp;</i>
            </li>
            <li>-</li>
            <li class="time">
              <input type="text" name="end_time" id="end_time" vlaue="" onFocus="WdatePicker({minDate:'%y-%M-{%d}'})"  placeholder="请选择结束时间" />
              <i class="icon_member icon_down">&nbsp;</i>
            </li>
            <li class="link clearfix">
              <!--hover为当前高亮样式-->
              <a title="" class="btn hover">今天</a>
              <a title="" class="btn">最近一周</a>
              <a title="" class="btn">最近一月</a>
              <a title="" class="btn">最近三个月</a>
              <a title="" class="btn">一年内</a>
            </li>
          </ul>
        </div>
        <!--钱包状态筛选结束-->

        <!--钱包内容列表开始-->
        <div class="wallet_list">
          <div class="list_head">
            <ul>
              <li>交易记录</li>
            </ul>
          </div>
          <div class="list_bottom">
            <!--交易记录为空开始-->
            <!--<div class="record_none">
                您还没有任何交易记录~
            </div>-->
            <!--交易记录为空结束-->

            <!--交易记录开始-->
            <div class="record_common record_trade">
              <ul class="title clearfix">
                <li class="date">日期</li>
                <li class="order">订单信息</li>
                <li class="other">对方</li>
                <li class="type">类型</li>
                <li class="money">金额</li>
              </ul>
              <ul class="content">
                <!--列表数据遍历开始-->
                <li class="clearfix">
                  <span class="date">2015-11-20</span>
									<span class="order">
										<em class="name">陪跑步</em>
										<em>订单号：36356512415454</em>
									</span>
                  <span class="other">我是一个好人</span>
                  <span class="type">售出</span>
                  <span class="money green">+30</span>
                  <!--<span class="money red">-30</span>-->
                </li>
                <!--列表数据遍历结束-->
                <!--列表数据遍历开始-->
                <li class="clearfix">
                  <span class="date">2015-11-20</span>
									<span class="order">
										<em class="name">陪跑步</em>
										<em>订单号：36356512415454</em>
									</span>
                  <span class="other">我是一个好人</span>
                  <span class="type">购买</span>
                  <span class="money green">+30</span>
                  <!--<span class="money red">-30</span>-->
                </li>
                <!--列表数据遍历结束-->
              </ul>
            </div>
            <!--交易记录结束-->

          </div>
        </div>
        <!--钱包内容列表结束-->

      </div>
    </div>
    <!--右侧内容部分结束-->

  </div>
  <!--我的钱包内容结束-->

</div>
<!--身体结束-->


<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->
<javascript-list>
  <!--日期插件datepicker开始-->
  <script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
  <!--日期插件datepicker结束-->
  <script type="text/javascript">
    $(function(){
      application.initPagePosition(['管理中心','我的钱包', '钱包详情']);
    });
  </script>
</javascript-list>
</body>
</html>
