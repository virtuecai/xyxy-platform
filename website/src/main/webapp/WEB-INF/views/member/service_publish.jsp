<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
	<title>发布服务_我的服务_星语星愿</title>

	<!--页面Css样式开始-->
	<link href="${ctx}/assets/css/member/service_publish.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

</head>

<body>

<!--右侧内容部分开始-->
<div class="container_right">
	<h2>发布服务 (<em class="red">*</em>为必填项)</h2>
	<!--发布服务表单开始-->
	<div class="module_form_box service_form_box" id="service_form_box">
		<form class="module_form service_form" name="service_form" method="" id="service_form">
			<ul>
				<li class="title_li">
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">服务标题</span>
					</p>
					<p class="form_common form_text">
						<input type="text" class="text_common text_input width_600" id="title" name="title" placeholder="请输入服务标题" message="服务标题不能为空（20个字符以内的英文、中文、数字）" />
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<li>
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">服务分类</span>
					</p>
					<p class="form_common form_text">
						<select name="service_one" id="service_one" class="text_common text_select width_200" message="请选择一级服务分类">
							<option value="">请选择一级服务分类</option>
						</select>
						<select name="service_two" id="service_two" class="text_common text_select width_200" message="请选择二级服务分类">
							<option value="">请选择二级服务分类</option>
						</select>
						<%--<select name="service_three" id="service_three" class="text_common text_select width_200"  message="请选择三级服务分类">
							<option value="">请选择三级服务分类</option>
						</select>--%>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<li class="price_li">
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">您的报价</span>
					</p>
					<p class="form_common form_text clearfix">
						<span><input type="number" class="text_common text_input width_190" id="price" name="price" placeholder="请输入您的心愿报价" message="您的心愿报价不能为空" /></span>
						<span class="text_input form_space width_10">单位</span>
						<span><input type="text" class="text_common text_input width_190" id="unit" name="unit" placeholder="请输入单位分/小时/次等" message="您的心愿报价单位不能为空" /></span>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<li class="time_li">
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">服务时间</span>
					</p>
					<p class="form_common form_text clearfix">
						<span class="form_time">
							<input readonly type="text" class="text_common text_input width_190" name="service_time"  id="service_time" placeholder="请选择服务时间" message="请选择服务时间或合适您的时间" value="" />
							<input type="hidden" name="serviceTimeRange" id="serviceTimeRange"/>
							<%--<i class="icon_member icon_down">&nbsp;</i>--%>
						</span>
						<%--<span class="form_space">或是</span>
						<span class="form_time">
							<input type="text" class="text_common text_input width_190" name="suit_time"  id="suit_time" placeholder="请选择合适您的时间" message="请选择服务时间或合适您的时间"  value="" />
						</span>--%>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<li class="ways_li" id="ways_li">
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">服务方式</span>
					</p>
					<p class="form_common form_text clearfix" message="服务方式不能为空">
						<input type="hidden" value="1" class="text_common text_input width_190" name="service_ways" id="service_ways">
						<em way="1" class="hover">上门服务</em>
						<em way="2">约定地点服务</em>
						<em way="3">线上服务</em>
						<em way="4">邮寄</em>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<li class="upload_li">
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">服务标题图</span>
					</p>
					<p class="form_common form_text clearfix">
								<span class="upload">
									<!-上传中、上传成功高亮样式hover--->
									<em class="upload_btn" id="upload_btn">点击上传图片</em>
									<!--<em class="upload_btn hover" id="upload_btn">上传中...</em>
									<em class="upload_btn hover" id="upload_btn">上传成功</em>-->
									<input type="file" name="files" id="upload_thumb" class="text_common upload_thumb" multiple message="服务标题图不能为空"/>
								</span>
						<span>gif/jpg/png（最多5张，3M以内）</span>
					</p>
					<p class="form_common form_imglsit" id="form_imglsit">
						<%--<a title="">
							<img src="${ctx}/assets/images/content/detail_01.jpg"  alt="" />
							<i class="icon_delete">x</i>
						</a>--%>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<li class="content_li">
					<p class="clearfix">
						<span class="form_point red">*</span>
						<span class="form_title">具体内容</span>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<div style="margin-left: 15px;">
					<script id="content" name="content" type="text/plain">
						请填写您心愿的具体内容~
					</script>
				</div>
				<li class="label_li">
					<p class="clearfix">
						<span class="form_point red">&nbsp;</span>
						<span class="form_title">服务标签</span>
					</p>
					<p class="form_common form_text width_600 clearfix">
								<span class="laber_input" id="laber_input">
									<input maxlength="20" type="text" class="text_common text_input width_600" id="label" name="label" value="" placeholder="添加标签吧, 多个以','隔开" />
								</span>
						<span class="label_btn label_can" id="label_btn" message="标签值不能为空">添加标签</span>
					</p>
					<p class="form_common form_label clearfix width_600" id="form_label">
						<!--hover为高亮样式-->
						<%--<span class="clearfix">
							<i class="icon_member icon_header"></i>
							<i class="icon_footer">职场</i>
							<i class="icon_delete">x</i>
						</span>--%>
					</p>
					<p class="form_common form_remark"></p>
				</li>
				<!--<li class="content_li">
                    <p class="clearfix">
                        <span class="form_point red">&nbsp;</span>
                        <span class="form_title">具体案例</span>
                    </p>
                    <p class="form_common form_text">
                        <textarea class="text_common text_textarea width_600" id="case" name="case" value="" placeholder="请填写您的案例~" message="请填写您的案例~"></textarea>
                    </p>
                    <p class="form_common form_remark">（1000个字符以内的中文、英文、数字）</p>
                </li>-->
				<li class="handle_li">
					<p class="form_common form_btn">
						<a class="btn btn_common form_submit" id="form_submit" data-status="1">提交审核</a>
						<%--<a class="btn btn_common form_reset"  id="form_reset" data-status="0">保存至草稿箱</a>--%>
					</p>
				</li>
			</ul>
		</form>
	</div>
	<!--发布服务表单结束-->

	<!--选择时间服务日期开始-->
	<div class="module_date_box" id="module_date_box">
		<ul class="date_title clearfix">
			<li>星期一</li>
			<li>星期二</li>
			<li>星期三</li>
			<li>星期四</li>
			<li>星期五</li>
			<li>星期六</li>
			<li>星期日</li>
		</ul>
		<ul class="date_content clearfix">
			<%--<li>
				<span class="hover">09:00-12:00</span>
				<span>12:00-15:00</span>
				<span>15:00-17:00</span>
				<span>17:00-21:00</span>
				<span>21:00-24:00</span>
			</li>--%>
		</ul>
		<div class="date_submit">
			<a class="btn date_submit_btn" id="date_submit_btn">确定</a>
		</div>
	</div>
	<!--选择时间服务日期结束-->
</div>
<!--右侧内容部分结束-->

<javascript-list>
	<script src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
	<script src="${ctx}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
	<script src="${ctx}/assets/plugins/jquery-validation/localization/messages_zh.min.js"></script>
	<script src="${ctx}/assets/plugins/ueditor/ueditor.config.js"></script>
	<script src="${ctx}/assets/plugins/ueditor/ueditor.all.xyxy.js"></script>
	<!-- 文件上传插件 -->
	<script src="${ctx}/assets/plugins/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
	<script src="${ctx}/assets/plugins/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
	<script src="${ctx}/assets/plugins/jQuery-File-Upload/js/jquery.fileupload.js"></script>
	<script src="${ctx}/assets/js/member/service.publish.js"></script>
</javascript-list>
</body>
</html>
