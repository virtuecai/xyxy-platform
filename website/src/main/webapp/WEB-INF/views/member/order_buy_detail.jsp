<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta id="wxKeywords" name="Keywords" content="星语星愿" />
  <meta id="wxDescription" name="Description" content="星语星愿" />
  <title>订单详情_购买的服务_我的订单_星语星愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/order_buy_detail.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->
</head>

<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

  <!--服务详情内容开始-->
  <div class="member_container clearfix">

    <!--代付款订单详情开始-->
    <div class="order_status order_nopay">
      <div class="order_common order_tips">
        <ul>
          <li class="title red">服务已拍下， 如买家在45分钟内未付款，系统将自动取消订单</li>
          <li>1、您可以与买家继续沟通服务事宜；</li>
          <li>2、如果您不想购买，可以<a href="" class="cancel_order link blue">取消订单</a>；</li>
          <li>3、如有任何疑问请拨打心语心愿全国咨询电话<span class="blue">400-123-12345</span>；</li>
        </ul>
      </div>
      <div class="order_common order_detail">
        <ul>
          <li class="title">订单详情</li>
          <li>
            <span>见面地址：</span>
            <span>湖南 长沙市 岳麓区 麓谷街道麓谷信息港21楼</span>
          </li>
          <li>
            <span>下单时间：</span>
            <span>2014-2-13 13:23:7</span>
          </li>
          <li>
            <span>订单编号：</span>
            <span>21655454564</span>
          </li>
          <li>
            <span>商户姓名：</span>
            <span>魏明宇</span>
          </li>
          <li>
            <span>购买服务：</span>
            <span>创业咨询为什么这么难</span>
          </li>
          <li>
            <span>订单金额：</span>
            <span class="red">300元</span>
          </li>
          <li>
            <span>买家留言：</span>
            <span>我需要美女咨询</span>
          </li>
        </ul>
      </div>
    </div>
    <!--代付款订单详情结束-->

    <!--交易成功订单详情开始-->
    <!--<div class="order_status order_success">
        <div class="order_common order_tips">
            <ul>
                <li class="title red">恭喜您交易成功，您还可以：</li>
                <li>1、<a href="" class="link blue">收藏</a>此服务商；</li>
                <li>2、可以对此次交易做出精彩<a class="link blue" href="">评价</a>；</li>
                <li>3、如有任何疑问请拨打星语星愿全国咨询电话<span class="blue">400-123-12345</span>；</li>
            </ul>
        </div>
        <div class="order_common order_detail">
            <ul>
                <li class="title">订单详情</li>
                <li>
                    <span>见面地址：</span>
                    <span>湖南 长沙市 岳麓区 麓谷街道麓谷信息港21楼</span>
                </li>
                <li>
                    <span>下单时间：</span>
                    <span>2014-2-13 13:23:7</span>
                </li>
                <li>
                    <span>成交时间：</span>
                    <span>2014-2-13 13:23:7</span>
                </li>
                <li>
                    <span>订单编号：</span>
                    <span>21655454564</span>
                </li>
                <li>
                    <span>商户姓名：</span>
                    <span>魏明宇</span>
                </li>
                <li>
                    <span>购买服务：</span>
                    <span>创业咨询为什么这么难</span>
                </li>
                <li>
                    <span>订单金额：</span>
                    <span class="red">300元</span>
                </li>
                <li>
                    <span>买家留言：</span>
                    <span>我需要美女咨询</span>
                </li>
            </ul>
        </div>
    </div>-->
    <!--交易成功订单详情结束-->

    <!--退款订单详情开始-->
    <!--<div class="order_status order_refund">
        <div class="order_common order_tips">
            <ul>
                <li class="title red">您的客户已经申请退款，您需要在<em>5</em>天内处理退款申请，如在5天内未操作，视为同意退款；如同意退款，订单款项会打回买家的钱包！</li>
            </ul>
        </div>
        <div class="order_common order_detail">
            <ul>
                <li class="title">订单详情</li>
                <li>
                    <span>见面地址：</span>
                    <span>湖南 长沙市 岳麓区 麓谷街道麓谷信息港21楼</span>
                </li>
                <li>
                    <span>下单时间：</span>
                    <span>2014-2-13 13:23:7</span>
                </li>
                <li>
                    <span>订单编号：</span>
                    <span>21655454564</span>
                </li>
                <li>
                    <span>商户姓名：</span>
                    <span>魏明宇</span>
                </li>
                <li>
                    <span>购买服务：</span>
                    <span>创业咨询为什么这么难</span>
                </li>
                <li>
                    <span>订单金额：</span>
                    <span class="red">300元</span>
                </li>
                <li>
                    <span>退款原因：</span>
                    <span>卖家没有相关服务<em class="red">[提交时间：2014-02-12 12.23]</em></span>
                </li>
                <li>
                    <span>不同意退款原因：</span>
                    <span>恶意退款<em class="red">[提交时间：2014-02-12 12.23]</em></span>
                </li>
            </ul>
        </div>
    </div>-->
    <!--退款订单详情结束-->

    <!--关闭订单详情开始-->
    <!--<div class="order_status order_close">
        <div class="order_common order_tips">
            <ul>
                <li class="title red">关闭原因：您拒绝服务（时间上冲突）</li>
                <li>1、您可以与买家（13914709262）继续购买服务事宜；</li>
                <li>3、如有任何疑问请拨打心语心愿全国咨询电话<span class="blue">400-123-12345</span>；</li>
            </ul>
        </div>
        <div class="order_common order_detail">
            <ul>
                <li class="title">订单详情</li>
                <li>
                    <span>见面地址：</span>
                    <span>湖南 长沙市 岳麓区 麓谷街道麓谷信息港21楼</span>
                </li>
                <li>
                    <span>下单时间：</span>
                    <span>2014-2-13 13:23:7</span>
                </li>
                <li>
                    <span>关闭时间：</span>
                    <span>2014-2-13 13:23:7</span>
                </li>
                <li>
                    <span>订单编号：</span>
                    <span>21655454564</span>
                </li>
                <li>
                    <span>商户姓名：</span>
                    <span>魏明宇</span>
                </li>
                <li>
                    <span>购买服务：</span>
                    <span>创业咨询为什么这么难</span>
                </li>
                <li>
                    <span>订单金额：</span>
                    <span class="red">300元</span>
                </li>
                <li>
                    <span>买家留言：</span>
                    <span>我需要美女咨询</span>
                </li>
            </ul>
        </div>
    </div>-->
    <!--关闭订单详情结束-->

    <!--已支付订单详情开始-->
    <!--<div class="order_status order_pay">
        <div class="order_common order_tips">
            <ul>
                <li class="title red">您已成功为此次服务付款，您还可以：</li>
                <li>1、确认此次服务：<a href="" class="link blue">确认服务</a>（确认服务平台会打款至服务商），系统会自动确认；</li>
                <li>2、如果您对此次服务不满意可以<a href="" class="link blue">退款</a>；</li>
                <li>3、联系服务商<span class="blue">139754645612</span>；</li>
            </ul>
        </div>
        <div class="order_common order_detail">
            <ul>
                <li class="title">订单详情</li>
                <li>
                    <span>见面地址：</span>
                    <span>湖南 长沙市 岳麓区 麓谷街道麓谷信息港21楼</span>
                </li>
                <li>
                    <span>下单时间：</span>
                    <span>2014-2-13 13:23:7</span>
                </li>
                <li>
                    <span>订单编号：</span>
                    <span>21655454564</span>
                </li>
                <li>
                    <span>商户姓名：</span>
                    <span>魏明宇</span>
                </li>
                <li>
                    <span>购买服务：</span>
                    <span>创业咨询为什么这么难</span>
                </li>
                <li>
                    <span>订单金额：</span>
                    <span class="red">300元</span>
                </li>
                <li>
                    <span>买家留言：</span>
                    <span>我需要美女咨询</span>
                </li>
            </ul>
        </div>
    </div>-->
    <!--已支付订单详情结束-->

  </div>
  <!--服务详情内容结束-->

</div>
<!--身体结束-->
<javascript-list>
  <script type="text/javascript">
    $(function(){
      application.initPagePosition(['管理中心','我的订单', '我购买的服务', '订单详情']);
    });
  </script>
</javascript-list>
</body>
</html>
