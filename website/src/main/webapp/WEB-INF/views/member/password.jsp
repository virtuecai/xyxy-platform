<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/15 0015
  Time: 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>密码修改_基本资料_管理中心_星语星愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/password.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->
</head>

<body>
    <!--右侧内容部分开始-->
    <div class="container_right">
      <h2>密码修改</h2>
      <div class="module_form_box password_form_box" id="password_form_box">
        <form class="module_form" name="password_form" method="" id="password_form">
          <ul>
            <li class="clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">当前密码</span>
							<span class="form_text">
								<input type="password" class="text_common text_input width_300" id="password" name="password" value="" isrequired="true" placeholder="请输入当前密码" null="当前密码不能为空" />
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">新密码</span>
							<span class="form_text">
								<input type="password" class="text_common text_input width_300" id="new_password" name="new_password" value="" isrequired="true" placeholder="请输入新密码" null="新密码不能为空" same="新密码不能与当前密码一致" />
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">确认密码</span>
							<span class="form_text">
								<input type="password" class="text_common text_input width_300" id="reply_password" name="reply_password" value="" isrequired="true" placeholder="请再次输入新密码" null="重复新密码不能为空" same="重复密码与新密码必须一致" />
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">&nbsp;</span>
              <span class="form_title">&nbsp;</span>
							<span class="form_btn">
								<a class="btn btn_common form_submit" id="form_submit">确认</a>
								<a class="btn btn_common form_reset"  id="form_reset">取消</a>
							</span>
            </li>
          </ul>
        </form>
      </div>
    </div>
    <!--右侧内容部分结束-->

  </div>
  <!--密码修改内容结束-->
</div>
<!--身体结束-->
<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->
    <javascript-list>
      <script type="text/javascript" src="${ctx}/assets/js/member/password.js"></script>
      </javascript-list>
</body>
</html>
