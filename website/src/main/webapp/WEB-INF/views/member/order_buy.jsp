<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta id="wxKeywords" name="Keywords" content="星语星愿" />
    <meta id="wxDescription" name="Description" content="星语星愿" />
    <title>购买的服务_我的订单_星语星愿</title>
    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/member/order_buy.css" rel="stylesheet" type="text/css" />
    <!--页面Css样式结束-->
</head>
<body>
<!--身体开始-->
<div class="layer clearfix">
    <!--右侧内容部分开始-->
    <div class="container_right">
        <h2>购买的服务</h2>

        <div class="container_order">
            <!--订单状态开始-->
            <div class="order_status">
                <ul class="clearfix">
                    <li default-value="0">
                        <a href="javascript:void(0);">全部订单</a>
                    </li>
                    <li default-value="2">
                        <a href="javascript:void(0);">待付款</a>
                    </li>
                    <li default-value="3">
                        <a href="javascript:void(0);">已支付</a>
                    </li>
                    <li default-value="4">
                        <a href="javascript:void(0);">退款中</a>
                    </li>
                    <li default-value="5">
                        <a href="javascript:void(0);">交易成功</a>
                    </li>
                    <li default-value="7">
                        <a href="javascript:void(0);">已关闭</a>
                    </li>
                </ul>
            </div>
            <!--订单状态结束-->

            <!--订单列表开始-->
            <div class="order_list" id="order_list">

                <!--订单列表为空时开始-->
                <!--<div class="order_none">
                    您的订单为空，快去挑选您喜欢的服务吧~
                </div>-->
                <!--订单列表为空时结束-->

                <!--订单列表遍历开始-->
                <div id="order_tempo">
                    <div class="order_list_box underscore-template">
                        <div class="list_head">
                            <ul class="clearfix">
                                <li class="number">编号：{{=orderSn}}</li>
                                <li class="time">发起时间：{{=confirmTime}}</li>
                                <li class="status">
                                    订单状态：<em class="green">已支付</em>
                                    <!--<em class="gray">已关闭</em>
                                     {{%if(payStatus == 1){%><em class="orange">待付款</em>
                                     {{%if(payStatus == 2){%><em class="green">已支付</em>
                                     {%if(payStatus == 3){%><em class="red">买家申请退款</em>
                                     {%if(payStatus == 4){%><em class="">交易成功</em> -->
                                </li>
                            </ul>
                        </div>
                        <div class="list_content">
                            <ul class="clearfix">
                                <li class="thumb">
                                    <a href=""> <img src="${ctx}/assets/images/content/content_03.jpg" alt=""/></a>
                                </li>
                                <li class="desc">
                                    <a href="" class="title">{{=goods.goodsName}}</a>
                                    <a href="" class="intro">姓名 服务方式</a>
                                </li>
                                <li class="deal">
                                    <a title="" class="delete">删除订单</a>
                                    <a title="" href="${ctx}/member/order/detail/saled_3_{{orderId}}">订单详情</a>
                                    <!--<a title="" class="green refund">同意退款</a>
                                    <a title="" class="red norefund">不同意退款</a>
                                    <a title="" class="red" href="" >立即支付</a>
                                    <a title="" class="green">同意退款</a>
                                    <a title="" class="red">拒绝接单</a>-->
                                </li>
                                <li class="money">订单金额：<span>￥<em>{{=goodsAmount}} </em></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--订单列表遍历结束-->

                <!--分页结构结束-->
                <div style="margin-top: 20px;" class="mricode-pagination"></div>

            </div>
            <!--订单列表结束-->

            <!--拒绝退款开始-->
            <div class="module_form_box norefund_form_box" id="norefund_form_box">
                <form class="module_form norefund_form" name="norefund_form" id="norefund_form" method="">
                    <ul>
                        <li class="clearfix">
                            <span class="form_point red">*</span>
                            <span class="form_title">所在地区</span>
                                <span class="form_text">
                                    <select name="province" id="province" class="text_common text_select" error="请选择省份">
                                        <option value="">请选择退款原因</option>
                                        <option value="">商品质量太差</option>
                                        <option value="">价格与实际质量不符合</option>
                                    </select>
                                </span>
                        </li>
                        <li class="clearfix">
                            <span class="form_point red">*</span>
                            <span class="form_title">约见人姓名</span>
                                <span class="form_text">
                                    <input type="text" class="text_common text_input" id="realname" name="realname"
                                           value="" placeholder="请填写约见人姓名" null="约见人姓名不能为空"/>
                                </span>
                        </li>
                        <li class="clearfix">
                            <span class="form_point red">&nbsp;</span>
                            <span class="form_title">&nbsp;</span>
                                <span class="form_btn">
                                    <a class="btn btn_common form_submit">保存</a>
                                    <a class="btn btn_common form_reset">取消</a>
                                </span>
                        </li>
                    </ul>
                </form>
            </div>
            <!--拒绝退款结束-->
        </div>
    </div>
    <!--右侧内容部分结束-->

</div>
<!--购买的服务内容结束-->
</div>
<!--身体结束-->
<javascript-list>
    <script type="text/javascript">
        //全局分页对象
        var pageObj;
        $(function(){
            $(".order_status>ul>li:eq(0)").addClass("hover");

            application.initPagePosition(['管理中心','我的订单', '我购买的服务']);
            //初始化分页
            initPagination();

            //点击订单类型的时候加载分页
            $(".order_status>ul>li").on("click",function(){
                $(this).addClass("hover").siblings().removeClass('hover');
                var params = {memberId: '${user.id}',type: $(this).attr("default-value")};
                if($(this).index()==0)
                    params = {memberId: '${user.id}'};
                pageObj.pagination('setParams', params);
                pageObj.pagination('remote');
            });
        });

        function initPagination(){
            pageObj = $(".mricode-pagination").pagination({
                pageSize: 5, // 指定 一页多少天数据
                debug: false,
                showInfo: false,
                showJump: true,
                showPageSizes: false,
                prevBtnText: '<i><</i> 上一页',
                nextBtnText: '下一页 <i>></i>',
                jumpBtnText: '确定',
                infoFormat: '共 {total} 条',
                noInfoText: '暂无数据',
                pageElementSort: ['$page', '$size', '$jump', '$info'],
                remote: {
                    url: ctx+"/member/order_buy/list_data",
                    params: {memberId: '${user.id}'},
                    totalName: 'result.total',
                    pageIndexName: 'pageNum',
                    pageSizeName: 'pageSize',
                    success: function (data) {
                        if(data.success) {
                            _.templateRender('#order_tempo', data.result.list);
                        } else {
                            layer.tips('请求失败!');
                        }
                    }
                }
            });

        }
    </script>
</javascript-list>
</body>
</html>
