<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/13 0013
  Time: 14:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>个人标签_基本资料_管理中心_星语星愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/label.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->
</head>

<body>
<!--身体开始-->
<div class="layer clearfix">

  <!--个人标签内容开始-->
  <div class="member_container clearfix">

    <!--右侧内容部分开始-->
    <div class="container_right">
      <h2>个人标签</h2>
      <div class="container_label" id="container_label">

        <!--我的职业开始-->
        <div class="label_common label_job" id="label_job">
          <div class="label_add clearfix">
            <span class="title">我的职业：</span>
						<span class="add clearfix">
							<i class="icon_member icon_add"></i>
							<em>添加职业</em>
						</span>
            <span class="tips">选择您的职业/技能，可以让大家更好的了解你的专业能力。</span>
          </div>
          <div class="label_list clearfix">
                <c:forEach items="${ProfessionList}" var="profess">
                  <c:choose>
                  <c:when test="${profess eq null ||empty profess}">
                        赶快添加你的第一个标签吧！
                  </c:when>
                  <c:otherwise>
                      <span class="clearfix">
                        <input type="hidden" value="${profess.id}" class="profession" id="myprofession${profess.id}" attrid="${profess.id}">
                        <input type="hidden" value="${profess.professionMemberId}" id="proMemberId">
                        <i class="icon_member icon_header"></i>
                        <i class="icon_footer">${profess.name}</i>
                        <i class="icon_delete">x</i>
                       </span>
                  </c:otherwise>
                </c:choose>
                </c:forEach>
          </div>
          <div class="module_form_box label_add_box" id="label_add_box">
            <form class="module_form label_add_form" name="label_add_form" method="" id="label_add_form">
              <ul>
                <li class="clearfix">
                  <span class="form_point red">*</span>
                  <span class="form_title">职业标签</span>
									<span class="form_text">
                                        <input type="text" class="text_common text_input width_190" id="protag" name="nickname" value="" isrequired="true" placeholder="请填写标签内容" null="亲，昵称不能为空" />
									</span>
                </li>
                <li class="clearfix">
                  <span class="form_point red">&nbsp;</span>
                  <span class="form_title">&nbsp;</span>
									<span class="form_btn">
										<a class="btn btn_common form_submit">保存</a>
										<a class="btn btn_common form_reset">取消</a>
									</span>
                </li>
              </ul>
            </form>
          </div>
        </div>
        <!--我的职业结束-->

        <!--我的个人标签开始-->
        <div class="label_common label_own" id="label_own">
          <div class="label_add clearfix">
            <span class="title">我的个人标签：</span>
						<span class="add clearfix">
							<i class="icon_member icon_add"></i>
							<em>添加标签</em>
						</span>
            <span class="tips">给自己添加个人标签，可以是爱好、特长、个性等。丰富的个人标签可以帮助大家更全面的了解你。</span>
          </div>
          <div class="label_list clearfix" id="specifi">
           <c:forEach items="${SpecificTagList}" var="specific">
                  <c:choose>
                  <c:when test="${specific eq null ||empty specific}">
                        赶快添加你的第一个标签吧！
                  </c:when>
                  <c:otherwise>
                      <span class="clearfix">
                        <input type="hidden" value="${specific.id}" class="specific_id" id="mysprcifictag${specific.id}" attrid="${specific.id}">
                        <input type="hidden" value="${specific.memberSpecificTagId}" id="spcMemberId">
                        <i class="icon_member icon_header"></i>
                        <i class="icon_footer">${specific.name}</i>
                        <i class="icon_delete">x</i>
                       </span>
                  </c:otherwise>
                </c:choose>
           </c:forEach>
          </div>
          <div class="module_form_box label_own_box" id="label_own_box">
            <form class="module_form label_own_form" name="label_own_form" method="" id="label_own_form">
              <ul>
                <li class="tips_li clearfix">
                  标签可以为您的职业或者爱好相关的关键字，最多可以添加<em class="total_num orange">10</em>个标签(不超过<em class="max_length orange">10</em>个字符)，快来给自己打上个性化的标签。您还可以添加<em class="allow_num orange">10</em>个。
                </li>
                <li class="label_li clearfix">
									<span class="form_text laber_input" id="laber_input">
										<input type="text" maxlength="10" class="text_common text_input" id="label" name="label" value="" placeholder="添加个标签吧" />
									</span>
                </li>
                <li class="label_list clearfix">

                </li>
                <li class="clearfix">
                  <span class="form_point red">&nbsp;</span>
                  <span class="form_title">&nbsp;</span>
									<span class="form_btn">
										<a class="btn btn_common form_submit">保存</a>
										<a class="btn btn_common form_reset">取消</a>
									</span>
                </li>
              </ul>
            </form>
          </div>
        </div>
        <!--我的个人标签结束-->

      </div>
    </div>
    <!--右侧内容部分结束-->

  </div>
  <!--个人标签内容结束-->

</div>
<!--身体结束-->

<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->
<javascript-list>
  <script type="text/javascript" src="${ctx}/assets/js/member/label.js"></script>
</javascript-list>
</body>
</html>
