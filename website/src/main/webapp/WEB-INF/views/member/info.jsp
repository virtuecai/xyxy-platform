<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/7 0007
  Time: 9:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <title>个人资料_管理中心_星语心愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/info.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->

</head>
    <!--右侧内容部分开始-->
    <div class="container_right">
      <h2>个人信息</h2>
      <div class="module_form_box info_form_box" id="info_form_box">
        <form class="module_form" name="info_form" method="" id="info_form">
          <ul>
            <li class="upload_li clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">个人头像</span>
                <span class="form_text">
                    <!-上传中、上传成功高亮样式hover--->
                    <em class="upload_btn" id="upload_btn">点击上传头像</em>
                    <!--<em class="upload_btn hover" id="upload_btn">上传中...</em>
                    <em class="upload_btn hover" id="upload_btn">上传成功</em>-->
                    <input type="file" name="files" id="upload_thumb" class="text_common upload_thumb" />
                </span>
              <span class="form_remark">git/jpg/jpeg格式（最多5张，3M以内）</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">昵称</span>
							<span class="form_text">
								<input type="text" class="text_common text_input width_190" id="nickname" name="nickname" value="${memberDetail.member.alias}" isrequired="true" placeholder="请填写昵称" null="亲，昵称不能为空" />
                            </span>
            </li>
            <li class="sex_li clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">性别</span>
							<span class="form_text">
                                <c:choose>
                                  <c:when test="${memberDetail.member.gender == 0}">
                                    <input type="radio" class="text_radio" name="sex" value="1"  /> 男
                                    <em class="space">&nbsp;</em>
                                    <input type="radio" class="text_radio" name="sex" value="0" checked="checked"/> 女
                                  </c:when>
                                  <c:otherwise>
                                    <input type="radio" class="text_radio" name="sex" value="1"  checked="checked" /> 男
                                    <em class="space">&nbsp;</em>
                                    <input type="radio" class="text_radio" name="sex" value="0" /> 女
                                  </c:otherwise>
                                </c:choose>
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">所在地</span>
                <span class="form_text">
								<select name="province" id="province"  class="text_common text_select width_200" isrequired="true" null="请选择省份,不能为空">
                                  <option value="${sysAreaList[0].id!=null?sysAreaList[0].id:-1}">${sysAreaList[0].areaname!=null?sysAreaList[0].areaname:'请选择所属省'}</option>
                                  <c:forEach items="${provincelist}" var="province">
                                    <option value="${province.provinId}">${province.provinceName}</option>
                                  </c:forEach>
                                </select>
								<select name="city" id="city" class="text_common text_select width_200">
                                </select>
								<select name="area" id="area" class="text_common text_select width_200">
                                  <option value="${sysAreaList[2].id}">${sysAreaList[2].areaname}</option>
                                </select>
                  <input type="hidden" value="${sysAreaList[1].id}" id="loadcityid">
                  <input type="hidden" value="${sysAreaList[2].id}" id="loadareaid">
							</span>
            </li>
            <li class="birthday_li clearfix">
              <span class="form_point">&nbsp;</span>
              <span class="form_title">生日</span>
							<span class="form_text">
								<input class="text_common text_input width_190" name="birthday" type="text" id="birthday" placeholder="请选择生日" null="亲，保存下您的生日吧"  value="<fmt:formatDate value="${memberDetail.member.birthday eq null?'':memberDetail.member.birthday}" pattern="yyyy年MM月dd日" />" onFocus="WdatePicker({minDate:'1900-01-01',maxDate:'2099-12-31'})" />
								<i class="icon_member icon_down">&nbsp;</i>
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point">&nbsp;</span>
              <span class="form_title">自我介绍</span>
							<span class="form_text">
								<textarea class="text_common text_textarea width_600" id="introduce" name="introduce" value="" placeholder="来句自我介绍吧~" null="来句自我介绍吧~">${memberDetail.member.articleContent}</textarea>
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">&nbsp;</span>
              <span class="form_title">&nbsp;</span>
							<span class="form_btn">
								<a class="btn btn_common form_submit" id="form_submit">保存</a>
								<%--<a class="btn btn_common form_reset"  id="form_reset">取消</a>--%>
							</span>
            </li>
          </ul>
        </form>
      </div>
    </div>
    <!--右侧内容部分结束-->

  </div>
  <!--个人资料内容结束-->

</div>
<!--身体结束-->

<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
  <input type="hidden" id="arcId" value="${memberDetail.member.articleId}">
</div>
</div>
<!--身体右边公用侧边栏结束-->
<javascript-list>
  <!--日期插件datepicker开始-->
  <script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
  <!--日期插件datepicker结束-->
  <!-- 文件上传插件 -->
  <script src="${ctx}/assets/plugins/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
  <script src="${ctx}/assets/plugins/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
  <script src="${ctx}/assets/plugins/jQuery-File-Upload/js/jquery.fileupload.js"></script>

  <script type="text/javascript" src="${ctx}/assets/js/member/info.js"></script>

  <script type="text/javascript">
    $(function(){

      $("#form_reset").on('click', function () {
        window.history.back();
      });

      //表单变量
      var moduleBox = '#info_form_box', moduleForm = '#info_form';


      //提交表单
      $('#form_submit').on('click',function(){
        var allFlag   = false;
        var checkItem = $('#info_form_box .text_common[isrequired]');
        checkItem.each(function(){
          var obj = $(this);
          if(validateForm(obj)){
            allFlag = true;
          }else{
            allFlag = false;
            $('html,body').animate({scrollTop: 0}, 500);
            return false;
          }
        });
        if(allFlag) {//提交表 单
          var date;
          if($("#birthday").val()){
            date= moment($("#birthday").val(), 'YYYY年MM月DD日').format('YYYY-MM-DD');
          }
          $.ajax({
            url:ctx+'/member/update',
            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
            data:{
              alias:$("#nickname").val(),
              gender:$("input[name='sex']:checked").val(),
              address:$("#area").val(),
              birthday:date,
              article:$("#introduce").val(),
              articleId:$("#arcId").val(),
              imgurl: $.trim($("#headImage").attr("src"))
            },
            type:'post',
            dataType:'json',
            beforeSend:function(){
              $("#form_submit").attr("disabled","disabled");
            },
            success:function(data){
              if(data) {
                layer.msg("修改成功");
                setTimeout(function(){location.href=ctx+'/member/';},3000);
              } else {
                layer.msg("修改失败");
                setTimeout(function(){location.href=ctx+'/member/';},3000);
              }
            },
            complete:function(){
              $("#form_submit").removeAttr("disabled");
            }
          });
        }
      });

      //表单失去焦点
      $(moduleBox+' .text_common[isrequired]').on('blur',function(){
        validateForm($(this));
      });
    });

    //表单信息验证
    function validateForm(obj){
      var flag	  	= false;
      var color		= '#FF7800';
      var uploadBtn 	= obj.siblings('.upload_btn');
      //表单值是否为空验证
      if(!obj.val()){
        if(obj.attr('type')=='file'){
          uploadBtn.addClass('error_border');
        }else if(!obj.hasClass('error_border')){
          obj.addClass('error_border');
        }
        layer.tips(obj.attr('null'), obj, {tips: [2, color]});
        flag = false;
      }else{
        if(obj.attr('type')=='file'){
          uploadBtn.removeClass('error_border');
        }else{
          obj.removeClass('error_border');
        }
        layer.closeAll('tips');
        flag = true;
      }
      return flag;
    }
  </script>
</javascript-list>
</body>
</html>