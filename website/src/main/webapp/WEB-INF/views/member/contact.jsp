<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/15 0015
  Time: 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>联系方式_基本资料_管理中心_星语心愿</title>
  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/member/contact.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->
</head>

<body>

<!--身体开始-->
<div class="layer clearfix">
    <!--右侧内容部分开始-->
    <div class="container_right">
      <h2>联系方式</h2>
      <!--联系方式表单开始-->
      <div class="module_form_box contact_form_box" id="contact_form_box">
        <form class="contact_form" name="contact_form" method="" id="contact_form">
          <ul>
            <li class="mobile_li clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">手机号码：</span>
							<span class="form_text">
								<input type="text" class="text_common text_input width_100" name="mobile" value="${mobile}" disabled="disabled" />
							</span>
							<span class="form_edit" id="form_edit">
								<i class="icon_member icon_edit"></i>
								<em>修改手机号码</em>
							</span>
            </li>
            <%--<li class="upload_li clearfix">--%>
              <%--<span class="form_point">&nbsp;</span>--%>
              <%--<span class="form_title">微信二维码：</span>--%>
              <%--<div class="form_upload clearfix">--%>
                <%--<div class="upload_card">--%>
                  <%--<em class="upload_btn" id="upload_btn">点击上传二维码</em>--%>
                  <%--<!--<em class="upload_btn hover" id="upload_btn">上传中...</em>--%>
                  <%--<em class="upload_btn hover" id="upload_btn">上传成功</em>-->--%>
                  <%--<input class="text_common upload_thumb" type="file" name="upload_thumb" id="upload_back_thumb">--%>
                  <%--<!--<img src="../assets/images/card_back.jpg" id="" alt="" id="upload_wechat" />-->--%>
                <%--</div>--%>
                <%--<div class="upload_tips">(图片大小不能超过3M)</div>--%>
              <%--</div>--%>
            <%--</li>--%>
            <li class="clearfix">
              <span class="form_point">&nbsp;</span>
              <span class="form_title">QQ号码：</span>
							<span class="form_text">
								<input type="text" class="text_common text_input width_190" value="${qq}" id="qq_number" name="qq_number"  placeholder="请输入QQ号码" />
							</span>
            </li>
            <li class="clearfix">
              <span class="form_point red">&nbsp;</span>
              <span class="form_title">&nbsp;</span>
							<span class="form_btn">
								<a class="btn btn_common form_submit" id="x">保存</a>
								<a class="btn btn_common form_reset">取消</a>
							</span>
            </li>
          </ul>
        </form>
      </div>
      <!--联系方式表单结束-->

      <!--修改手机号码表单开始-->
      <div class="module_form_box mobile_form_box" id="mobile_form_box">
        <form class="mobile_form" name="mobile_form" method="" id="mobile_form">
          <ul>
            <li class="mobile_li clearfix">
              <span class="form_point red">*</span>
              <span class="form_title">手机号码：</span>
							<span class="form_text">
								<input type="text" class="text_common text_input width_100" id="mobile" name="mobile" value="" isrequired="true" error="请输入正确的手机号"  placeholder="请输入手机号码" null="手机号码不能为空" />
							</span>
							<%--<span class="form_yzm">--%>
								<%--<a class="btn yzm_default">发送验证码</a>--%>
								<%--<!--<a class="btn yzm_time"><em class="red">60</em>秒后重新获取</a>-->--%>
							<%--</span>--%>
            </li>
            <%--<li class="clearfix">--%>
              <%--<span class="form_point red">*</span>--%>
              <%--<span class="form_title">验证码：</span>--%>
							<%--<span class="form_text">--%>
								<%--<input type="text" class="text_common text_input width_200" id="yzm" name="yzm" value="" isrequired="true" placeholder="请输入短信验证码" null="短信验证码不能为空" />--%>
							<%--</span>--%>
            <%--</li>--%>
            <li class="clearfix">
              <span class="form_point red">&nbsp;</span>
              <span class="form_title">&nbsp;</span>
							<span class="form_btn">
								<a class="btn btn_common form_submit" id="mobile_submit_btn">保存</a>
								<a class="btn btn_common form_reset">取消</a>
							</span>
            </li>
          </ul>
        </form>
      </div>
      <!--修改手机号码表单结束-->
    </div>
    <!--右侧内容部分结束-->

  </div>
  <!--个人资料内容结束-->

</div>
<!--身体结束-->

<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->
<javascript-list>
  <script type="text/javascript" src="${ctx}/assets/js/member/contact.js"></script>
<!--弹出层插件layer结束-->
  </javascript-list>
</body>
</html>

