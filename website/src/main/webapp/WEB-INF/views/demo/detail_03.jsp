<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta id="wxKeywords" name="Keywords" content="星语心愿" />
<meta id="wxDescription" name="Description" content="星语心愿" />
<title>巴菲特午餐_PE投资_理财_财富理财保险_星语心愿</title>

<!--图片etalage缩放开始-->
<link href="${ctx}/assets/plugins/etalage/css/etalage.css" rel="stylesheet" type="text/css" />
<!--图片etalage缩放结束-->

<!--页面Css样式开始-->
<link href="${ctx}/assets/css/service/detail_test.css" rel="stylesheet" type="text/css" />
<!--页面Css样式结束-->
</head>

<body>
<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">财富理财保险</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">理财</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">PE投资</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">巴菲特午餐</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->

	<!--服务详情内容开始-->
	<div class="service_container clearfix">

		<!--左侧服务内容开始-->
		<div class="container_left">

			<!--预定服务开始-->
			<div class="service_reserve clearfix">

				<!--滚动图片展示开始-->
				<div class="service_photo">
					<ul id="etalage">
						<li>
							<img class="etalage_thumb_image"  src="${ctx}/assets/images/content/lxj.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj.jpg" title="巴菲特午餐" />
							<!--第二张图放大图-->
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="巴菲特午餐" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="巴菲特午餐" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="巴菲特午餐" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="巴菲特午餐" />
						</li>
					</ul>
				</div>
				<!--滚动图片展示结束-->

				<!--服务介绍开始-->
				<div class="service_introduce" id="service_introduce">
					<form action="" name="service_form" id="service_form" method="">
						<ul>
							<li class="title">
								PE投资
							</li>
							<li class="label">
							<span><!--高亮样式hover-->
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">VC咨询</i>
		                    </span>
		                    <span>
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">PE投资</i>
		                    </span>
							<span class="hover">
			                    <i class="icon_service icon_header"></i>
			                    <i class="icon_footer">巴菲特午餐</i>
		                    </span>
							</li>
							<li class="cost pe_cost clearfix">
								<span class="left">起拍价：<em class="orange">￥1000</em> 元/次</span>
								<span class="right">竞拍次数：<em class="orange">15</em> 次</span>
								<span class="left">当前价：<em class="orange">￥1000</em> 元/次</span>
								<span class="right">距离结束：<em class="orange">2</em> 天 <em class="orange">5</em> 时</span>
								<span class="left price_add clearfix">
									<em>您出价：</em>
									<input type="text" name="price_last" id="price_last" class="price_last" value="￥8500" attrval="8500" addPrice="500" disabled />
									<em class="price_add_btn" id="price_add_btn">+</em>
								</span>
								<span class="right">加价幅度：<em class="orange">500</em> 元/次</span>
							</li>
							<li class="common_li time">
								<em>服务时间：</em>
								<input name="service_time" type="text" id="service_time" placeholder="请选择购买服务时间" class="" value="每周五12:00-15:00" />
								<!-- onFocus="WdatePicker({minDate:'%y-%M-{%d}'})"-->
							</li>
							<!--<li class="common_li time_block clearfix" id="time_block">
								<em>具体时间：</em>
								<span class="clearfix">
									<i class="btn can hover">09:00-12:00</i>
									<i class="btn can">17:00-21:00</i>
									<i class="btn can">21:00-24:00</i>
								</span>
								&lt;!&ndash;注意：hover为高亮样式，&ndash;&gt;
							</li>-->
							<li class="common_li ways clearfix" id="ways">
								<em>服务方式：</em>
								<span class="clearfix">
									<i class="btn">线上服务</i>
									<i class="btn">电子咨询</i>
									<i class="btn hover">视频对话</i>
									<i class="btn">预约见面</i>
								</span>
								<!--注意：不同的服务方式会对应订单的不同地址-->
							</li>
							<li class="handle">
								<span class="btn subscribe" id="subscribe">立即预约</span>
								<span class="btn addcart" id="addcart">加入心愿车</span>
							</li>
							<li class="tools clearfix">
								<span id="collect" class="clearfix">
									<i class="icon_service icon_collect"></i>
									<em>收藏</em>
									<!--<i class="icon_service icon_collected"></i>
									<em class="orange">已收藏</em>-->
								</span>
								<span id="share" class="share bdsharebuttonbox clearfix">
									<a class="bds_more" data-cmd="more"></a>
									<em>分享</em>
								</span>
								<span id="comment" class="comment clearfix">
									<i class="icon_service icon_comment"></i>
									<em>评论</em>
									<em class="num">4</em>
								</span>
								<span id="report" class="report clearfix">
									<!--<i class="icon_service icon_report"></i>-->
									<em>举报</em>
								</span>
							</li>
						</ul>
					</form>
				</div>
				<!--服务介绍开始-->

			</div>
			<!--预定服务开始-->

			<!--服务详情、评价、案例开始-->
			<div class="service_detail">

				<!--头部选项开始-->
				<div class="service_detail_header" id="service_detail_header">
					<ul class="clearfix">
						<li attrid="detail_content" class="hover">
							<i class="icon_service icon_down"></i>
							服务详情
						</li>
						<li attrid="detail_comment" class="">
							<i class="icon_service icon_down"></i>
							顾客评价
							<em class="comments_num">12</em>
						</li>
						<li attrid="detail_case" class="">
							<i class="icon_service icon_down"></i>
							我的案例
						</li>
					</ul>
				</div>
				<!--头部选项结束-->

				<!--底部显示开始-->
				<div class="service_detail_footer">

					<!--详情内容开始-->
					<div class="detail_common detail_content" id="detail_content">
						<!--以下为测试内容，做动态处理时可删除--开始-->
						<h2>我可以提供的咨询包括</h2>
						<p>
							一、梳理海外投资的脉络、海外投资的法律风险控制，从较高层面对设计海外投资的架构、如何进行尽职调查、聘用中介、聘用外部顾问、撰写交易文件、并购谈判等全流程进行剖析。<br/>

							二、可对海外投资所涉及到的具体问题进行剖析、方案设计。<br/>

							三、适合各专业人员、公司高级法律顾问、公司管理层等。<br/>

							四、也可以选择如下任一话题进行深入交流：<br/>

							• 国际并购保密协议(Confidentiality Agreement)审查、风险及谈判点<br/>
							• 交易前期协议（备忘录 -MOU、意向书(LOI)、协议要点书(HOA)或者条款书(Term Sheet)）审查与谈判技巧<br/>
							• 国际油气并购中的联合作业协议（JOA）的条款、风险点精讲<br/>
							• 油气并购中的勘探权转入/转出协议(Farmin/Farmout Agreement)谈判点、风险点分析<br/>
							• 对产品分成合同（PSC）及其变种等进行详细介绍，谈判点、风险点揭示<br/>
							• 海外油气资产、股权购买协议（SPA）谈判条款、风险点精讲<br/>
							• 对并购美国页岩油气资产的流程、风险点、协议谈判点进行深入分析<br/>
							• 以下证券交易所上市公司并购精讲任选一类：美国上市公司、英国上市公司、加拿大上市公司、澳大利亚上市公司、香港<br/>
							上市公司、新加坡上市公司<br/>
							• 海外投资中中介使用及外聘顾问协议的谈判点和风险点<br/>
							• 海外投资股东协议的谈判点和风险点<br/>
						</p>
					</div>
					<!--详情内容结束-->

					<!--评价内容开始-->
					<div class="detail_common detail_comment" id="detail_comment">


					</div>
					<!--评价内容结束-->

					<!--我的案例开始-->
					<div class="detail_common detail_content detail_case" id="detail_case">

					</div>
					<!--我的案例结束-->

				</div>
				<!--底部显示结束-->

			</div>
			<!--服务详情、评价、案例结束-->

		</div>
		<!--左侧服务内容结束-->

		<!--右侧人物介绍开始-->
		<div class="container_right">
			<div class="people_focus clearfix">
				<!--主体居中开始-->
				<div class="people_focus_main">
					<div class="focus_img">
						<a href="" title=""><img src="${ctx}/assets/images/content/lxj.jpg" alt=""  /></a>
					</div>
					<div class="focus_intro">
						<span class="name">张正大</span>
						<span class="label">
							<i class="icon_service icon_zheng"></i>
	                    	<i class="icon_service icon_v_member"></i>
						</span>
						<span class="focus_on">
							<!--未关注-->
							<em class="btn onfocus cmain_bg_color">+ 关注</em>
							<!--已关注-->
							<!--<em class="btn offfocus bg_gray">已关注</em>-->
						</span>
					</div>
				</div>
				<!--主体居中结束-->
			</div>
			<div class="people_intro">
				<ul>
					<li class="label clearfix">
						<span class="clearfix hover"><!--高亮样式hover-->
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">法律咨询</i>
						</span>
						<span class="clearfix">
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">投融资顾问</i>
						</span>
						<span class="clearfix">
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">海外投资</i>
						</span>
					</li>
					<li class="description">
						法学硕士，毕业于北京大学民商法学专业，曾在多家世界500强企业任职，现就职于中国海洋石油总公司法律部，任项目管理处处长，主要职能为集团管理层对重大项目的决策提供法律支持，管理中海油集团内国内外重大并购、重大投资、重大融资、资产剥离和处置、重大生产经营及建设项目的法律风险和重大、疑难、复杂法律咨询类工作。
						曾被世界知名法律权威咨询机构Legal 500评为亚太地区最佳公司法律顾问之一，同时也是世界石油谈判者协会（AIPN）标准合同起草委员会成员之一。
						擅长海外并购、跨境投资、融资法律方面的问题，对大型、复杂的商业谈判有丰富的经验。
					</li>
					<li class="resfuse">
						<h3>关于退款</h3>
						订单取消时，已冻结金额将退至您的余额中。您可以选择下次继续预约，或申请退款至您的支付宝账户。
					</li>
				</ul>
			</div>
		</div>
		<!--右侧人物介绍结束-->

	</div>
	<!--服务详情内容结束-->

</div>
<!--身体结束-->


<javascript-list>
<!--etalage图片缩放插件开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/etalage/js/jquery.etalage.min.js"></script>
<!--etalage图片缩放插件结束-->
<!--日期插件datepicker开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
<!--日期插件datepicker结束-->
<!--页面JS结束-->
<script type="text/javascript">
	$(function(){


		//加价开始
		$('#price_add_btn').on('click',function(){
			var totalPrice = 0;
			var priceLast  = $('#price_last');
			var priceAttr  = parseInt(priceLast.attr('attrval'));
			var addPrice   = parseInt(priceLast.attr('addprice'));

			totalPrice = priceAttr + addPrice;

			priceLast.val('￥'+totalPrice);
			priceLast.attr('attrval',totalPrice);
		});

		//缩略图轮播
		$('#etalage').etalage({
			thumb_image_width: 300,
			thumb_image_height: 300,
			source_image_width: 900,
			source_image_height: 900,
			show_hint: false,
			show_icon: false,
			small_thumbs: 5,
			click_callback: function(image_anchor, instance_id){}
		});

		//选择服务方式
		$('#service_introduce ul li.ways i').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//选择时间段
		$('#service_introduce ul li.time_block i.can').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//服务时间点选择
		$('#service_time').on('focus',function(){
			var objVal = $(this).val();
			if(objVal){

			}
		});

		//预约服务提交表单
		$('#subscribe').on('click',function(){
			$('#service_form').submit();
		});

		//加入心愿车
		$('#addcart').on('click',function(){
			//操作
		});

		//百度分享插件
		window._bd_share_config = {
			"common": {
				"bdSnsKey": {},
				"bdText": "",
				"bdMini": "2",
				"bdMiniList": false,
				"bdPic": "",
				"bdStyle": "2",
				"bdSize": "16"
			},
			"share": {}
		};
		with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];

		//详情、评论选项卡
		$('#service_detail_header>ul>li').on('click',function(){
			var attrid = $(this).attr('attrid');
			$(this).addClass('hover').siblings().removeClass('hover');
			$('#'+attrid).fadeIn().siblings('.detail_common').hide();
		});

		//加载相册扩展模块
		layer.config({
			extend: 'extend/layer.ext.js'
		});
		//初始化layer
		layer.ready(function(){
			//使用相册
			layer.photos({
				photos: '#msg_imglist'
			});
		});
	})
</script>
</javascript-list>
</body>
</html>
