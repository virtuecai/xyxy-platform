<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
    <title>服务广场</title>

    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/service/search.css" rel="stylesheet" type="text/css" />
    <!--页面Css样式结束-->

</head>

<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--服务内容开始-->
<div class="service_container">

    <!--一级分类推荐开始-->
    <div class="service_search">
        <span class="keywords">投资</span>
        相关服务 <span class="nums">1239</span> 条
    </div>
    <!--一级分类推荐结束-->

    <!--服务分类选择开始-->
    <div class="service_selector">
        <dl class="clearfix">
            <dt>投资区域：</dt>
            <dd class="all"><a href="" title="">全部</a></dd>
            <dd><a href="" title="">长沙</a></dd>
            <dd><a href="" title="">深圳</a></dd>
            <dd><a href="" title="">北京</a></dd>
            <dd><a href="" title="">上海</a></dd>
        </dl>
        <dl class="clearfix">
            <dt>投资方向：</dt>
            <dd><a href="" title="">移动互联网</a></dd>
            <dd><a href="" title="">TMT</a></dd>
            <dd><a href="" title="">电子商务</a></dd>
            <dd><a href="" title="">绿色环保</a></dd>
            <dd><a href="" title="">生物科技</a></dd>
            <dd><a href="" title="">生态农业</a></dd>
            <dd><a href="" title="">新能源</a></dd>
        </dl>
        <dl class="clearfix">
            <dt>投资资金：</dt>
            <dd><a href="" title="">100万以内</a></dd>
            <dd><a href="" title="">100-500万</a></dd>
            <dd><a href="" title="">500-1000万</a></dd>
            <dd><a href="" title="">1000-5000万</a></dd>
            <dd><a href="" title="">5000万以上</a></dd>
        </dl>
        <dl class="clearfix">
            <dt>服务方式：</dt>
            <dd><a href="" title="">线上服务</a></dd>
            <dd><a href="" title="">电话咨询</a></dd>
            <dd><a href="" title="">视频对话</a></dd>
            <dd><a href="" title="">预约见面</a></dd>
            <dd><a href="" title="">巴菲特午餐</a></dd>
        </dl>
    </div>
    <!--服务分类选择结束-->

    <!--服务内容主体开始-->
    <div class="service_main clearfix">
        <!--条件筛选开始-->
        <form name="service_form" id="service_form">
            <div class="service_filter clearfix">
                <div class="status clearfix">
                    <a href="" title="" class="hover">推荐</a>
                    <a href="" title="" class="">最新</a>
                </div>
                <!--<div class="other clearfix">
                    <div class="other_common other_address">
                        <div class="selected">全部地区</div>
                        <div class="address_list list_common" id="address_list">
                            <ul>
                                <li attrid="1">华东地区</li>
                                <li attrid="2">华中地区</li>
                                <li attrid="3">华北地区</li>
                            </ul>
                        </div>
                        <i class="icon_service icon_down"></i>
                        <input name='address' value="" class="address" type="hidden" />
                    </div>
                    <div class="other_common other_service">
                        <div class="selected">全部服务方式</div>
                        <div class="way_list list_common" id="way_list">
                            <ul>
                                <li attrid="1">项目咨询</li>
                                <li attrid="2">天使投资</li>
                            </ul>
                        </div>
                        <i class="icon_service icon_down"></i>
                        <input name='service' value="" class="service" type="hidden" />
                    </div>
                </div>-->
            </div>
        </form>
        <!--条件筛选结束-->

        <!--筛选内容列表开始-->
        <div class="service_list clearfix">
            <dl>
                <dt>
                    <a href="${ctx}/service/search?key=天使投资" title="">
                        <img src="${ctx}/assets/demo/001.png" alt="" />

							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">天使投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span class="hover"><!--高亮样式hover-->
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">天使投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">PE投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/001.png" alt=""  />
                        <em>刘友常</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>

            <dl>
                <dt>
                    <a href="${ctx}/service/search?key=VC投资" title="">
                        <img src="${ctx}/assets/demo/002.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">风险投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>￥1500</em>元/起拍价
                </dd>
                <dd class="label clearfix">
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目咨询</i>
						</span>
						<span class="hover">
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">巴菲特午餐</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/002.png" alt=""  />
                        <em>李晓辉</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="${ctx}/service/search?key=PE投资" title="">
                        <img src="${ctx}/assets/demo/004.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">PE 投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span class="hover">
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">PE投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/004.png" alt=""  />
                        <em>张正大</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="${ctx}/service/search?key=投资" title="">
                        <img src="${ctx}/assets/demo/003.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">vc 投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>￥500</em>元/起拍价
                </dd>
                <dd class="label clearfix">
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目咨询</i>
						</span>
						<span class="hover">
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">巴菲特午餐</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/003.png" alt=""  />
                        <em>邱少</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="" title="">
                        <img src="${ctx}/assets/demo/005.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">天使投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">天使投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/005.png" alt=""  />
                        <em>张怡</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="" title="">
                        <img src="${ctx}/assets/demo/006.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">PE 投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span class="hover">
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">PE投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/006.png" alt=""  />
                        <em>张伟</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="" title="">
                        <img src="${ctx}/assets/demo/007.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">天使投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span class="hover">
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">天使投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/007.png" alt=""  />
                        <em>方便面</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="" title="">
                        <img src="${ctx}/assets/demo/008.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">创业投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span class="hover">
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">天使投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/008.png" alt=""  />
                        <em>刘无常</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="" title="">
                        <img src="${ctx}/assets/demo/009.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">天使投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>￥1000</em>元/起拍价
                </dd>
                <dd class="label clearfix">
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目咨询</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">巴菲特午餐</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/009.png" alt=""  />
                        <em>林少忠</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <a href="" title="">
                        <img src="${ctx}/assets/demo/010.png" alt="" />
							<span class="description">
								<em>
                                    [线上服务] 互联网项目投资，平均投资金额 100万 - 500万元。
                                </em>
							</span>
                    </a>
                </dt>
                <dd class="title clearfix">
                    <a href="" title="">PE 投资</a>
                </dd>
                <dd class="price clearfix">
                    <em>免费咨询</em>
                </dd>
                <dd class="label clearfix">
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">PE投资</i>
						</span>
						<span>
							<i class="icon_service icon_header"></i>
							<i class="icon_footer">项目投资</i>
						</span>
                </dd>
                <dd class="member clearfix">
                    <a href="" class="headpic clearfix" title="">
                        <img src="${ctx}/assets/demo/010.png" alt=""  />
                        <em>刘萍萍</em>
                    </a>
                    <a class="address">
                        湖南 长沙
                    </a>
                </dd>
            </dl>
        </div>
        <!--筛选内容列表结束-->

        <!--分页结构开始-->
        <div class="web_page clearfix">
            <ul>
                <li class="link prev">
                    <a href="" title=""><em><</em> 上一页</a>
                </li>
                <li class="link current"><!--当前页样式current-->
                    <a href="" title="">1</a>
                </li>
                <li class="link">
                    <a href="" title="">2</a>
                </li>
                <li class="link">
                    <a href="" title="">3</a>
                </li>
                <li class="link">
                    <a href="" title="">4</a>
                </li>
                <li class="link">
                    <a href="" title="">5</a>
                </li>
                <li class="link">
                    <a href="" title="">6</a>
                </li>
                <li class="more">
                    ...
                </li>
                <li class="link next">
                    <a href="" title="">下一页 <em>></em></a>
                </li>
                <li class="total">
                    共<em>124</em>页
                </li>
                <li class="jump">
                    到第<input type="text" value="1" name="pagenum" />页
                    <span class="submit_page">确定</span>
                </li>
            </ul>
        </div>
        <!--分页结构结束-->

    </div>
    <!--服务内容主体开始-->

</div>
<!--服务内容结束-->

<script type="text/javascript" >
    indexFlag = true;
</script>
</body>
</html>