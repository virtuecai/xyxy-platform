<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta id="wxKeywords" name="Keywords" content="星语心愿" />
<meta id="wxDescription" name="Description" content="星语心愿" />
<title>麓谷政策_企业服务_商务中心_星语心愿</title>
<!--全局resetCss样式开始-->
<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
<!--全局resetCss样式结束-->

<!--公用commonCss样式开始-->
<link href="${ctx}/assets/css/common/common.css" rel="stylesheet" type="text/css" />
<!--公用commonCss样式结束-->

<!--公用面包屑positionCss样式开始-->
<link href="${ctx}/assets/css/common/position.css" rel="stylesheet" type="text/css" />
<!--公用面包屑positionCss样式结束-->

<!--公用分页pageCss样式开始-->
<link href="${ctx}/assets/css/common/page.css" rel="stylesheet" type="text/css" />
<!--公用分页pageCss样式结束-->

<!--图片etalage缩放开始-->
<link href="${ctx}/assets/plugins/etalage/css/etalage.css" rel="stylesheet" type="text/css" />
<!--图片etalage缩放结束-->

<!--页面Css样式开始-->
<link href="${ctx}/assets/css/service/detail_test.css" rel="stylesheet" type="text/css" />
<!--页面Css样式结束-->

<!--主题Css样式开始-->
<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
<!--主题Css样式结束-->
</head>

<body>
<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">商务中心</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">企业服务</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">招商</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->

	<!--服务详情内容开始-->
	<div class="service_container clearfix">

		<!--左侧服务内容开始-->
		<div class="container_left">

			<!--预定服务开始-->
			<div class="service_reserve clearfix">

				<!--滚动图片展示开始-->
				<div class="service_photo">
					<ul id="etalage">
						<li>
							<img class="etalage_thumb_image"  src="${ctx}/assets/images/content/lxj.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj.jpg" title="VC投资" />
							<!--第二张图放大图-->
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
					</ul>
				</div>
				<!--滚动图片展示结束-->

				<!--服务介绍开始-->
				<div class="service_introduce" id="service_introduce">
					<form action="" name="service_form" id="service_form" method="">
						<ul>
							<li class="title">
								麓谷政策
							</li>
							<li class="label lugu_label">
								<span><!--高亮样式hover-->
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">回湘企业优惠政策</i>
								</span>
								<span class="hover">
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">回湘人才优惠政策</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">本地企业优惠政策</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">本地人才优惠政策</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">巴特非午餐</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">其它优惠政策</i>
								</span>
							</li>
							<li class="cost">
								<em>费用：</em>
								<span>免费</span>
							</li>
							<li class="common_li time">
								<em>服务时间：</em>
								<i class="icon_service icon_down">&nbsp;</i>
								<input name="service_time" type="text" id="service_time" placeholder="全天" class="" value="" onFocus="WdatePicker({minDate:'%y-%M-{%d}'})" />
							</li>
							<!--<li class="common_li time_block clearfix" id="time_block">
								<em>具体时间：</em>
								<span class="clearfix">
									<i class="btn can hover">09:00-12:00</i>
									<i class="btn can">17:00-21:00</i>
									<i class="btn can">21:00-24:00</i>
								</span>
								&lt;!&ndash;注意：hover为高亮样式，&ndash;&gt;
							</li>-->
							<li class="common_li ways clearfix" id="ways">
								<em>服务方式：</em>
								<span class="clearfix">
									<i class="btn hover">线上服务</i>
									<i class="btn">电子咨询</i>
									<i class="btn">视频对话</i>
									<i class="btn">预约见面</i>
								</span>
								<!--注意：不同的服务方式会对应订单的不同地址-->
							</li>
							<li class="handle">
								<span class="btn subscribe" id="subscribe">立即预约</span>
								<span class="btn addcart" id="addcart">加入心愿车</span>
							</li>
							<li class="tools lugu_tools clearfix">
								<span id="collect" class="clearfix">
									<i class="icon_service icon_collect"></i>
									<em>收藏</em>
									<!--<i class="icon_service icon_collected"></i>
									<em class="orange">已收藏</em>-->
								</span>
								<span id="share" class="share bdsharebuttonbox clearfix">
									<a class="bds_more" data-cmd="more"></a>
									<em>分享</em>
								</span>
								<span id="comment" class="comment clearfix">
									<i class="icon_service icon_comment"></i>
									<em>评论</em>
									<em class="num">4</em>
								</span>
								<span id="report" class="report clearfix">
									<!--<i class="icon_service icon_report"></i>-->
									<em>举报</em>
								</span>
							</li>
						</ul>
					</form>
				</div>
				<!--服务介绍开始-->

			</div>
			<!--预定服务开始-->

			<!--服务详情、评价、案例开始-->
			<div class="service_detail">

				<!--头部选项开始-->
				<div class="service_detail_header" id="service_detail_header">
					<ul class="clearfix">
						<li attrid="detail_content" class="hover">
							<i class="icon_service icon_down"></i>
							服务详情
						</li>
						<li attrid="detail_comment" class="">
							<i class="icon_service icon_down"></i>
							顾客评价
							<em class="comments_num">12</em>
						</li>
						<li attrid="detail_case" class="">
							<i class="icon_service icon_down"></i>
							我的案例
						</li>
					</ul>
				</div>
				<!--头部选项结束-->

				<!--底部显示开始-->
				<div class="service_detail_footer">

					<!--详情内容开始-->
					<div class="detail_common detail_content" id="detail_content">
						<!--以下为测试内容，做动态处理时可删除--开始-->
						<p>
							1. 区内高新技术企业从被认定之日起，减按15%的税率征收企业所得税。<br/><br/>
							2. 每年安排2亿元产业发展专项财政资金，用于支持重大支柱产业、特色产业和技术创新项目。
							另外，每年安排2000万元产业发展专项资金，用于支持服务外包、动漫游戏、电子商务企业市场拓展、人才培训、公共服务平台建设和企业资质认证等。<br/><br/>
							3．对一次性固定资产投资在5亿元以上的重大电子信息行业生产经营企业，
							在项目投产后，根据项目对高新区财税的贡献情况，给予其按固定资产投资总额不低于10%的产业发展配套资金支持。<br/><br/>
							4.对一次性固定资产投资形成单个环节（在拉晶、铸锭、切片、封装四个环节中）的产能达到750MW或整个产业链500MW以上的重大光伏生产经营企业，在项目实施过程中，
							根据项目对高新区财税的贡献情况，分期给予其按固定资产投资总额10%以内（含10%）的光伏产业专项发展资金支持。<br/><br/>
							5．对未在麓谷设立生产基地的各类企业总部和地区总部，其年纳税总额在500万元（含500万元）以上的，视其财政贡献情况，
							经审定，自营运之日起3年内按其所缴税收高新区财政实得部分不低于10%的比例给予奖励。<br/><br/>
							6．对世界500强、国内100强企业进入信息产业园从事软件、服务外包、电子商务、动漫文化创意、电子信息制造产业研发生产或设立企业总部、地区总部，
							根据其品牌影响、投资规模和产出效益等情况，一次性给予100至200万元的产业发展配套资金支持。<br/><br/>
							7. 上述3、4、5、6款所指企业和企业总部、区域总部的高管人员如果获得长沙高新区年度表彰的有突出贡献的创新专家、优秀企业家和优秀科技人才，自获奖之日起，
							三年之内在麓谷首次购房的，其购房前3年度个人所得税高新区财政实得部分的100%奖励给本人，其奖励额度最高不超过房价的的80%。<br/><br/>
							8. 根据受贷企业新增税收情况，高新区财政给予不超过银行贷款利息额的40%的贷款贴息，但同一企业获得的各级政府支持的总贴息额最高不超过贷款利息额的100%。<br/><br/>
							9. 区内企业根据情况可采取提前报关、联网报关、快速报关、上门验收、加急通关、担保验收等便捷通关措施。<br/><br/>
						</p>
					</div>
					<!--详情内容结束-->

					<!--评价内容开始-->
					<div class="detail_common detail_comment" id="detail_comment">


					</div>
					<!--评价内容结束-->

					<!--我的案例开始-->
					<div class="detail_common detail_content detail_case" id="detail_case">

					</div>
					<!--我的案例结束-->

				</div>
				<!--底部显示结束-->

			</div>
			<!--服务详情、评价、案例结束-->

		</div>
		<!--左侧服务内容结束-->

		<!--右侧人物介绍开始-->
		<div class="container_right">
			<div class="people_focus clearfix">
				<!--主体居中开始-->
				<div class="people_focus_main">
					<div class="focus_img">
						<a href="" title=""><img src="${ctx}/assets/images/content/lxj.jpg" alt=""  /></a>
					</div>
					<div class="focus_intro">
						<span class="name">赵伟</span>
						<span class="label">
							<i class="icon_service icon_zheng"></i>
	                    	<i class="icon_service icon_v_member"></i>
						</span>
						<span class="focus_on">
							<!--未关注-->
							<em class="btn onfocus cmain_bg_color">+ 关注</em>
							<!--已关注-->
							<!--<em class="btn offfocus bg_gray">已关注</em>-->
						</span>
					</div>
				</div>
				<!--主体居中结束-->
			</div>
			<div class="people_intro">
				<ul>
					<li class="label clearfix">
						<span class="clearfix"><!--高亮样式hover-->
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">长沙信息产业园优惠咨询</i>
						</span>
					</li>
					<li class="description">
						长沙信息产业园起步于1997年，是国家火炬计划首批“四大软件园”之一，建立了国家软件产业基地、动漫游戏产业振兴基地、移动电子商务示范基地、数字媒体产业基地等17个国家级产业基地。
						园区聚集了通信与互联网、软件与信息服务、文化创意、电子信息制造等各类企业1500多家，
						引进58同城、金蝶软件、中清龙图、善领云商、友阿云商、央视商城、易宝支付、江苏名通、万达信息、上海帝联、京湘联集团等600多家知名移动互联网企业，
						省市政府及长沙高新区已出台一系列政策，力争将信息产业园打造成中国移动互联网产业聚集区和创新高地。
					</li>
					<li class="resfuse">
						<h3>关于退款</h3>
						订单取消时，已冻结金额将退至您的余额中。您可以选择下次继续预约，或申请退款至您的支付宝账户。
					</li>
				</ul>
			</div>
		</div>
		<!--右侧人物介绍结束-->

	</div>
	<!--服务详情内容结束-->

</div>
<!--身体结束-->

<javascript-list>
<!--etalage图片缩放插件开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/etalage/js/jquery.etalage.min.js"></script>
<!--etalage图片缩放插件结束-->
<!--日期插件datepicker开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
<!--日期插件datepicker结束-->
<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->
<!--页面JS结束-->
<script type="text/javascript">
	$(function(){


		//缩略图轮播
		$('#etalage').etalage({
			thumb_image_width: 300,
			thumb_image_height: 300,
			source_image_width: 900,
			source_image_height: 900,
			show_hint: false,
			show_icon: false,
			small_thumbs: 5,
			click_callback: function(image_anchor, instance_id){}
		});

		//选择服务方式
		$('#service_introduce ul li.ways i').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//选择时间段
		$('#service_introduce ul li.time_block i.can').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//服务时间点选择
		$('#service_time').on('focus',function(){
			var objVal = $(this).val();
			if(objVal){

			}
		});

		//预约服务提交表单
		$('#subscribe').on('click',function(){
			$('#service_form').submit();
		});

		//加入心愿车
		$('#addcart').on('click',function(){
			//操作
		});

		//百度分享插件
		window._bd_share_config = {
			"common": {
				"bdSnsKey": {},
				"bdText": "",
				"bdMini": "2",
				"bdMiniList": false,
				"bdPic": "",
				"bdStyle": "2",
				"bdSize": "16"
			},
			"share": {}
		};
		with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];

		//详情、评论选项卡
		$('#service_detail_header>ul>li').on('click',function(){
			var attrid = $(this).attr('attrid');
			$(this).addClass('hover').siblings().removeClass('hover');
			$('#'+attrid).fadeIn().siblings('.detail_common').hide();
		});

		//加载相册扩展模块
		layer.config({
			extend: 'extend/layer.ext.js'
		});
		//初始化layer
		layer.ready(function(){
			//使用相册
			layer.photos({
				photos: '#msg_imglist'
			});
		});
	})
</script>
	</javascript-list>
</body>
</html>
