<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta id="wxKeywords" name="Keywords" content="星语心愿" />
<meta id="wxDescription" name="Description" content="星语心愿" />
<title>VC投资_投资咨询_理财_财富理财保险_星语心愿</title>
<!--全局resetCss样式开始-->
<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
<!--全局resetCss样式结束-->

<!--公用commonCss样式开始-->
<link href="${ctx}/assets/css/common/common.css" rel="stylesheet" type="text/css" />
<!--公用commonCss样式结束-->

<!--公用面包屑positionCss样式开始-->
<link href="${ctx}/assets/css/common/position.css" rel="stylesheet" type="text/css" />
<!--公用面包屑positionCss样式结束-->

<!--公用分页pageCss样式开始-->
<link href="${ctx}/assets/css/common/page.css" rel="stylesheet" type="text/css" />
<!--公用分页pageCss样式结束-->

<!--图片etalage缩放开始-->
<link href="${ctx}/assets/plugins/etalage/css/etalage.css" rel="stylesheet" type="text/css" />
<!--图片etalage缩放结束-->

<!--页面Css样式开始-->
<link href="${ctx}/assets/css/service/detail_test.css" rel="stylesheet" type="text/css" />
<!--页面Css样式结束-->

<!--主题Css样式开始-->
<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
<!--主题Css样式结束-->
</head>

<body>
<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">财富理财保险</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">理财</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">投资咨询</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">VC投资</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->

	<!--服务详情内容开始-->
	<div class="service_container clearfix">

		<!--左侧服务内容开始-->
		<div class="container_left">

			<!--预定服务开始-->
			<div class="service_reserve clearfix">

				<!--滚动图片展示开始-->
				<div class="service_photo">
					<ul id="etalage">
						<li>
							<img class="etalage_thumb_image"  src="${ctx}/assets/images/content/lxj.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj.jpg" title="VC投资" />
							<!--第二张图放大图-->
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/lxj_content.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/lxj_content.jpg" title="VC投资" />
						</li>
					</ul>
				</div>
				<!--滚动图片展示结束-->

				<!--服务介绍开始-->
				<div class="service_introduce" id="service_introduce">
					<form action="" name="service_form" id="service_form" method="">
						<ul>
							<li class="title">
								风险投资
							</li>
							<li class="label">
								<span><!--高亮样式hover-->
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">天使投资</i>
								</span>
								<span class="hover">
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">VC咨询</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">PE投资</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">巴菲特午餐</i>
								</span>
							</li>
							<li class="cost">
								<em>费用：</em>
								<span>免费</span>
							</li>
							<li class="common_li time">
								<em>服务时间：</em>
								<i class="icon_service icon_down">&nbsp;</i>
								<input name="service_time" type="text" id="service_time" placeholder="请选择购买服务时间" class="" value="" onFocus="WdatePicker({minDate:'%y-%M-{%d}'})" />
							</li>
							<li class="common_li time_block clearfix" id="time_block">
								<em>具体时间：</em>
								<span class="clearfix">
									<i class="btn can hover">09:00-12:00</i>
									<i class="btn can">17:00-21:00</i>
									<i class="btn can">21:00-24:00</i>
								</span>
								<!--注意：hover为高亮样式，-->
							</li>
							<li class="common_li ways clearfix" id="ways">
								<em>服务方式：</em>
								<span class="clearfix">
									<i class="btn">线上服务</i>
									<i class="btn">电子咨询</i>
									<i class="btn hover">视频对话</i>
									<i class="btn">预约见面</i>
								</span>
								<!--注意：不同的服务方式会对应订单的不同地址-->
							</li>
							<li class="handle">
								<span class="btn subscribe" id="subscribe">立即预约</span>
								<span class="btn addcart" id="addcart">加入心愿车</span>
							</li>
							<li class="tools clearfix">
								<span id="collect" class="clearfix">
									<i class="icon_service icon_collect"></i>
									<em>收藏</em>
									<!--<i class="icon_service icon_collected"></i>
									<em class="orange">已收藏</em>-->
								</span>
								<span id="share" class="share bdsharebuttonbox clearfix">
									<a class="bds_more" data-cmd="more"></a>
									<em>分享</em>
								</span>
								<span id="comment" class="comment clearfix">
									<i class="icon_service icon_comment"></i>
									<em>评论</em>
									<em class="num">4</em>
								</span>
								<span id="report" class="report clearfix">
									<!--<i class="icon_service icon_report"></i>-->
									<em>举报</em>
								</span>
							</li>
						</ul>
					</form>
				</div>
				<!--服务介绍开始-->

			</div>
			<!--预定服务开始-->

			<!--服务详情、评价、案例开始-->
			<div class="service_detail">

				<!--头部选项开始-->
				<div class="service_detail_header" id="service_detail_header">
					<ul class="clearfix">
						<li attrid="detail_content" class="hover">
							<i class="icon_service icon_down"></i>
							服务详情
						</li>
						<li attrid="detail_comment" class="">
							<i class="icon_service icon_down"></i>
							顾客评价
							<em class="comments_num">12</em>
						</li>
						<li attrid="detail_case" class="">
							<i class="icon_service icon_down"></i>
							我的案例
						</li>
					</ul>
				</div>
				<!--头部选项结束-->

				<!--底部显示开始-->
				<div class="service_detail_footer">

					<!--详情内容开始-->
					<div class="detail_common detail_content" id="detail_content">
						<!--以下为测试内容，做动态处理时可删除--开始-->
						<h2>为什么要约我</h2>
						<p class="clearfix">
							<img src="${ctx}/assets/images/content/lxj.jpg" alt="" />
						</p>
						<p>
							我的工作给了我认识各种有意思人的机会。我有很多基友也有闺蜜。
							我喜欢看细腻描写人性的小说，电影和话剧。我会一个人坐在大街旁观察行人或发呆，我也喜欢看一些通俗的娱乐节目且为提前设置好的结果而紧张。我珍惜每个人都有自己丰富的内心世界，喜怒哀乐。
							我认为每个宗教都有自己存在和传承的意义。我是一个个体人文主义者，尽管我还是爱给人打标签。
							我不觉得我是个有特殊技能或品质的人，走到今天很多事情都是偶然。
							谈话的过程也是我们共同学习的过程，我相信serendipity(机缘巧合） 。
							我相信生命中有很多无法解释的潜在，我选择善良高于自私，我喜欢睿智多于聪明，我倾向自然多于美丽。
							我觉得每一次相遇都是生命中的缘分。我希望我们短暂的对话让你认识一个真实的我和你自己。
						</p>
					</div>
					<!--详情内容结束-->

					<!--评价内容开始-->
					<div class="detail_common detail_comment" id="detail_comment">


					</div>
					<!--评价内容结束-->

					<!--我的案例开始-->
					<div class="detail_common detail_content detail_case" id="detail_case">
						<!--以下为测试内容，做动态处理时可删除--开始
						<p>
							1、2012年投资12308汽车票，12308是由中国道路运输协会和全国站场工作委员会合作共建的全国公路客运互联网信息服务平台。
							广大旅客通过手机端（客户端app或者微信公众号）或者PC端均可直接登录平台查询出行所需的车站、车次、线路、票价等信息，并购买出行汽车票。
							12308通过网站及客户端的全平台覆盖，从而为广大旅客提供更为优化便捷的出行解决方案。
						</p>
						<p class="clearfix">
							<img src="${ctx}/assets/images/content/case_01.jpg" alt="" />
							<img src="${ctx}/assets/images/content/case_02.jpg" alt="" />
						</p>
						<p>视频链接地址：http://tv.people.com.cn/n/2015/0213/c39805-26560811.html</p>
						<p>
							2、2015年创办鼎晖投资，鼎晖投资的和美医疗集团是国内第一家登录资本巿场的妇产专科连锁医疗集团，上市后更多的社会资本的注入以及医疗改革的推进、
							大力扶持私立医疗机构发展、全面放开二胎等一系列利好政策的推动下，凭借其品牌定位、资本、规模，为和美医疗终将成为中国第一高端妇儿医疗集团提供了可能。根据弗若斯特沙利文的行业研究报告数据，
							2009年至2013年，我国妇幼保健院复合年增长率为2.0%，而私立妇产专科医院复合年增长率为18.0%，增长率远高于公立妇产医院。
						</p>
						<p>
							<img src="${ctx}/assets/images/content/case_03.jpg" alt="" />
						</p>
						以上为测试内容，做动态处理时可删除--结束-->
					</div>
					<!--我的案例结束-->

				</div>
				<!--底部显示结束-->

			</div>
			<!--服务详情、评价、案例结束-->

		</div>
		<!--左侧服务内容结束-->

		<!--右侧人物介绍开始-->
		<div class="container_right">
			<div class="people_focus clearfix">
				<!--主体居中开始-->
				<div class="people_focus_main">
					<div class="focus_img">
						<a href="" title=""><img src="${ctx}/assets/images/content/lxj.jpg" alt=""  /></a>
					</div>
					<div class="focus_intro">
						<span class="name">李晓军</span>
						<span class="label">
							<i class="icon_service icon_zheng"></i>
	                    	<i class="icon_service icon_v_member"></i>
						</span>
						<span class="focus_on">
							<!--未关注-->
							<em class="btn onfocus cmain_bg_color">+ 关注</em>
							<!--已关注-->
							<!--<em class="btn offfocus bg_gray">已关注</em>-->
						</span>
					</div>
				</div>
				<!--主体居中结束-->
			</div>
			<div class="people_intro">
				<ul>
					<li class="label clearfix">
						<span class="clearfix hover"><!--高亮样式hover-->
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">创业投资</i>
						</span>
						<span class="clearfix">
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">投资人</i>
						</span>
					</li>
					<li class="description">
						我是李骁军，我生长于河南的一个小县城，后来去了一个不大不小的城市读大学，毕业后到了美国最大的乡村大都市留学工作。
						因为厌倦了南加州不变的晴好天气折腾到了东海岸。11年前开始了双城记的海鸥生活。
						再一次的太平洋穿越和国内的出差让我成为了众多航空公司的终身白金卡会员。
					</li>
					<li class="resfuse">
						<h3>关于退款</h3>
						订单取消时，已冻结金额将退至您的余额中。您可以选择下次继续预约，或申请退款至您的支付宝账户。
					</li>
				</ul>
			</div>
		</div>
		<!--右侧人物介绍结束-->

	</div>
	<!--服务详情内容结束-->

</div>
<!--身体结束-->


<javascript-list>
<!--etalage图片缩放插件开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/etalage/js/jquery.etalage.min.js"></script>
<!--etalage图片缩放插件结束-->
<!--日期插件datepicker开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
<!--日期插件datepicker结束-->
<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->
<!--页面JS结束-->
<script type="text/javascript">
	$(function(){

		//缩略图轮播
		$('#etalage').etalage({
			thumb_image_width: 300,
			thumb_image_height: 300,
			source_image_width: 900,
			source_image_height: 900,
			show_hint: false,
			show_icon: false,
			small_thumbs: 5,
			click_callback: function(image_anchor, instance_id){}
		});

		//选择服务方式
		$('#service_introduce ul li.ways i').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//选择时间段
		$('#service_introduce ul li.time_block i.can').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//服务时间点选择
		$('#service_time').on('focus',function(){
			var objVal = $(this).val();
			if(objVal){

			}
		});

		//预约服务提交表单
		$('#subscribe').on('click',function(){
			$('#service_form').submit();
		});

		//加入心愿车
		$('#addcart').on('click',function(){
			//操作
		});

		//百度分享插件
		window._bd_share_config = {
			"common": {
				"bdSnsKey": {},
				"bdText": "",
				"bdMini": "2",
				"bdMiniList": false,
				"bdPic": "",
				"bdStyle": "2",
				"bdSize": "16"
			},
			"share": {}
		};
		with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];

		//详情、评论选项卡
		$('#service_detail_header>ul>li').on('click',function(){
			var attrid = $(this).attr('attrid');
			$(this).addClass('hover').siblings().removeClass('hover');
			$('#'+attrid).fadeIn().siblings('.detail_common').hide();
		});

		//加载相册扩展模块
		layer.config({
			extend: 'extend/layer.ext.js'
		});
		//初始化layer
		layer.ready(function(){
			//使用相册
			layer.photos({
				photos: '#msg_imglist'
			});
		});
	})
</script>
	</javascript-list>
</body>
</html>
