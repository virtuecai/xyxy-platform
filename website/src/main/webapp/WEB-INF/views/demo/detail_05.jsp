<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta id="wxKeywords" name="Keywords" content="星语心愿" />
<meta id="wxDescription" name="Description" content="星语心愿" />
<title>法语导游_导游_出行旅游_星语心愿</title>
<!--全局resetCss样式开始-->
<link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
<!--全局resetCss样式结束-->

<!--公用commonCss样式开始-->
<link href="${ctx}/assets/css/common/common.css" rel="stylesheet" type="text/css" />
<!--公用commonCss样式结束-->

<!--公用面包屑positionCss样式开始-->
<link href="${ctx}/assets/css/common/position.css" rel="stylesheet" type="text/css" />
<!--公用面包屑positionCss样式结束-->

<!--公用分页pageCss样式开始-->
<link href="${ctx}/assets/css/common/page.css" rel="stylesheet" type="text/css" />
<!--公用分页pageCss样式结束-->

<!--图片etalage缩放开始-->
<link href="${ctx}/assets/plugins/etalage/css/etalage.css" rel="stylesheet" type="text/css" />
<!--图片etalage缩放结束-->

<!--页面Css样式开始-->
<link href="${ctx}/assets/css/service/detail_test.css" rel="stylesheet" type="text/css" />
<!--页面Css样式结束-->

<!--主题Css样式开始-->
<link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
<!--主题Css样式结束-->
</head>

<body>
<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">出行旅游</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">导游</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">法语导游</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->

	<!--服务详情内容开始-->
	<div class="service_container clearfix">

		<!--左侧服务内容开始-->
		<div class="container_left">

			<!--预定服务开始-->
			<div class="service_reserve clearfix">

				<!--滚动图片展示开始-->
				<div class="service_photo">
					<ul id="etalage">
						<li>
							<img class="etalage_thumb_image"  src="${ctx}/assets/images/content/dh.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/dh.jpg" title="美容" />
							<!--第二张图放大图-->
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/dh_content_02.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/dh_content_02.jpg" title="美容" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/dh_content_02.jpg" />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/dh_content_02.jpg" title="美容" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/dh.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/dh.jpg" title="美容" />
						</li>
						<li>
							<img class="etalage_thumb_image" src="${ctx}/assets/images/content/dh_content_02.jpg"  />
							<img class="etalage_source_image" src="${ctx}/assets/images/content/dh_content_02.jpg" title="美容" />
						</li>
					</ul>
				</div>
				<!--滚动图片展示结束-->

				<!--服务介绍开始-->
				<div class="service_introduce" id="service_introduce">
					<form action="" name="service_form" id="service_form" method="">
						<ul>
							<li class="title">
								美容
							</li>
							<li class="label lugu_label">
								<span><!--高亮样式hover-->
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">美容养生</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">微整咨询</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">服装搭配</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">彩妆</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">美甲</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">脸部护理</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">情感顾问</i>
								</span>
								<span>
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">减肥</i>
								</span>
								<span class="hover">
									<i class="icon_service icon_header"></i>
									<i class="icon_footer">形象设计</i>
								</span>
							</li>
							<li class="cost">
								<em>费用：</em>
								<span>免费</span>
							</li>
							<li class="common_li time">
								<em>服务时间：</em>
								<i class="icon_service icon_down">&nbsp;</i>
								<input name="service_time" type="text" id="service_time" placeholder="全天" class="" value="" onFocus="WdatePicker({minDate:'%y-%M-{%d}'})" />
							</li>
							<!--<li class="common_li time_block clearfix" id="time_block">
								<em>具体时间：</em>
								<span class="clearfix">
									<i class="btn can hover">09:00-12:00</i>
									<i class="btn can">17:00-21:00</i>
									<i class="btn can">21:00-24:00</i>
								</span>
								&lt;!&ndash;注意：hover为高亮样式，&ndash;&gt;
							</li>-->
							<li class="common_li ways clearfix" id="ways">
								<em>服务方式：</em>
								<span class="clearfix">
									<i class="btn">线上服务</i>
									<i class="btn">电子咨询</i>
									<i class="btn hover">视频对话</i>
									<i class="btn">预约见面</i>
								</span>
								<!--注意：不同的服务方式会对应订单的不同地址-->
							</li>
							<li class="handle">
								<span class="btn subscribe" id="subscribe">立即预约</span>
								<span class="btn addcart" id="addcart">加入心愿车</span>
							</li>
							<li class="tools lugu_tools clearfix">
								<span id="collect" class="clearfix">
									<i class="icon_service icon_collect"></i>
									<em>收藏</em>
									<!--<i class="icon_service icon_collected"></i>
									<em class="orange">已收藏</em>-->
								</span>
								<span id="share" class="share bdsharebuttonbox clearfix">
									<a class="bds_more" data-cmd="more"></a>
									<em>分享</em>
								</span>
								<span id="comment" class="comment clearfix">
									<i class="icon_service icon_comment"></i>
									<em>评论</em>
									<em class="num">4</em>
								</span>
								<span id="report" class="report clearfix">
									<!--<i class="icon_service icon_report"></i>-->
									<em>举报</em>
								</span>
							</li>
						</ul>
					</form>
				</div>
				<!--服务介绍开始-->

			</div>
			<!--预定服务开始-->

			<!--服务详情、评价、案例开始-->
			<div class="service_detail">

				<!--头部选项开始-->
				<div class="service_detail_header" id="service_detail_header">
					<ul class="clearfix">
						<li attrid="detail_content" class="hover">
							<i class="icon_service icon_down"></i>
							服务详情
						</li>
						<li attrid="detail_comment" class="">
							<i class="icon_service icon_down"></i>
							顾客评价
							<em class="comments_num">12</em>
						</li>
						<li attrid="detail_case" class="">
							<i class="icon_service icon_down"></i>
							我的案例
						</li>
					</ul>
				</div>
				<!--头部选项结束-->

				<!--底部显示开始-->
				<div class="service_detail_footer">

					<!--详情内容开始-->
					<div class="detail_common detail_content" id="detail_content">
						<!--以下为测试内容，做动态处理时可删除--开始-->
						<p class="clearfix">
							<img src="${ctx}/assets/images/content/dh_content_01.jpg" alt="" />
							<img src="${ctx}/assets/images/content/dh_content_02.jpg" alt="" />
						</p>
						<p>
							面对琳琅满目的的护肤和彩妆品，大家难免都会产生疑问：同样的功效我该选择哪个品牌的产品？为什么用了某个产品却没有相应的效果？哪种彩妆品最适合我的肤色？为什么明星卸妆和上妆后反差如此之大，而会有人说我化妆还不如不化好看……<br/><br/>
							实际上，实际上这一切问题都源于没有找到和正确使用适合自己的护肤和彩妆产品。我可以针对你个人的肌肤状况和生活习惯，定制出最适合你的护肤和彩妆方案。<br/>
							痘痘的成因有无数种，可以根据你的具体成因，帮你制定相应的“战痘方案”。<br/><br/>
							油性皮肤有拯救方案么？保湿和控油到底有什么联系？如何才能够有效保湿？<br/>
							美白淡斑如何有效，白，不能否认有基因的原因但怎样在在身条件下配合合适的产品和方法让自己肌肤胜雪呢?<br/>
							抗衰老和抗氧化是永远的话题，这是一个无比巨大且复杂的过程。遗憾的是，目前并不存在返老还童的方法，但确实通过一些护肤品，配以规律的日常生活，能够让皮肤更加透亮白皙，让假性皱纹淡化。<br/>
							选择哪些颜色和质地的彩妆产品，化出适合自己的妆容，达到一个微整形的效果<br/>
							如何10分钟内化出完美日常妆容，不同场合的妆容该如何快速转换，妆容与服装该如何搭配<br/>
							除了适合的产品以外，适合的使用方法也不可或缺。怎样按摩可以令面部皮肤紧致；使用什么手法可以让100元的护肤品产生1000元的功效；什么样的搭配可以产生1+1》2的效果，令眼睛明眸善睐……<br/>
						</p>
					</div>
					<!--详情内容结束-->

					<!--评价内容开始-->
					<div class="detail_common detail_comment" id="detail_comment">


					</div>
					<!--评价内容结束-->

					<!--我的案例开始-->
					<div class="detail_common detail_content detail_case" id="detail_case">

					</div>
					<!--我的案例结束-->

				</div>
				<!--底部显示结束-->

			</div>
			<!--服务详情、评价、案例结束-->

		</div>
		<!--左侧服务内容结束-->

		<!--右侧人物介绍开始-->
		<div class="container_right">
			<div class="people_focus clearfix">
				<!--主体居中开始-->
				<div class="people_focus_main">
					<div class="focus_img">
						<a href="" title=""><img src="${ctx}/assets/images/content/dh.jpg" alt=""  /></a>
					</div>
					<div class="focus_intro">
						<span class="name">丁华</span>
						<span class="label">
							<i class="icon_service icon_zheng"></i>
	                    	<i class="icon_service icon_v_member"></i>
						</span>
						<span class="focus_on">
							<!--未关注-->
							<em class="btn onfocus cmain_bg_color">+ 关注</em>
							<!--已关注-->
							<!--<em class="btn offfocus bg_gray">已关注</em>-->
						</span>
					</div>
				</div>
				<!--主体居中结束-->
			</div>
			<div class="people_intro">
				<ul>
					<li class="label clearfix">
						<span class="clearfix"><!--高亮样式hover-->
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">形象设计</i>
						</span>
						<span class="clearfix">
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">情感顾问</i>
						</span>
						<span class="clearfix">
							<i class="icon_service icon_header"></i>
							<i class="icon_service icon_footer">减肥</i>
						</span>
					</li>
					<li class="description">
						面对琳琅满目的的护肤和彩妆品，大家难免都会产生疑问：同样的功效我该选择哪个品牌的产品？
						为什么用了某个产品却没有相应的效果？哪种彩妆品最适合我的肤色？为什么明星卸妆和上妆后反差如此之大，而会有人说我化妆还不如不化好看……
					</li>
					<li class="resfuse">
						<h3>关于退款</h3>
						订单取消时，已冻结金额将退至您的余额中。您可以选择下次继续预约，或申请退款至您的支付宝账户。
					</li>
				</ul>
			</div>
		</div>
		<!--右侧人物介绍结束-->

	</div>
	<!--服务详情内容结束-->

</div>
<!--身体结束-->


<javascript-list>
<!--etalage图片缩放插件开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/etalage/js/jquery.etalage.min.js"></script>
<!--etalage图片缩放插件结束-->
<!--日期插件datepicker开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/datepicker/WdatePicker.js"></script>
<!--日期插件datepicker结束-->
<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->
<!--页面JS结束-->
<script type="text/javascript">
	$(function(){


		//缩略图轮播
		$('#etalage').etalage({
			thumb_image_width: 300,
			thumb_image_height: 300,
			source_image_width: 900,
			source_image_height: 900,
			show_hint: false,
			show_icon: false,
			small_thumbs: 5,
			click_callback: function(image_anchor, instance_id){}
		});

		//选择服务方式
		$('#service_introduce ul li.ways i').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//选择时间段
		$('#service_introduce ul li.time_block i.can').on('click',function(){
			$(this).addClass('hover').siblings('i').removeClass('hover');
		});

		//服务时间点选择
		$('#service_time').on('focus',function(){
			var objVal = $(this).val();
			if(objVal){

			}
		});

		//预约服务提交表单
		$('#subscribe').on('click',function(){
			$('#service_form').submit();
		});

		//加入心愿车
		$('#addcart').on('click',function(){
			//操作
		});

		//百度分享插件
		window._bd_share_config = {
			"common": {
				"bdSnsKey": {},
				"bdText": "",
				"bdMini": "2",
				"bdMiniList": false,
				"bdPic": "",
				"bdStyle": "2",
				"bdSize": "16"
			},
			"share": {}
		};
		with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];

		//详情、评论选项卡
		$('#service_detail_header>ul>li').on('click',function(){
			var attrid = $(this).attr('attrid');
			$(this).addClass('hover').siblings().removeClass('hover');
			$('#'+attrid).fadeIn().siblings('.detail_common').hide();
		});

		//加载相册扩展模块
		layer.config({
			extend: 'extend/layer.ext.js'
		});
		//初始化layer
		layer.ready(function(){
			//使用相册
			layer.photos({
				photos: '#msg_imglist'
			});
		});
	})
</script>
	</javascript-list>
</body>
</html>
