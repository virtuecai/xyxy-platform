<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/1/5 0005
  Time: 14:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta id="wxKeywords" name="Keywords" content="星语星愿" />
  <meta id="wxDescription" name="Description" content="星语星愿" />
  <title>用户登录_星语星愿</title>
  <!--全局resetCss样式开始-->
  <link href="${ctx}/assets/css/reset.css" rel="stylesheet" type="text/css" />
  <!--全局resetCss样式结束-->

  <!--公用commonCss样式开始-->
  <link href="${ctx}/assets/css/common/common.css" rel="stylesheet" type="text/css" />
  <!--公用commonCss样式结束-->

  <!--公用formCss样式开始-->
  <link href="${ctx}/assets/css/common/form.css" rel="stylesheet" type="text/css" />
  <!--公用formCss样式结束-->

  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/public/login.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->

  <!--主题themeCss样式开始-->
  <link href="${ctx}/assets/css/theme.css" rel="stylesheet" type="text/css" />
  <!--主题themeCss样式结束-->
  <!-- 全局公共样式 -->
  <link href="${ctx}/assets/css/common/application.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<!--头部开始-->
<div class="header clearfix">


</div>
<!--头部结束-->

<!--身体开始-->
<div class="layer">

  <!--头部LOGO开始-->
  <div class="public_common public_logo">
    <a href="${ctx}/" title="">
      <img src="assets/images/logo.jpg" alt="" />
    </a>
  </div>
  <!--头部LOGO结束-->

  <!--登录模块开始-->
  <div class="login_module">

    <!--模块背景开始-->
    <div class="module_bg" style="background-image:url(${ctx}/assets/images/content/public_bg.jpg);"></div>
    <!--模块背景结束-->

    <!--模块内容开始-->
    <div class="module_content">
      <!--主体内容开始-->
      <div class="public_common module_content_main clearfix">

        <!--内容广告开始-->
        <div class="content_main_ad">
          <img src="assets/images/content/bg_word.png" alt=""  />
        </div>
        <!--内容广告结束-->

        <!--内容表单开始-->
        <div class="module_form_box content_main_form">
          <form name="login_form" id="login_form" method="">
            <ul>
              <li class="form_word form_tip clearfix">
                <h3 class="left orange">用户登录</h3>
                <a href="${ctx}/register" class="right">立即注册</a>
              </li>
              <li class="form_text">
                <i class="icon_common icon_name"></i>
                <input type="text" name="realname" maxlength="11" id="realname" class="text_common text_input" value=""   error="帐号不能少于6位" placeholder="请输入登录账号(手机号码)" null="登录账号不能为空" />
              </li>
              <li class="form_text">
                <i class="icon_common icon_pwd"></i>
                <input type="password" name="password" id="password" class="text_common text_input" value=""  error="密码不能少于6位" placeholder="请输入登录密码" null="登录密码不能为空" />
              </li>
              <li class="form_text form_yzm clearfix">
                <input type="text" name="yzm" id="yzm" class="text_common text_input text_yzm" value="" placeholder="验证码" null="验证码不能为空" />
                <img src="${ctx}/jcaptcha.jpg" style="margin-left: 20px;" id="code" onclick="this.src='${ctx}/jcaptcha.jpg?d='+new Date().getTime()"/>
              </li>
              <li class="form_btn">
                <span class="btn btn_common form_submit" id="form_submit">登录</span>
              </li>
              <li class="form_word clearfix">
							<span class="left">
								<input type="checkbox" name="keep" id="keep" class="keep" value="" />
								记住密码
							</span>
                <a href="" class="right orange">忘记密码</a>
              </li>
            </ul>
          </form>
        </div>
        <!--内容表单结束-->

      </div>
      <!--主体内容结束-->
    </div>
    <!--模块内容结束-->

  </div>
  <!--登录模块结束-->

</div>
<!--身体结束-->

<!--底部开始-->
<div class="footer clearfix">

  <!--底部主体开始-->
  <div class="footer_main clearfix">

    <!--底部快捷导航开始-->
    <div class="footer_link clearfix">
      <dl>
        <dt>
          新手指南
        </dt>
        <dd>
          <a href="">注册登录</a>
        </dd>
        <dd>
          <a href="">完善资料</a>
        </dd>
        <dd>
          <a href="">购买流程</a>
        </dd>
        <dd>
          <a href="">会员介绍</a>
        </dd>
        <dd>
          <a href="">常见问题</a>
        </dd>
      </dl>
      <dl>
        <dt>
          支付方式
        </dt>
        <dd>
          <a href="">在线支付</a>
        </dd>
        <dd>
          <a href="">星币支付</a>
        </dd>
        <dd>
          <a href="">转账汇款</a>
        </dd>
      </dl>
      <dl>
        <dt>
          安全保障
        </dt>
        <dd>
          <a href="">实名认证</a>
        </dd>
        <dd>
          <a href="">资质认证</a>
        </dd>
        <dd>
          <a href="">支付安全</a>
        </dd>
        <dd>
          <a href="">专业服务</a>
        </dd>
        <dd>
          <a href="">售后无忧</a>
        </dd>
      </dl>
      <dl>
        <dt>
          售后服务
        </dt>
        <dd>
          <a href="">发票相关</a>
        </dd>
        <dd>
          <a href="">退款说明</a>
        </dd>
        <dd>
          <a href="">忘记密码</a>
        </dd>
        <dd>
          <a href="">意见反馈</a>
        </dd>
      </dl>
      <dl>
        <dt>
          联系我们
        </dt>
        <dd>
          <a href="">新浪微博</a>
        </dd>
        <dd>
          <a href="">官方微信</a>
        </dd>
        <dd>
          <a href="">关于我们</a>
        </dd>
        <dd>
          <a href="">联系我们</a>
        </dd>
      </dl>
    </div>
    <!--底部快捷导航结束-->

    <!--底部二维码导航开始-->
    <div class="footer_code">
      <img src="assets/images/code.jpg" alt="" />
      <p>微信二维码</p>
    </div>
    <!--底部二维码导航结束-->

    <!--底部联系方式开始-->
    <div class="footer_tel">
      <p class="title">咨询电话</p>
      <p class="number">400-666-6789</p>
      <p>E-mail：400000@qq.com</p>
      <p>QQ：4000 000</p>
    </div>
    <!--底部联系方式结束-->

  </div>
  <!--底部主体结束-->

  <div class="space_hx">&nbsp;</div>

  <!--底部版权信息开始-->
  <div class="footer_copyright">
    © 2015 长沙星语心愿信息技术有限公司 版权所有   湘ICP备10000087号
  </div>
  <!--底部版权信息结束-->

</div>
<!--底部结束-->

<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->

<!--Jquery库开始-->
<script type="text/javascript" src="${ctx}/assets/js/lib/jquery-1.8.3.min.js"></script>
<!--Jquery库结束-->
<!--公用cmmonJS开始-->
<script type="text/javascript" src="${ctx}/assets/js/common/common.min.js"></script>
<!--公用cmmonJS结束-->
<!--弹出层插件layer开始-->
<script type="text/javascript" src="${ctx}/assets/plugins/layer/layer.js"></script>
<!--弹出层插件layer结束-->
<script type="text/javascript" src="${ctx}/assets/js/public/loginnovalidata.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/application.js"></script>


<script>
  window['ctx'] = '${ctx}';
</script>
</body>
</html>
