<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
	<title>星店_星语星愿</title>

	<!--页面Css样式开始-->
	<link href="${ctx}/assets/css/shop/index.css" rel="stylesheet" type="text/css" />
	<!--页面Css样式结束-->

</head>

<body>

<!-- 上部搜索html片段 -->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>

<!-- 分类菜单html片段 -->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>

<!--身体开始-->
<div class="layer clearfix">

	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">星店</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">个性标签</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">全部</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->

	<!--星店内容开始-->
	<div class="shop_container">

		<!--星店分类选择开始-->
		<div class="shop_selector">
			<ul class="clearfix">
				<c:forEach items="${tagList}" var="tag">
					<li class="changeType" typeid="1" tagid="${tag.tagId}"><!--高亮样式hover-->
						<a href="javascript:void(0);" title="">${tag.tagName}</a>
					</li>
				</c:forEach>
			</ul>
		</div>
		<!--星店分类选择结束-->

		<!--星店内容主体开始-->
		<div class="shop_main clearfix">
			<!--条件筛选开始-->
			<form name="shop_form" id="shop_form">
				<div class="shop_filter clearfix">
					<div class="status clearfix">
						<a class="changeType hover" typeid="2"  href="javascript:void(0);" title="">推荐</a>
						<a class="changeType" typeid="1"  href="javascript:void(0);" title="" class="">最新</a>
					</div>
					<!--
					<div class="other clearfix">
						<div class="other_common other_address">
							<select name="address" id="address" class="text_select">
								<option value="">全部地区</option>
								<option value="1">华中</option>
								<option value="2">华北</option>
							</select>
						</div>
						<div class="other_common other_shop">
							<select name="shop" id="shop" class="text_select">
								<option value="">全部服务方式</option>
								<option value="1">上门服务</option>
								<option value="2">邮寄地址</option>
							</select>
						</div>
					</div>-->
				</div>
			</form>
			<!--条件筛选结束-->
			<!--筛选内容列表开始-->
			<div class="shop_list clearfix" id="member_tempo">
				<dl class="underscore-template">
					<a href="${ctx}/shop/{{=memberId}}/home" title="">
						<dt>
							<img src="/" alt="" src-default="${ctx}/assets/images/default_headpic.jpg" />
						<p class="nums">{{=countFollow}}</p>
						<p class="description">
							{{var goods=eval('('+goods+')');}}
							{{ _.each(goods, function(obj){ }}
		                    	<span class="clearfix">
		                    		<em class="left">{{=obj.goods_name}}</em>
		                    		<em class="right">{{=obj.goods_price}}</em>
		                    	</span>
							{{ }) }}
						</p>
						</dt>
						<dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>{{=alias}}</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		{{var address=eval('('+address+')');}}
								{{=address.province}}-{{=address.city}}
		                	</span>
						</dd>
						<!-- 隐藏描述 -->
						<dd class="label clearfix">
							{{var tagList=eval('('+tagList+')');}}
							{{ _.each(tagList, function(obj){ }}
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">{{=obj.name}}</i>
		                    </span>
							{{ }) }}
						</dd>
					</a>
				</dl>
			</div>
			<!--筛选内容列表结束-->

			<!--分页元素-->
			<div class="mricode-pagination"></div>
		</div>
		<!--星店内容主体开始-->

	</div>
	<!--星店内容结束-->

</div>
<!--身体结束-->

<javascript-list>
	<script type="text/javascript">
		//全局分页对象
		var pageObj;
		$(function(){
			//地区、服务筛选
			$('#shop_form .other_common').on('mouseenter mouseleave',function(){
				$(this).find('i').toggleClass('icon_up');
				$(this).find('.list_common').stop().slideToggle()
			});
			$('#shop_form .list_common>ul>li').on('click',function(){
				var attrid  = $(this).attr('attrid');
				var attrtxt = $(this).html();
				$(this).parents('.list_common').siblings('input').val(attrid);
				$(this).parents('.list_common').siblings('.selected').html(attrtxt);
				$(this).parents('.list_common').slideUp();
			})

			//初始化分页
			loadPage();

			//切换高亮
			$('.changeType').on('click',function(){
				$(this).addClass("hover").siblings().removeClass('hover');
				pageObj.pagination('setParams', {type:$(this).attr("typeid"), tagId:$(this).attr("tagid")});
				pageObj.pagination('remote');
			});

		})


		//懒加载
		function loadImage(url,callback) {
			var img = new Image();

			img.src = url;

			if(img.complete) {  // 如果图片已经存在于浏览器缓存，直接调用回调函数

				callback.call(img);
				return; // 直接返回，不用再处理onload事件
			}

			img.onload = function(){
				img.onload = null;
				callback.call(img);
			}
		}

		//加载分页内容
		function loadPage(){
			pageObj = $(".mricode-pagination").pagination({
				pageSize: 20, // 指定 一页多少天数据
				debug: false,
				showInfo: false,
				showJump: true,
				showPageSizes: false,
				prevBtnText: '<i><</i> 上一页',
				nextBtnText: '下一页 <i>></i>',
				jumpBtnText: '确定',
				infoFormat: '共 {total} 条',
				noInfoText: '暂无数据',
				pageElementSort: ['$page', '$size', '$jump', '$info'],
				remote: {
					url: ctx + "/shop/index/list",
					totalName: 'result.total',
					pageIndexName: 'pageNum',
					pageSizeName: 'pageSize',
					success: function (data) {
						if(data.success) {
							_.templateRender('#member_tempo', data.result.list);
						} else {
							layer.tips('请求失败!');
						}
					}
				}
			});
		}
	</script>
</javascript-list>
</body>
</html>
