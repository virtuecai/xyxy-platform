<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta id="wxKeywords" name="Keywords" content="星语星愿" />
<meta id="wxDescription" name="Description" content="星语星愿" />
<title>子级分类_星店_星语星愿</title>

<!--页面Css样式开始-->
<link href="${ctx}/assets/css/shop/index_children.css" rel="stylesheet" type="text/css" />
<!--页面Css样式结束-->
</head>

<body>

<!--头部中间内容开始-->
<jsp:include page="/WEB-INF/views/common/header.search.jsp"/>
<!--头部中间内容结束-->
<!--头部导航主体开始-->
<jsp:include page="/WEB-INF/views/common/header.nav.jsp"/>
<!--头部导航主体结束-->

<!--身体开始-->
<div class="layer clearfix">
	
	<!--头部面包屑开始-->
	<div class="position">
		<ul class="clearfix">
			<li>
				<a href="#" title="">星店</a>
			</li>
			<li class="line">></li>
			<li>
				<a href="#" title="">分类</a>
			</li>
		</ul>
	</div>
	<!--头部面包屑结束-->
	
	<!--星店内容开始-->
	<div class="shop_container">
	
		<!--一级分类推荐开始-->
		<div class="shop_recom">
			<a href="javascript:void(0);" class="present">${topTag.tagName}</a>
			<a href="${ctx}/shop/index" class="go_back">返回上一级↑</a>
		</div>
		<!--一级分类推荐结束-->
			
		<!--星店分类选择开始-->
		<div class="shop_selector">
			<ul class="clearfix">
				<c:forEach items="${tagList}" var="tag">
				<li class=""><!--高亮样式hover-->
					<a href="javascript:void(0);" title="">${tag.tagName}</a>
				</li>
				</c:forEach>
			</ul>
		</div>
		<!--星店分类选择结束-->
		
		<!--星店内容主体开始-->
		<div class="shop_main clearfix">
			<!--条件筛选开始-->
			<form name="shop_form" id="shop_form">
				<div class="shop_filter clearfix">
					<div class="status clearfix">
						<a href="" title="" class="hover">综合排序</a>
						<a href="" title="" class="">发布时间</a>
					</div>
					<div class="other clearfix">
						<div class="other_common other_address">
							<div class="selected">全部地区</div>
							<div class="address_list list_common" id="address_list">
								<ul>
									<li attrid="1">华东地区</li>
									<li attrid="2">华中地区</li>
									<li attrid="3">华北地区</li>
								</ul>
							</div>
							<i class="icon_shop icon_down"></i>
							<input name='address' value="" class="address" type="hidden" />
						</div>
						<div class="other_common other_shop">
							<div class="selected">全部服务方式</div>
							<div class="way_list list_common" id="way_list">
								<ul>
									<li attrid="1">项目咨询</li>
									<li attrid="2">天使投资</li>
								</ul>
							</div>
							<i class="icon_shop icon_down"></i>
							<input name='shop' value="" class="shop" type="hidden" />
						</div>
					</div>
				</div>
			</form>
			<!--条件筛选结束-->
			
			<!--筛选内容列表开始-->
			<div class="shop_list clearfix">
				<dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_01.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span class="hover"><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_02.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_03.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_04.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_05.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_02.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_07.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_08.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_01.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		                
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_02.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_03.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_04.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_04.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_04.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
	            <dl>
					<a href="" title="">
		                <dt>
		                    <img src="${ctx}/assets/images/content/2f_04.jpg" alt="" />
		                    <p class="nums">112</p>
		                    <p class="description">
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    	<span class="clearfix">
		                    		<em class="left">工商变更</em>
		                    		<em class="right">￥200</em>
		                    	</span>
		                    </p>
		                </dt>
		                <dd class="member clearfix">
		                	<span class="name clearfix">
		                		<em>韩梅梅</em>
			                    <i class="icon_shop icon_zheng"></i>
			                    <i class="icon_shop icon_v_member"></i>
		                	</span>
		                	<span class="address">
		                		湖南 长沙
		                	</span>
		                </dd>
		                <dd class="title clearfix">互联网项目天使投资咨询，平均投资资金1000万元</dd>
		                <dd class="label clearfix">
		                	<span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span><!--高亮样式hover-->
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                    <span>
			                    <i class="icon_shop icon_header"></i>
			                    <i class="icon_footer">天使投资</i>
		                    </span>
		                </dd>
		            </a>
	            </dl>
			</div>
			<!--筛选内容列表结束-->
			
			<!--分页结构开始-->
			<div class="web_page clearfix">
				<ul>
					<li class="link prev">
						<a href="" title=""><em><</em> 上一页</a>
					</li>
					<li class="link current"><!--当前页样式current-->
						<a href="" title="">1</a>
					</li>
					<li class="link">
						<a href="" title="">2</a>
					</li>
					<li class="link">
						<a href="" title="">3</a>
					</li>
					<li class="link">
						<a href="" title="">4</a>
					</li>
					<li class="link">
						<a href="" title="">5</a>
					</li>
					<li class="link">
						<a href="" title="">6</a>
					</li>
					<li class="more">
						...
					</li>
					<li class="link next">
						<a href="" title="">下一页 <em>></em></a>
					</li>
					<li class="total">
						共<em>50</em>页
					</li>
					<li class="jump">
						到第<input type="text" value="1" name="pagenum" />页
						<span class="submit_page">确定</span>
					</li>
				</ul>
			</div>
			<!--分页结构结束-->
			
		</div>
		<!--星店内容主体开始-->
		
	</div>
	<!--星店内容结束-->
	
</div>
<!--身体结束-->
<script type="text/javascript">
	$(function(){
		//地区、服务筛选
		$('#shop_form .other_common').on('mouseenter mouseleave',function(){
			$(this).find('i').toggleClass('icon_up');
			$(this).find('.list_common').stop().slideToggle()
		});
		$('#shop_form .list_common>ul>li').on('click',function(){
			var attrid  = $(this).attr('attrid');
			var attrtxt = $(this).html();
			$(this).parents('.list_common').siblings('input').val(attrid);
			$(this).parents('.list_common').siblings('.selected').html(attrtxt);
			$(this).parents('.list_common').slideUp();
		})
	})
	
</script>
</body>
</html>
