<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta id="wxKeywords" name="Keywords" content="星语星愿" />
  <meta id="wxDescription" name="Description" content="星语星愿" />
  <title>个人主页_星店_星语星愿</title>

  <!--页面Css样式开始-->
  <link href="${ctx}/assets/css/shop/home.css" rel="stylesheet" type="text/css" />
  <!--页面Css样式结束-->
</head>

<body>
<!--身体开始-->
<div class="layer clearfix">

  <!--头部个人背景墙开始-->
  <div class="home_page_bg" style="background: url('${ctx}/assets/images/content/home_page_01.jpg')"></div>
  <!--头部个人背景墙结束-->

  <!--个人主页内容开始-->
  <div class="home_page_main clearfix">

    <!--底部左侧内容开始-->
    <div class="page_main_left">

      <!--个人基本信息开始-->
      <div class="left_common member_basic">
        <div class="headpic">
          <img id="photo" onerror="this.src='${ctx}/assets/images/default_headpic.jpg'" alt=""  />
        </div>
        <div class="other">
          <ul>
            <li>
							<span class="name">
								${member.alias}
							</span>
							<span class="label">
		                    	<i class="icon_shop icon_zheng"></i>
		                    	<i class="icon_shop icon_v_member"></i>
                              <c:if test="${member.gender==0}"><i class="icon_sex">不详</i></c:if>
                              <c:if test="${member.gender==1}"><i class="icon_sex">男</i></c:if>
                              <c:if test="${member.gender==2}"><i class="icon_sex">女</i></c:if>
							</span>
							<span class="age">

							</span>
							<span class="address">
								来自 <em>${member.address==''?' 未填写 ':member.address}</em>
							</span>
            </li>
            <li>
              联系方式：${member.userName}
              <i class="icon_shop icon_qq"></i>
              <i class="icon_shop icon_wechat"></i>
            </li>
          </ul>
        </div>
      </div>
      <!--个人基本信息结束-->

      <!--人物简介开始-->
      <div class="left_common member_intro">
        <h2>
          <i class="icon_shop icon_user"></i>
          他是谁
        </h2>
        <div class="intro_detail">

        </div>
      </div>
      <!--人物简介结束-->

      <!--服务介绍开始-->
      <div class="left_common member_service">
        <h2>
          <i class="icon_shop icon_service"></i>
          他的服务
        </h2>
        <div class="service_list">
          <!--没有服务情况开始-->
          <!--<div class="service_none">
              他暂时还没有发布任何服务。<br/>
              如对他感兴趣，就在下面给他留言吧！
          </div>-->
          <!--没有服务情况结束-->

          <!--服务列表service_list_box遍历开始-->
          <div id="goods-template">
          <div class="service_list_box clearfix underscore-template">
            <div class="thumb">
              <a href="" title="">
                <img src="{{=goodPics?goodPics[0]: ''}}" alt="" src-default="${ctx}/assets/images/default_content.png"/>
              </a>
            </div>
            <div class="content">
              <ul>
                <li class="name">
                  <span class="title">{{=goodsName}}</span>
                  <span class="price">￥<em>{{=goodPrice}}</em>元/{{=goodUnit}}</span>
                </li>
                <li class="label clearfix">
                  {{ _.each(serverTags, function(s){ }}
                        <span class="clearfix">
                          <i class="icon_shop icon_header"></i>
                          <i class="icon_shop icon_footer">{{=s.tagName}}</i>
						</span>
                  {{ }) }}
                </li>
                <li class="intro">{{=goodBrief}}</li>
                <li class="operation">
                 <%-- <a href="${ctx}/#" title="" class="btn detail">服务详情</a>--%>
                  <a href="${ctx}/service/detail/{{=goodId}}" title="" class="btn order">立即预约</a>
                </li>
              </ul>
            </div>
          </div>
          </div>
          <!--服务列表service_list_box遍历结束-->
        </div>
      </div>
      <!--服务介绍结束-->

      <!--发布留言开始-->
      <div class="left_common message_add">
        <form name="message_form" method="" id="message_form">
          <h2>
            <i class="icon_shop icon_message"></i>
            发布留言
          </h2>
          <textarea name="message" class="message" placeholder="请输入留言内容"></textarea>
          <span class="btn cmain_bg_color submit_msg" id="submit_msg">发 布</span>
        </form>
      </div>
      <!--发布留言结束-->

      <!--留言列表开始-->
      <div class="left_common message_list">
        <h2>他收到的留言（<em>1</em>）</h2>

        <!--没有评论情况开始-->
        <!--<div class="comment_none">
            来做第一个给他留言的人吧~
        </div>-->
        <!--没有评论情况结束-->

        <!--单条评论开始--单条评论遍历-->
        <div class="comment_list clearfix">
          <!--左边-评论人头像开始-->
          <div class="comment_headpic">
            <a href="" title="">
              <img src="${ctx}/assets/images/content/comment_small_01.jpg" alt="" />
            </a>
          </div>
          <!--左边-评论人头像结束-->

          <!--右边-评论信息开始-->
          <div class="comment_msg">
            <!--评论人信息开始-->
            <div class="msg_people">
              <ul class="clearfix">
                <li class="left name">巧克力</li>
                <li class="left address ">
                  <em>30岁</em>,<em>湖南 长沙</em>
                </li>
                <li class="right more">
                  <i class="icon_shop icon_down"></i>
                  <ul>
                    <li><a href="">举报</a></li>
                  </ul>
                </li>
                <li class="right time">2015-11-24 15：52</li>
              </ul>
            </div>
            <!--评论人信息结束-->

            <!--服务印象开始-->
            <div class="msg_service">
              <ul class="clearfix">
                <li class="title">
                  服务印象：
                </li>
                <li class="clearfix">
                  <i class="icon_shop icon_header"></i>
                  <i class="icon_shop icon_footer">专业</i>
                </li>
                <li>
                  <i class="icon_shop icon_header"></i>
                  <i class="icon_shop icon_footer">耐心</i>
                </li>
              </ul>
            </div>
            <!--服务印象结束-->

            <!--评论描述开始-->
            <div class="msg_desc">
              拉伸热身、身体开度、形体训练、体能训练、身体控制、爵士舞基本功、
              舞蹈教学技巧、舞蹈表演技巧、爵士舞编舞技巧拉伸热身身体开度、形体训练、体能训练、身体控制、爵士舞基本功、舞蹈教学技巧舞蹈表演技巧爵士舞编舞技巧
            </div>
            <!--评论描述结束-->

            <!--评论附属图片开始-->
            <div class="msg_imglist" id="msg_imglist">
              <!-- layer-src表示大图  layer-pid表示图片id  src表示缩略图-->
              <ul class="clearfix">
                <li><img layer-src="${ctx}/assets/images/content/content_01.jpg" src="${ctx}/assets/images/content/content_01.jpg" alt="附属图片1" layer-pid="" /></li>
                <li><img layer-src="${ctx}/assets/images/content/content_02.jpg" src="${ctx}/assets/images/content/content_02.jpg" alt="附属图片2" layer-pid="" /></li>
                <li><img layer-src="${ctx}/assets/images/content/content_03.jpg" src="${ctx}/assets/images/content/content_03.jpg" alt="附属图片3" layer-pid="" /></li>
              </ul>
            </div>
            <!--评论附属图片结束-->
          </div>
          <!--右边-评论信息结束-->
        </div>
        <!--单条评论结束--单条评论结束-->

        <!--分页结构开始-->
        <div class="web_page clearfix" id="web_page">
          <ul>
            <li class="link prev">
              <a href="" title=""><em><</em> 上一页</a>
            </li>
            <li class="link current"><!--当前页样式current-->
              <a href="" title="">1</a>
            </li>
            <li class="link">
              <a href="" title="">2</a>
            </li>
            <li class="link">
              <a href="" title="">3</a>
            </li>
            <li class="link">
              <a href="" title="">4</a>
            </li>
            <li class="link">
              <a href="" title="">5</a>
            </li>
            <li class="link">
              <a href="" title="">6</a>
            </li>
            <li class="more">
              ...
            </li>
            <li class="link next">
              <a href="" title="">下一页 <em>></em></a>
            </li>
            <li class="total">
              共<em>50</em>页
            </li>
            <li class="jump">
              到第<input type="text" value="1" name="pagenum" />页
              <span class="submit_page">确定</span>
            </li>
          </ul>
        </div>
        <!--分页结构结束-->

      </div>
      <!--留言列表结束-->

    </div>
    <!--底部左侧内容结束-->

    <!--底部右侧人物介绍开始-->
    <div class="page_main_right">
      <div class="member_focus clearfix">
        <ul>
          <li class="focus_on" id="is_gz">
            <!--未关注-->
            <em class="btn onfocus cmain_bg_color">+ 关注</em>
            <!--已关注-->
            <!--<em class="btn offfocus bg_gray">已关注</em>-->
          </li>
          <li>
            他的粉丝 <em id="fs_count">55435</em> 人
          </li>
          <li>
            他的关注 <em id="gz_count">343</em>人
          </li>
        </ul>
      </div>
      <div class="member_label">
        <ul>
          <li class="title">
            他的职业标签
          </li>
          <li id="zytag" class="label profession clearfix">

          </li>
          <li class="title">
            他的个人标签
          </li>
          <li id="grtag" class="label clearfix">

          </li>
          <li class="title">
            他和他的关系
          </li>
          <li class="title small_title">
            我关注的人中，<em>38</em>人也关注了他
          </li>
          <li class="link clearfix">
            <a href=""><img src="${ctx}/assets/images/content/headpic_01.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
          </li>
          <li class="title small_title">
            我们共同关注了，<em>38</em>人
          </li>
          <li class="link clearfix">
            <a href=""><img src="${ctx}/assets/images/content/headpic_01.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
            <a href=""><img src="${ctx}/assets/images/content/headpic_02.jpg" alt=""></a>
          </li>
        </ul>
      </div>
    </div>
    <!--右侧人物介绍结束-->

  </div>
  <!--个人主页内容结束-->

</div>
<!--身体结束-->
<javascript-list>
  <script type="text/javascript">
    //会员ID
    var mid = "${member.id}";
    var aid = "${member.articleId}";
    var imgid = "${member.imagePkgId}";
    $(".age").text(moment().diff('${member.birthday}', 'years')+'岁');
  </script>
  <script type="text/javascript" src="${ctx}/assets/js/shop/home.js"></script>
</javascript-list>
</body>
</html>