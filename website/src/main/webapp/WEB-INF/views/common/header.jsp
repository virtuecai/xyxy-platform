<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!--头部开始-->
<div class="header clearfix">

  <!--头部工具栏开始-->
  <div class="header_tool">

    <div class="tool_main clearfix">

      <!--左侧工具栏开始-->
      <div class="tool_left clearfix">
        <c:choose>
          <c:when test="${!empty user}">
            <a href="#" class="green nickname" title="${user.name}">${user.name}</a>
            <a>您好，欢迎来到星语心愿</a>
            <a href="javascript:confirm()" class="red loginout">[退出登录]</a>
          </c:when>
          <c:otherwise>
            <a>您好，欢迎来到星语心愿，请</a>
            <a href="${ctx}/login" class="green login">登录</a>
            <a href="${ctx}/register" class="orange reg">免费注册</a>
          </c:otherwise>
        </c:choose>

      </div>
      <!--左侧工具栏结束-->

      <!--右侧工具栏开始-->
      <div class="tool_right clearfix" id="tool_right">
        <ul class="clearfix">
          <c:choose>
            <c:when test="${empty user}">

            </c:when>
            <c:otherwise>
              <li class="clearfix">
                <a href="${ctx}/member/" class="member_info">
                  <span class="tool_img tool_headpic"><img src="${user.imageUrl}" onerror="this.src='${ctx}/assets/images/default_headpic.jpg'"/></span>
                  <span>${user.name}</span>
                  <span class="tool_icon"><i class="icon_common icon_down"></i></span>
                </a>
                <!--展开顶级菜单开始-->
                <ul>
                  <li>
                    <a href="${ctx}/shop/${user.id}/home" title="">我的主页</a>
                  </li>
                  <li>
                    <a href="${ctx}/member/" title="">账号设置</a>
                  </li>
                  <li>
                    <a href="javascript:confirm()" title="">退出</a>
                  </li>
                </ul>
                <!--展开顶级菜单结束-->
              </li>
              <%--<li class="msg_nums clearfix">--%>
                <%--<a href="" title="">--%>
                  <%--<span>消息</span>--%>
                  <%--<i class="icon_common icon_nums">12</i>--%>
                <%--</a>--%>
              <%--</li>--%>
              <li class="clearfix">
                <a href="${ctx}/member/order_buy" title="">
                  <span>我的订单</span>
                  <span class="tool_icon"><i class="icon_common icon_down"></i></span>
                </a>
              </li>
              <li class="clearfix">
                <a href="" title="">
                  <span>收藏夹</span>
                  <span class="tool_icon"><i class="icon_common icon_down"></i></span>
                </a>
              </li>
            </c:otherwise>
          </c:choose>
          <li class="clearfix">
            <a href="" title="">
              <span>服务分类</span>
              <span class="tool_icon"><i class="icon_common icon_down"></i></span>
            </a>
          </li>
          <li class="clearfix">
            <a href="" title="">
              <span>联系客服</span>
              <span class="tool_icon"><i class="icon_common icon_down"></i></span>
            </a>
          </li>
          <li class="clearfix">
            <a href="" title="">
              <span class="tool_icon"><i class="icon_common icon_app"></i></span>
              <span class="orange">APP下载</span>
            </a>
          </li>
        </ul>
      </div>
      <!--右侧工具栏结束-->
    </div>
    <!--头部工具栏主体结束-->

  </div>
  <!--头部工具栏结束-->
  <script>
    function confirm() {
      layer.confirm('确认退出吗？', {
        btn  :['确认','再考虑下'], //按钮
        title:'温馨提示',
        icon :0
      },function() {
        window.location.href=ctx+"/logout";
      });
    }
  </script>

</div>
<!--头部结束-->