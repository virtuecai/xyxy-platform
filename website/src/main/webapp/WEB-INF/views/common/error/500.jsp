<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="zh">
<head>
    <title>出错啦</title>
    <!--页面Css样式开始-->
    <link href="${ctx}/assets/css/public/500.css" rel="stylesheet" type="text/css" />
    <!--页面Css样式结束-->
</head>

<body>

<!--身体部分开始-->
<div class="layer">
    <div class="error_box">
        <div class="error_img">
            <img src="${ctx}/assets/images/500.jpg" alt="" />
        </div>
        <div class="error_link">
            <a href="${ctx}/" title="" class="btn">返回首页</a>
            <a href="javascript:history.back(-1);" title="" class="btn">返回上一页</a>
        </div>
    </div>
</div>
<!--身体部分结束-->

</body>
</html>
