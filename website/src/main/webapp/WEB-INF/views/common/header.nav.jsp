<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!--头部导航开始-->
<div class="header_nav">

  <!--头部导航主体开始-->
  <div class="header_nav_main clearfix">
    <!--左侧导航开始-->
    <div class="nav_common nav_left main_bg_color" id="header_nav_left" slideDisabled="${param.slideDisabled}">
      <div class="nav_left_title">
        <i class="icon_common icon_menu"></i>
        <span>热门服务推荐</span>
        <i class="icon_common icon_down_green"></i>
      </div>
      <!--服务菜单开始--一级菜单最多15个-->
      <div class="nav_left_menu" id="nav_left_menu">
        <img src="${ctx}/assets/plugins/layer/skin/default/loading-1.gif" class="category-tree-loading"/>
        <ul class="category-tree-list">
          <li class="underscore-template">
            <a href="${ctx}/service/category/{{=id}}" title="" class="clearfix">
              <i class="icon_common {{=iconClass}}"></i><!--遍历第二个样式名，如icon_shzs-->
              <span>{{=name}}</span>
              <i class="icon_common icon_more"></i><!--向右图标，固定样式即可-->
            </a>
            <ul>
              <!-- 二级 -->
              {{_.each(childList, function(tree){ }}
              <li>
                <h2 class="clearfix">
                  <a href="${ctx}/service/category/{{=tree.id}}" class="title">
                    <%--<i class="icon_common icon_wlwkf"></i>--%>
                    <span>{{=tree.name}}</span>
                  </a>
                  <a href="javascript:void(0);" class="more">更多 <em>》</em></a>
                </h2>
                <h3 class="clearfix">
                  <!-- 三级 -->
                  {{_.each(tree.childList, function(tree){ }}
                    <a href="${ctx}/service/category/{{=tree.id}}">{{=tree.name}}</a>
                  {{ }) }}
                </h3>
              </li>
              {{ }) }}
            </ul>
          </li>

        </ul>
      </div>
    </div>
    <!--左侧导航结束-->

    <!--右侧导航开始-->
    <div class="nav_common nav_right">
      <ul class="clearfix">
        <li>
          <a href="${ctx}/">
            <span class="title">首页</span>
          </a>
        </li>
        <li>
          <a href="${ctx}/service/index">
            <span class="title">服务广场</span>
            <span class="icon_common icon_tips">首单免费</span>
          </a>
        </li>
        <li>
          <a href="${ctx}/shop/index">
            <span class="title">星店</span>
            <span class="icon_common icon_tips">入驻有奖</span>
          </a>
        </li>
      </ul>
    </div>
    <!--右侧导航结束-->
  </div>
  <!--头部导航主体结束-->

</div>
<!--头部导航结束-->
