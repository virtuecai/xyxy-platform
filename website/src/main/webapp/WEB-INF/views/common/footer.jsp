<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!--底部开始-->
<div class="footer clearfix">

  <!--提供的服务、保障开始-->
  <div class="service footer_service">
    <ul class="clearfix">
      <li>
        <i class="icon_common icon_five5"></i>
        <span>五大服务保障</span>
      </li>
      <li>
        <i class="icon_common icon_smrz"></i>
        <span>实名认证</span>
      </li>
      <li>
        <i class="icon_common icon_zzrz"></i>
        <span>资质认证</span>
      </li>
      <li>
        <i class="icon_common icon_zfaq"></i>
        <span>支付安全</span>
      </li>
      <li>
        <i class="icon_common icon_zyfw"></i>
        <span>专业服务</span>
      </li>
      <li>
        <i class="icon_common icon_shwy"></i>
        <span>售后无忧</span>
      </li>
    </ul>
  </div>
  <!--提供的服务、保障结束-->

  <!--底部主体开始-->
  <div class="footer_main clearfix">

    <!--底部快捷导航开始-->
    <div class="footer_link clearfix">
      <dl>
        <dt>
          新手指南
        </dt>
        <dd>
          <a href="">注册登录</a>
        </dd>
        <dd>
          <a href="">完善资料</a>
        </dd>
        <dd>
          <a href="">购买流程</a>
        </dd>
        <dd>
          <a href="">会员介绍</a>
        </dd>
        <dd>
          <a href="">常见问题</a>
        </dd>
      </dl>
      <dl>
        <dt>
          支付方式
        </dt>
        <dd>
          <a href="">在线支付</a>
        </dd>
        <dd>
          <a href="">星币支付</a>
        </dd>
        <dd>
          <a href="">转账汇款</a>
        </dd>
      </dl>
      <dl>
        <dt>
          安全保障
        </dt>
        <dd>
          <a href="">实名认证</a>
        </dd>
        <dd>
          <a href="">资质认证</a>
        </dd>
        <dd>
          <a href="">支付安全</a>
        </dd>
        <dd>
          <a href="">专业服务</a>
        </dd>
        <dd>
          <a href="">售后无忧</a>
        </dd>
      </dl>
      <dl>
        <dt>
          售后服务
        </dt>
        <dd>
          <a href="">发票相关</a>
        </dd>
        <dd>
          <a href="">退款说明</a>
        </dd>
        <dd>
          <a href="">忘记密码</a>
        </dd>
        <dd>
          <a href="">意见反馈</a>
        </dd>
      </dl>
      <dl>
        <dt>
          联系我们
        </dt>
        <dd>
          <a href="">新浪微博</a>
        </dd>
        <dd>
          <a href="">官方微信</a>
        </dd>
        <dd>
          <a href="">关于我们</a>
        </dd>
        <dd>
          <a href="">联系我们</a>
        </dd>
      </dl>
    </div>
    <!--底部快捷导航结束-->

    <!--底部二维码导航开始-->
    <div class="footer_code">
      <img src="${ctx}/assets/images/code.jpg" alt=""/>

      <p>微信二维码</p>
    </div>
    <!--底部二维码导航结束-->

    <!--底部联系方式开始-->
    <div class="footer_tel">
      <p class="title">咨询电话</p>

      <p class="number">400-1314-666</p>

      <p>E-mail：kefu@xyxy.com</p>

      <p>QQ：4000 000</p>
    </div>
    <!--底部联系方式结束-->

  </div>
  <!--底部主体结束-->

  <div class="space_hx">&nbsp;</div>

  <!--底部版权信息开始-->
  <div class="footer_copyright">
    © 2015 长沙星语心愿信息技术有限公司 版权所有 湘ICP备10000087号
  </div>
  <!--底部版权信息结束-->

</div>
<!--底部结束-->

<!--身体右边公用侧边栏开始-->
<div class="body_fixed_nav" id="body_fixed_nav">
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_msg"></i></span>
    <span class="word">在线留言</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_love"></i></span>
    <span class="word">我的最爱</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_history"></i></span>
    <span class="word">浏览历史</span>
  </a>
  <a href="" title="" class="clearfix">
    <span class="icon"><i class="icon_common icon_collect"></i></span>
    <span class="word">我的收藏</span>
  </a>
  <a href="javascript:;" title="" id="nav_back_top" class="clearfix">
    <span class="icon"><i class="icon_common icon_backtop"></i></span>
    <span class="word">回到顶部</span>
  </a>
</div>
<!--身体右边公用侧边栏结束-->