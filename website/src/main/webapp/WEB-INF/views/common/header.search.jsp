<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!--头部中间内容开始-->
<div class="header_content clearfix">

  <!--头部logo开始-->
  <div class="header_logo">
    <a href="${ctx}/" title>
      <img src="${ctx}/assets/images/logo.jpg" alt="" />
    </a>
  </div>
  <!--头部logo结束-->

  <!--头部搜索框开始-->
  <div class="header_search">
    <div class="search_keywords clearfix">
        <div class="search_common search_classify" id="search_classify">
          <%--<div class="selected">搜服务</div>
          <ul>
            <li classifyid="1">搜服务</li>
            <li classifyid="2">搜星店</li>
          </ul>
          <i class="icon_common icon_down"></i>
          <input type="hidden" value="1" class="" id="classify" name="classify" />--%>
          <i class="icon_common icon_search"></i>
        </div>
        <div class="search_common search_text">
          <input type="text" name="keywords" id="keywords" class="keywords" placeholder="搜索关键字/服务/标签/姓名" />
        </div>
        <div class="search_common search_btn cmain_bg_color" id="search_btn" onclick="onSearchBtn()">搜索</div>
        <!--关键字匹配开始-->
        <div class="keyword_focus" id="keyword_focus1">
          <ul>
            <li>
              <a href="${ctx}/service/search?key=天使投资" title="">天使投资</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=VC投资" title="">VC投资</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=PE投资" title="">PE投资</a>
            </li>
            <li>
              <a href="#" title="">融资众筹</a>
            </li>
            <li>
              <a href="#" title="">股权众筹</a>
            </li>
            <li>
              <a href="#" title="">产品众筹</a>
            </li>
            <li>
              <a href="#" title="">P2P</a>
            </li>
          </ul>
        </div>
        <!--关键字匹配结束-->
        <!--关键字匹配开始-->
        <div class="keyword_focus" id="keyword_focus2">
          <ul>
            <li>
              <a href="${ctx}/service/search?key=麓谷政策" title="">柳枝行动</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=麓谷政策" title="">回湘企业优惠政策</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=麓谷政策" title="">回湘人才优惠政策</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=麓谷政策" title="">岳麓峰会</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=麓谷政策" title="">本地企业优惠政策</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=麓谷政策" title="">本地人才优惠政策</a>
            </li>
          </ul>
        </div>
        <!--关键字匹配结束-->
        <!--关键字匹配开始-->
        <div class="keyword_focus" id="keyword_focus3">
          <ul>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">股票开户</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">证券开户</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">基金</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">压岁钱理财</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">私募</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">外汇</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=证券开户" title="">P2P</a>
            </li>
          </ul>
        </div>
        <!--关键字匹配结束-->
        <!--关键字匹配开始-->
        <div class="keyword_focus" id="keyword_focus4">
          <ul>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">美容养生</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">微整咨询</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">服装搭配</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">彩妆</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">形象设计</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">情感顾问</a>
            </li>
            <li>
              <a href="${ctx}/service/search?key=美容" title="">脸部护理</a>
            </li>
          </ul>
        </div>
        <!--关键字匹配结束-->
        <!--关键字匹配开始-->
        <!--关键字匹配结束-->
    </div>
    <div class="search_tips clearfix">
      <a href="${ctx}/service/search?key=投资" title="">投资</a>
      <a href="#" title="">创业</a>
      <a href="#" title="">法律服务</a>
      <a href="#" title="">理财</a>
      <a href="#" title="">教练</a>
      <a href="#" title="">年检</a>
      <a href="#" title="">模特</a>
      <a href="#" title="">导游</a>
      <a href="#" title="">心理咨询</a>
      <a href="#" title="">培训</a>
      <a href="#" title="">代驾</a>
      <a href="#" title="">挂号</a>
      <a href="#" title="">新三板</a>
    </div>
  </div>
  <!--头部搜索框结束-->

  <!--头部购物车盒子开始-->
  <div class="header_shopcar">
    <a href="${ctx}/car/index" title="" class="clearfix">
      <span class="shopcar_icon"><i class="icon_common icon_shopcar"></i></span>
      <span>我的心愿车</span>
      <%--<span class="icon_common shopcar_num">12</span>--%>
      <span class="shopcar_link"><i class="icon_common icon_right"></i></span>
    </a>
  </div>
  <!--头部搜索框结束-->

</div>
<!--头部中间内容结束-->

<div class="space_hx">&nbsp;</div>

<div class="space_hx">&nbsp;</div>
<script>
  function onSearchBtn(){
    var key = $("#keywords").val();
    location.href = "${ctx}/service/search?key="+key;
  }
</script>