<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!--左侧公用部分开始-->
<div class="container_left">

  <div class="container_headpic">
    <img id="headImage" src="${user.imageUrl}" onerror="this.src='${ctx}/assets/images/default_headpic.jpg'"  alt="" />
    <h3>${user.name}</h3>
  </div>

  <div class="container_menu">
    <!--注意：高亮样式为hover，添加再a标签内-->
    <ul>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_order"></i>
          <span>我的订单</span>
        </a>
        <ul>
          <li>
            <a href="${ctx}/member/order_saled">我售出的服务</a>
          </li>
          <li>
            <a href="${ctx}/member/order_buy">我购买的服务</a>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_service"></i>
          <span>我的服务</span>
        </a>
        <ul>
          <li>
            <a href="${ctx}/member/service/publish" title="发布服务">发布服务</a>
          </li>
          <li>
            <a href="${ctx}/member/service/manage" title="管理服务">管理服务</a>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_wallet"></i>
          <span>我的钱包</span>
        </a>
        <ul>
          <li>
            <a href="${ctx}/member/mywallet" title="">钱包详情</a>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_collect"></i>
          <span>我的收藏</span>
        </a>
        <ul>
          <li>
            <a href="" title="">收藏列表</a>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_friend"></i>
          <span>我的好友</span>
        </a>
        <ul>
          <li>
            <a href="" title="">我的关注</a>
          </li>
          <li>
            <a href="" title="x">我的粉丝</a>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_message"></i>
          <span>我的消息</span>
        </a>
        <ul>
          <li>
            <a href="" title="">消息列表</a>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_info"></i>
          <span>基本资料</span>
        </a>
        <ul>
          <li>
            <a href="${ctx}/member/" title="" class="hover">个人信息</a>
          </li>
          <li>
            <a href="${ctx}/member/label" title="">个人标签</a>
          </li>
          <li>
            <a href="${ctx}/member/contact" title="">联系方式</a>
          </li>
          <%--<li>--%>
            <%--<a href="${ctx}/member/headpic" title="">头像设置</a>--%>
          <%--</li>--%>
          <li>
            <a href="${ctx}/member/password" title="">密码修改</a>
          </li>
          <li>
            <a href="" title="">认证信息</a>
            <ul>
              <li>
                <a href="" title="">企业认证</a>
              </li>
              <li>
                <a href="" title="">个人认证</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li>
        <a title="" class="clearfix">
          <i class="icon_member icon_customer"></i>
          <span>客户服务</span>
        </a>
        <ul>
          <li>
            <a href="" title="">举报管理</a>
          </li>
          <li>
            <a href="" title="">仲裁管理</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>

</div>
<!--左侧公用部分结束-->