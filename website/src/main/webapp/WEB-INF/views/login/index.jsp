<%--
  Created by IntelliJ IDEA.
  User: czd
  Date: 15/12/15
  Time: 16:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>登录页面</title>
</head>
<body>
    <h1>登录页面</h1>
    <form action="${ctx}/login" method="post">
        <input type="text" name="userName"/> <br/>
        <input type="text" name="password"/> <br/>
        验证码: <img src="${ctx}/jcaptcha.jpg"  onclick="this.src='${ctx}/jcaptcha.jpg?d='+new Date().getTime()"/><input type="text" name="jcaptcha" value="" />
        <button type="submit">登录</button>
    </form>
</body>
</html>
