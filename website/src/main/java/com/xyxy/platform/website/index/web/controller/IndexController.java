package com.xyxy.platform.website.index.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.service.member.MemberService;
import com.xyxy.platform.modules.service.member.vo.MemberDetail;
import com.xyxy.platform.modules.service.trade.CategoryService;
import com.xyxy.platform.modules.service.trade.CategoryTreeService;
import com.xyxy.platform.modules.service.trade.GoodsService;
import com.xyxy.platform.modules.service.trade.vo.CategoryTree;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 首页
 * Created by VirtueCai on 15/12/1.
 */
@Controller
@RequestMapping("/")
public class IndexController extends BaseController {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryTreeService categoryTreeService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(){
        request.setAttribute("recommend_member",memberService.countHotMember());
        request.setAttribute("goods_member",6);
        request.setAttribute("publish_goods",goodsService.countPublishGoods());
        return "index/index";
    }

    @ResponseBody
    @RequestMapping(value = "/load/category/tree", method = RequestMethod.GET)
    public ResponseMessage<?> loadCategoryTree(){
        List<CategoryTree> list = categoryTreeService.getCategoryTree();
        return ResponseUtils.jsonSuccess(list);
    }


    //最新服务
    @ResponseBody
    @RequestMapping("/getnewgoods")
    public PageInfo newgoods(){
        Integer countpage=goodsService.countGoodsPage();
        Random rand = new Random();
        if(countpage==0){
            countpage=8;
        }
        double i = Math.floor(countpage/8);
        int page = rand.nextInt((int)i);
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("pageNum",page);
        map.put("pageSize", 8);
        PageInfo<GoodsDto> newGoods = goodsService.selectNewGoodsByMap(map);
        return newGoods;
    }

    

    @ResponseBody
    @RequestMapping("/getkeygoods")
    public PageInfo keygoods(){
        Integer countpage=goodsService.countGoodsPage();
        if(countpage==0){
            countpage=8;
        }
        Random rand = new Random();
        double i = Math.floor(countpage/8);
        int page = rand.nextInt((int)i);
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("pageNum",page);
        map.put("pageSize", 8);
        map.put("categoryId", 1);
        PageInfo<GoodsDto> newGoods = goodsService.selectNewGoodsByMap(map);
        return newGoods;
    }

    

    //最热星会员
    @ResponseBody
    @RequestMapping("/gethotmember")
    public PageInfo hotmember(String key){
        Integer countpage=memberService.countRecommedPage();
        if(countpage==0){
            countpage=6;
        }
        double i = Math.floor(countpage/6);
        Random rand = new Random();
        int page = rand.nextInt((int)i);
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("page",page);
        map.put("rows", 6);
        Map map2=new HashMap();
        if(page==0) page=1;
        PageInfo<MemberDetail> recommend = memberService.findRecommend(key, page, 6, map2);
        return recommend;
    }



	//路演————最新服务
	private static PageInfo newGoodsPage;
	@ResponseBody
	@RequestMapping("/lynewgoods")
	public PageInfo lynewgoods(){
		if(newGoodsPage==null){
			newGoodsPage=new PageInfo();
			List<GoodsDto> newGoods =new ArrayList<GoodsDto>();
			String[] goodsSns =new String[]{
					"gd2016012514573438763980",
					"gd201601251541054213478",
					"gd2016012515410150216985",
					"gd2016012515410580548153",
					"gd2016012516040643675291",
					"gd2016012516040857107483",
					"gd2016012516392315773614",
					"gd2016012516392791284592"
			};
			for(String sn : goodsSns){
				GoodsDto goodsDto = goodsService.selectGoodsDtoBySn(sn);
				if(goodsDto!=null){
					newGoods.add(goodsDto);
				}
			}
			newGoodsPage.setList(newGoods);
		}
		return newGoodsPage;
	}


	//路演————标签服务
	@ResponseBody
	@RequestMapping("/lykeyggods")
	public PageInfo lykeygoods(Integer key){
		//路演————标签服务 设计师   ID  12   互联网
		List<GoodsDto> hlwGoods = null;
		//带标签的服务,  设计师   ID  12   互联网
		PageInfo page=new PageInfo();
		if(hlwGoods == null){
			hlwGoods = new ArrayList<GoodsDto>();
			String[] goodsSns =null;
			switch (key){
				case 1:
					//互联网
					goodsSns = new String[]{
							"gd2016012514573599134705" , //网络工程咨询服务
							"gd2016012514574588004376" , //电商咨询
							"gd2016012514574729779352" , //电商创业咨询
							"gd2016012514574149746107" , //3D设计
							"gd2016012514573917286021" , //动画设计咨询
							"gd2016012514574018757132" , //室内设计
							"gd2016012514573790648719" , //ios产品研发
							"gd2016012514573716487504"  //软件测试
					};
					break;
				case 2:
					//运动健身
					goodsSns = new String[]{
							"gd2016012516392315773614", //陪练
							"gd2016012516392384239524", //健康美食达人
							"gd2016012516392447523716", //健身教练
							"gd2016012517034782450269", //旅游项目旅游团导游服务
							"gd2016012516392746479325", //舞蹈教练
							"gd2016012516392862119742", //挑选健康内衣
							"gd2016012516392791284592", //时尚包包
							"gd2016012516392628128096"  //健康咨询
					};
					break;
				case 3:
					//颜值变现
					goodsSns = new String[]{
							"gd2016012515410150216985",
							"gd201601251541026772031",
							"gd2016012515410269851260",
							"gd2016012515410334458914",
							"gd201601251541040621947",
							"gd201601251541054213478",
							"gd2016012515410580548153",
							"gd2016012515410644784913"
					};
					break;
			}
			for(String sn : goodsSns){
				GoodsDto goodsDto = goodsService.selectGoodsDtoBySn(sn);
				if(goodsDto!=null){
					hlwGoods.add(goodsDto);
				}
			}
			page.setList(hlwGoods);
		}
		return page;
	}

	//路演————最热星会员
	private static PageInfo hotMemberPage;
	@ResponseBody
	@RequestMapping("/lyhotmember")
	public PageInfo lyhotmember(){
		if(hotMemberPage == null){
			hotMemberPage=new PageInfo();
			List<MemberDetail> hotMemberList = new ArrayList<MemberDetail>();
			Long[] memberids = new Long[]{
					271916L,  //任振宇
					277881L,  //宁东辰
					283405L,  //年检 许会计
					282364L,  //代驾 张振详
					274721L,  //彭文芊
					286263L  //刘佳佳123
					//284463L,  //陈杰杰123
					//286263L //育婴黄女士1
			};
			for(Long memberid : memberids){
				MemberDetail memberDetail = memberService.findMemberDetailById(memberid);
				if(memberDetail != null){
					hotMemberList.add(memberDetail);
				}
			}
			hotMemberPage.setList(hotMemberList);
		}
		return hotMemberPage;
	}

}
