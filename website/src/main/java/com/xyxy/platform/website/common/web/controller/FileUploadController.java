package com.xyxy.platform.website.common.web.controller;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.file.SystemFileService;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 简介: 临时文件上传
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-28 17:44
 */
@Controller
@RequestMapping("/common/fileupload/")
public class FileUploadController extends BaseController {


    @Autowired
    private SystemFileService systemFile;

    @ResponseBody
    @RequestMapping(value = "tmp", method = RequestMethod.POST)
    public ResponseMessage<?> tmpFileUpload(@RequestParam MultipartFile[] files) throws IOException {

        if (null == files || files.length <= 0) {
            return ResponseUtils.jsonFail(null, "请选择需要上传的图片!");
        }

        //如果只是上传一个文件，则只需要MultipartFile类型接收文件即可，而且无需显式指定@RequestParam注解
        //如果想上传多个文件，那么这里就要用MultipartFile[]类型来接收文件，并且还要指定@RequestParam注解
        //并且上传多个文件时，前台表单中的所有<input type="file"/>的name都应该是myfiles，否则参数里的myfiles无法获取到所有上传的文件
        List<Map<String, Object>> imageItemList = Lists.newArrayList();
        for (MultipartFile file : files) {
            if (file.isEmpty()) {
                logger.warn("空文件, 无法上传!");
            } else {
                logger.debug("文件长度: {}", file.getSize());
                logger.debug("文件类型: {}", file.getContentType());
                logger.debug("文件名称: {}", file.getName());
                logger.debug("文件原名: {}", file.getOriginalFilename());

                //如果用的是Tomcat服务器，则文件会上传到\\%TOMCAT_HOME%\\webapps\\YourWebProject\\WEB-INF\\upload\\文件夹中
                //String realPath = request.getSession().getServletContext().getRealPath("/WEB-INF/upload");
                //这里不必处理IO流关闭的问题，因为FileUtils.copyInputStreamToFile()方法内部会自动把用到的IO流关掉，看它的源码就知道

                String fileUrl = systemFile.saveTmpFile(file.getInputStream(), file.getOriginalFilename());

                Map tmpFileInfo = new HashMap();
                tmpFileInfo.put("size", file.getSize());
                tmpFileInfo.put("contentType", file.getContentType());
                tmpFileInfo.put("name", file.getName());
                tmpFileInfo.put("originalFilename", file.getOriginalFilename());
                tmpFileInfo.put("url", fileUrl);
                imageItemList.add(tmpFileInfo);
            }
        }
        return ResponseUtils.jsonSuccess(imageItemList, "文件上传成功!");
    }

}
