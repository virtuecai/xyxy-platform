package com.xyxy.platform.website.showcase.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.entity.trade.CategoryExample;
import com.xyxy.platform.modules.repository.mybatis.trade.CategoryMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-07 13:46
 */
@Controller
@RequestMapping("/showcase")
public class PageDemoController extends BaseController {

    @Autowired
    CategoryMapper categoryMapper;

    /**
     * 页面跳转
     * @return
     */
    @RequestMapping(value = "pagination", method = RequestMethod.GET)
    public String paginationView() {
        return "showcase/pagination";
    }

    /**
     * 查询分页数据
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "pagination/data", method = RequestMethod.GET)
    public ResponseMessage<?> paginationData(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            String categoryName
    ) {
        PageHelper.startPage(pageNum + 1, pageSize);
        CategoryExample sql = new CategoryExample();
        if(StringUtils.isNotEmpty(categoryName)) {
            sql.or().andCategoryTypeLike("%" + categoryName + "%");
        }
        List<Category> list = categoryMapper.selectByExample(sql);
        PageInfo<Category> pageInfo = new PageInfo<>(list);

        return ResponseUtils.jsonSuccess(pageInfo, "分页查询数据成功!");
    }

}
