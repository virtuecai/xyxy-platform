package com.xyxy.platform.website.member.web.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.trade.OrderInfo;
import com.xyxy.platform.modules.service.trade.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-11
 */
@Controller
@RequestMapping("/member")
public class OrderBuyController {

    @Autowired
    private OrderService orderService;

    /**
     * 我购买的服务
     * @return
     */
    @RequestMapping(value = "/order_buy", method = RequestMethod.GET)
    public String orderBuy(){
        return "member/order_buy";
    }

    /**
     * 获取订单类型数据数据列表
     *
     * @param memberId 会员ID
     * @param type 订单类型
     * @param pageNum 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping(value = "/order_buy/list_data", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage listData(@RequestParam long memberId
            , @RequestParam(required = false) String type
            , @RequestParam Integer pageNum
            , @RequestParam Integer pageSize) {
        List<OrderInfo> orderInfoList = orderService.findOrderInfoList(1, String.valueOf(memberId),type , pageNum==0?1:pageNum+1, pageSize);
        PageInfo pageInfo = new PageInfo(orderInfoList);
        return ResponseUtils.jsonSuccess(pageInfo);
    }

    /**
     * 获取订单详情信息
     * @param orderNo 订单编号
     * @param memberId 卖家会员ID
     * @return
     */
    @RequestMapping(value = "/order/detail/buy_{mid}_{id}", method = RequestMethod.GET)
    public String orderBuyDetail(@PathVariable("id") String orderNo
            , @PathVariable("mid") long memberId
            ,Model model) {
        OrderInfo memberOrder = orderService.getSallOrder(memberId, orderNo);
        model.addAttribute("memberOrder", memberOrder);
        return "member/order_buy_detail";
    }
}
