package com.xyxy.platform.website.common.constants;

/**
 * 常量定义
 * Created by VirtueCai on 15/12/15.
 */
public class AppConstants {

    /**
     * 当前登录用户
     */
    public static final String CURRENT_USER = "user";

    /**
     * 登录成功后跳转页面
     */
    public final static String LOGIN_SUCCESS_REDIRECT_URL = "login_success_redirect_url";

}
