package com.xyxy.platform.website.showcase.web.controller;

import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.repository.mybatis.trade.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 简介: 百度 UEditor 富文本编辑器示例
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-07 13:46
 */
@Controller
@RequestMapping("/showcase")
public class BaiduUeditorController extends BaseController {

    @Autowired
    CategoryMapper categoryMapper;

    /**
     * 页面跳转
     * @return
     */
    @RequestMapping(value = "ueditor", method = RequestMethod.GET)
    public String ueditorView() {
        return "showcase/ueditor";
    }

}
