package com.xyxy.platform.website.common.exception;

import org.springframework.http.HttpStatus;

/**
 * 简介: 专用于Restful Service的异常.
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-22 09:56
 */
public class RestException extends RuntimeException {

    public HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

    public RestException() {
    }

    public RestException(HttpStatus status) {
        this.status = status;
    }

    public RestException(String message) {
        super(message);
    }

    public RestException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }
}
