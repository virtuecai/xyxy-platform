package com.xyxy.platform.website.shop.web.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.controller.BaseController;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.service.shop.ShopHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-05
 */
@Controller
@RequestMapping("/shop")
public class HomeController extends BaseController{

    @Autowired
    private ShopHomeService shopHomeService;

    /**
     * 跳转到个人主页
     * @param memberId 会员ID
     * @return
     */
    @RequestMapping(value = "/{id}/home",method = RequestMethod.GET)
    public ModelAndView home(@PathVariable(value="id") long memberId){
        ModelAndView modelAndView = new ModelAndView();
        Member member = shopHomeService.getMemberInfo(memberId);
        if(member!=null) {
            modelAndView.addObject("member", member);
            modelAndView.setViewName("/shop/home");
        }else
            modelAndView.setViewName(page404);
        return modelAndView;
    }

    /**
     * 获取个人主页会员头像
     * @return
     */
    @RequestMapping(value = "/{id}/home/member_img",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage member_img(@PathVariable(value="id") long memberId){
        String imgStr = shopHomeService.getMemberImg(memberId);
        return ResponseUtils.jsonSuccess(imgStr);
    }

    /**
     * 获取个人主页会员介绍
     * @return
     */
    @RequestMapping(value = "/{id}/home/member_desc",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage member_desc(@PathVariable(value="id") long memberId){
        String descStr = shopHomeService.getMemberDesc(memberId);
        return ResponseUtils.jsonSuccess(descStr);
    }

    /**
     * 获取个人主页分页留言信息
     * @param pageNum 页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping(value = "/{id}/home/messages_page",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage messages_page(@PathVariable(value="id") long memberId
            , @RequestParam Integer pageNum
            , @RequestParam Integer pageSize){
        PageInfo pageInfo = shopHomeService.getMessagePage(memberId, pageNum, pageSize);
        return ResponseUtils.jsonSuccess(pageInfo);
    }

    /**
     * 获取个人主页推荐服务
     * @return
     */
    @RequestMapping(value = "/{id}/home/goods_list",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage goods_list(@PathVariable(value="id") long memberId){
        List list = shopHomeService.getGoodsList(memberId);
        return ResponseUtils.jsonSuccess(list);
    }

    /**
     * 个人主页提交留言
     * @param content 留言内容
     * @return
     */
    @RequestMapping(value = "/{id}/home/submit_messages",method = RequestMethod.POST)
    @ResponseBody
    public ResponseMessage submit_message(@PathVariable(value="id") long memberId
            , @RequestParam String content){
        int result = shopHomeService.submitMessage(memberId, content);
        return ResponseUtils.jsonSuccess(result);
    }

    /**
     * 是否已经关注 个人主页统计粉丝和我关注的人 Map[是否已关注boolean,他的粉丝统计int,他的关注统计int]
     * @return
     */
    @RequestMapping(value = "/{id}/home/with_count",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage with_count(@PathVariable(value="id") long memberId
            , @RequestParam long toMemberId){
        Map map = shopHomeService.getWithCount(memberId, toMemberId);
        return ResponseUtils.jsonSuccess(map);
    }

    /**
     * 个人主页获取职业标签和个人标签列表信息 Map[职业标签List,个人标签List]
     * @return
     */
    @RequestMapping(value = "/{id}/home/tag_list",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage tag_list(@PathVariable(value="id") long memberId){
        Map map = shopHomeService.getTagList(memberId);
        return ResponseUtils.jsonSuccess(map);
    }

    /**
     * 个人主页获取我关注的人中 有那些人人也关注了他
     * 我们相同关注的人 Map[list , list]
     * @param toMemberId 浏览人ID
     * @return
     */
    @RequestMapping(value = "/{id}/home/with_list",method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage with_list(@PathVariable(value="id") long memberId
            , @RequestParam long toMemberId){
        Map map = shopHomeService.getMemberWithList(memberId, toMemberId);
        return ResponseUtils.jsonSuccess(map);
    }


}
