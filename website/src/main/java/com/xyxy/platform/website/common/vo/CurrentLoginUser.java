package com.xyxy.platform.website.common.vo;

import java.io.Serializable;

/**
 * 用于存储session中的当前登陆用户基本信息
 * Created by VirtueCai on 15/12/15.
 */
public class CurrentLoginUser implements Serializable {

    /**
     * member.id
     */
    private Long id;

    /**
     * member.alias or member.username
     */
    private String name;

    /**
     * member 头像
     */
    private String imageUrl;

    public CurrentLoginUser() {
    }

    public CurrentLoginUser(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CurrentLoginUser(Long id, String name, String imageUrl) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
