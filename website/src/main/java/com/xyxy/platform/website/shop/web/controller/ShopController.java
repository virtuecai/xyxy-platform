package com.xyxy.platform.website.shop.web.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.service.shop.ShopService;
import com.xyxy.platform.website.shop.web.vo.ShopConditions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-07
 */
@Controller
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    private ShopService shopService;

    /**
     * 跳转到星店页面一级分类
     * @return
     */
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView();
        List list = shopService.getSpecificTags(0, 20, true);
        modelAndView.addObject("tagList", list);
        modelAndView.setViewName("/shop/index");
        return modelAndView;
    }

    /**
     * 根据关键词搜索星店列表
     * @param type 首页类型 1、推荐 2、最新
     * @param pageNum 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping(value = "/index/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage search_list(@RequestParam(required = false) String keyword
            , @RequestParam(required = false) Integer type
            , @RequestParam(required = false) Integer tagId
            , @RequestParam Integer pageNum
            , @RequestParam Integer pageSize){
        Map map = Maps.newHashMap();
        if(tagId!=null)
            map.put("categoryId", tagId);
        PageInfo pageInfo = shopService.getShopList(keyword, pageNum==0?1:pageNum+1, pageSize, map, type);
        return ResponseUtils.jsonSuccess(pageInfo);
    }
}
