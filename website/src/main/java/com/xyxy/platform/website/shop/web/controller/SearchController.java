package com.xyxy.platform.website.shop.web.controller;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.ResponseUtils;
import com.xyxy.platform.modules.core.web.response.ResponseMessage;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.service.shop.SearchShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-07
 */
@Controller
@RequestMapping("/shop")
public class SearchController {

    @Autowired
    private SearchShopService searchShopService;

    /**
     * 跳转到搜索页面
     * @return
     */
    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public ModelAndView search_page(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/shop/search");
        return modelAndView;
    }

    /**
     * 根据关键词搜索星店列表
     * @param keyword 关键词
     * @param pageNum 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage search_list(@PathVariable(value = "keyword") String keyword,
                                       @RequestParam(defaultValue = "1") Integer pageNum,
                                       @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo pageInfo = searchShopService.searchShopList(null, keyword, pageNum, pageSize);
        return ResponseUtils.jsonSuccess(pageInfo);
    }
}