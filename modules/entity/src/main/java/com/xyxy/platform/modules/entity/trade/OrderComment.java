package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class OrderComment implements Serializable {
    private Long id;

    private Long orderId;

    private String orderSn;

    private Long memberId;

    private Integer describeNo;

    private Integer quality;

    private Integer attitude;

    private String tags;

    private Long imgsPkgId;

    private Date createTime;

    private Date updateTime;

    private Integer idDel;

    private String detailed;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getDescribeNo() {
        return describeNo;
    }

    public void setDescribeNo(Integer describeNo) {
        this.describeNo = describeNo;
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    public Integer getAttitude() {
        return attitude;
    }

    public void setAttitude(Integer attitude) {
        this.attitude = attitude;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags == null ? null : tags.trim();
    }

    public Long getImgsPkgId() {
        return imgsPkgId;
    }

    public void setImgsPkgId(Long imgsPkgId) {
        this.imgsPkgId = imgsPkgId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIdDel() {
        return idDel;
    }

    public void setIdDel(Integer idDel) {
        this.idDel = idDel;
    }

    public String getDetailed() {
        return detailed;
    }

    public void setDetailed(String detailed) {
        this.detailed = detailed == null ? null : detailed.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", orderSn=").append(orderSn);
        sb.append(", memberId=").append(memberId);
        sb.append(", describeNo=").append(describeNo);
        sb.append(", quality=").append(quality);
        sb.append(", attitude=").append(attitude);
        sb.append(", tags=").append(tags);
        sb.append(", imgsPkgId=").append(imgsPkgId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", idDel=").append(idDel);
        sb.append(", detailed=").append(detailed);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}