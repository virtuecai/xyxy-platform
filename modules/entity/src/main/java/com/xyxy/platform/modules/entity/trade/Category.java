package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long categoryId;

    private String categoryType;

    private Integer isDel;

    private String categoryShow;

    private Integer isHot;

    private Integer isBest;

    private Byte descOrder;

    private Long pid;

    /**
     * 2016-1-7 变更为 父级ID路径
     * @updator: caizhengda
     */
    private String idPath;

    private Date createTime;

    private Date updateTime;

    private String keywords;

    private String categoryDesc;

    /**
     * 获得 子级的 id path
     * @updator: caizhengda
     * @return
     */
    public String getMakeSelfAsParentIds() {
        return getIdPath() + getCategoryId() + "/";
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType == null ? null : categoryType.trim();
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public String getCategoryShow() {
        return categoryShow;
    }

    public void setCategoryShow(String categoryShow) {
        this.categoryShow = categoryShow == null ? null : categoryShow.trim();
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getIsBest() {
        return isBest;
    }

    public void setIsBest(Integer isBest) {
        this.isBest = isBest;
    }

    public Byte getDescOrder() {
        return descOrder;
    }

    public void setDescOrder(Byte descOrder) {
        this.descOrder = descOrder;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath == null ? null : idPath.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc == null ? null : categoryDesc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", categoryId=").append(categoryId);
        sb.append(", categoryType=").append(categoryType);
        sb.append(", isDel=").append(isDel);
        sb.append(", categoryShow=").append(categoryShow);
        sb.append(", isHot=").append(isHot);
        sb.append(", isBest=").append(isBest);
        sb.append(", descOrder=").append(descOrder);
        sb.append(", pid=").append(pid);
        sb.append(", idPath=").append(idPath);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", keywords=").append(keywords);
        sb.append(", categoryDesc=").append(categoryDesc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}