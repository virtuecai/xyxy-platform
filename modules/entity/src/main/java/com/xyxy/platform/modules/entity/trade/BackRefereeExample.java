package com.xyxy.platform.modules.entity.trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BackRefereeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BackRefereeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRefereeIdIsNull() {
            addCriterion("referee_id is null");
            return (Criteria) this;
        }

        public Criteria andRefereeIdIsNotNull() {
            addCriterion("referee_id is not null");
            return (Criteria) this;
        }

        public Criteria andRefereeIdEqualTo(Long value) {
            addCriterion("referee_id =", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotEqualTo(Long value) {
            addCriterion("referee_id <>", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdGreaterThan(Long value) {
            addCriterion("referee_id >", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("referee_id >=", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdLessThan(Long value) {
            addCriterion("referee_id <", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdLessThanOrEqualTo(Long value) {
            addCriterion("referee_id <=", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdIn(List<Long> values) {
            addCriterion("referee_id in", values, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotIn(List<Long> values) {
            addCriterion("referee_id not in", values, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdBetween(Long value1, Long value2) {
            addCriterion("referee_id between", value1, value2, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotBetween(Long value1, Long value2) {
            addCriterion("referee_id not between", value1, value2, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeIsNull() {
            addCriterion("referee_type is null");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeIsNotNull() {
            addCriterion("referee_type is not null");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeEqualTo(Integer value) {
            addCriterion("referee_type =", value, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeNotEqualTo(Integer value) {
            addCriterion("referee_type <>", value, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeGreaterThan(Integer value) {
            addCriterion("referee_type >", value, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("referee_type >=", value, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeLessThan(Integer value) {
            addCriterion("referee_type <", value, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeLessThanOrEqualTo(Integer value) {
            addCriterion("referee_type <=", value, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeIn(List<Integer> values) {
            addCriterion("referee_type in", values, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeNotIn(List<Integer> values) {
            addCriterion("referee_type not in", values, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeBetween(Integer value1, Integer value2) {
            addCriterion("referee_type between", value1, value2, "refereeType");
            return (Criteria) this;
        }

        public Criteria andRefereeTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("referee_type not between", value1, value2, "refereeType");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNull() {
            addCriterion("member_id is null");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNotNull() {
            addCriterion("member_id is not null");
            return (Criteria) this;
        }

        public Criteria andMemberIdEqualTo(Long value) {
            addCriterion("member_id =", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotEqualTo(Long value) {
            addCriterion("member_id <>", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThan(Long value) {
            addCriterion("member_id >", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThanOrEqualTo(Long value) {
            addCriterion("member_id >=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThan(Long value) {
            addCriterion("member_id <", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThanOrEqualTo(Long value) {
            addCriterion("member_id <=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdIn(List<Long> values) {
            addCriterion("member_id in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotIn(List<Long> values) {
            addCriterion("member_id not in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdBetween(Long value1, Long value2) {
            addCriterion("member_id between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotBetween(Long value1, Long value2) {
            addCriterion("member_id not between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeIsNull() {
            addCriterion("referee_note_type is null");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeIsNotNull() {
            addCriterion("referee_note_type is not null");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeEqualTo(Long value) {
            addCriterion("referee_note_type =", value, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeNotEqualTo(Long value) {
            addCriterion("referee_note_type <>", value, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeGreaterThan(Long value) {
            addCriterion("referee_note_type >", value, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeGreaterThanOrEqualTo(Long value) {
            addCriterion("referee_note_type >=", value, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeLessThan(Long value) {
            addCriterion("referee_note_type <", value, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeLessThanOrEqualTo(Long value) {
            addCriterion("referee_note_type <=", value, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeIn(List<Long> values) {
            addCriterion("referee_note_type in", values, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeNotIn(List<Long> values) {
            addCriterion("referee_note_type not in", values, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeBetween(Long value1, Long value2) {
            addCriterion("referee_note_type between", value1, value2, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andRefereeNoteTypeNotBetween(Long value1, Long value2) {
            addCriterion("referee_note_type not between", value1, value2, "refereeNoteType");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdIsNull() {
            addCriterion("img_ckg_id is null");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdIsNotNull() {
            addCriterion("img_ckg_id is not null");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdEqualTo(Long value) {
            addCriterion("img_ckg_id =", value, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdNotEqualTo(Long value) {
            addCriterion("img_ckg_id <>", value, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdGreaterThan(Long value) {
            addCriterion("img_ckg_id >", value, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdGreaterThanOrEqualTo(Long value) {
            addCriterion("img_ckg_id >=", value, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdLessThan(Long value) {
            addCriterion("img_ckg_id <", value, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdLessThanOrEqualTo(Long value) {
            addCriterion("img_ckg_id <=", value, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdIn(List<Long> values) {
            addCriterion("img_ckg_id in", values, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdNotIn(List<Long> values) {
            addCriterion("img_ckg_id not in", values, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdBetween(Long value1, Long value2) {
            addCriterion("img_ckg_id between", value1, value2, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andImgCkgIdNotBetween(Long value1, Long value2) {
            addCriterion("img_ckg_id not between", value1, value2, "imgCkgId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusIsNull() {
            addCriterion("referee_status is null");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusIsNotNull() {
            addCriterion("referee_status is not null");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusEqualTo(Integer value) {
            addCriterion("referee_status =", value, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusNotEqualTo(Integer value) {
            addCriterion("referee_status <>", value, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusGreaterThan(Integer value) {
            addCriterion("referee_status >", value, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("referee_status >=", value, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusLessThan(Integer value) {
            addCriterion("referee_status <", value, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusLessThanOrEqualTo(Integer value) {
            addCriterion("referee_status <=", value, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusIn(List<Integer> values) {
            addCriterion("referee_status in", values, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusNotIn(List<Integer> values) {
            addCriterion("referee_status not in", values, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusBetween(Integer value1, Integer value2) {
            addCriterion("referee_status between", value1, value2, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andRefereeStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("referee_status not between", value1, value2, "refereeStatus");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(Integer value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(Integer value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(Integer value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(Integer value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<Integer> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<Integer> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(Integer value1, Integer value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}