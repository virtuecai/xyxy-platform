package com.xyxy.platform.modules.entity.sns;

import java.io.Serializable;
import java.util.Date;
/*会员关注日志表*/
public class MemberFollowLog implements Serializable {
    private Long followLogId;   //关注日志ID

    private Long memberId;  //会员ID

    private Long toMemberId;    //被关注会员ID

    private Integer action; //操作类型

    private Date createTime;    //创建时间

    private static final long serialVersionUID = 1L;

    public Long getFollowLogId() {
        return followLogId;
    }

    public void setFollowLogId(Long followLogId) {
        this.followLogId = followLogId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getToMemberId() {
        return toMemberId;
    }

    public void setToMemberId(Long toMemberId) {
        this.toMemberId = toMemberId;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", followLogId=").append(followLogId);
        sb.append(", memberId=").append(memberId);
        sb.append(", toMemberId=").append(toMemberId);
        sb.append(", action=").append(action);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}