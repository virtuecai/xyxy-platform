package com.xyxy.platform.modules.entity.member;

import java.io.Serializable;

public class SpecificTag implements Serializable {
    private Long id;

    private String name;

    private String description;

    private Long parentId;

    private String parentIds;

    private Integer available;

    private Integer idx;

    private Long memberSpecificTagId;

    public Long getMemberSpecificTagId() {
        return memberSpecificTagId;
    }

    public void setMemberSpecificTagId(Long memberSpecificTagId) {
        this.memberSpecificTagId = memberSpecificTagId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }



    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds == null ? null : parentIds.trim();
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", parentId=").append(parentId);
        sb.append(", parentIds=").append(parentIds);
        sb.append(", available=").append(available);
        sb.append(", idx=").append(idx);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}