package com.xyxy.platform.modules.entity.da;

import java.io.Serializable;
import java.util.Date;

public class RecommendMember implements Serializable {
    private Long id;

    private Long memberId;

    private Byte recommendResource;

    private Date date;

    private Long idx;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Byte getRecommendResource() {
        return recommendResource;
    }

    public void setRecommendResource(Byte recommendResource) {
        this.recommendResource = recommendResource;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getIdx() {
        return idx;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", memberId=").append(memberId);
        sb.append(", recommendResource=").append(recommendResource);
        sb.append(", date=").append(date);
        sb.append(", idx=").append(idx);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}