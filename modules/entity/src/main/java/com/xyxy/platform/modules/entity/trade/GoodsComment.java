package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class GoodsComment implements Serializable {
    private Long commentId;

    private Integer commentType;

    private Long commentValue;

    private Long fromMemberId;

    private Long memberId;

    private String ip;

    private Integer isDel;

    private Long pid;

    private Date createTime;

    private Date updateTime;

    private Long imgsPkgId;

    private String content;

    private static final long serialVersionUID = 1L;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Integer getCommentType() {
        return commentType;
    }

    public void setCommentType(Integer commentType) {
        this.commentType = commentType;
    }

    public Long getCommentValue() {
        return commentValue;
    }

    public void setCommentValue(Long commentValue) {
        this.commentValue = commentValue;
    }

    public Long getFromMemberId() {
        return fromMemberId;
    }

    public void setFromMemberId(Long fromMemberId) {
        this.fromMemberId = fromMemberId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getImgsPkgId() {
        return imgsPkgId;
    }

    public void setImgsPkgId(Long imgsPkgId) {
        this.imgsPkgId = imgsPkgId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", commentId=").append(commentId);
        sb.append(", commentType=").append(commentType);
        sb.append(", commentValue=").append(commentValue);
        sb.append(", fromMemberId=").append(fromMemberId);
        sb.append(", memberId=").append(memberId);
        sb.append(", ip=").append(ip);
        sb.append(", isDel=").append(isDel);
        sb.append(", pid=").append(pid);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", imgsPkgId=").append(imgsPkgId);
        sb.append(", content=").append(content);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}