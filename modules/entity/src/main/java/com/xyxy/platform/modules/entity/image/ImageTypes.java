package com.xyxy.platform.modules.entity.image;

/**
 * 简介: 图片类型
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-23 11:51
 */
public enum ImageTypes {

    // member 会员
    /**
     * 头像
     */
    USER_ICON(1l),
    /**
     * 微信二维码
     */
    WEIXIN_CODE(2l),
    /**
     * 身份证, 按顺序, 先正, 后反
     */
    ID_CARD(3l),
    /**
     * 职业证件照
     */
    POSITION(4l),
    /**
     * 服务标题(封面)图, 五张
     */
    GOODS_COVERS_IMAGES(5L);

    private Long typeId;

    private ImageTypes(Long typeId) {
        this.typeId = typeId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
}
