package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class Cart implements Serializable {
    private Long cartId;

    private Long memberId;

    private Long goodsId;

    private Integer number;

    private Date createTime;

    private Date updateTime;
    
    private String goodscontent;

    private static final long serialVersionUID = 1L;

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    
    /**
	 * goodscontent
	 *
	 * @return  the goodscontent
	 * @since   1.0.0
	*/
	
	public String getGoodscontent() {
		return goodscontent;
	}

	/**
	 * @param goodscontent the goodscontent to set
	 */
	public void setGoodscontent(String goodscontent) {
		this.goodscontent = goodscontent;
	}

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cartId=").append(cartId);
        sb.append(", memberId=").append(memberId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodscontent=").append(goodscontent);
        sb.append(", number=").append(number);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}