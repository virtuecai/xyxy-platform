package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class GoodsServerTag implements Serializable {
    private Long goodsTagId;

    private Long goodsId;

    private Long serverTagId1;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getGoodsTagId() {
        return goodsTagId;
    }

    public void setGoodsTagId(Long goodsTagId) {
        this.goodsTagId = goodsTagId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getServerTagId1() {
        return serverTagId1;
    }

    public void setServerTagId1(Long serverTagId1) {
        this.serverTagId1 = serverTagId1;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", goodsTagId=").append(goodsTagId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", serverTagId1=").append(serverTagId1);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}