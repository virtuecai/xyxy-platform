package com.xyxy.platform.modules.entity.image;

import java.io.Serializable;

public class ImageItem implements Serializable {
    private Long id;

    private Long imagePkgId;

    private Long imageTypeId;

    private Integer width;

    private Integer height;

    private String uri;

    private Integer idx;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getImagePkgId() {
        return imagePkgId;
    }

    public void setImagePkgId(Long imagePkgId) {
        this.imagePkgId = imagePkgId;
    }

    public Long getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(Long imageTypeId) {
        this.imageTypeId = imageTypeId;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri == null ? null : uri.trim();
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", imagePkgId=").append(imagePkgId);
        sb.append(", imageTypeId=").append(imageTypeId);
        sb.append(", width=").append(width);
        sb.append(", height=").append(height);
        sb.append(", uri=").append(uri);
        sb.append(", idx=").append(idx);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}