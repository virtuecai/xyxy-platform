package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class GoodsCases implements Serializable {
    private Long caseId;

    private Long goodsId;

    private Long caseArticleId;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCaseArticleId() {
        return caseArticleId;
    }

    public void setCaseArticleId(Long caseArticleId) {
        this.caseArticleId = caseArticleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", caseId=").append(caseId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", caseArticleId=").append(caseArticleId);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}