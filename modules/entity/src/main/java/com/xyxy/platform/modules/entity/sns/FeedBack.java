package com.xyxy.platform.modules.entity.sns;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
/*留言消息*/
public class FeedBack implements Serializable {

    @NotBlank
    private Long msgId; //消息ID唯一标识

    private Long msgPid;    //父消息ID

    private Long msgMemberId;   //会员ID

    private String msgIp;   //消息IP

    private String msgTitle;    //消息标题

    private Integer msgType;    //消息类型 0、留言 1、投诉

    private Integer msgStatus;  //消息状态 0、隐藏 1、显示

    private Date msgTime;   //消息创建时间

    private Long msgCkgId;  //留言图片

    private Integer msgOrder;   //排序号

    private Integer msgArea;    //留言范围

    private Integer isDel;  //逻辑删除标识 0、否 1、是

    private Long memberId;

    private String msgContent;  //消息内容

    private static final long serialVersionUID = 1L;

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public Long getMsgPid() {
        return msgPid;
    }

    public void setMsgPid(Long msgPid) {
        this.msgPid = msgPid;
    }

    public Long getMsgMemberId() {
        return msgMemberId;
    }

    public void setMsgMemberId(Long msgMemberId) {
        this.msgMemberId = msgMemberId;
    }

    public String getMsgIp() {
        return msgIp;
    }

    public void setMsgIp(String msgIp) {
        this.msgIp = msgIp == null ? null : msgIp.trim();
    }

    public String getMsgTitle() {
        return msgTitle;
    }

    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle == null ? null : msgTitle.trim();
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public Integer getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(Integer msgStatus) {
        this.msgStatus = msgStatus;
    }

    public Date getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(Date msgTime) {
        this.msgTime = msgTime;
    }

    public Long getMsgCkgId() {
        return msgCkgId;
    }

    public void setMsgCkgId(Long msgCkgId) {
        this.msgCkgId = msgCkgId;
    }

    public Integer getMsgOrder() {
        return msgOrder;
    }

    public void setMsgOrder(Integer msgOrder) {
        this.msgOrder = msgOrder;
    }

    public Integer getMsgArea() {
        return msgArea;
    }

    public void setMsgArea(Integer msgArea) {
        this.msgArea = msgArea;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent == null ? null : msgContent.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", msgId=").append(msgId);
        sb.append(", msgPid=").append(msgPid);
        sb.append(", msgMemberId=").append(msgMemberId);
        sb.append(", msgIp=").append(msgIp);
        sb.append(", msgTitle=").append(msgTitle);
        sb.append(", msgType=").append(msgType);
        sb.append(", msgStatus=").append(msgStatus);
        sb.append(", msgTime=").append(msgTime);
        sb.append(", msgCkgId=").append(msgCkgId);
        sb.append(", msgOrder=").append(msgOrder);
        sb.append(", msgArea=").append(msgArea);
        sb.append(", isDel=").append(isDel);
        sb.append(", memberId=").append(memberId);
        sb.append(", msgContent=").append(msgContent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}