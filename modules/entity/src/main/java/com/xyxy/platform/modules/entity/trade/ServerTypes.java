package com.xyxy.platform.modules.entity.trade;

/**
 * 简介: 服务方式
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-11 21:45
 */
public enum ServerTypes {

    // 上门
    COME(1),
    // 预定地点
    CONVERTIONS(2),
    // 线上服务
    ONLINE(3),
    // 邮寄
    MAIL(4);

    private Integer type;

    private ServerTypes(Integer type) {
        this.type = type;
    }

}
