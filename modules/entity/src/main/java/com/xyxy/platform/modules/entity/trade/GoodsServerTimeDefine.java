package com.xyxy.platform.modules.entity.trade;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 简介: 服务时间段定义
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-14 09:57
 */

public enum GoodsServerTimeDefine {

    A(1, "09:00-12:00"),
    B(2, "12:00-15:00"),
    C(3, "15:00-17:00"),
    D(4, "17:00-21:00"),
    E(5, "21:00-24:00");

    /**
     * 第几个时间段 0 开始
     */
    private Integer idx;

    /**
     * 时间段描述用于format date
     */
    private String rangeDesc;

    private GoodsServerTimeDefine(Integer idx, String rangeDesc) {
        this.idx = idx;
        this.rangeDesc = rangeDesc;
    }

    @JSONField(serialize = true)
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @JSONField(serialize = true)
    public String getRangeDesc() {
        return rangeDesc;
    }

    public void setRangeDesc(String rangeDesc) {
        this.rangeDesc = rangeDesc;
    }

    /**
     * 根据时间段下标获得改时间段定义
     *
     * @param idx
     * @return
     */
    public static GoodsServerTimeDefine from(Integer idx) {
        GoodsServerTimeDefine[] values = values();
        int length = values.length;
        for (int i = 0; i < length; ++i) {
            GoodsServerTimeDefine status = values[i];
            if (status.idx == idx) {
                return status;
            }
        }
        throw new IllegalArgumentException("找不到该类型的状态:" + idx);
    }

    @Override
    public String toString() {
        return "GoodsServerTimeDefine{" +
                "idx=" + idx +
                ", rangeDesc='" + rangeDesc + '\'' +
                '}';
    }
}
