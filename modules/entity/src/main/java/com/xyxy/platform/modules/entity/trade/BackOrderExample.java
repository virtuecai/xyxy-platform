package com.xyxy.platform.modules.entity.trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BackOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BackOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBackIdIsNull() {
            addCriterion("back_id is null");
            return (Criteria) this;
        }

        public Criteria andBackIdIsNotNull() {
            addCriterion("back_id is not null");
            return (Criteria) this;
        }

        public Criteria andBackIdEqualTo(Long value) {
            addCriterion("back_id =", value, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdNotEqualTo(Long value) {
            addCriterion("back_id <>", value, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdGreaterThan(Long value) {
            addCriterion("back_id >", value, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdGreaterThanOrEqualTo(Long value) {
            addCriterion("back_id >=", value, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdLessThan(Long value) {
            addCriterion("back_id <", value, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdLessThanOrEqualTo(Long value) {
            addCriterion("back_id <=", value, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdIn(List<Long> values) {
            addCriterion("back_id in", values, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdNotIn(List<Long> values) {
            addCriterion("back_id not in", values, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdBetween(Long value1, Long value2) {
            addCriterion("back_id between", value1, value2, "backId");
            return (Criteria) this;
        }

        public Criteria andBackIdNotBetween(Long value1, Long value2) {
            addCriterion("back_id not between", value1, value2, "backId");
            return (Criteria) this;
        }

        public Criteria andBackSnIsNull() {
            addCriterion("back_sn is null");
            return (Criteria) this;
        }

        public Criteria andBackSnIsNotNull() {
            addCriterion("back_sn is not null");
            return (Criteria) this;
        }

        public Criteria andBackSnEqualTo(String value) {
            addCriterion("back_sn =", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnNotEqualTo(String value) {
            addCriterion("back_sn <>", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnGreaterThan(String value) {
            addCriterion("back_sn >", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnGreaterThanOrEqualTo(String value) {
            addCriterion("back_sn >=", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnLessThan(String value) {
            addCriterion("back_sn <", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnLessThanOrEqualTo(String value) {
            addCriterion("back_sn <=", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnLike(String value) {
            addCriterion("back_sn like", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnNotLike(String value) {
            addCriterion("back_sn not like", value, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnIn(List<String> values) {
            addCriterion("back_sn in", values, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnNotIn(List<String> values) {
            addCriterion("back_sn not in", values, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnBetween(String value1, String value2) {
            addCriterion("back_sn between", value1, value2, "backSn");
            return (Criteria) this;
        }

        public Criteria andBackSnNotBetween(String value1, String value2) {
            addCriterion("back_sn not between", value1, value2, "backSn");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderSnIsNull() {
            addCriterion("order_sn is null");
            return (Criteria) this;
        }

        public Criteria andOrderSnIsNotNull() {
            addCriterion("order_sn is not null");
            return (Criteria) this;
        }

        public Criteria andOrderSnEqualTo(String value) {
            addCriterion("order_sn =", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotEqualTo(String value) {
            addCriterion("order_sn <>", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnGreaterThan(String value) {
            addCriterion("order_sn >", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnGreaterThanOrEqualTo(String value) {
            addCriterion("order_sn >=", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnLessThan(String value) {
            addCriterion("order_sn <", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnLessThanOrEqualTo(String value) {
            addCriterion("order_sn <=", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnLike(String value) {
            addCriterion("order_sn like", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotLike(String value) {
            addCriterion("order_sn not like", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnIn(List<String> values) {
            addCriterion("order_sn in", values, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotIn(List<String> values) {
            addCriterion("order_sn not in", values, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnBetween(String value1, String value2) {
            addCriterion("order_sn between", value1, value2, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotBetween(String value1, String value2) {
            addCriterion("order_sn not between", value1, value2, "orderSn");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNull() {
            addCriterion("member_id is null");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNotNull() {
            addCriterion("member_id is not null");
            return (Criteria) this;
        }

        public Criteria andMemberIdEqualTo(Long value) {
            addCriterion("member_id =", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotEqualTo(Long value) {
            addCriterion("member_id <>", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThan(Long value) {
            addCriterion("member_id >", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThanOrEqualTo(Long value) {
            addCriterion("member_id >=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThan(Long value) {
            addCriterion("member_id <", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThanOrEqualTo(Long value) {
            addCriterion("member_id <=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdIn(List<Long> values) {
            addCriterion("member_id in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotIn(List<Long> values) {
            addCriterion("member_id not in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdBetween(Long value1, Long value2) {
            addCriterion("member_id between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotBetween(Long value1, Long value2) {
            addCriterion("member_id not between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andPostscriptIsNull() {
            addCriterion("postscript is null");
            return (Criteria) this;
        }

        public Criteria andPostscriptIsNotNull() {
            addCriterion("postscript is not null");
            return (Criteria) this;
        }

        public Criteria andPostscriptEqualTo(String value) {
            addCriterion("postscript =", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptNotEqualTo(String value) {
            addCriterion("postscript <>", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptGreaterThan(String value) {
            addCriterion("postscript >", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptGreaterThanOrEqualTo(String value) {
            addCriterion("postscript >=", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptLessThan(String value) {
            addCriterion("postscript <", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptLessThanOrEqualTo(String value) {
            addCriterion("postscript <=", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptLike(String value) {
            addCriterion("postscript like", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptNotLike(String value) {
            addCriterion("postscript not like", value, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptIn(List<String> values) {
            addCriterion("postscript in", values, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptNotIn(List<String> values) {
            addCriterion("postscript not in", values, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptBetween(String value1, String value2) {
            addCriterion("postscript between", value1, value2, "postscript");
            return (Criteria) this;
        }

        public Criteria andPostscriptNotBetween(String value1, String value2) {
            addCriterion("postscript not between", value1, value2, "postscript");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBackStatusIsNull() {
            addCriterion("back_status is null");
            return (Criteria) this;
        }

        public Criteria andBackStatusIsNotNull() {
            addCriterion("back_status is not null");
            return (Criteria) this;
        }

        public Criteria andBackStatusEqualTo(Integer value) {
            addCriterion("back_status =", value, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusNotEqualTo(Integer value) {
            addCriterion("back_status <>", value, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusGreaterThan(Integer value) {
            addCriterion("back_status >", value, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("back_status >=", value, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusLessThan(Integer value) {
            addCriterion("back_status <", value, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusLessThanOrEqualTo(Integer value) {
            addCriterion("back_status <=", value, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusIn(List<Integer> values) {
            addCriterion("back_status in", values, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusNotIn(List<Integer> values) {
            addCriterion("back_status not in", values, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusBetween(Integer value1, Integer value2) {
            addCriterion("back_status between", value1, value2, "backStatus");
            return (Criteria) this;
        }

        public Criteria andBackStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("back_status not between", value1, value2, "backStatus");
            return (Criteria) this;
        }

        public Criteria andActionNoteIsNull() {
            addCriterion("action_note is null");
            return (Criteria) this;
        }

        public Criteria andActionNoteIsNotNull() {
            addCriterion("action_note is not null");
            return (Criteria) this;
        }

        public Criteria andActionNoteEqualTo(String value) {
            addCriterion("action_note =", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteNotEqualTo(String value) {
            addCriterion("action_note <>", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteGreaterThan(String value) {
            addCriterion("action_note >", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteGreaterThanOrEqualTo(String value) {
            addCriterion("action_note >=", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteLessThan(String value) {
            addCriterion("action_note <", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteLessThanOrEqualTo(String value) {
            addCriterion("action_note <=", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteLike(String value) {
            addCriterion("action_note like", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteNotLike(String value) {
            addCriterion("action_note not like", value, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteIn(List<String> values) {
            addCriterion("action_note in", values, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteNotIn(List<String> values) {
            addCriterion("action_note not in", values, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteBetween(String value1, String value2) {
            addCriterion("action_note between", value1, value2, "actionNote");
            return (Criteria) this;
        }

        public Criteria andActionNoteNotBetween(String value1, String value2) {
            addCriterion("action_note not between", value1, value2, "actionNote");
            return (Criteria) this;
        }

        public Criteria andNoteIdIsNull() {
            addCriterion("note_id is null");
            return (Criteria) this;
        }

        public Criteria andNoteIdIsNotNull() {
            addCriterion("note_id is not null");
            return (Criteria) this;
        }

        public Criteria andNoteIdEqualTo(Integer value) {
            addCriterion("note_id =", value, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdNotEqualTo(Integer value) {
            addCriterion("note_id <>", value, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdGreaterThan(Integer value) {
            addCriterion("note_id >", value, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("note_id >=", value, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdLessThan(Integer value) {
            addCriterion("note_id <", value, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdLessThanOrEqualTo(Integer value) {
            addCriterion("note_id <=", value, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdIn(List<Integer> values) {
            addCriterion("note_id in", values, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdNotIn(List<Integer> values) {
            addCriterion("note_id not in", values, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdBetween(Integer value1, Integer value2) {
            addCriterion("note_id between", value1, value2, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteIdNotBetween(Integer value1, Integer value2) {
            addCriterion("note_id not between", value1, value2, "noteId");
            return (Criteria) this;
        }

        public Criteria andNoteStrIsNull() {
            addCriterion("note_str is null");
            return (Criteria) this;
        }

        public Criteria andNoteStrIsNotNull() {
            addCriterion("note_str is not null");
            return (Criteria) this;
        }

        public Criteria andNoteStrEqualTo(String value) {
            addCriterion("note_str =", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrNotEqualTo(String value) {
            addCriterion("note_str <>", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrGreaterThan(String value) {
            addCriterion("note_str >", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrGreaterThanOrEqualTo(String value) {
            addCriterion("note_str >=", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrLessThan(String value) {
            addCriterion("note_str <", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrLessThanOrEqualTo(String value) {
            addCriterion("note_str <=", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrLike(String value) {
            addCriterion("note_str like", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrNotLike(String value) {
            addCriterion("note_str not like", value, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrIn(List<String> values) {
            addCriterion("note_str in", values, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrNotIn(List<String> values) {
            addCriterion("note_str not in", values, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrBetween(String value1, String value2) {
            addCriterion("note_str between", value1, value2, "noteStr");
            return (Criteria) this;
        }

        public Criteria andNoteStrNotBetween(String value1, String value2) {
            addCriterion("note_str not between", value1, value2, "noteStr");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdIsNull() {
            addCriterion("back_note_id is null");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdIsNotNull() {
            addCriterion("back_note_id is not null");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdEqualTo(Integer value) {
            addCriterion("back_note_id =", value, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdNotEqualTo(Integer value) {
            addCriterion("back_note_id <>", value, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdGreaterThan(Integer value) {
            addCriterion("back_note_id >", value, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("back_note_id >=", value, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdLessThan(Integer value) {
            addCriterion("back_note_id <", value, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdLessThanOrEqualTo(Integer value) {
            addCriterion("back_note_id <=", value, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdIn(List<Integer> values) {
            addCriterion("back_note_id in", values, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdNotIn(List<Integer> values) {
            addCriterion("back_note_id not in", values, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdBetween(Integer value1, Integer value2) {
            addCriterion("back_note_id between", value1, value2, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIdNotBetween(Integer value1, Integer value2) {
            addCriterion("back_note_id not between", value1, value2, "backNoteId");
            return (Criteria) this;
        }

        public Criteria andBackNoteIsNull() {
            addCriterion("back_note is null");
            return (Criteria) this;
        }

        public Criteria andBackNoteIsNotNull() {
            addCriterion("back_note is not null");
            return (Criteria) this;
        }

        public Criteria andBackNoteEqualTo(String value) {
            addCriterion("back_note =", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteNotEqualTo(String value) {
            addCriterion("back_note <>", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteGreaterThan(String value) {
            addCriterion("back_note >", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteGreaterThanOrEqualTo(String value) {
            addCriterion("back_note >=", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteLessThan(String value) {
            addCriterion("back_note <", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteLessThanOrEqualTo(String value) {
            addCriterion("back_note <=", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteLike(String value) {
            addCriterion("back_note like", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteNotLike(String value) {
            addCriterion("back_note not like", value, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteIn(List<String> values) {
            addCriterion("back_note in", values, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteNotIn(List<String> values) {
            addCriterion("back_note not in", values, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteBetween(String value1, String value2) {
            addCriterion("back_note between", value1, value2, "backNote");
            return (Criteria) this;
        }

        public Criteria andBackNoteNotBetween(String value1, String value2) {
            addCriterion("back_note not between", value1, value2, "backNote");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}