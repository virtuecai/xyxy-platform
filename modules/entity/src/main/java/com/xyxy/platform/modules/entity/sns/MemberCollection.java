package com.xyxy.platform.modules.entity.sns;

import java.io.Serializable;
import java.util.Date;

/*会员商品收藏*/
public class MemberCollection implements Serializable {

    private Long collectionId;  //收藏ID唯一标识

    private Long memberId;  //会员ID

    private Long goodsid;   //商品ID

    private Date createTime;    //创建时间

    private static final long serialVersionUID = 1L;

    public Long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(Long collectionId) {
        this.collectionId = collectionId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(Long goodsid) {
        this.goodsid = goodsid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", collectionId=").append(collectionId);
        sb.append(", memberId=").append(memberId);
        sb.append(", goodsId=").append(goodsid);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}