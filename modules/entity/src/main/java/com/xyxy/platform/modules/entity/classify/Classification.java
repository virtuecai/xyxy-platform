package com.xyxy.platform.modules.entity.classify;

import java.io.Serializable;

/**
 * Classification
 *
 * @author yangdianjun
 * @date 2015/11/19 0019
 */
public class Classification implements Serializable {

    private Long id;
    private String title;
    private Long parentId;
    private Integer level;
    private String  parentIds;
    private Integer idx; //排序

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }


    public String getMakeSelfAsParentIds() {
        return getParentIds()  +"/" + getId()  ;
    }

    public boolean isRootNode(){
        return parentId == 0;
    }


}
