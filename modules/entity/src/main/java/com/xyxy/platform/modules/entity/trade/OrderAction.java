package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class OrderAction implements Serializable {
    private Long actionId;

    private Long orderId;

    private Long actionUser;

    private Integer orderStatus;

    private String actionIp;

    private String actionNote;

    private Date actionTime;

    private static final long serialVersionUID = 1L;

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getActionUser() {
        return actionUser;
    }

    public void setActionUser(Long actionUser) {
        this.actionUser = actionUser;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getActionIp() {
        return actionIp;
    }

    public void setActionIp(String actionIp) {
        this.actionIp = actionIp == null ? null : actionIp.trim();
    }

    public String getActionNote() {
        return actionNote;
    }

    public void setActionNote(String actionNote) {
        this.actionNote = actionNote == null ? null : actionNote.trim();
    }

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", actionId=").append(actionId);
        sb.append(", orderId=").append(orderId);
        sb.append(", actionUser=").append(actionUser);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", actionIp=").append(actionIp);
        sb.append(", actionNote=").append(actionNote);
        sb.append(", actionTime=").append(actionTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}