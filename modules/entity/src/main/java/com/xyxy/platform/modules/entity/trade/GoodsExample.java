package com.xyxy.platform.modules.entity.trade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GoodsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGoodsIdIsNull() {
            addCriterion("goods_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNotNull() {
            addCriterion("goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdEqualTo(Long value) {
            addCriterion("goods_id =", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotEqualTo(Long value) {
            addCriterion("goods_id <>", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThan(Long value) {
            addCriterion("goods_id >", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThanOrEqualTo(Long value) {
            addCriterion("goods_id >=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThan(Long value) {
            addCriterion("goods_id <", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThanOrEqualTo(Long value) {
            addCriterion("goods_id <=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIn(List<Long> values) {
            addCriterion("goods_id in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotIn(List<Long> values) {
            addCriterion("goods_id not in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdBetween(Long value1, Long value2) {
            addCriterion("goods_id between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotBetween(Long value1, Long value2) {
            addCriterion("goods_id not between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andCatIdIsNull() {
            addCriterion("cat_id is null");
            return (Criteria) this;
        }

        public Criteria andCatIdIsNotNull() {
            addCriterion("cat_id is not null");
            return (Criteria) this;
        }

        public Criteria andCatIdEqualTo(Long value) {
            addCriterion("cat_id =", value, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdNotEqualTo(Long value) {
            addCriterion("cat_id <>", value, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdGreaterThan(Long value) {
            addCriterion("cat_id >", value, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdGreaterThanOrEqualTo(Long value) {
            addCriterion("cat_id >=", value, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdLessThan(Long value) {
            addCriterion("cat_id <", value, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdLessThanOrEqualTo(Long value) {
            addCriterion("cat_id <=", value, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdIn(List<Long> values) {
            addCriterion("cat_id in", values, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdNotIn(List<Long> values) {
            addCriterion("cat_id not in", values, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdBetween(Long value1, Long value2) {
            addCriterion("cat_id between", value1, value2, "catId");
            return (Criteria) this;
        }

        public Criteria andCatIdNotBetween(Long value1, Long value2) {
            addCriterion("cat_id not between", value1, value2, "catId");
            return (Criteria) this;
        }

        public Criteria andGoodsSnIsNull() {
            addCriterion("goods_sn is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSnIsNotNull() {
            addCriterion("goods_sn is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSnEqualTo(String value) {
            addCriterion("goods_sn =", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnNotEqualTo(String value) {
            addCriterion("goods_sn <>", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnGreaterThan(String value) {
            addCriterion("goods_sn >", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnGreaterThanOrEqualTo(String value) {
            addCriterion("goods_sn >=", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnLessThan(String value) {
            addCriterion("goods_sn <", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnLessThanOrEqualTo(String value) {
            addCriterion("goods_sn <=", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnLike(String value) {
            addCriterion("goods_sn like", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnNotLike(String value) {
            addCriterion("goods_sn not like", value, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnIn(List<String> values) {
            addCriterion("goods_sn in", values, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnNotIn(List<String> values) {
            addCriterion("goods_sn not in", values, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnBetween(String value1, String value2) {
            addCriterion("goods_sn between", value1, value2, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsSnNotBetween(String value1, String value2) {
            addCriterion("goods_sn not between", value1, value2, "goodsSn");
            return (Criteria) this;
        }

        public Criteria andGoodsNameIsNull() {
            addCriterion("goods_name is null");
            return (Criteria) this;
        }

        public Criteria andGoodsNameIsNotNull() {
            addCriterion("goods_name is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsNameEqualTo(String value) {
            addCriterion("goods_name =", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameNotEqualTo(String value) {
            addCriterion("goods_name <>", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameGreaterThan(String value) {
            addCriterion("goods_name >", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameGreaterThanOrEqualTo(String value) {
            addCriterion("goods_name >=", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameLessThan(String value) {
            addCriterion("goods_name <", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameLessThanOrEqualTo(String value) {
            addCriterion("goods_name <=", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameLike(String value) {
            addCriterion("goods_name like", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameNotLike(String value) {
            addCriterion("goods_name not like", value, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameIn(List<String> values) {
            addCriterion("goods_name in", values, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameNotIn(List<String> values) {
            addCriterion("goods_name not in", values, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameBetween(String value1, String value2) {
            addCriterion("goods_name between", value1, value2, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsNameNotBetween(String value1, String value2) {
            addCriterion("goods_name not between", value1, value2, "goodsName");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdIsNull() {
            addCriterion("goods_company_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdIsNotNull() {
            addCriterion("goods_company_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdEqualTo(Byte value) {
            addCriterion("goods_company_id =", value, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdNotEqualTo(Byte value) {
            addCriterion("goods_company_id <>", value, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdGreaterThan(Byte value) {
            addCriterion("goods_company_id >", value, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("goods_company_id >=", value, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdLessThan(Byte value) {
            addCriterion("goods_company_id <", value, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdLessThanOrEqualTo(Byte value) {
            addCriterion("goods_company_id <=", value, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdIn(List<Byte> values) {
            addCriterion("goods_company_id in", values, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdNotIn(List<Byte> values) {
            addCriterion("goods_company_id not in", values, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdBetween(Byte value1, Byte value2) {
            addCriterion("goods_company_id between", value1, value2, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyIdNotBetween(Byte value1, Byte value2) {
            addCriterion("goods_company_id not between", value1, value2, "goodsCompanyId");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteIsNull() {
            addCriterion("goods_company_note is null");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteIsNotNull() {
            addCriterion("goods_company_note is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteEqualTo(String value) {
            addCriterion("goods_company_note =", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteNotEqualTo(String value) {
            addCriterion("goods_company_note <>", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteGreaterThan(String value) {
            addCriterion("goods_company_note >", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteGreaterThanOrEqualTo(String value) {
            addCriterion("goods_company_note >=", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteLessThan(String value) {
            addCriterion("goods_company_note <", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteLessThanOrEqualTo(String value) {
            addCriterion("goods_company_note <=", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteLike(String value) {
            addCriterion("goods_company_note like", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteNotLike(String value) {
            addCriterion("goods_company_note not like", value, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteIn(List<String> values) {
            addCriterion("goods_company_note in", values, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteNotIn(List<String> values) {
            addCriterion("goods_company_note not in", values, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteBetween(String value1, String value2) {
            addCriterion("goods_company_note between", value1, value2, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andGoodsCompanyNoteNotBetween(String value1, String value2) {
            addCriterion("goods_company_note not between", value1, value2, "goodsCompanyNote");
            return (Criteria) this;
        }

        public Criteria andClickCountIsNull() {
            addCriterion("click_count is null");
            return (Criteria) this;
        }

        public Criteria andClickCountIsNotNull() {
            addCriterion("click_count is not null");
            return (Criteria) this;
        }

        public Criteria andClickCountEqualTo(Integer value) {
            addCriterion("click_count =", value, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountNotEqualTo(Integer value) {
            addCriterion("click_count <>", value, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountGreaterThan(Integer value) {
            addCriterion("click_count >", value, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("click_count >=", value, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountLessThan(Integer value) {
            addCriterion("click_count <", value, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountLessThanOrEqualTo(Integer value) {
            addCriterion("click_count <=", value, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountIn(List<Integer> values) {
            addCriterion("click_count in", values, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountNotIn(List<Integer> values) {
            addCriterion("click_count not in", values, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountBetween(Integer value1, Integer value2) {
            addCriterion("click_count between", value1, value2, "clickCount");
            return (Criteria) this;
        }

        public Criteria andClickCountNotBetween(Integer value1, Integer value2) {
            addCriterion("click_count not between", value1, value2, "clickCount");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNull() {
            addCriterion("member_id is null");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNotNull() {
            addCriterion("member_id is not null");
            return (Criteria) this;
        }

        public Criteria andMemberIdEqualTo(Long value) {
            addCriterion("member_id =", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotEqualTo(Long value) {
            addCriterion("member_id <>", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThan(Long value) {
            addCriterion("member_id >", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThanOrEqualTo(Long value) {
            addCriterion("member_id >=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThan(Long value) {
            addCriterion("member_id <", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThanOrEqualTo(Long value) {
            addCriterion("member_id <=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdIn(List<Long> values) {
            addCriterion("member_id in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotIn(List<Long> values) {
            addCriterion("member_id not in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdBetween(Long value1, Long value2) {
            addCriterion("member_id between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotBetween(Long value1, Long value2) {
            addCriterion("member_id not between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andMarketPriceIsNull() {
            addCriterion("market_price is null");
            return (Criteria) this;
        }

        public Criteria andMarketPriceIsNotNull() {
            addCriterion("market_price is not null");
            return (Criteria) this;
        }

        public Criteria andMarketPriceEqualTo(BigDecimal value) {
            addCriterion("market_price =", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceNotEqualTo(BigDecimal value) {
            addCriterion("market_price <>", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceGreaterThan(BigDecimal value) {
            addCriterion("market_price >", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("market_price >=", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceLessThan(BigDecimal value) {
            addCriterion("market_price <", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("market_price <=", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceIn(List<BigDecimal> values) {
            addCriterion("market_price in", values, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceNotIn(List<BigDecimal> values) {
            addCriterion("market_price not in", values, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("market_price between", value1, value2, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("market_price not between", value1, value2, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceIsNull() {
            addCriterion("shop_price is null");
            return (Criteria) this;
        }

        public Criteria andShopPriceIsNotNull() {
            addCriterion("shop_price is not null");
            return (Criteria) this;
        }

        public Criteria andShopPriceEqualTo(BigDecimal value) {
            addCriterion("shop_price =", value, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceNotEqualTo(BigDecimal value) {
            addCriterion("shop_price <>", value, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceGreaterThan(BigDecimal value) {
            addCriterion("shop_price >", value, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("shop_price >=", value, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceLessThan(BigDecimal value) {
            addCriterion("shop_price <", value, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("shop_price <=", value, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceIn(List<BigDecimal> values) {
            addCriterion("shop_price in", values, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceNotIn(List<BigDecimal> values) {
            addCriterion("shop_price not in", values, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("shop_price between", value1, value2, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andShopPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("shop_price not between", value1, value2, "shopPrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceIsNull() {
            addCriterion("promote_price is null");
            return (Criteria) this;
        }

        public Criteria andPromotePriceIsNotNull() {
            addCriterion("promote_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromotePriceEqualTo(BigDecimal value) {
            addCriterion("promote_price =", value, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceNotEqualTo(BigDecimal value) {
            addCriterion("promote_price <>", value, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceGreaterThan(BigDecimal value) {
            addCriterion("promote_price >", value, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promote_price >=", value, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceLessThan(BigDecimal value) {
            addCriterion("promote_price <", value, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promote_price <=", value, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceIn(List<BigDecimal> values) {
            addCriterion("promote_price in", values, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceNotIn(List<BigDecimal> values) {
            addCriterion("promote_price not in", values, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promote_price between", value1, value2, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromotePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promote_price not between", value1, value2, "promotePrice");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeIsNull() {
            addCriterion("promote_start_time is null");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeIsNotNull() {
            addCriterion("promote_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeEqualTo(Date value) {
            addCriterion("promote_start_time =", value, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeNotEqualTo(Date value) {
            addCriterion("promote_start_time <>", value, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeGreaterThan(Date value) {
            addCriterion("promote_start_time >", value, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("promote_start_time >=", value, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeLessThan(Date value) {
            addCriterion("promote_start_time <", value, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("promote_start_time <=", value, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeIn(List<Date> values) {
            addCriterion("promote_start_time in", values, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeNotIn(List<Date> values) {
            addCriterion("promote_start_time not in", values, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeBetween(Date value1, Date value2) {
            addCriterion("promote_start_time between", value1, value2, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("promote_start_time not between", value1, value2, "promoteStartTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeIsNull() {
            addCriterion("promote_end_time is null");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeIsNotNull() {
            addCriterion("promote_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeEqualTo(Date value) {
            addCriterion("promote_end_time =", value, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeNotEqualTo(Date value) {
            addCriterion("promote_end_time <>", value, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeGreaterThan(Date value) {
            addCriterion("promote_end_time >", value, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("promote_end_time >=", value, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeLessThan(Date value) {
            addCriterion("promote_end_time <", value, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("promote_end_time <=", value, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeIn(List<Date> values) {
            addCriterion("promote_end_time in", values, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeNotIn(List<Date> values) {
            addCriterion("promote_end_time not in", values, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeBetween(Date value1, Date value2) {
            addCriterion("promote_end_time between", value1, value2, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andPromoteEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("promote_end_time not between", value1, value2, "promoteEndTime");
            return (Criteria) this;
        }

        public Criteria andServerTypeIsNull() {
            addCriterion("server_type is null");
            return (Criteria) this;
        }

        public Criteria andServerTypeIsNotNull() {
            addCriterion("server_type is not null");
            return (Criteria) this;
        }

        public Criteria andServerTypeEqualTo(Integer value) {
            addCriterion("server_type =", value, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeNotEqualTo(Integer value) {
            addCriterion("server_type <>", value, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeGreaterThan(Integer value) {
            addCriterion("server_type >", value, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("server_type >=", value, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeLessThan(Integer value) {
            addCriterion("server_type <", value, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeLessThanOrEqualTo(Integer value) {
            addCriterion("server_type <=", value, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeIn(List<Integer> values) {
            addCriterion("server_type in", values, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeNotIn(List<Integer> values) {
            addCriterion("server_type not in", values, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeBetween(Integer value1, Integer value2) {
            addCriterion("server_type between", value1, value2, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("server_type not between", value1, value2, "serverType");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteIsNull() {
            addCriterion("server_time_note is null");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteIsNotNull() {
            addCriterion("server_time_note is not null");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteEqualTo(String value) {
            addCriterion("server_time_note =", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteNotEqualTo(String value) {
            addCriterion("server_time_note <>", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteGreaterThan(String value) {
            addCriterion("server_time_note >", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteGreaterThanOrEqualTo(String value) {
            addCriterion("server_time_note >=", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteLessThan(String value) {
            addCriterion("server_time_note <", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteLessThanOrEqualTo(String value) {
            addCriterion("server_time_note <=", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteLike(String value) {
            addCriterion("server_time_note like", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteNotLike(String value) {
            addCriterion("server_time_note not like", value, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteIn(List<String> values) {
            addCriterion("server_time_note in", values, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteNotIn(List<String> values) {
            addCriterion("server_time_note not in", values, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteBetween(String value1, String value2) {
            addCriterion("server_time_note between", value1, value2, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andServerTimeNoteNotBetween(String value1, String value2) {
            addCriterion("server_time_note not between", value1, value2, "serverTimeNote");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNull() {
            addCriterion("keywords is null");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNotNull() {
            addCriterion("keywords is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordsEqualTo(String value) {
            addCriterion("keywords =", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotEqualTo(String value) {
            addCriterion("keywords <>", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThan(String value) {
            addCriterion("keywords >", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThanOrEqualTo(String value) {
            addCriterion("keywords >=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThan(String value) {
            addCriterion("keywords <", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThanOrEqualTo(String value) {
            addCriterion("keywords <=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLike(String value) {
            addCriterion("keywords like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotLike(String value) {
            addCriterion("keywords not like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsIn(List<String> values) {
            addCriterion("keywords in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotIn(List<String> values) {
            addCriterion("keywords not in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsBetween(String value1, String value2) {
            addCriterion("keywords between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotBetween(String value1, String value2) {
            addCriterion("keywords not between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefIsNull() {
            addCriterion("goods_brief is null");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefIsNotNull() {
            addCriterion("goods_brief is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefEqualTo(String value) {
            addCriterion("goods_brief =", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefNotEqualTo(String value) {
            addCriterion("goods_brief <>", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefGreaterThan(String value) {
            addCriterion("goods_brief >", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefGreaterThanOrEqualTo(String value) {
            addCriterion("goods_brief >=", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefLessThan(String value) {
            addCriterion("goods_brief <", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefLessThanOrEqualTo(String value) {
            addCriterion("goods_brief <=", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefLike(String value) {
            addCriterion("goods_brief like", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefNotLike(String value) {
            addCriterion("goods_brief not like", value, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefIn(List<String> values) {
            addCriterion("goods_brief in", values, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefNotIn(List<String> values) {
            addCriterion("goods_brief not in", values, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefBetween(String value1, String value2) {
            addCriterion("goods_brief between", value1, value2, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsBriefNotBetween(String value1, String value2) {
            addCriterion("goods_brief not between", value1, value2, "goodsBrief");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdIsNull() {
            addCriterion("goods_article_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdIsNotNull() {
            addCriterion("goods_article_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdEqualTo(Long value) {
            addCriterion("goods_article_id =", value, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdNotEqualTo(Long value) {
            addCriterion("goods_article_id <>", value, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdGreaterThan(Long value) {
            addCriterion("goods_article_id >", value, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("goods_article_id >=", value, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdLessThan(Long value) {
            addCriterion("goods_article_id <", value, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdLessThanOrEqualTo(Long value) {
            addCriterion("goods_article_id <=", value, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdIn(List<Long> values) {
            addCriterion("goods_article_id in", values, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdNotIn(List<Long> values) {
            addCriterion("goods_article_id not in", values, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdBetween(Long value1, Long value2) {
            addCriterion("goods_article_id between", value1, value2, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andGoodsArticleIdNotBetween(Long value1, Long value2) {
            addCriterion("goods_article_id not between", value1, value2, "goodsArticleId");
            return (Criteria) this;
        }

        public Criteria andIsSaleIsNull() {
            addCriterion("is_sale is null");
            return (Criteria) this;
        }

        public Criteria andIsSaleIsNotNull() {
            addCriterion("is_sale is not null");
            return (Criteria) this;
        }

        public Criteria andIsSaleEqualTo(Integer value) {
            addCriterion("is_sale =", value, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleNotEqualTo(Integer value) {
            addCriterion("is_sale <>", value, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleGreaterThan(Integer value) {
            addCriterion("is_sale >", value, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_sale >=", value, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleLessThan(Integer value) {
            addCriterion("is_sale <", value, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleLessThanOrEqualTo(Integer value) {
            addCriterion("is_sale <=", value, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleIn(List<Integer> values) {
            addCriterion("is_sale in", values, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleNotIn(List<Integer> values) {
            addCriterion("is_sale not in", values, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleBetween(Integer value1, Integer value2) {
            addCriterion("is_sale between", value1, value2, "isSale");
            return (Criteria) this;
        }

        public Criteria andIsSaleNotBetween(Integer value1, Integer value2) {
            addCriterion("is_sale not between", value1, value2, "isSale");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(Integer value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(Integer value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(Integer value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(Integer value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<Integer> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<Integer> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(Integer value1, Integer value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsBestIsNull() {
            addCriterion("is_best is null");
            return (Criteria) this;
        }

        public Criteria andIsBestIsNotNull() {
            addCriterion("is_best is not null");
            return (Criteria) this;
        }

        public Criteria andIsBestEqualTo(Integer value) {
            addCriterion("is_best =", value, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestNotEqualTo(Integer value) {
            addCriterion("is_best <>", value, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestGreaterThan(Integer value) {
            addCriterion("is_best >", value, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_best >=", value, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestLessThan(Integer value) {
            addCriterion("is_best <", value, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestLessThanOrEqualTo(Integer value) {
            addCriterion("is_best <=", value, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestIn(List<Integer> values) {
            addCriterion("is_best in", values, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestNotIn(List<Integer> values) {
            addCriterion("is_best not in", values, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestBetween(Integer value1, Integer value2) {
            addCriterion("is_best between", value1, value2, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsBestNotBetween(Integer value1, Integer value2) {
            addCriterion("is_best not between", value1, value2, "isBest");
            return (Criteria) this;
        }

        public Criteria andIsNewIsNull() {
            addCriterion("is_new is null");
            return (Criteria) this;
        }

        public Criteria andIsNewIsNotNull() {
            addCriterion("is_new is not null");
            return (Criteria) this;
        }

        public Criteria andIsNewEqualTo(Integer value) {
            addCriterion("is_new =", value, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewNotEqualTo(Integer value) {
            addCriterion("is_new <>", value, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewGreaterThan(Integer value) {
            addCriterion("is_new >", value, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_new >=", value, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewLessThan(Integer value) {
            addCriterion("is_new <", value, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewLessThanOrEqualTo(Integer value) {
            addCriterion("is_new <=", value, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewIn(List<Integer> values) {
            addCriterion("is_new in", values, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewNotIn(List<Integer> values) {
            addCriterion("is_new not in", values, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewBetween(Integer value1, Integer value2) {
            addCriterion("is_new between", value1, value2, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsNewNotBetween(Integer value1, Integer value2) {
            addCriterion("is_new not between", value1, value2, "isNew");
            return (Criteria) this;
        }

        public Criteria andIsHotIsNull() {
            addCriterion("is_hot is null");
            return (Criteria) this;
        }

        public Criteria andIsHotIsNotNull() {
            addCriterion("is_hot is not null");
            return (Criteria) this;
        }

        public Criteria andIsHotEqualTo(Integer value) {
            addCriterion("is_hot =", value, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotNotEqualTo(Integer value) {
            addCriterion("is_hot <>", value, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotGreaterThan(Integer value) {
            addCriterion("is_hot >", value, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_hot >=", value, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotLessThan(Integer value) {
            addCriterion("is_hot <", value, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotLessThanOrEqualTo(Integer value) {
            addCriterion("is_hot <=", value, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotIn(List<Integer> values) {
            addCriterion("is_hot in", values, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotNotIn(List<Integer> values) {
            addCriterion("is_hot not in", values, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotBetween(Integer value1, Integer value2) {
            addCriterion("is_hot between", value1, value2, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsHotNotBetween(Integer value1, Integer value2) {
            addCriterion("is_hot not between", value1, value2, "isHot");
            return (Criteria) this;
        }

        public Criteria andIsPromoteIsNull() {
            addCriterion("is_promote is null");
            return (Criteria) this;
        }

        public Criteria andIsPromoteIsNotNull() {
            addCriterion("is_promote is not null");
            return (Criteria) this;
        }

        public Criteria andIsPromoteEqualTo(Integer value) {
            addCriterion("is_promote =", value, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteNotEqualTo(Integer value) {
            addCriterion("is_promote <>", value, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteGreaterThan(Integer value) {
            addCriterion("is_promote >", value, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_promote >=", value, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteLessThan(Integer value) {
            addCriterion("is_promote <", value, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteLessThanOrEqualTo(Integer value) {
            addCriterion("is_promote <=", value, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteIn(List<Integer> values) {
            addCriterion("is_promote in", values, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteNotIn(List<Integer> values) {
            addCriterion("is_promote not in", values, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteBetween(Integer value1, Integer value2) {
            addCriterion("is_promote between", value1, value2, "isPromote");
            return (Criteria) this;
        }

        public Criteria andIsPromoteNotBetween(Integer value1, Integer value2) {
            addCriterion("is_promote not between", value1, value2, "isPromote");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andSellerNoteIsNull() {
            addCriterion("seller_note is null");
            return (Criteria) this;
        }

        public Criteria andSellerNoteIsNotNull() {
            addCriterion("seller_note is not null");
            return (Criteria) this;
        }

        public Criteria andSellerNoteEqualTo(String value) {
            addCriterion("seller_note =", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteNotEqualTo(String value) {
            addCriterion("seller_note <>", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteGreaterThan(String value) {
            addCriterion("seller_note >", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteGreaterThanOrEqualTo(String value) {
            addCriterion("seller_note >=", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteLessThan(String value) {
            addCriterion("seller_note <", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteLessThanOrEqualTo(String value) {
            addCriterion("seller_note <=", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteLike(String value) {
            addCriterion("seller_note like", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteNotLike(String value) {
            addCriterion("seller_note not like", value, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteIn(List<String> values) {
            addCriterion("seller_note in", values, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteNotIn(List<String> values) {
            addCriterion("seller_note not in", values, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteBetween(String value1, String value2) {
            addCriterion("seller_note between", value1, value2, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andSellerNoteNotBetween(String value1, String value2) {
            addCriterion("seller_note not between", value1, value2, "sellerNote");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusIsNull() {
            addCriterion("goods_status is null");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusIsNotNull() {
            addCriterion("goods_status is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusEqualTo(Integer value) {
            addCriterion("goods_status =", value, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusNotEqualTo(Integer value) {
            addCriterion("goods_status <>", value, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusGreaterThan(Integer value) {
            addCriterion("goods_status >", value, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("goods_status >=", value, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusLessThan(Integer value) {
            addCriterion("goods_status <", value, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusLessThanOrEqualTo(Integer value) {
            addCriterion("goods_status <=", value, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusIn(List<Integer> values) {
            addCriterion("goods_status in", values, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusNotIn(List<Integer> values) {
            addCriterion("goods_status not in", values, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusBetween(Integer value1, Integer value2) {
            addCriterion("goods_status between", value1, value2, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("goods_status not between", value1, value2, "goodsStatus");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidIsNull() {
            addCriterion("goods_img_pkgid is null");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidIsNotNull() {
            addCriterion("goods_img_pkgid is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidEqualTo(Long value) {
            addCriterion("goods_img_pkgid =", value, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidNotEqualTo(Long value) {
            addCriterion("goods_img_pkgid <>", value, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidGreaterThan(Long value) {
            addCriterion("goods_img_pkgid >", value, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidGreaterThanOrEqualTo(Long value) {
            addCriterion("goods_img_pkgid >=", value, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidLessThan(Long value) {
            addCriterion("goods_img_pkgid <", value, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidLessThanOrEqualTo(Long value) {
            addCriterion("goods_img_pkgid <=", value, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidIn(List<Long> values) {
            addCriterion("goods_img_pkgid in", values, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidNotIn(List<Long> values) {
            addCriterion("goods_img_pkgid not in", values, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidBetween(Long value1, Long value2) {
            addCriterion("goods_img_pkgid between", value1, value2, "goodsImgPkgid");
            return (Criteria) this;
        }

        public Criteria andGoodsImgPkgidNotBetween(Long value1, Long value2) {
            addCriterion("goods_img_pkgid not between", value1, value2, "goodsImgPkgid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}