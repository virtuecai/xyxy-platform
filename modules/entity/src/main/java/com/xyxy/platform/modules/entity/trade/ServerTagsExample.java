package com.xyxy.platform.modules.entity.trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServerTagsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ServerTagsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andServerTagIdIsNull() {
            addCriterion("server_tag_id is null");
            return (Criteria) this;
        }

        public Criteria andServerTagIdIsNotNull() {
            addCriterion("server_tag_id is not null");
            return (Criteria) this;
        }

        public Criteria andServerTagIdEqualTo(Long value) {
            addCriterion("server_tag_id =", value, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdNotEqualTo(Long value) {
            addCriterion("server_tag_id <>", value, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdGreaterThan(Long value) {
            addCriterion("server_tag_id >", value, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdGreaterThanOrEqualTo(Long value) {
            addCriterion("server_tag_id >=", value, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdLessThan(Long value) {
            addCriterion("server_tag_id <", value, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdLessThanOrEqualTo(Long value) {
            addCriterion("server_tag_id <=", value, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdIn(List<Long> values) {
            addCriterion("server_tag_id in", values, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdNotIn(List<Long> values) {
            addCriterion("server_tag_id not in", values, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdBetween(Long value1, Long value2) {
            addCriterion("server_tag_id between", value1, value2, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andServerTagIdNotBetween(Long value1, Long value2) {
            addCriterion("server_tag_id not between", value1, value2, "serverTagId");
            return (Criteria) this;
        }

        public Criteria andTagNameIsNull() {
            addCriterion("tag_name is null");
            return (Criteria) this;
        }

        public Criteria andTagNameIsNotNull() {
            addCriterion("tag_name is not null");
            return (Criteria) this;
        }

        public Criteria andTagNameEqualTo(String value) {
            addCriterion("tag_name =", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameNotEqualTo(String value) {
            addCriterion("tag_name <>", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameGreaterThan(String value) {
            addCriterion("tag_name >", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameGreaterThanOrEqualTo(String value) {
            addCriterion("tag_name >=", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameLessThan(String value) {
            addCriterion("tag_name <", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameLessThanOrEqualTo(String value) {
            addCriterion("tag_name <=", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameLike(String value) {
            addCriterion("tag_name like", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameNotLike(String value) {
            addCriterion("tag_name not like", value, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameIn(List<String> values) {
            addCriterion("tag_name in", values, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameNotIn(List<String> values) {
            addCriterion("tag_name not in", values, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameBetween(String value1, String value2) {
            addCriterion("tag_name between", value1, value2, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagNameNotBetween(String value1, String value2) {
            addCriterion("tag_name not between", value1, value2, "tagName");
            return (Criteria) this;
        }

        public Criteria andTagPinyinIsNull() {
            addCriterion("tag_pinyin is null");
            return (Criteria) this;
        }

        public Criteria andTagPinyinIsNotNull() {
            addCriterion("tag_pinyin is not null");
            return (Criteria) this;
        }

        public Criteria andTagPinyinEqualTo(String value) {
            addCriterion("tag_pinyin =", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinNotEqualTo(String value) {
            addCriterion("tag_pinyin <>", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinGreaterThan(String value) {
            addCriterion("tag_pinyin >", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinGreaterThanOrEqualTo(String value) {
            addCriterion("tag_pinyin >=", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinLessThan(String value) {
            addCriterion("tag_pinyin <", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinLessThanOrEqualTo(String value) {
            addCriterion("tag_pinyin <=", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinLike(String value) {
            addCriterion("tag_pinyin like", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinNotLike(String value) {
            addCriterion("tag_pinyin not like", value, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinIn(List<String> values) {
            addCriterion("tag_pinyin in", values, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinNotIn(List<String> values) {
            addCriterion("tag_pinyin not in", values, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinBetween(String value1, String value2) {
            addCriterion("tag_pinyin between", value1, value2, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagPinyinNotBetween(String value1, String value2) {
            addCriterion("tag_pinyin not between", value1, value2, "tagPinyin");
            return (Criteria) this;
        }

        public Criteria andTagOrderIsNull() {
            addCriterion("tag_order is null");
            return (Criteria) this;
        }

        public Criteria andTagOrderIsNotNull() {
            addCriterion("tag_order is not null");
            return (Criteria) this;
        }

        public Criteria andTagOrderEqualTo(Integer value) {
            addCriterion("tag_order =", value, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderNotEqualTo(Integer value) {
            addCriterion("tag_order <>", value, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderGreaterThan(Integer value) {
            addCriterion("tag_order >", value, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("tag_order >=", value, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderLessThan(Integer value) {
            addCriterion("tag_order <", value, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderLessThanOrEqualTo(Integer value) {
            addCriterion("tag_order <=", value, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderIn(List<Integer> values) {
            addCriterion("tag_order in", values, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderNotIn(List<Integer> values) {
            addCriterion("tag_order not in", values, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderBetween(Integer value1, Integer value2) {
            addCriterion("tag_order between", value1, value2, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andTagOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("tag_order not between", value1, value2, "tagOrder");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}