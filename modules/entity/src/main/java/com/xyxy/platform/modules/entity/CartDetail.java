/**
 *
 * com.xyxy.platform.modules.entity
 * CartDetail.java
 *
 * 2016-2016年1月7日-上午9:52:39
 */
package com.xyxy.platform.modules.entity;

import java.math.BigDecimal;

/**
 *
 * CartDetail
 *
 * xiangzhipeng
 * xiangzhipeng
 * 2016年1月7日 上午9:52:39
 *
 * @version 1.0.0
 *
 */
public class CartDetail {
	private String username;

	private long id;

	private String img;

	private String goodInfo;

	private BigDecimal price;

	private int num;

	private BigDecimal totalPrice;

	private String goodsContent;

	private String serviceStartTime;

	private String serviceEndTime;

	private String serviceTime;




	/**
	 * id
	 *
	 * @return  the id
	 * @since   1.0.0
	 */

	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * username
	 *
	 * @return  the username
	 * @since   1.0.0
	 */

	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * img
	 *
	 * @return  the img
	 * @since   1.0.0
	 */

	public String getImg() {
		return img;
	}

	/**
	 * @param img the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * goodInfo
	 *
	 * @return  the goodInfo
	 * @since   1.0.0
	 */

	public String getGoodInfo() {
		return goodInfo;
	}

	/**
	 * @param goodInfo the goodInfo to set
	 */
	public void setGoodInfo(String goodInfo) {
		this.goodInfo = goodInfo;
	}

	/**
	 * price
	 *
	 * @return  the price
	 * @since   1.0.0
	 */

	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param bigDecimal the price to set
	 */
	public void setPrice(BigDecimal bigDecimal) {
		this.price = bigDecimal;
	}

	/**
	 * num
	 *
	 * @return  the num
	 * @since   1.0.0
	 */

	public int getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(int num) {
		this.num = num;
	}

	/**
	 * totalPrice
	 *
	 * @return  the totalPrice
	 * @since   1.0.0
	 */

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * goodsContent
	 *
	 * @return  the goodsContent
	 * @since   1.0.0
	 */

	public String getGoodsContent() {
		return goodsContent;
	}

	/**
	 * @param goodsContent the goodsContent to set
	 */
	public void setGoodsContent(String goodsContent) {
		this.goodsContent = goodsContent;
	}

	/**
	 * serviceStartTime
	 *
	 * @return  the serviceStartTime
	 * @since   1.0.0
	 */

	public String getServiceStartTime() {
		return serviceStartTime;
	}

	/**
	 * @param serviceStartTime the serviceStartTime to set
	 */
	public void setServiceStartTime(String serviceStartTime) {
		this.serviceStartTime = serviceStartTime;
	}

	/**
	 * serviceEndTime
	 *
	 * @return  the serviceEndTime
	 * @since   1.0.0
	 */

	public String getServiceEndTime() {
		return serviceEndTime;
	}

	/**
	 * @param serviceEndTime the serviceEndTime to set
	 */
	public void setServiceEndTime(String serviceEndTime) {
		this.serviceEndTime = serviceEndTime;
	}

	/**
	 * serviceTime
	 *
	 * @return  the serviceTime
	 * @since   1.0.0
	 */

	public String getServiceTime() {
		return serviceTime;
	}

	/**
	 * @param serviceTime the serviceTime to set
	 */
	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}


}
