package com.xyxy.platform.modules.entity.sns;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeedBackExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FeedBackExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMsgIdIsNull() {
            addCriterion("msg_id is null");
            return (Criteria) this;
        }

        public Criteria andMsgIdIsNotNull() {
            addCriterion("msg_id is not null");
            return (Criteria) this;
        }

        public Criteria andMsgIdEqualTo(Long value) {
            addCriterion("msg_id =", value, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdNotEqualTo(Long value) {
            addCriterion("msg_id <>", value, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdGreaterThan(Long value) {
            addCriterion("msg_id >", value, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdGreaterThanOrEqualTo(Long value) {
            addCriterion("msg_id >=", value, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdLessThan(Long value) {
            addCriterion("msg_id <", value, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdLessThanOrEqualTo(Long value) {
            addCriterion("msg_id <=", value, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdIn(List<Long> values) {
            addCriterion("msg_id in", values, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdNotIn(List<Long> values) {
            addCriterion("msg_id not in", values, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdBetween(Long value1, Long value2) {
            addCriterion("msg_id between", value1, value2, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgIdNotBetween(Long value1, Long value2) {
            addCriterion("msg_id not between", value1, value2, "msgId");
            return (Criteria) this;
        }

        public Criteria andMsgPidIsNull() {
            addCriterion("msg_pid is null");
            return (Criteria) this;
        }

        public Criteria andMsgPidIsNotNull() {
            addCriterion("msg_pid is not null");
            return (Criteria) this;
        }

        public Criteria andMsgPidEqualTo(Long value) {
            addCriterion("msg_pid =", value, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidNotEqualTo(Long value) {
            addCriterion("msg_pid <>", value, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidGreaterThan(Long value) {
            addCriterion("msg_pid >", value, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidGreaterThanOrEqualTo(Long value) {
            addCriterion("msg_pid >=", value, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidLessThan(Long value) {
            addCriterion("msg_pid <", value, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidLessThanOrEqualTo(Long value) {
            addCriterion("msg_pid <=", value, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidIn(List<Long> values) {
            addCriterion("msg_pid in", values, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidNotIn(List<Long> values) {
            addCriterion("msg_pid not in", values, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidBetween(Long value1, Long value2) {
            addCriterion("msg_pid between", value1, value2, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgPidNotBetween(Long value1, Long value2) {
            addCriterion("msg_pid not between", value1, value2, "msgPid");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdIsNull() {
            addCriterion("msg_member_id is null");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdIsNotNull() {
            addCriterion("msg_member_id is not null");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdEqualTo(Long value) {
            addCriterion("msg_member_id =", value, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdNotEqualTo(Long value) {
            addCriterion("msg_member_id <>", value, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdGreaterThan(Long value) {
            addCriterion("msg_member_id >", value, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdGreaterThanOrEqualTo(Long value) {
            addCriterion("msg_member_id >=", value, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdLessThan(Long value) {
            addCriterion("msg_member_id <", value, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdLessThanOrEqualTo(Long value) {
            addCriterion("msg_member_id <=", value, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdIn(List<Long> values) {
            addCriterion("msg_member_id in", values, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdNotIn(List<Long> values) {
            addCriterion("msg_member_id not in", values, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdBetween(Long value1, Long value2) {
            addCriterion("msg_member_id between", value1, value2, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgMemberIdNotBetween(Long value1, Long value2) {
            addCriterion("msg_member_id not between", value1, value2, "msgMemberId");
            return (Criteria) this;
        }

        public Criteria andMsgIpIsNull() {
            addCriterion("msg_ip is null");
            return (Criteria) this;
        }

        public Criteria andMsgIpIsNotNull() {
            addCriterion("msg_ip is not null");
            return (Criteria) this;
        }

        public Criteria andMsgIpEqualTo(String value) {
            addCriterion("msg_ip =", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpNotEqualTo(String value) {
            addCriterion("msg_ip <>", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpGreaterThan(String value) {
            addCriterion("msg_ip >", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpGreaterThanOrEqualTo(String value) {
            addCriterion("msg_ip >=", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpLessThan(String value) {
            addCriterion("msg_ip <", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpLessThanOrEqualTo(String value) {
            addCriterion("msg_ip <=", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpLike(String value) {
            addCriterion("msg_ip like", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpNotLike(String value) {
            addCriterion("msg_ip not like", value, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpIn(List<String> values) {
            addCriterion("msg_ip in", values, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpNotIn(List<String> values) {
            addCriterion("msg_ip not in", values, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpBetween(String value1, String value2) {
            addCriterion("msg_ip between", value1, value2, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgIpNotBetween(String value1, String value2) {
            addCriterion("msg_ip not between", value1, value2, "msgIp");
            return (Criteria) this;
        }

        public Criteria andMsgTitleIsNull() {
            addCriterion("msg_title is null");
            return (Criteria) this;
        }

        public Criteria andMsgTitleIsNotNull() {
            addCriterion("msg_title is not null");
            return (Criteria) this;
        }

        public Criteria andMsgTitleEqualTo(String value) {
            addCriterion("msg_title =", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleNotEqualTo(String value) {
            addCriterion("msg_title <>", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleGreaterThan(String value) {
            addCriterion("msg_title >", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleGreaterThanOrEqualTo(String value) {
            addCriterion("msg_title >=", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleLessThan(String value) {
            addCriterion("msg_title <", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleLessThanOrEqualTo(String value) {
            addCriterion("msg_title <=", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleLike(String value) {
            addCriterion("msg_title like", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleNotLike(String value) {
            addCriterion("msg_title not like", value, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleIn(List<String> values) {
            addCriterion("msg_title in", values, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleNotIn(List<String> values) {
            addCriterion("msg_title not in", values, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleBetween(String value1, String value2) {
            addCriterion("msg_title between", value1, value2, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTitleNotBetween(String value1, String value2) {
            addCriterion("msg_title not between", value1, value2, "msgTitle");
            return (Criteria) this;
        }

        public Criteria andMsgTypeIsNull() {
            addCriterion("msg_type is null");
            return (Criteria) this;
        }

        public Criteria andMsgTypeIsNotNull() {
            addCriterion("msg_type is not null");
            return (Criteria) this;
        }

        public Criteria andMsgTypeEqualTo(Integer value) {
            addCriterion("msg_type =", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeNotEqualTo(Integer value) {
            addCriterion("msg_type <>", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeGreaterThan(Integer value) {
            addCriterion("msg_type >", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_type >=", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeLessThan(Integer value) {
            addCriterion("msg_type <", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeLessThanOrEqualTo(Integer value) {
            addCriterion("msg_type <=", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeIn(List<Integer> values) {
            addCriterion("msg_type in", values, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeNotIn(List<Integer> values) {
            addCriterion("msg_type not in", values, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeBetween(Integer value1, Integer value2) {
            addCriterion("msg_type between", value1, value2, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_type not between", value1, value2, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgStatusIsNull() {
            addCriterion("msg_status is null");
            return (Criteria) this;
        }

        public Criteria andMsgStatusIsNotNull() {
            addCriterion("msg_status is not null");
            return (Criteria) this;
        }

        public Criteria andMsgStatusEqualTo(Integer value) {
            addCriterion("msg_status =", value, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusNotEqualTo(Integer value) {
            addCriterion("msg_status <>", value, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusGreaterThan(Integer value) {
            addCriterion("msg_status >", value, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_status >=", value, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusLessThan(Integer value) {
            addCriterion("msg_status <", value, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusLessThanOrEqualTo(Integer value) {
            addCriterion("msg_status <=", value, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusIn(List<Integer> values) {
            addCriterion("msg_status in", values, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusNotIn(List<Integer> values) {
            addCriterion("msg_status not in", values, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusBetween(Integer value1, Integer value2) {
            addCriterion("msg_status between", value1, value2, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_status not between", value1, value2, "msgStatus");
            return (Criteria) this;
        }

        public Criteria andMsgTimeIsNull() {
            addCriterion("msg_time is null");
            return (Criteria) this;
        }

        public Criteria andMsgTimeIsNotNull() {
            addCriterion("msg_time is not null");
            return (Criteria) this;
        }

        public Criteria andMsgTimeEqualTo(Date value) {
            addCriterion("msg_time =", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeNotEqualTo(Date value) {
            addCriterion("msg_time <>", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeGreaterThan(Date value) {
            addCriterion("msg_time >", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("msg_time >=", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeLessThan(Date value) {
            addCriterion("msg_time <", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeLessThanOrEqualTo(Date value) {
            addCriterion("msg_time <=", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeIn(List<Date> values) {
            addCriterion("msg_time in", values, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeNotIn(List<Date> values) {
            addCriterion("msg_time not in", values, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeBetween(Date value1, Date value2) {
            addCriterion("msg_time between", value1, value2, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeNotBetween(Date value1, Date value2) {
            addCriterion("msg_time not between", value1, value2, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdIsNull() {
            addCriterion("msg_ckg_id is null");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdIsNotNull() {
            addCriterion("msg_ckg_id is not null");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdEqualTo(Long value) {
            addCriterion("msg_ckg_id =", value, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdNotEqualTo(Long value) {
            addCriterion("msg_ckg_id <>", value, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdGreaterThan(Long value) {
            addCriterion("msg_ckg_id >", value, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdGreaterThanOrEqualTo(Long value) {
            addCriterion("msg_ckg_id >=", value, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdLessThan(Long value) {
            addCriterion("msg_ckg_id <", value, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdLessThanOrEqualTo(Long value) {
            addCriterion("msg_ckg_id <=", value, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdIn(List<Long> values) {
            addCriterion("msg_ckg_id in", values, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdNotIn(List<Long> values) {
            addCriterion("msg_ckg_id not in", values, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdBetween(Long value1, Long value2) {
            addCriterion("msg_ckg_id between", value1, value2, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgCkgIdNotBetween(Long value1, Long value2) {
            addCriterion("msg_ckg_id not between", value1, value2, "msgCkgId");
            return (Criteria) this;
        }

        public Criteria andMsgOrderIsNull() {
            addCriterion("msg_order is null");
            return (Criteria) this;
        }

        public Criteria andMsgOrderIsNotNull() {
            addCriterion("msg_order is not null");
            return (Criteria) this;
        }

        public Criteria andMsgOrderEqualTo(Integer value) {
            addCriterion("msg_order =", value, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderNotEqualTo(Integer value) {
            addCriterion("msg_order <>", value, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderGreaterThan(Integer value) {
            addCriterion("msg_order >", value, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_order >=", value, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderLessThan(Integer value) {
            addCriterion("msg_order <", value, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderLessThanOrEqualTo(Integer value) {
            addCriterion("msg_order <=", value, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderIn(List<Integer> values) {
            addCriterion("msg_order in", values, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderNotIn(List<Integer> values) {
            addCriterion("msg_order not in", values, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderBetween(Integer value1, Integer value2) {
            addCriterion("msg_order between", value1, value2, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_order not between", value1, value2, "msgOrder");
            return (Criteria) this;
        }

        public Criteria andMsgAreaIsNull() {
            addCriterion("msg_area is null");
            return (Criteria) this;
        }

        public Criteria andMsgAreaIsNotNull() {
            addCriterion("msg_area is not null");
            return (Criteria) this;
        }

        public Criteria andMsgAreaEqualTo(Integer value) {
            addCriterion("msg_area =", value, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaNotEqualTo(Integer value) {
            addCriterion("msg_area <>", value, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaGreaterThan(Integer value) {
            addCriterion("msg_area >", value, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_area >=", value, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaLessThan(Integer value) {
            addCriterion("msg_area <", value, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaLessThanOrEqualTo(Integer value) {
            addCriterion("msg_area <=", value, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaIn(List<Integer> values) {
            addCriterion("msg_area in", values, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaNotIn(List<Integer> values) {
            addCriterion("msg_area not in", values, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaBetween(Integer value1, Integer value2) {
            addCriterion("msg_area between", value1, value2, "msgArea");
            return (Criteria) this;
        }

        public Criteria andMsgAreaNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_area not between", value1, value2, "msgArea");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(Integer value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(Integer value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(Integer value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(Integer value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<Integer> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<Integer> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(Integer value1, Integer value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}