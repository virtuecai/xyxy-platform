package com.xyxy.platform.modules.entity;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-23 16:02
 */
public enum IsDelete {

    /**
     * 否 0
     */
    NO,
    /**
     * 是 1
     */
    YES

}
