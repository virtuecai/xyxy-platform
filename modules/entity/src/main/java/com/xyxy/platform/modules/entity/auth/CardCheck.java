package com.xyxy.platform.modules.entity.auth;

import java.io.Serializable;
import java.util.Date;

public class CardCheck implements Serializable {
    private Long id;

    private Long memberId;

    private Integer cardType;

    private String realName;

    private String cardNo;

    private Integer cardGender;

    private Long imagePkgId;

    private Date createTime;

    private Date updateTime;

    private Integer checkStatus;

    private Long userId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo == null ? null : cardNo.trim();
    }

    public Integer getCardGender() {
        return cardGender;
    }

    public void setCardGender(Integer cardGender) {
        this.cardGender = cardGender;
    }

    public Long getImagePkgId() {
        return imagePkgId;
    }

    public void setImagePkgId(Long imagePkgId) {
        this.imagePkgId = imagePkgId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", memberId=").append(memberId);
        sb.append(", cardType=").append(cardType);
        sb.append(", realName=").append(realName);
        sb.append(", cardNo=").append(cardNo);
        sb.append(", cardGender=").append(cardGender);
        sb.append(", imagePkgId=").append(imagePkgId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", checkStatus=").append(checkStatus);
        sb.append(", userId=").append(userId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}