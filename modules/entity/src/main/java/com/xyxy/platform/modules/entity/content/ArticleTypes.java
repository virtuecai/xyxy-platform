package com.xyxy.platform.modules.entity.content;

/**
 * 简介: 文章分类
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-20 14:52
 */
public enum ArticleTypes {

    /**
     * 服务详情
     */
    GOODS_DETAIL(1),
    /**
     * 会员个人介绍
     */
    MEMBER_INTRODUCTION(2),
    /**
     * 服务案例
     */
    GOODS_CASE(2);

    private Integer typeId;

    private ArticleTypes(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getTypeId() {
        return typeId;
    }

}
