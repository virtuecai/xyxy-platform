package com.xyxy.platform.modules.entity.member;

import com.alibaba.fastjson.annotation.JSONField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Member implements Serializable {
    private Long id;

    private String userName;

    private String password;

    private String question;

    private String answer;

    private String email;

    private Integer gender;

    private BigDecimal userMoney;

    private BigDecimal frozenMoney;

    private Integer rankPoints;

    private Date lastTime;

    private String lastIp;

    private String alias;

    private Integer userStatus;

    private Date createTime;

    private Date updateTime;

    private String address;

    private Date birthday;

    private Long articleId;

    private String articleContent;

    private String mobile;

    private Long weixinCkgId;

    private String qq;

    private Integer cardType;

    private String realName;

    private String cardSn;

    private Integer cardGender;

    private Integer isDel;

    private Long imagePkgId;

    private static final long serialVersionUID = 1L;

    public String getArticleContent() {
        return articleContent;
    }
    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    @JSONField(serialize = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public BigDecimal getUserMoney() {
        return userMoney;
    }

    public void setUserMoney(BigDecimal userMoney) {
        this.userMoney = userMoney;
    }

    public BigDecimal getFrozenMoney() {
        return frozenMoney;
    }

    public void setFrozenMoney(BigDecimal frozenMoney) {
        this.frozenMoney = frozenMoney;
    }

    public Integer getRankPoints() {
        return rankPoints;
    }

    public void setRankPoints(Integer rankPoints) {
        this.rankPoints = rankPoints;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp == null ? null : lastIp.trim();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Long getWeixinCkgId() {
        return weixinCkgId;
    }

    public void setWeixinCkgId(Long weixinCkgId) {
        this.weixinCkgId = weixinCkgId;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getCardSn() {
        return cardSn;
    }

    public void setCardSn(String cardSn) {
        this.cardSn = cardSn == null ? null : cardSn.trim();
    }

    public Integer getCardGender() {
        return cardGender;
    }

    public void setCardGender(Integer cardGender) {
        this.cardGender = cardGender;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Long getImagePkgId() {
        return imagePkgId;
    }

    public void setImagePkgId(Long imagePkgId) {
        this.imagePkgId = imagePkgId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userName=").append(userName);
        sb.append(", password=").append(password);
        sb.append(", question=").append(question);
        sb.append(", answer=").append(answer);
        sb.append(", email=").append(email);
        sb.append(", gender=").append(gender);
        sb.append(", userMoney=").append(userMoney);
        sb.append(", frozenMoney=").append(frozenMoney);
        sb.append(", rankPoints=").append(rankPoints);
        sb.append(", lastTime=").append(lastTime);
        sb.append(", lastIp=").append(lastIp);
        sb.append(", alias=").append(alias);
        sb.append(", userStatus=").append(userStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", address=").append(address);
        sb.append(", birthday=").append(birthday);
        sb.append(", articleId=").append(articleId);
        sb.append(", mobile=").append(mobile);
        sb.append(", weixinCkgId=").append(weixinCkgId);
        sb.append(", qq=").append(qq);
        sb.append(", cardType=").append(cardType);
        sb.append(", realName=").append(realName);
        sb.append(", cardSn=").append(cardSn);
        sb.append(", cardGender=").append(cardGender);
        sb.append(", isDel=").append(isDel);
        sb.append(", imagePkgId=").append(imagePkgId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}