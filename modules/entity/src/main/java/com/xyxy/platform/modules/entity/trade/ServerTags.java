package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class ServerTags implements Serializable {
    private Long serverTagId;

    private String tagName;

    private String tagPinyin;

    private Integer tagOrder;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getServerTagId() {
        return serverTagId;
    }

    public void setServerTagId(Long serverTagId) {
        this.serverTagId = serverTagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName == null ? null : tagName.trim();
    }

    public String getTagPinyin() {
        return tagPinyin;
    }

    public void setTagPinyin(String tagPinyin) {
        this.tagPinyin = tagPinyin == null ? null : tagPinyin.trim();
    }

    public Integer getTagOrder() {
        return tagOrder;
    }

    public void setTagOrder(Integer tagOrder) {
        this.tagOrder = tagOrder;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serverTagId=").append(serverTagId);
        sb.append(", tagName=").append(tagName);
        sb.append(", tagPinyin=").append(tagPinyin);
        sb.append(", tagOrder=").append(tagOrder);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}