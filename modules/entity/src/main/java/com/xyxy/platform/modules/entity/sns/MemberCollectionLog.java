package com.xyxy.platform.modules.entity.sns;

import java.io.Serializable;
import java.util.Date;
/*会员商品收藏日志*/
public class MemberCollectionLog implements Serializable {
    private Long collectionLog; //商品日志ID

    private Long memberId;  //会员ID

    private Long goodsId;   //商品ID

    private Integer action; //操作类型

    private Date createTime;    //创建时间

    private static final long serialVersionUID = 1L;

    public Long getCollectionLog() {
        return collectionLog;
    }

    public void setCollectionLog(Long collectionLog) {
        this.collectionLog = collectionLog;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", collectionLog=").append(collectionLog);
        sb.append(", memberId=").append(memberId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", action=").append(action);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}