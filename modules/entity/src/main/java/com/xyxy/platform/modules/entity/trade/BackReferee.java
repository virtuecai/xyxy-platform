package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class BackReferee implements Serializable {
    private Long refereeId;

    private Integer refereeType;

    private Long orderId;

    private Long memberId;

    private Long refereeNoteType;

    private Long imgCkgId;

    private Date createTime;

    private Date updateTime;

    private Integer refereeStatus;

    private Integer isDel;

    private String refereeDesc;

    private static final long serialVersionUID = 1L;

    public Long getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(Long refereeId) {
        this.refereeId = refereeId;
    }

    public Integer getRefereeType() {
        return refereeType;
    }

    public void setRefereeType(Integer refereeType) {
        this.refereeType = refereeType;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getRefereeNoteType() {
        return refereeNoteType;
    }

    public void setRefereeNoteType(Long refereeNoteType) {
        this.refereeNoteType = refereeNoteType;
    }

    public Long getImgCkgId() {
        return imgCkgId;
    }

    public void setImgCkgId(Long imgCkgId) {
        this.imgCkgId = imgCkgId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getRefereeStatus() {
        return refereeStatus;
    }

    public void setRefereeStatus(Integer refereeStatus) {
        this.refereeStatus = refereeStatus;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public String getRefereeDesc() {
        return refereeDesc;
    }

    public void setRefereeDesc(String refereeDesc) {
        this.refereeDesc = refereeDesc == null ? null : refereeDesc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", refereeId=").append(refereeId);
        sb.append(", refereeType=").append(refereeType);
        sb.append(", orderId=").append(orderId);
        sb.append(", memberId=").append(memberId);
        sb.append(", refereeNoteType=").append(refereeNoteType);
        sb.append(", imgCkgId=").append(imgCkgId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", refereeStatus=").append(refereeStatus);
        sb.append(", isDel=").append(isDel);
        sb.append(", refereeDesc=").append(refereeDesc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}