package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class SaleLog implements Serializable {
    private Long saleId;

    private Integer saleType;

    private Long saleValueid;

    private Integer action;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getSaleId() {
        return saleId;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public Long getSaleValueid() {
        return saleValueid;
    }

    public void setSaleValueid(Long saleValueid) {
        this.saleValueid = saleValueid;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", saleId=").append(saleId);
        sb.append(", saleType=").append(saleType);
        sb.append(", saleValueid=").append(saleValueid);
        sb.append(", action=").append(action);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}