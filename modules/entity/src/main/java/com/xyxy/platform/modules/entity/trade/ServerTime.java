package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class ServerTime implements Serializable {
    private Long serverTimeId;

    private Long goodsId;

    private String time1;

    private String time2;

    private String time3;

    private String time4;

    private String time5;

    private String time6;

    private String time7;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getServerTimeId() {
        return serverTimeId;
    }

    public void setServerTimeId(Long serverTimeId) {
        this.serverTimeId = serverTimeId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1 == null ? null : time1.trim();
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2 == null ? null : time2.trim();
    }

    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {
        this.time3 = time3 == null ? null : time3.trim();
    }

    public String getTime4() {
        return time4;
    }

    public void setTime4(String time4) {
        this.time4 = time4 == null ? null : time4.trim();
    }

    public String getTime5() {
        return time5;
    }

    public void setTime5(String time5) {
        this.time5 = time5 == null ? null : time5.trim();
    }

    public String getTime6() {
        return time6;
    }

    public void setTime6(String time6) {
        this.time6 = time6 == null ? null : time6.trim();
    }

    public String getTime7() {
        return time7;
    }

    public void setTime7(String time7) {
        this.time7 = time7 == null ? null : time7.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serverTimeId=").append(serverTimeId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", time1=").append(time1);
        sb.append(", time2=").append(time2);
        sb.append(", time3=").append(time3);
        sb.append(", time4=").append(time4);
        sb.append(", time5=").append(time5);
        sb.append(", time6=").append(time6);
        sb.append(", time7=").append(time7);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}