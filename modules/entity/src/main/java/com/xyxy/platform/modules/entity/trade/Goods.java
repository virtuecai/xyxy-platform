package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Goods implements Serializable {
    private Long goodsId;

    private Long catId;

    private String goodsSn;

    private String goodsName;

    private Byte goodsCompanyId;

    private String goodsCompanyNote;

    private Integer clickCount;

    private Long memberId;

    private BigDecimal marketPrice;

    private BigDecimal shopPrice;

    private BigDecimal promotePrice;

    private Date promoteStartTime;

    private Date promoteEndTime;

    private Integer serverType;

    private String serverTimeNote;

    private String keywords;

    private String goodsBrief;

    private Long goodsArticleId;

    private Integer isSale;

    private Date createTime;

    private Integer isDel;

    private Integer isBest;

    private Integer isNew;

    private Integer isHot;

    private Integer isPromote;

    private Date updateTime;

    private String sellerNote;

    private Integer goodsStatus;

    private Long goodsImgPkgid;

    private static final long serialVersionUID = 1L;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn == null ? null : goodsSn.trim();
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    public Byte getGoodsCompanyId() {
        return goodsCompanyId;
    }

    public void setGoodsCompanyId(Byte goodsCompanyId) {
        this.goodsCompanyId = goodsCompanyId;
    }

    public String getGoodsCompanyNote() {
        return goodsCompanyNote;
    }

    public void setGoodsCompanyNote(String goodsCompanyNote) {
        this.goodsCompanyNote = goodsCompanyNote == null ? null : goodsCompanyNote.trim();
    }

    public Integer getClickCount() {
        return clickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getPromotePrice() {
        return promotePrice;
    }

    public void setPromotePrice(BigDecimal promotePrice) {
        this.promotePrice = promotePrice;
    }

    public Date getPromoteStartTime() {
        return promoteStartTime;
    }

    public void setPromoteStartTime(Date promoteStartTime) {
        this.promoteStartTime = promoteStartTime;
    }

    public Date getPromoteEndTime() {
        return promoteEndTime;
    }

    public void setPromoteEndTime(Date promoteEndTime) {
        this.promoteEndTime = promoteEndTime;
    }

    public Integer getServerType() {
        return serverType;
    }

    public void setServerType(Integer serverType) {
        this.serverType = serverType;
    }

    public String getServerTimeNote() {
        return serverTimeNote;
    }

    public void setServerTimeNote(String serverTimeNote) {
        this.serverTimeNote = serverTimeNote == null ? null : serverTimeNote.trim();
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public String getGoodsBrief() {
        return goodsBrief;
    }

    public void setGoodsBrief(String goodsBrief) {
        this.goodsBrief = goodsBrief == null ? null : goodsBrief.trim();
    }

    public Long getGoodsArticleId() {
        return goodsArticleId;
    }

    public void setGoodsArticleId(Long goodsArticleId) {
        this.goodsArticleId = goodsArticleId;
    }

    public Integer getIsSale() {
        return isSale;
    }

    public void setIsSale(Integer isSale) {
        this.isSale = isSale;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getIsBest() {
        return isBest;
    }

    public void setIsBest(Integer isBest) {
        this.isBest = isBest;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getIsPromote() {
        return isPromote;
    }

    public void setIsPromote(Integer isPromote) {
        this.isPromote = isPromote;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSellerNote() {
        return sellerNote;
    }

    public void setSellerNote(String sellerNote) {
        this.sellerNote = sellerNote == null ? null : sellerNote.trim();
    }

    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }

    public Long getGoodsImgPkgid() {
        return goodsImgPkgid;
    }

    public void setGoodsImgPkgid(Long goodsImgPkgid) {
        this.goodsImgPkgid = goodsImgPkgid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", goodsId=").append(goodsId);
        sb.append(", catId=").append(catId);
        sb.append(", goodsSn=").append(goodsSn);
        sb.append(", goodsName=").append(goodsName);
        sb.append(", goodsCompanyId=").append(goodsCompanyId);
        sb.append(", goodsCompanyNote=").append(goodsCompanyNote);
        sb.append(", clickCount=").append(clickCount);
        sb.append(", memberId=").append(memberId);
        sb.append(", marketPrice=").append(marketPrice);
        sb.append(", shopPrice=").append(shopPrice);
        sb.append(", promotePrice=").append(promotePrice);
        sb.append(", promoteStartTime=").append(promoteStartTime);
        sb.append(", promoteEndTime=").append(promoteEndTime);
        sb.append(", serverType=").append(serverType);
        sb.append(", serverTimeNote=").append(serverTimeNote);
        sb.append(", keywords=").append(keywords);
        sb.append(", goodsBrief=").append(goodsBrief);
        sb.append(", goodsArticleId=").append(goodsArticleId);
        sb.append(", isSale=").append(isSale);
        sb.append(", createTime=").append(createTime);
        sb.append(", isDel=").append(isDel);
        sb.append(", isBest=").append(isBest);
        sb.append(", isNew=").append(isNew);
        sb.append(", isHot=").append(isHot);
        sb.append(", isPromote=").append(isPromote);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", sellerNote=").append(sellerNote);
        sb.append(", goodsStatus=").append(goodsStatus);
        sb.append(", goodsImgPkgid=").append(goodsImgPkgid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}