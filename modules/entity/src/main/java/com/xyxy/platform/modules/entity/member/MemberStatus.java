package com.xyxy.platform.modules.entity.member;

/**
 * 简介: 会员状态
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-23 16:05
 */
public enum MemberStatus {

    /**
     * 锁定 0
     */
    LOCKED,
    /**
     * 正常 1
     */
    NORMAL

}
