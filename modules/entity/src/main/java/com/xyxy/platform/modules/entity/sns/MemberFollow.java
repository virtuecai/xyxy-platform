package com.xyxy.platform.modules.entity.sns;

import java.io.Serializable;
import java.util.Date;
/*会员关注表*/
public class MemberFollow implements Serializable {
    private Long followId;  //关注ID

    private Long memberId;  //会员ID

    private Long toMemberId;    //被关注会员ID

    private Date createTime;    //创建时间

    private static final long serialVersionUID = 1L;

    public Long getFollowId() {
        return followId;
    }

    public void setFollowId(Long followId) {
        this.followId = followId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getToMemberId() {
        return toMemberId;
    }

    public void setToMemberId(Long toMemberId) {
        this.toMemberId = toMemberId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", followId=").append(followId);
        sb.append(", memberId=").append(memberId);
        sb.append(", toMemberId=").append(toMemberId);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}