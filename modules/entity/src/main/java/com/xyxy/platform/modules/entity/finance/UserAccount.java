package com.xyxy.platform.modules.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UserAccount implements Serializable {
    public Integer getIsYue() {
        return isYue;
    }

    public void setIsYue(Integer isYue) {
        this.isYue = isYue;
    }
    //是否用余额支付
    private Integer isYue;



    private Long accountId;

    private Long adminUser;

    private String adminNote;

    private BigDecimal amount;

    private Date createTime;

    private Date updateTime;

    private String memberNote;

    private Integer action;

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    private Integer paymentId;

    private Integer isPaid;

    private Long memberId;

    private static final long serialVersionUID = 1L;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getAdminUser() {
        return adminUser;
    }

    public void setAdminUser(Long adminUser) {
        this.adminUser = adminUser;
    }

    public String getAdminNote() {
        return adminNote;
    }

    public void setAdminNote(String adminNote) {
        this.adminNote = adminNote == null ? null : adminNote.trim();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getMemberNote() {
        return memberNote;
    }

    public void setMemberNote(String memberNote) {
        this.memberNote = memberNote == null ? null : memberNote.trim();
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }


    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", accountId=").append(accountId);
        sb.append(", adminUser=").append(adminUser);
        sb.append(", adminNote=").append(adminNote);
        sb.append(", amount=").append(amount);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", memberNote=").append(memberNote);
        sb.append(", action=").append(action);
        sb.append(", paymentId=").append(paymentId);
        sb.append(", isPaid=").append(isPaid);
        sb.append(", memberId=").append(memberId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public boolean propertiesIsNull(){
        if(this.getAction()==null||this.getAmount()==null||this.getAdminUser()==null
                ||this.getIsPaid()==null||this.getMemberId()==null||this.getPaymentId()==null||this.getIsYue()==null){
                return false;
        }
        return true;
    }

}