package com.xyxy.platform.modules.entity.classifyjob;

import java.io.Serializable;

/**
 * ClassificationJob
 *
 * @author yangdianjun
 * @date 2015/11/27 0027
 */
public class ClassificationJob implements Serializable {

    private Long id;
    private String title;
    private Long parentId;
    private Integer level;
    private String  parentIds;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }


    public String getMakeSelfAsParentIds() {
        return getParentIds()  +"/" + getId()  ;
    }

    public boolean isRootNode(){
        return parentId == 0;
    }
}
