package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderInfo implements Serializable {
    private Long orderId;
    
    private Goods goods;
    
    private Long goodsId;

    private String orderSn;

    private Long memberId;

    private Integer orderStatus;

    private String orderStatusNote;

    private Integer payStatus;

    private Integer backStatus;

    private Long addressId;

    private String postscript;

    private Long payId;

    private String payName;

    private BigDecimal goodsAmount;

    private BigDecimal discount;

    private BigDecimal orderAmount;

    private BigDecimal surplus;

    private BigDecimal payAmount;

    private String referer;

    private Date createTime;

    private Date confirmTime;

    private Date payTime;

    private Date serverStartTime;

    private Date serverEndTime;

    private String toBuyer;

    private String payNote;

    private Integer isDel;

    private Long sallId;

    private static final long serialVersionUID = 1L;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusNote() {
        return orderStatusNote;
    }

    public void setOrderStatusNote(String orderStatusNote) {
        this.orderStatusNote = orderStatusNote == null ? null : orderStatusNote.trim();
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getBackStatus() {
        return backStatus;
    }

    public void setBackStatus(Integer backStatus) {
        this.backStatus = backStatus;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript == null ? null : postscript.trim();
    }

    public Long getPayId() {
        return payId;
    }

    public void setPayId(Long payId) {
        this.payId = payId;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName == null ? null : payName.trim();
    }

    public BigDecimal getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(BigDecimal goodsAmount) {
        this.goodsAmount = goodsAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getSurplus() {
        return surplus;
    }

    public void setSurplus(BigDecimal surplus) {
        this.surplus = surplus;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer == null ? null : referer.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getServerStartTime() {
        return serverStartTime;
    }

    public void setServerStartTime(Date serverStartTime) {
        this.serverStartTime = serverStartTime;
    }

    public Date getServerEndTime() {
        return serverEndTime;
    }

    public void setServerEndTime(Date serverEndTime) {
        this.serverEndTime = serverEndTime;
    }

    public String getToBuyer() {
        return toBuyer;
    }

    public void setToBuyer(String toBuyer) {
        this.toBuyer = toBuyer == null ? null : toBuyer.trim();
    }

    public String getPayNote() {
        return payNote;
    }

    public void setPayNote(String payNote) {
        this.payNote = payNote == null ? null : payNote.trim();
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Long getSallId() {
        return sallId;
    }

    public void setSallId(Long sallId) {
        this.sallId = sallId;
    }

	/**
	 * goods
	 *
	 * @return  the goods
	 * @since   1.0.0
	*/
	
	public Goods getGoods() {
		return goods;
	}

	/**
	 * @param goods the goods to set
	 */
	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	
	/**
	 * goodsId
	 *
	 * @return  the goodsId
	 * @since   1.0.0
	*/
	
	public Long getGoodsId() {
		return goodsId;
	}

	/**
	 * @param goodsId the goodsId to set
	 */
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderId=").append(orderId);
        sb.append(", orderSn=").append(orderSn);
        sb.append(", memberId=").append(memberId);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", orderStatusNote=").append(orderStatusNote);
        sb.append(", payStatus=").append(payStatus);
        sb.append(", backStatus=").append(backStatus);
        sb.append(", addressId=").append(addressId);
        sb.append(", postscript=").append(postscript);
        sb.append(", payId=").append(payId);
        sb.append(", payName=").append(payName);
        sb.append(", goodsAmount=").append(goodsAmount);
        sb.append(", discount=").append(discount);
        sb.append(", orderAmount=").append(orderAmount);
        sb.append(", surplus=").append(surplus);
        sb.append(", payAmount=").append(payAmount);
        sb.append(", referer=").append(referer);
        sb.append(", createTime=").append(createTime);
        sb.append(", confirmTime=").append(confirmTime);
        sb.append(", payTime=").append(payTime);
        sb.append(", serverStartTime=").append(serverStartTime);
        sb.append(", serverEndTime=").append(serverEndTime);
        sb.append(", toBuyer=").append(toBuyer);
        sb.append(", payNote=").append(payNote);
        sb.append(", isDel=").append(isDel);
        sb.append(", sallId=").append(sallId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}