package com.xyxy.platform.modules.entity.trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodsServerTagExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GoodsServerTagExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGoodsTagIdIsNull() {
            addCriterion("goods_tag_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdIsNotNull() {
            addCriterion("goods_tag_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdEqualTo(Long value) {
            addCriterion("goods_tag_id =", value, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdNotEqualTo(Long value) {
            addCriterion("goods_tag_id <>", value, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdGreaterThan(Long value) {
            addCriterion("goods_tag_id >", value, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdGreaterThanOrEqualTo(Long value) {
            addCriterion("goods_tag_id >=", value, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdLessThan(Long value) {
            addCriterion("goods_tag_id <", value, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdLessThanOrEqualTo(Long value) {
            addCriterion("goods_tag_id <=", value, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdIn(List<Long> values) {
            addCriterion("goods_tag_id in", values, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdNotIn(List<Long> values) {
            addCriterion("goods_tag_id not in", values, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdBetween(Long value1, Long value2) {
            addCriterion("goods_tag_id between", value1, value2, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsTagIdNotBetween(Long value1, Long value2) {
            addCriterion("goods_tag_id not between", value1, value2, "goodsTagId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNull() {
            addCriterion("goods_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNotNull() {
            addCriterion("goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdEqualTo(Long value) {
            addCriterion("goods_id =", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotEqualTo(Long value) {
            addCriterion("goods_id <>", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThan(Long value) {
            addCriterion("goods_id >", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThanOrEqualTo(Long value) {
            addCriterion("goods_id >=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThan(Long value) {
            addCriterion("goods_id <", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThanOrEqualTo(Long value) {
            addCriterion("goods_id <=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIn(List<Long> values) {
            addCriterion("goods_id in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotIn(List<Long> values) {
            addCriterion("goods_id not in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdBetween(Long value1, Long value2) {
            addCriterion("goods_id between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotBetween(Long value1, Long value2) {
            addCriterion("goods_id not between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andServerTagId1IsNull() {
            addCriterion("server_tag_id1 is null");
            return (Criteria) this;
        }

        public Criteria andServerTagId1IsNotNull() {
            addCriterion("server_tag_id1 is not null");
            return (Criteria) this;
        }

        public Criteria andServerTagId1EqualTo(Long value) {
            addCriterion("server_tag_id1 =", value, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1NotEqualTo(Long value) {
            addCriterion("server_tag_id1 <>", value, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1GreaterThan(Long value) {
            addCriterion("server_tag_id1 >", value, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1GreaterThanOrEqualTo(Long value) {
            addCriterion("server_tag_id1 >=", value, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1LessThan(Long value) {
            addCriterion("server_tag_id1 <", value, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1LessThanOrEqualTo(Long value) {
            addCriterion("server_tag_id1 <=", value, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1In(List<Long> values) {
            addCriterion("server_tag_id1 in", values, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1NotIn(List<Long> values) {
            addCriterion("server_tag_id1 not in", values, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1Between(Long value1, Long value2) {
            addCriterion("server_tag_id1 between", value1, value2, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andServerTagId1NotBetween(Long value1, Long value2) {
            addCriterion("server_tag_id1 not between", value1, value2, "serverTagId1");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}