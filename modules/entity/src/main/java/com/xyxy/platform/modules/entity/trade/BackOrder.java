package com.xyxy.platform.modules.entity.trade;

import java.io.Serializable;
import java.util.Date;

public class BackOrder implements Serializable {
    private Long backId;

    private String backSn;

    private Long orderId;

    private String orderSn;

    private Long memberId;

    private Date createTime;

    private Long userId;

    private String postscript;

    private Date updateTime;

    private Integer backStatus;

    private String actionNote;

    private Integer noteId;

    private String noteStr;

    private Integer backNoteId;

    private String backNote;

    private static final long serialVersionUID = 1L;

    public Long getBackId() {
        return backId;
    }

    public void setBackId(Long backId) {
        this.backId = backId;
    }

    public String getBackSn() {
        return backSn;
    }

    public void setBackSn(String backSn) {
        this.backSn = backSn == null ? null : backSn.trim();
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript == null ? null : postscript.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getBackStatus() {
        return backStatus;
    }

    public void setBackStatus(Integer backStatus) {
        this.backStatus = backStatus;
    }

    public String getActionNote() {
        return actionNote;
    }

    public void setActionNote(String actionNote) {
        this.actionNote = actionNote == null ? null : actionNote.trim();
    }

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    public String getNoteStr() {
        return noteStr;
    }

    public void setNoteStr(String noteStr) {
        this.noteStr = noteStr == null ? null : noteStr.trim();
    }

    public Integer getBackNoteId() {
        return backNoteId;
    }

    public void setBackNoteId(Integer backNoteId) {
        this.backNoteId = backNoteId;
    }

    public String getBackNote() {
        return backNote;
    }

    public void setBackNote(String backNote) {
        this.backNote = backNote == null ? null : backNote.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", backId=").append(backId);
        sb.append(", backSn=").append(backSn);
        sb.append(", orderId=").append(orderId);
        sb.append(", orderSn=").append(orderSn);
        sb.append(", memberId=").append(memberId);
        sb.append(", createTime=").append(createTime);
        sb.append(", userId=").append(userId);
        sb.append(", postscript=").append(postscript);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", backStatus=").append(backStatus);
        sb.append(", actionNote=").append(actionNote);
        sb.append(", noteId=").append(noteId);
        sb.append(", noteStr=").append(noteStr);
        sb.append(", backNoteId=").append(backNoteId);
        sb.append(", backNote=").append(backNote);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}