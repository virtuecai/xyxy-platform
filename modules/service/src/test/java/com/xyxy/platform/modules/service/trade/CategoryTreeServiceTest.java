package com.xyxy.platform.modules.service.trade;

import com.xyxy.platform.modules.service.trade.vo.CategoryTree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by czd on 16/1/22.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class CategoryTreeServiceTest {

    @Autowired
    CategoryTreeService categoryTreeService;

    @Test
    public void testGenerateCategoryTree() throws Exception {
        List<CategoryTree> list = categoryTreeService.getCategoryTree();
        System.out.println(list);
    }
}