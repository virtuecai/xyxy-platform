package com.xyxy.platform.modules.service.member.impl;

import java.util.HashMap;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.encrypt.des3.Des3;
import com.xyxy.platform.modules.service.member.MemberService;
import com.xyxy.platform.modules.service.member.vo.MemberDetail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-23 15:11
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class MemberServiceImplTest {

    @Autowired
    MemberService memberService;

    @Test
    public void testFindRecommend() throws Exception {
        PageInfo<MemberDetail> pageInfo = memberService.findRecommend("台球", 1, 10,new HashMap<String, Object>());
        System.out.println(pageInfo);
    }

    @Test
    public void testFindNewest() throws Exception {
        PageInfo<MemberDetail> pageInfo = memberService.findNewest("台球", 1, 10,new HashMap<String, Object>());
        System.out.println(pageInfo);
    }

    @Test
    public void encodeTest () {
        try {
            String text = Des3.decode("allen");
            System.out.println(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}