package com.xyxy.platform.modules.repository.mybatis.sns;

import com.xyxy.platform.modules.entity.sns.MemberCollectionExample;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-22 09:07
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class MemberCollectionMapperTest {

    @Autowired
    MemberCollectionMapper mapper;

    @Test
    public void queryTest() {
        mapper.selectGoodsPage(1L);
    }

}