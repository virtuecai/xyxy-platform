package com.xyxy.platform.modules.service.image;

import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.image.ImageTypes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * 简介: 图片服务测试类
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-05 17:08
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class ImageItemServiceTest {

    @Autowired
    ImageItemService imageItemService;

    @Test
    public void testFindImageItemList() throws Exception {
        List<ImageItem> imageItemList = imageItemService.findImageItemList(1l);
    }

    @Test
    public void testFindImageItemList1() throws Exception {
        imageItemService.findImageItemList(1l, ImageTypes.USER_ICON);
    }
}