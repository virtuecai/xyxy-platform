package com.xyxy.platform.modules.service.trade.impl;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.xyxy.platform.modules.service.trade.GoodsService;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-08 10:09
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class GoodsServiceImplTest {

    @Autowired
    GoodsService goodsService;

    @Test
    public void testSelectBestGoodsByMap() throws Exception {
        Map<String, Object> params = Maps.newHashMap();
        params.put("pageNum", 1);
        params.put("pageSize", 20);
        params.put("categoryId", 12);

//        PageInfo<GoodsDto> pageInfo = new PageInfo(goodsService.selectBestGoodsByMap(params));
//        System.out.println(pageInfo);
    }

    @Test
    public void testSelectHotGoodsByMap() throws Exception {
        Map<String, Object> params = Maps.newHashMap();
        params.put("pageNum", 1);
        params.put("pageSize", 2);
        //params.put("categoryId", 1);

//        PageInfo<GoodsDto> pageInfo = new PageInfo(goodsService.selectHotGoodsByMap(params));
//        System.out.println(pageInfo);
    }

    @Test
    public void testSelectNewGoodsByMap() throws Exception {
        Map<String, Object> params = Maps.newHashMap();
        params.put("pageNum", 1);
        params.put("pageSize", 2);
        //params.put("categoryId", 1);

//        PageInfo<GoodsDto> pageInfo = new PageInfo(goodsService.selectNewGoodsByMap(params));
//        System.out.println(pageInfo);
    }
}