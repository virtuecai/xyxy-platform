package com.xyxy.platform.modules.service.common.sms.impl;

import com.xyxy.platform.modules.core.cache.memcached.SpyMemcachedClient;
import com.xyxy.platform.modules.service.common.exception.SmsException;
import com.xyxy.platform.modules.service.common.sms.SmsService;
import com.xyxy.platform.modules.service.common.sms.vo.SmsTemplateType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-06 10:14
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class SmsServiceImplTest {

    @Autowired
    SmsService service;

    @Autowired
    private SpyMemcachedClient cache;

    @Test
    public void testGenerateContent() throws Exception {
        String content = service.generateContent(SmsTemplateType.REG_MSG, "7199", "123");
        service.sendSms("",content);
        System.out.println(content);
    }

    @Test
    public void testSendRegisterCode() {
        try {
            service.sendRegisterCode("18867366468", "127.0.0.1");
        } catch (SmsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cleanCache() {
        String mobileNumber[] = {"18867366468", "18867366467", "18867366466", "18867366465"};
        for(String mobile: mobileNumber) {
            cache.delete(mobileNumber + "_reg");
        }
        String ips[] = {"127.0.0.1"};
        for(String ip: ips) {
            cache.delete(ip + "_reg");
        }
        System.out.println("删除缓存成功!");
    }
}