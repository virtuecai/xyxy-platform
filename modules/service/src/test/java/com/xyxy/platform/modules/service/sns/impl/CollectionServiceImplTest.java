package com.xyxy.platform.modules.service.sns.impl;

import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author liushun
 * @version 1.0
 * @Date 2015-12-22
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class CollectionServiceImplTest {

    @Autowired
    private CollectionServiceImpl collectionService;

    @Test
    public void testGetCollectionPage() throws Exception {
        PageInfo page = collectionService.getCollectionPage(1L, 1, 1);
        System.out.println(page);
    }

}