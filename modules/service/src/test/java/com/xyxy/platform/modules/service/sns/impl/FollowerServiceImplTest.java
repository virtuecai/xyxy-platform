package com.xyxy.platform.modules.service.sns.impl;

import com.xyxy.platform.modules.repository.mybatis.sns.MemberCollectionMapper;
import com.xyxy.platform.modules.repository.mybatis.sns.MemberFollowMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author liushun
 * @version 1.0
 * @Date 2015-12-28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class FollowerServiceImplTest {

    @Autowired
    private MemberCollectionMapper mapper;

    @Test
    public void Test(){
       int i =  mapper.selectIsGoodsCollection(1L,1L);
        System.out.println(i);
    }

}