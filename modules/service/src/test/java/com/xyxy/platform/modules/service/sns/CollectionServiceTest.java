package com.xyxy.platform.modules.service.sns;

import com.github.pagehelper.PageInfo;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2015-12-22
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class CollectionServiceTest extends TestCase {

    @Autowired
    private CollectionService collectionService;
    @Test
    public void testGetCollectionList() throws Exception {
        PageInfo page = collectionService.getCollectionPage(1L, 1, 10);
    }
}