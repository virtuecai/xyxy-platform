package com.xyxy.platform.modules.service.trade.impl;

import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.service.trade.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-07 19:24
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class CategoryServiceImplTest {

    @Autowired
    CategoryService service;

    @Test
    public void testFindByParentId() throws Exception {
        List<Category> categoryList = service.findByParentId(0l);
        System.out.println(categoryList);
    }
}