package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.Goods;
import com.xyxy.platform.modules.entity.trade.GoodsExample;

import java.util.List;
import java.util.Map;

import com.xyxy.platform.modules.service.trade.vo.GoodsDto;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface GoodsMapper {
    int countByExample(GoodsExample example);

    int deleteByExample(GoodsExample example);

    int deleteByPrimaryKey(Long goodsId);

    int insert(Goods record);

    int insertSelective(Goods record);

    List<Goods> selectByExample(GoodsExample example);

    Goods selectByPrimaryKey(Long goodsId);

    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

    /**
     * 简介: 获取推荐服务列表
     *
     * @param paramsMap 参数 Map
     * @return
     * @author zhaowei
     * @Date 2016年1月7日 下午1:55:26
     * @version 1.0
     * @updator caizhengda
     */
    List<Goods> selectBestByMap(Map<String, Object> paramsMap);

    /**
     * 简介: 获取热门服务列表
     *
     * @param paramsMap 参数 Map
     * @return
     * @author zhaowei
     * @Date 2016年1月5日 下午7:03:48
     * @version 1.0
     * @updator caizhengda
     */
    List<Goods> selectHotByMap(Map<String, Object> paramsMap);

    /**
     * 简介: 获取最新服务列表
     *
     * @param paramsMap 参数 Map
     * @return
     * @author zhaowei
     * @Date 2016年1月5日 下午7:03:48
     * @version 1.0
     * @updator caizhengda
     */
    List<Goods> selectNewByMap(Map<String, Object> paramsMap);

    /**
     *
     * selectByGoodsId
     * @param id
     * @return
     *List<Goods>
     * @exception
     * @since  1.0.0
     */
    List<Goods> selectByGoodsId(long id);

    /**
     * 简介: 根据商品编号获取售出数
     * @author zhaowei
     * @Date 2016年1月8日 下午5:41:30
     * @version 1.0
     * @param goodsSn
     * @return
     */
    int selectSallGoodsCount(String goodsSn);

    /**
     * 获取发布的服务次数
     * @return
     */
    int countPublishGoods();

    /**
     * 根据每页条数获取总页数
     * @param pageSize
     * @return
     */
    Integer countGoodsPage();

    /**
     * liushun 根据会员ID只查询 {商品ID，商品名称，商品价格}
     * @param memberId 会员ID
     * @return
     */
    List<GoodsDto> selectMemberGooods(long memberId);
}