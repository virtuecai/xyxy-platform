package com.xyxy.platform.modules.repository.mybatis.sns;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.MemberCollection;
import com.xyxy.platform.modules.entity.sns.MemberCollectionExample;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberCollectionMapper {
    int countByExample(MemberCollectionExample example);

    int deleteByExample(MemberCollectionExample example);

    int deleteByPrimaryKey(Long collectionId);

    int insert(MemberCollection record);

    int insertSelective(MemberCollection record);

    List<MemberCollection> selectByExample(MemberCollectionExample example);

    MemberCollection selectByPrimaryKey(Long collectionId);

    int updateByExampleSelective(@Param("record") MemberCollection record, @Param("example") MemberCollectionExample example);

    int updateByExample(@Param("record") MemberCollection record, @Param("example") MemberCollectionExample example);

    int updateByPrimaryKeySelective(MemberCollection record);

    int updateByPrimaryKey(MemberCollection record);

    /*自定义查询接口*/

    /**
     * 根据会员ID查询收藏夹信息包含商品信息
     * @param memberId 会员ID
     * @return
     */
    List<HashMap> selectGoodsPage(Long memberId);

    /**
     * 判断商品是否已被收藏
     * @param memberId 会员ID
     * @param goodsId 商品ID
     * @return
     */
    int selectIsGoodsCollection(@Param("memberId") long memberId, @Param("goodsId") long goodsId);
}