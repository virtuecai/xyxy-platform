package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.BackReferee;
import com.xyxy.platform.modules.entity.trade.BackRefereeExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface BackRefereeMapper {
    int countByExample(BackRefereeExample example);

    int deleteByExample(BackRefereeExample example);

    int deleteByPrimaryKey(Long refereeId);

    int insert(BackReferee record);

    int insertSelective(BackReferee record);

    List<BackReferee> selectByExampleWithBLOBs(BackRefereeExample example);

    List<BackReferee> selectByExample(BackRefereeExample example);

    BackReferee selectByPrimaryKey(Long refereeId);

    int updateByExampleSelective(@Param("record") BackReferee record, @Param("example") BackRefereeExample example);

    int updateByExampleWithBLOBs(@Param("record") BackReferee record, @Param("example") BackRefereeExample example);

    int updateByExample(@Param("record") BackReferee record, @Param("example") BackRefereeExample example);

    int updateByPrimaryKeySelective(BackReferee record);

    int updateByPrimaryKeyWithBLOBs(BackReferee record);

    int updateByPrimaryKey(BackReferee record);
}