package com.xyxy.platform.modules.service.member;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.service.member.vo.MemberDetail;
import com.xyxy.platform.modules.service.member.vo.SimpleMember;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:41
 */
public interface MemberService {


    /**
     * 登录
     * @param userAccount 用户账号
     * @param password 密码
     * @return
     */
    Member login(String userAccount, String password) throws Exception;

    /**
     * 生成 token
     * @param member 会员信息
     * @return
     */
    String generateToken(Member member);

    /**
     * 根据 token 从缓存中获取 简单member信息
     * @param token 授权 token
     * @return
     */
    SimpleMember getMemberByToken(String token);

    /**
     * 注册新用户
     * @param member
     * @return
     */
    Member register(Member member);

    /**
     * 检查该手机号是否已注册
     * @param mobile
     * @return
     */
    boolean exist(String mobile);

    /**
     * 重设密码
     * @param id 用户ID
     * @param newPassword 新密码
     * @return
     */
    boolean resetPassword(Long id, String newPassword);

    /**
     * open api 退出, 从缓存中删除 token
     * @param token
     */
    void deleteToken(String token);

    /**
     * 查询推荐用户
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<MemberDetail> findRecommend(String keyword, Integer pageNum, Integer pageSize , Map<String, Object> parMap);

    /**
     * liushun 用For循环迭代
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param parMap
     * @return
     */
    PageInfo findForRecommend(String keyword, Integer pageNum, Integer pageSize,Map<String, Object> parMap);


    /**
     * 查找最新的会员, 根据update时间进行排序
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<MemberDetail> findNewest(String keyword, Integer pageNum, Integer pageSize,Map<String, Object> parMap);

    /**
     * liushun 用For循环迭代
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param parMap
     * @return
     */
    PageInfo findForNewest(String keyword, Integer pageNum, Integer pageSize,Map<String, Object> parMap);
    /**
     * 简介: 根据会员id获取会员信息
     * @Date 2016年1月5日 下午8:01:58
     * @version 1.0
     *
     * @param memberId
     * @return
     */
    MemberDetail findMemberDetailById(long memberId);


    /**
     * 采用Solrj索引框架检索数据信息
     * @param keyword 查询关键词
     * @param pageNum 开始页数
     * @param pageSize 每页大小
     * @param parMap 条件集合
     * @param type 类型 1、最新会员 2、推荐会员
     * @return
     */
    PageInfo findMemberInfoSolrj(String keyword, String pageNum, String pageSize , Map<String, Object> parMap, Integer type);

    /**
     * 获取已经推荐的热门星会员数。
     * @return
     */
    Integer countHotMember();

    /**
     * 获取已发布服务的星会员
     * @return
     */
    Integer countGoodsByMember();

    /**
     * 更新用户信息根据ID
     * @param member
     * @return
     */
    boolean updateMemebrInfoById(Member member);
    /**
     * 获取推荐星会员总页数
     * @param pageSize
     * @return
     */
    Integer countRecommedPage();

    /**
     * 根据会员ID 查询基本信息
     * @param memberId
     * @return
     */
    Member findById(Long memberId);
}
