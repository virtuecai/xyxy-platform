package com.xyxy.platform.modules.service.member.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.cache.memcached.SpyMemcachedClient;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.member.SpecificTag;
import com.xyxy.platform.modules.entity.member.SpecificTagExample;
import com.xyxy.platform.modules.repository.mybatis.member.SpecificTagMapper;
import com.xyxy.platform.modules.service.member.SpecificTagService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 简介: 会员个性标签服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:39
 */
@Service
@Transactional(readOnly = false)
public class SpecificTagServiceImpl implements SpecificTagService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SpecificTagMapper mapper;

    @Autowired
    private SpyMemcachedClient memcachedClient;

    private final String cachePrefix = "SpecificTag_";

    @Override
    public SpecificTag create(SpecificTag specificTag) {
        if (null == specificTag) {
            return null;
        }
        mapper.insertSelective(specificTag);
        memcachedClient.set(getCacheKey(specificTag.getId()), 0, specificTag);
        return specificTag;
    }

    @Override
    public List<SpecificTag> create(List<SpecificTag> tagList) {
        List<SpecificTag> result = Lists.newArrayList();
        if (Collections3.isEmpty(tagList)) {
            return Lists.newArrayList();
        }
        for (SpecificTag tag : tagList) {
            result.add(this.create(tag));
        }
        return result;
    }

    @Override
    public boolean update(SpecificTag specificTag) {
        int row = mapper.updateByPrimaryKeySelective(specificTag);
        if (row > 0) {
            memcachedClient.set(getCacheKey(specificTag.getId()), 0, specificTag);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void update(List<SpecificTag> tagList) {
        if (Collections3.isEmpty(tagList)) {
            return;
        }
        for (SpecificTag tag : tagList) {
            this.update(tag);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SpecificTag findById(Long id) {
        SpecificTag tag = memcachedClient.get(getCacheKey(id));
        if (null == tag) {
            tag = mapper.selectByPrimaryKey(id);
            memcachedClient.set(getCacheKey(id), 0, tag);
        }
        return tag;
    }

    @Override
    public SpecificTag findByName(String name) {
        SpecificTagExample sql = new SpecificTagExample();
        sql.or().andNameEqualTo(name);
        List<SpecificTag> tagList = mapper.selectByExample(sql);
        if (Collections3.isNotEmpty(tagList)) {
            return tagList.get(0);
        }
        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        memcachedClient.delete(getCacheKey(id));
        return mapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public List<SpecificTag> findByIdList(List<Long> specificIdList) {
        List<SpecificTag> result = Lists.newArrayList();
        if (null != specificIdList && Collections3.isNotEmpty(specificIdList)) {
            for (Long id : specificIdList) {
                SpecificTag tmp = this.findById(id);
                if (null == tmp) continue;
                result.add(tmp);
            }
        }
        return result;
    }

    @Override
    public List<SpecificTag> checkNotInDb(List<SpecificTag> tagList) {
        List<SpecificTag> result = Lists.newArrayList();
        if (null == tagList || Collections3.isEmpty(tagList)) {
            return Lists.newArrayList();
        }
        for (SpecificTag tag : tagList) {
            if (null != tag && null != tag.getId() && null != this.findById(tag.getId())) {
                result.add(tag);
                continue;
            }
            if (StringUtils.isNotEmpty(tag.getName())) {
                tag = this.findByName(tag.getName());
            }
            if(null == tag.getId()){
                tag = this.create(tag);
            }
            result.add(tag);
        }
        return result;
    }

    /**
     * 拼接缓存key
     *
     * @param id 标签ID 主键
     * @return
     */
    private String getCacheKey(Long id) {
        return cachePrefix + id;
    }
}
