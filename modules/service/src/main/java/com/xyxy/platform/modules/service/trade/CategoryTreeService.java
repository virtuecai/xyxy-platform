package com.xyxy.platform.modules.service.trade;

import com.xyxy.platform.modules.service.trade.vo.CategoryTree;

import java.util.List;

/**
 * 用于构建首页分类树对象以及制定目录生成json数据js文件.
 * Created by czd on 16/1/22.
 */
public interface CategoryTreeService {

    /**
     * 获得分类树信息
     * @return
     */
    List<CategoryTree> getCategoryTree();

    /**
     * 清理分类树 缓存
     */
    void clearCache();
}
