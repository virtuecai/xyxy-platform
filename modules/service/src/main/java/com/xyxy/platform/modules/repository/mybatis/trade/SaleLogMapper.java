package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.SaleLog;
import com.xyxy.platform.modules.entity.trade.SaleLogExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface SaleLogMapper {
    int countByExample(SaleLogExample example);

    int deleteByExample(SaleLogExample example);

    int deleteByPrimaryKey(Long saleId);

    int insert(SaleLog record);

    int insertSelective(SaleLog record);

    List<SaleLog> selectByExample(SaleLogExample example);

    SaleLog selectByPrimaryKey(Long saleId);

    int updateByExampleSelective(@Param("record") SaleLog record, @Param("example") SaleLogExample example);

    int updateByExample(@Param("record") SaleLog record, @Param("example") SaleLogExample example);

    int updateByPrimaryKeySelective(SaleLog record);

    int updateByPrimaryKey(SaleLog record);
}