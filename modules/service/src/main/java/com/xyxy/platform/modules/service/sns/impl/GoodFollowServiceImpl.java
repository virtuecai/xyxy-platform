package com.xyxy.platform.modules.service.sns.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyxy.platform.modules.entity.trade.GoodFollowExample;
import com.xyxy.platform.modules.repository.mybatis.trade.GoodFollowMapper;
import com.xyxy.platform.modules.service.sns.GoodFollowService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 服务关注服务实现类
 * @author Administrator
 */
@Service
@Transactional(readOnly = true)
public class GoodFollowServiceImpl implements GoodFollowService {

	@Autowired
	private GoodFollowMapper  goodFollowMapper;

	@Override
	public int countGoodFollowByGoodId(long goodId) {
		GoodFollowExample goodFoolowExample = new GoodFollowExample();
		goodFoolowExample.or().andGoodIdEqualTo(goodId);
		int count = goodFollowMapper.countByExample(goodFoolowExample);
		return count;
	}

}
