package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.entity.member.MemberExample;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xyxy.platform.modules.service.member.vo.MemberDetail;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberMapper {
    int countByExample(MemberExample example);

    int deleteByExample(MemberExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Member record);

    int insertSelective(Member record);

    List<Member> selectByExample(MemberExample example);

    Member selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Member record, @Param("example") MemberExample example);

    int updateByExample(@Param("record") Member record, @Param("example") MemberExample example);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);

    List<MemberDetail> findRecommend(Map<String, Object> parMap);

    List<Member> findForRecommend(Map<String, Object> parMap);

    List<MemberDetail> findNewest(Map<String, Object> parMap);

    List<Member> findForNewest(Map<String, Object> parMap);

    List<HashMap> findSpecificTags(@Param("tagId") long tagId, @Param("number") Integer number, @Param("isToParent") boolean isToParent);

    HashMap findSpecificTag(@Param("tagId") long tagId);

    int countRecommed();

    int countGoodsByMember();

    Integer countRecommedPage();

}