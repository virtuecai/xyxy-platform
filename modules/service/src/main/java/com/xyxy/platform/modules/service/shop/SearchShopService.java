package com.xyxy.platform.modules.service.shop;

import com.github.pagehelper.PageInfo;

import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-07
 */
public interface SearchShopService {

    /**
     * 根据关键词搜索星店列表
     * @param map{
     *
     * } 条件集合
     * @param keyword 关键词
     * @param pageNum 当前页数
     * @param pageSize 每页大小
     * @return
     */
    PageInfo searchShopList(Map map, String keyword, Integer pageNum, Integer pageSize);
}
