package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.UserTemplate;
import com.xyxy.platform.modules.entity.member.UserTemplateExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface UserTemplateMapper {
    int countByExample(UserTemplateExample example);

    int deleteByExample(UserTemplateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserTemplate record);

    int insertSelective(UserTemplate record);

    List<UserTemplate> selectByExample(UserTemplateExample example);

    UserTemplate selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserTemplate record, @Param("example") UserTemplateExample example);

    int updateByExample(@Param("record") UserTemplate record, @Param("example") UserTemplateExample example);

    int updateByPrimaryKeySelective(UserTemplate record);

    int updateByPrimaryKey(UserTemplate record);
}