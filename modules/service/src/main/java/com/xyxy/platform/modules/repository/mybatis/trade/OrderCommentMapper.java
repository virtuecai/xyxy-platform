package com.xyxy.platform.modules.repository.mybatis.trade;

import java.util.List;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import org.apache.ibatis.annotations.Param;

import com.xyxy.platform.modules.entity.trade.OrderComment;
import com.xyxy.platform.modules.entity.trade.OrderCommentExample;

@MyBatisRepository
public interface OrderCommentMapper {
    int countByExample(OrderCommentExample example);

    int deleteByExample(OrderCommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderComment record);

    int insertSelective(OrderComment record);

    List<OrderComment> selectByExampleWithBLOBs(OrderCommentExample example);

    List<OrderComment> selectByExample(OrderCommentExample example);

    OrderComment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderComment record, @Param("example") OrderCommentExample example);

    int updateByExampleWithBLOBs(@Param("record") OrderComment record, @Param("example") OrderCommentExample example);

    int updateByExample(@Param("record") OrderComment record, @Param("example") OrderCommentExample example);

    int updateByPrimaryKeySelective(OrderComment record);

    int updateByPrimaryKeyWithBLOBs(OrderComment record);

    int updateByPrimaryKey(OrderComment record);
}