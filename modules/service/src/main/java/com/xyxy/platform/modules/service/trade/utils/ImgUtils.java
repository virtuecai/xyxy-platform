package com.xyxy.platform.modules.service.trade.utils;

import org.apache.commons.lang3.StringUtils;

import com.xyxy.platform.modules.core.utils.PropertiesLoader;

/**
 * 临时图片工具类
 * @author zhaowei
 */
public class ImgUtils {
	private static PropertiesLoader loader = new PropertiesLoader("resources.properties");
	private static String tempImgPath;//临时图片目录
	private static String memberOrderCommentPath;//会员订单评论图片根目录   /${根目录}/${会员id}/图片目录
	
	/**
	 * 简介: 获取临时图片目录
	 * @author zhaowei
	 * @Date 2015年12月23日 下午2:28:23
	 * @version 1.0
	 *
	 * @return
	 */
	public static String getTempImgPath(){
		if(StringUtils.isEmpty(tempImgPath)){
			tempImgPath = loader.getProperty("temp.imgPath");
		}
		return tempImgPath;
	}
	
	/**
	 * 简介: 获取会员订单评价图片根目录
	 * @author zhaowei
	 * @Date 2015年12月23日 下午7:20:59
	 * @version 1.0
	 *
	 * @return
	 */
	public static String getMemberOrderCommentPath(){
		if(StringUtils.isEmpty(memberOrderCommentPath)){
			memberOrderCommentPath = loader.getProperty("member.order.comment.path");
		}
		return memberOrderCommentPath;
	}
}
