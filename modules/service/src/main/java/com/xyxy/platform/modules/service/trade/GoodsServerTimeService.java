package com.xyxy.platform.modules.service.trade;

import com.xyxy.platform.modules.entity.trade.GoodsServerTime;

import java.util.List;

/**
 * 简介: 服务时间段
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-14 09:53
 */
public interface GoodsServerTimeService {

    /**
     * 保存服务时间段
     * 先删除, 后添加
     * @param serverTimeList 服务时间段列表
     * @param goodsId 服务ID
     * @return
     */
    Integer save(List<GoodsServerTime> serverTimeList, Long goodsId);

    /**
     * 根据商品ID获得该商品的服务时间段列表
     * @param goodsId 服务ID
     * @return
     */
    List<GoodsServerTime> findByGoodsId(Long goodsId);

    /**
     * 根据服务ID 删除服务的服务时间段信息
     * @param goodsId 服务ID
     * @return
     */
    Integer delete(Long goodsId);

}
