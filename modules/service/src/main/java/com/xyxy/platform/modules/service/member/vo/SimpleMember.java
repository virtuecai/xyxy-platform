package com.xyxy.platform.modules.service.member.vo;

import java.io.Serializable;

/**
 * 简介: 简单member对象
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:39
 */
public class SimpleMember implements Serializable {

    private Long id;

    private String name;

    private String token;

    public SimpleMember() {
    }

    public SimpleMember(Long id, String name, String token) {
        this.id = id;
        this.name = name;
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
