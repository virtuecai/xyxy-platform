package com.xyxy.platform.modules.service.common.address.impl;

import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.repository.mybatis.common.SysAreaMapper;
import com.xyxy.platform.modules.service.common.address.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2015-12-30
 */
@Service
public class AddressServiceImpl implements AddressService{

    @Autowired
    private SysAreaMapper sysAreaMapper;

    /**
     * 获取全部省份
     * @return
     */
    @Override
    public List getListProvince() {
        return sysAreaMapper.selectAllProvince();
    }
    /**
     * 根据省份ID获取城市列表
     * @param provinceId 省份ID
     * @return
     */
    @Override
    public List getListCity(Integer provinceId) {
        return sysAreaMapper.selectCityList(provinceId);
    }
    /**
     * 根据城市ID获取城市列表
     * @param cityId 城市ID
     * @return
     */
    @Override
    public List getListArea(Integer cityId) {
        return sysAreaMapper.selectAreaList(cityId);
    }
	@Override
	public List getPathArea(Integer noteId ) {
		SysArea sysArea = sysAreaMapper.selectByPrimaryKey(noteId);
		if(sysArea == null){
			return null;
		}
		List<SysArea> sysAreaList  = new ArrayList<SysArea>();
		sysAreaList.add(sysArea);
		sysAreaList =  getPathArea(sysAreaList);
		Collections.reverse(sysAreaList);
		return sysAreaList;
	}

	private List<SysArea> getPathArea(List<SysArea> sysAreaList){
		SysArea sysArea = sysAreaList.get(sysAreaList.size() - 1);
		if(sysArea.getParentid() == 0){
			return sysAreaList;
		}
		sysArea = sysAreaMapper.selectByPrimaryKey(sysArea.getParentid());
		sysAreaList.add(sysArea);
		return  getPathArea(sysAreaList);
	}

	@Override
	public String getAddressStr(Integer addressId, int type, String str) {
		Map map = sysAreaMapper.findAddressStr(addressId);
		if(type==1)
			return map.get("province").toString();
		else if(type==2)
			return map.get("province")+str+map.get("city");
		else if(type==3)
			return map.get("province")+str+map.get("city")+str+map.get("area");
		else
			return map.get("province").toString();
	}

}
