package com.xyxy.platform.modules.service.common.sms.vo;

/**
 * 简介: 短信模版类型
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-06 10:04
 */
public enum SmsTemplateType {

    /**
     *  String REG_MSG=;
     //找回密码
     String ZH_MSG="";
     //修改绑定手机
     String XGSJ_MSG="";
     //绑定新手机
     String BDXSJ_MSG="";
     //个人信息认证
     String GRXX_MSG="";
     //个人信息未通过验证
     String GRXXE_MSG="";
     //下单成功
     String XDCG_MSG="";
     */

    /**
     * 注册短信模版
     */
    REG_MSG,
    ZH_MSG,
    XGSJ_MSG,
    BDXSJ_MSG,
    GRXX_MSG,
    GRXXE_MSG,
    XDCG_MSG

}
