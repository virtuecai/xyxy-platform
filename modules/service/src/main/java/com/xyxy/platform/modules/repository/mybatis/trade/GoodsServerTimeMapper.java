package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.GoodsServerTime;
import com.xyxy.platform.modules.entity.trade.GoodsServerTimeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface GoodsServerTimeMapper {
    int countByExample(GoodsServerTimeExample example);

    int deleteByExample(GoodsServerTimeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GoodsServerTime record);

    int insertSelective(GoodsServerTime record);

    List<GoodsServerTime> selectByExample(GoodsServerTimeExample example);

    GoodsServerTime selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GoodsServerTime record, @Param("example") GoodsServerTimeExample example);

    int updateByExample(@Param("record") GoodsServerTime record, @Param("example") GoodsServerTimeExample example);

    int updateByPrimaryKeySelective(GoodsServerTime record);

    int updateByPrimaryKey(GoodsServerTime record);
}