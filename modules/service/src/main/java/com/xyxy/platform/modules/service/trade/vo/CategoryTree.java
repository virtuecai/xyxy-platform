package com.xyxy.platform.modules.service.trade.vo;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * 用于首页分类树状显示
 * Created by czd on 16/1/22.
 */
public class CategoryTree implements Serializable {

    private Long id; //分类ID
    private String name; //分类名称
    private Long parentId; //分类父级ID
    private String iconClass; //图标样式, 用于页面显示icon

    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    private Long isDelete;

    private List<CategoryTree> childList = Lists.newArrayList(); //子集分类

    public CategoryTree() {
    }

    public CategoryTree(Long id, String name, Long parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    public List<CategoryTree> getChildList() {
        return childList;
    }

    public void setChildList(List<CategoryTree> childList) {
        this.childList = childList;
    }
}
