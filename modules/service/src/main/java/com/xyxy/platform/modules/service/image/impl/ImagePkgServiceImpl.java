package com.xyxy.platform.modules.service.image.impl;

import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.image.ImagePkg;
import com.xyxy.platform.modules.entity.image.ImagePkgExample;
import com.xyxy.platform.modules.repository.mybatis.image.ImagePkgMapper;
import com.xyxy.platform.modules.service.image.ImagePkgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 简介: 图片组
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-20 16:57
 */
@Service
public class ImagePkgServiceImpl implements ImagePkgService {

    @Autowired
    private ImagePkgMapper imagePkgMapper;

    @Override
    public ImagePkg findById(Long imagePkgId) {
        if (null == imagePkgId) return null;
        ImagePkgExample sql = new ImagePkgExample();
        sql.or().andIdEqualTo(imagePkgId);
        List<ImagePkg> imagePkgList = imagePkgMapper.selectByExample(sql);
        return Collections3.isNotEmpty(imagePkgList) ? Collections3.getFirst(imagePkgList) : null;
    }

    @Override
    public Integer insert(ImagePkg imagePkg) {
        return imagePkgMapper.insert(imagePkg);
    }
}
