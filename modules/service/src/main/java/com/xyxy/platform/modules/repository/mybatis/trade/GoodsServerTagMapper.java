package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.GoodsServerTag;
import com.xyxy.platform.modules.entity.trade.GoodsServerTagExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface GoodsServerTagMapper {
    int countByExample(GoodsServerTagExample example);

    int deleteByExample(GoodsServerTagExample example);

    int deleteByPrimaryKey(Long goodsTagId);

    int insert(GoodsServerTag record);

    int insertSelective(GoodsServerTag record);

    List<GoodsServerTag> selectByExample(GoodsServerTagExample example);

    GoodsServerTag selectByPrimaryKey(Long goodsTagId);

    int updateByExampleSelective(@Param("record") GoodsServerTag record, @Param("example") GoodsServerTagExample example);

    int updateByExample(@Param("record") GoodsServerTag record, @Param("example") GoodsServerTagExample example);

    int updateByPrimaryKeySelective(GoodsServerTag record);

    int updateByPrimaryKey(GoodsServerTag record);
    
}