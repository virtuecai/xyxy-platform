package com.xyxy.platform.modules.service.content;

import com.xyxy.platform.modules.entity.content.Article;

/**
 * ArticleService
 *
 * @author yangdianjun
 * @version 1.0
 * @date 2016/1/12 0012
 */
public interface ArticleService {

    /**
     * 插入一条文文章数据
     * @param article
     * @return
     */
    Article insertArticle(Article article);

    /**
     * 根据id获取文章内容
     * @param articleId
     * @return
     */
    Article selectById(Long articleId);

    /**
     * 更新文章内容
     * @param article
     * @return
     */
    Article update(Article article);



}
