package com.xyxy.platform.modules.service.trade.vo;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.entity.trade.GoodsComment;
import com.xyxy.platform.modules.entity.trade.ServerTags;

public class GoodsDto implements Serializable {
	
	private static final Long serialVersionUID = 7491137977251477559L;
	private String goodsName ;	//服务标题 
	private String content ;	//服务详情
	private String goodPic;		//服务封面
	private Float goodPrice;	//服务价格
	private String goodBrief;  //服务简单描述
	private String goodUnit;	//单位
	private String time;		//时间, 服务时间段, 枚举定义范围
	private String timeNote;	//时间描述
	private Integer type;			//服务方式类型
	private Long cateId;		//服务分类id
	private String tags;		//服务标签id串
	private Category category ;	//服务分类
	private String[] goodPics;	//服务图片数组
	private String memberName;	//会员名称
	private Long memberId;		//会员id
	private Long goodId;		//服务id
	private String goodSn;		//服务编号
	private Long pkgId;			//图片集id
	
	
	/**
	 * 关注数量
	 */
	private Integer followCount ;	 //关注数量
	private Integer sallCount; 		 //售出数量
	private Integer collectionCount; //收藏数
	private Integer evaluateCount; 	 //评价数
	
	/**
	 * 服务标签集合
	 */
	private List<ServerTags> serverTags; //服务标签集合
	
	/**
	 * 昵称
	 */
	private String nickName;	//销售会员昵称
	
	/**
	 * 头像地址
	 */
	private String imageUrl;   //销售会员头像地址
	
	/**
	 * 显示地址
	 */
	private List<SysArea>  address;	//地区对象
	
	private Integer gender ; //性别
	
	private boolean authentication; //是否认证
	
	private List<GoodsComment> commentList ;//评价列表
	
	
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Float getGoodPrice() {
		return goodPrice;
	}

	public String getGoodBrief() {
		return goodBrief;
	}

	public void setGoodBrief(String goodBrief) {
		this.goodBrief = goodBrief;
	}

	public void setGoodPrice(Float goodPrice) {
		this.goodPrice = goodPrice;
	}
	public String getGoodUnit() {
		return goodUnit;
	}
	public void setGoodUnit(String goodUnit) {
		this.goodUnit = goodUnit;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Long getCateId() {
		return cateId;
	}
	public void setCateId(Long cateId) {
		this.cateId = cateId;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getGoodPic() {
		return goodPic;
	}
	public void setGoodPic(String goodPic) {
		this.goodPic = goodPic;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	public String[] getGoodPics() {
		return goodPics;
	}
	public void setGoodPics(String[] goodPics) {
		this.goodPics = goodPics;
	}
	
	public String getTimeNote() {
		return timeNote;
	}
	public void setTimeNote(String timeNote) {
		this.timeNote = timeNote;
	}
	
	public Integer getFollowCount() {
		return followCount;
	}
	public void setFollowCount(Integer followCount) {
		this.followCount = followCount;
	}
	
	public List<ServerTags> getServerTags() {
		return serverTags;
	}
	public void setServerTags(List<ServerTags> serverTags) {
		this.serverTags = serverTags;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<SysArea>  getAddress() {
		return address;
	}
	public void setAddress(List<SysArea>  address) {
		this.address = address;
	}
	
	public Long getGoodId() {
		return goodId;
	}
	public void setGoodId(Long goodId) {
		this.goodId = goodId;
	}
	public String getGoodSn() {
		return goodSn;
	}
	public void setGoodSn(String goodSn) {
		this.goodSn = goodSn;
	}
	
	public Integer getSallCount() {
		return sallCount;
	}
	public void setSallCount(Integer sallCount) {
		this.sallCount = sallCount;
	}
	public Integer getCollectionCount() {
		return collectionCount;
	}
	public void setCollectionCount(Integer collectionCount) {
		this.collectionCount = collectionCount;
	}
	public Integer getEvaluateCount() {
		return evaluateCount;
	}
	public void setEvaluateCount(Integer evaluateCount) {
		this.evaluateCount = evaluateCount;
	}
	public Long getPkgId() {
		return pkgId;
	}
	public void setPkgId(Long pkgId) {
		this.pkgId = pkgId;
	}
	
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public boolean isAuthentication() {
		return authentication;
	}
	public void setAuthentication(boolean authentication) {
		this.authentication = authentication;
	}
	
	public List<GoodsComment> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<GoodsComment> commentList) {
		this.commentList = commentList;
	}
	@Override
	public String toString() {
		return "GoodsDto [goodsName=" + goodsName + ", content=" + content + ", goodPic=" + goodPic + ", goodPrice="
				+ goodPrice + ", goodUnit=" + goodUnit + ", time=" + time + ", timeNote=" + timeNote + ", type=" + type
				+ ", cateId=" + cateId + ", tags=" + tags + ", category=" + category + ", goodPics="
				+ Arrays.toString(goodPics) + ", memberName=" + memberName + ", memberId=" + memberId + ", goodId="
				+ goodId + ", goodSn=" + goodSn + ", pkgId=" + pkgId + ", followCount=" + followCount + ", sallCount="
				+ sallCount + ", collectionCount=" + collectionCount + ", evaluateCount=" + evaluateCount
				+ ", serverTags=" + serverTags + ", nickName=" + nickName + ", imageUrl=" + imageUrl + ", address="
				+ address + ", gender=" + gender + ", authentication=" + authentication + ", commentList=" + commentList
				+ "]";
	}
	
}
