package com.xyxy.platform.modules.service.member;

import com.xyxy.platform.modules.entity.member.Profession;

import java.util.List;

/**
 * ProfessionService
 *
 * @author yangdianjun
 * @date 2016/1/13 0013
 */
public interface ProfessionService {
    /**
     * 单个创建
     * @param
     * @return
     */
    Profession create(Profession profession);

    /**
     * 批量创建(foreach 单个创建)
     * @param tagList
     * @return
     */
    List<Profession> create(List<Profession> tagList);

    /**
     * 单个更新
     * @param
     * @return
     */
    boolean update(Profession profession);

    /**
     * 批量更新(foreach 单个更新)
     * @param
     */
    void update(List<Profession> professionList);


    /**
     * 根据 主键ID 查询
     *
     * @return
     */
    Profession findById(Long id);

    /**
     * 根据标签名, ==查询
     * @param name
     * @return
     */
    Profession findByName(String name);

    /**
     * 根据主键查询 个性标签
     *
     * @param id
     * @return
     */
    boolean deleteById(Long id);

    /**
     * 根据ID list 查询列表
     * @param specificIdList
     * @return
     */
    List<Profession> findByIdList(List<Long> professionIdList);

    /**
     * 检测接受到得表单数据,  存在直接返回, 不存在则新增入库
     * @param tagList
     * @return
     */
    List<Profession> checkNotInDb(List<Profession> tagList);
}
