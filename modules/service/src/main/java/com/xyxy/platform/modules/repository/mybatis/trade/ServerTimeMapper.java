package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.ServerTime;
import com.xyxy.platform.modules.entity.trade.ServerTimeExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface ServerTimeMapper {
    int countByExample(ServerTimeExample example);

    int deleteByExample(ServerTimeExample example);

    int deleteByPrimaryKey(Long serverTimeId);

    int insert(ServerTime record);

    int insertSelective(ServerTime record);

    List<ServerTime> selectByExample(ServerTimeExample example);

    ServerTime selectByPrimaryKey(Long serverTimeId);

    int updateByExampleSelective(@Param("record") ServerTime record, @Param("example") ServerTimeExample example);

    int updateByExample(@Param("record") ServerTime record, @Param("example") ServerTimeExample example);

    int updateByPrimaryKeySelective(ServerTime record);

    int updateByPrimaryKey(ServerTime record);
}