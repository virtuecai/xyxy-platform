package com.xyxy.platform.modules.service.common.exception;

/**
 * 简介: 恶意ip短信攻击
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-02-23 11:26
 */
public class SmsException extends Exception {

    private String message;
    private Integer code;

    /**
     * 1: 该ip使用过多手机号进行注册
     * 2: 该手机进行注册验证码发送超过次数(比如5次一天)限制
     * 3: 短信验证码还有有效期内, 禁止频繁发送
     * @return
     */
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public SmsException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }
}
