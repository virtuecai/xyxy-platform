package com.xyxy.platform.modules.service.member.impl;

import com.xyxy.platform.modules.entity.member.MemberLoginLog;
import com.xyxy.platform.modules.repository.mybatis.member.MemberLoginLogMapper;
import com.xyxy.platform.modules.service.member.MemberLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 简介: 会员服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:39
 */
@Service
@Transactional
public class MemberLoginLogServiceImpl implements MemberLoginLogService {

    @Autowired
    private MemberLoginLogMapper logMapper;

    @Async
    @Override
    public void createLoginLog(MemberLoginLog log) {
        logMapper.insert(log);
    }
}
