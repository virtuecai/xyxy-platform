package com.xyxy.platform.modules.service.member.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.member.MemberProfession;
import com.xyxy.platform.modules.entity.member.MemberProfessionExample;
import com.xyxy.platform.modules.entity.member.Profession;
import com.xyxy.platform.modules.repository.mybatis.member.MemberProfessionMapper;
import com.xyxy.platform.modules.service.member.MemberProfessionService;
import com.xyxy.platform.modules.service.member.ProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * MemberProfessionServiceImpl
 *
 * @author yangdianjun
 * @date 2016/1/13 0013
 */
@Service
public class MemberProfessionServiceImpl implements MemberProfessionService{
    @Autowired
    private MemberProfessionMapper mapper;

    @Autowired
    private ProfessionService professionService;

    @Override
    public MemberProfession create(Profession profession, Long memberId) {
        if (null == memberId || null == profession) {
            return null;
        }
        MemberProfession memberProfession = new MemberProfession();
        memberProfession.setMemberId(memberId);
        memberProfession.setProfessionId(profession.getId());
        mapper.insertSelective(memberProfession);
        return memberProfession;
    }

    @Override
    public List<MemberProfession> create(List<Profession> tagList, Long memberId) {
        List<MemberProfession> result = Lists.newArrayList();
        if (null == memberId || Collections3.isEmpty(tagList)) {
            return Lists.newArrayList();
        }
        int idx = 0;
        for (Profession tag : tagList) {
            MemberProfession memberProfession = new MemberProfession();
            memberProfession.setMemberId(memberId);
            memberProfession.setProfessionId(tag.getId());
            memberProfession.setIdx(idx++);
            mapper.insertSelective(memberProfession);
            result.add(memberProfession);
        }
        return result;
    }

    @Override
    public boolean update(MemberProfession memberProfession) {
        mapper.updateByPrimaryKeySelective(memberProfession);
        return false;
    }

    @Override
    public void update(List<MemberProfession> memberProfessionList) {
        if (null == memberProfessionList || Collections3.isEmpty(memberProfessionList)) {
            return;
        }
        for (MemberProfession memberSpecificTag : memberProfessionList) {
            this.update(memberSpecificTag);
        }
    }

    @Override
    public List<MemberProfession> update(List<Profession> tagList, Long memberId) {
        this.deleteByMemberId(memberId);
        List<MemberProfession> result = this.create(tagList, memberId);
        return result;
    }

    @Override
    public List<MemberProfession> findByMemberId(Long memberId) {
        MemberProfessionExample sql = new MemberProfessionExample();
        sql.or().andMemberIdEqualTo(memberId);
        sql.setOrderByClause("idx asc");
        List<MemberProfession> list = mapper.selectByExample(sql);
        if (Collections3.isEmpty(list)) {
            return Lists.newArrayList();
        }
        return list;
    }

    @Override
    public boolean deleteByPrimaryKey(Long id) {
        return mapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public boolean deleteByMemberId(Long memberId) {
        MemberProfessionExample sql = new MemberProfessionExample();
        sql.or().andMemberIdEqualTo(memberId);
        return mapper.deleteByExample(sql) > 0;
    }

    @Override
    public MemberProfession findByTagName(MemberProfession memberProfession) {
        return mapper.selectByTagName(memberProfession);
    }

    public ProfessionService getProfessionService() {
        return professionService;
    }
}
