package com.xyxy.platform.modules.service.trade.vo;

import java.io.Serializable;

/**
 * 商品分类dto
 * @author zhaowei
 */
public class CategoryDto implements Serializable{
	private static final long serialVersionUID = -2639613162043210834L;
	private long categoryId;      //分类id
	private String categoryName;  //分类名称
	private String icon;		  //自定义icon
	private long pid;			  //父id   一级默认为0	
	private String idPath;		  //id路径 例如：/0/1
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public long getPid() {
		return pid;
	}
	public void setPid(long pid) {
		this.pid = pid;
	}
	public String getIdPath() {
		return idPath;
	}
	public void setIdPath(String idPath) {
		this.idPath = idPath;
	}
	
}	
