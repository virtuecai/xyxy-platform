package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.MemberProfession;
import com.xyxy.platform.modules.entity.member.MemberProfessionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberProfessionMapper {
    int countByExample(MemberProfessionExample example);

    int deleteByExample(MemberProfessionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MemberProfession record);

    int insertSelective(MemberProfession record);

    List<MemberProfession> selectByExample(MemberProfessionExample example);

    MemberProfession selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MemberProfession record, @Param("example") MemberProfessionExample example);

    int updateByExample(@Param("record") MemberProfession record, @Param("example") MemberProfessionExample example);

    int updateByPrimaryKeySelective(MemberProfession record);

    int updateByPrimaryKey(MemberProfession record);

    MemberProfession selectByTagName(MemberProfession memberProfession);
}