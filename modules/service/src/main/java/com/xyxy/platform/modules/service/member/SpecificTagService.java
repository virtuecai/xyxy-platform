package com.xyxy.platform.modules.service.member;

import com.xyxy.platform.modules.entity.member.SpecificTag;

import java.util.List;

/**
 * 简介: 个性标签服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:41
 */
public interface SpecificTagService {

    /**
     * 单个创建
     *
     * @param specificTag
     * @return
     */
    SpecificTag create(SpecificTag specificTag);

    /**
     * 批量创建(foreach 单个创建)
     *
     * @param tagList
     * @return
     */
    List<SpecificTag> create(List<SpecificTag> tagList);

    /**
     * 单个更新
     *
     * @param specificTag
     * @return
     */
    boolean update(SpecificTag specificTag);

    /**
     * 批量更新(foreach 单个更新)
     *
     * @param tagList
     */
    void update(List<SpecificTag> tagList);

    /**
     * 根据 主键ID 查询
     *
     * @return
     */
    SpecificTag findById(Long id);

    /**
     * 根据标签名, ==查询
     * @param name
     * @return
     */
    SpecificTag findByName(String name);

    /**
     * 根据主键查询 个性标签
     *
     * @param id
     * @return
     */
    boolean deleteById(Long id);

    /**
     * 根据ID list 查询列表
     * @param specificIdList
     * @return
     */
    List<SpecificTag> findByIdList(List<Long> specificIdList);

    /**
     * 检测接受到得表单数据,  存在直接返回, 不存在则新增入库
     * @param tagList
     * @return
     */
    List<SpecificTag> checkNotInDb(List<SpecificTag> tagList);
}
