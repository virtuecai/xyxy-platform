package com.xyxy.platform.modules.service.trade.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.xyxy.platform.modules.entity.trade.OrderComment;
import com.xyxy.platform.modules.entity.trade.OrderCommentExample;
import com.xyxy.platform.modules.repository.mybatis.trade.OrderCommentMapper;
import com.xyxy.platform.modules.service.trade.OrderCommentService;
import com.xyxy.platform.modules.service.trade.vo.OrderCommentDto;

@Service
public class OrderCommentServiceImpl implements OrderCommentService{

	@Autowired
	private OrderCommentMapper orderCommentMapper;
	
	@Override
	public List<OrderCommentDto> findOrderCommentList(OrderCommentDto orderCommentDto, int page, int rows) {
		List<OrderCommentDto> result = null;
		OrderCommentExample orderCommentExample = new OrderCommentExample();
		orderCommentExample.or().andOrderSnEqualTo(orderCommentDto.getOrderSn());
		
		PageHelper.startPage(page, rows);
		List<OrderComment> orderCommentList = orderCommentMapper.selectByExample(orderCommentExample);
		if(orderCommentList!=null&&orderCommentList.size()>0){
			result = new ArrayList<OrderCommentDto>();
			String imgUrl = "";
			String tags = "";
			String alias = "";
			int gender = 0 ;
			String detailed = "";
			int isAuth = 0;
			String time = "";
			List<String> imgs =new ArrayList<String>();
			for(OrderComment orderComment : orderCommentList){
				imgUrl = "";
				tags = "";
				alias = "";
				detailed = "";
				time = "";
				imgs.clear();
				
				//TODO: 根据会员id获取头像地址
				
				//获取图片数组
				
				orderCommentDto = new OrderCommentDto();
				orderCommentDto.setMemberId(String.valueOf(orderComment.getMemberId()));
				orderCommentDto.setImgurl(imgUrl);
				orderCommentDto.setTags(tags);
				orderCommentDto.setAlias(alias);
				orderCommentDto.setGender(gender);
				orderCommentDto.setDetailed(detailed);
				orderCommentDto.setImgs(imgs);
				orderCommentDto.setTime(time);
				orderCommentDto.setIsAuth(isAuth);
				result.add(orderCommentDto);
			}
		}else{
			result = Collections.EMPTY_LIST;
		}
		return result;
	}
	
}
