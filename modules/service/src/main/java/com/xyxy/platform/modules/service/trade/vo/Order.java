package com.xyxy.platform.modules.service.trade.vo;

import java.io.Serializable;
import java.util.Date;

import com.xyxy.platform.modules.entity.trade.Goods;

/** 
 * 订单信息dto
 * @author vic E-mail: 251361939@qq.com
 * @version 创建时间：2015年12月19日 下午6:53:30 
 */
public class Order implements Serializable {

	private static final long serialVersionUID = -3372215362645658283L;
	private String orderSn;//订单号标识
	private int status;//订单状态
	private String imgurl;//用户头像
	private String alias;//用户昵称
	private int gender;//用户性别
	private String goodName;//商品名称
	private int number;//商品数量
	private String goodPrice;//商品价格
	private String goodUnit;//商品单位
	private String orderTime;//下单时间  utc8
	private Long memberid;//会员id
	
	private long addressId;//用户地址id
	private Date time;//服务开始时间 
	private String postscript;//备注说明
	private Goods goods;//商品对象
	private int payStatus;//订单状态
	private String referer;//订单来源

	
	
	/**
	 * referer
	 *
	 * @return  the referer
	 * @since   1.0.0
	*/
	
	public String getReferer() {
		return referer;
	}
	/**
	 * @param referer the referer to set
	 */
	public void setReferer(String referer) {
		this.referer = referer;
	}
	/**
	 * 订单号标识
	 * @return
	 */
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	/**
	 * 订单状态
	 * @return
	 */
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * 用户头像
	 * @return
	 */
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 用户昵称
	 * @return
	 */
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 用户性别
	 * @return
	 */
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	/**
	 * 商品名称
	 * @return
	 */
	public String getGoodName() {
		return goodName;
	}
	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	/**
	 * 商品数量
	 * @return
	 */
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	/**
	 * 商品价格
	 * @return
	 */
	public String getGoodPrice() {
		return goodPrice;
	}
	public void setGoodPrice(String goodPrice) {
		this.goodPrice = goodPrice;
	}
	/**
	 * 商品单位
	 * @return
	 */
	public String getGoodUnit() {
		return goodUnit;
	}
	public void setGoodUnit(String goodUnit) {
		this.goodUnit = goodUnit;
	}
	/**
	 * 下单时间  utc8   
	 * @return
	 */
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	
	/**
	 * 商品信息
	 * @return
	 */
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	
	
	
	/**
	 * 用户地址id
	 * @return
	 */
	public long getAddressId() {
		return addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	/**
	 * 备注说明
	 * @return
	 */

	/**
	 * 服务时间
	 * @return
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * postscript
	 *
	 * @return  the postscript
	 * @since   1.0.0
	*/
	
	public String getPostscript() {
		return postscript;
	}
	/**
	 * @param postscript the postscript to set
	 */
	public void setPostscript(String postscript) {
		this.postscript = postscript;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 会员id
	 * @return
	 */
	public Long getMemberid() {
		return memberid;
	}
	public void setMemberid(Long memberid) {
		this.memberid = memberid;
	}
	
	/**
	 * payStatus
	 *
	 * @return  the payStatus
	 * @since   1.0.0
	*/
	
	public int getPayStatus() {
		return payStatus;
	}
	/**
	 * @param payStatus the payStatus to set
	 */
	public void setPayStatus(int payStatus) {
		this.payStatus = payStatus;
	}
	@Override
	public String toString() {
		return "Order [orderSn=" + orderSn + ", status=" + status + ", imgurl="
				+ imgurl + ", alias=" + alias + ", gender=" + gender
				+ ", goodName=" + goodName + ", number=" + number
				+ ", goodPrice=" + goodPrice + ", goodUnit=" + goodUnit
				+ ", orderTime=" + orderTime + ", memberid=" + memberid
				+ ", addressId=" + addressId + ", time=" + time + ", postscript="
				+ postscript + ", goods=" + goods + "]";
	}
	
}
