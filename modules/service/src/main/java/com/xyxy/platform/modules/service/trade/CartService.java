package com.xyxy.platform.modules.service.trade;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.xyxy.platform.modules.entity.trade.Cart;

public interface CartService {
    /**
     * 根据会员ID拿到购物车的所有东西
     * selectCartByMemberId
     *
     * @param id
     * @return Goods
     * @throws
     * @since 1.0.0
     */
    List<Cart> selectCartByMemberId(long id);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    Cart selectCartByCartId(long id);

    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deletebyId(long id);

    /**
     * 添加服务至购物车
     * 条件含有:
     * @param cart
     * @return
     */
    Boolean insert(Cart cart);
}
