package com.xyxy.platform.modules.service.member.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.member.MemberSpecificTag;
import com.xyxy.platform.modules.entity.member.MemberSpecificTagExample;
import com.xyxy.platform.modules.entity.member.SpecificTag;
import com.xyxy.platform.modules.repository.mybatis.member.MemberSpecificTagMapper;
import com.xyxy.platform.modules.service.member.MemberSpecificTagService;
import com.xyxy.platform.modules.service.member.SpecificTagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 简介: 会员个性标签服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:39
 */
@Service
@Transactional(readOnly = false)
public class MemberSpecificTagServiceImpl implements MemberSpecificTagService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberSpecificTagMapper memberSpecificTagMapper;

    @Autowired
    private SpecificTagService specificTagService;

    @Override
    public MemberSpecificTag create(SpecificTag specificTag, Long memberId) {
        if (null == memberId || null == specificTag) {
            return null;
        }
        MemberSpecificTag memberSpecificTag = new MemberSpecificTag();
        memberSpecificTag.setMemberId(memberId);
        memberSpecificTag.setSpecificId(specificTag.getId());
        memberSpecificTagMapper.insertSelective(memberSpecificTag);
        return memberSpecificTag;
    }

    @Override
    public List<MemberSpecificTag> create(List<SpecificTag> tagList, Long memberId) {
        List<MemberSpecificTag> result = Lists.newArrayList();
        if (null == memberId || Collections3.isEmpty(tagList)) {
            return Lists.newArrayList();
        }
        int idx = 0;
        for (SpecificTag tag : tagList) {
            MemberSpecificTag memberSpecificTag = new MemberSpecificTag();
            memberSpecificTag.setMemberId(memberId);
            memberSpecificTag.setSpecificId(tag.getId());
            memberSpecificTag.setIdx(idx++);
            memberSpecificTagMapper.insertSelective(memberSpecificTag);
            result.add(memberSpecificTag);
        }
        return result;
    }

    @Override
    public boolean update(MemberSpecificTag memberSpecificTag) {
        memberSpecificTagMapper.updateByPrimaryKeySelective(memberSpecificTag);
        return false;
    }

    @Override
    public void update(List<MemberSpecificTag> memberSpecificTagList) {
        if (null == memberSpecificTagList || Collections3.isEmpty(memberSpecificTagList)) {
            return;
        }
        for (MemberSpecificTag memberSpecificTag : memberSpecificTagList) {
            this.update(memberSpecificTag);
        }
    }

    @Override
    public List<MemberSpecificTag> update(List<SpecificTag> tagList, Long memberId) {
        this.deleteByMemberId(memberId);
        List<MemberSpecificTag> result = this.create(tagList, memberId);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<MemberSpecificTag> findByMemberId(Long memberId) {
        MemberSpecificTagExample sql = new MemberSpecificTagExample();
        sql.or().andMemberIdEqualTo(memberId);
        sql.setOrderByClause("idx asc");
        List<MemberSpecificTag> list = memberSpecificTagMapper.selectByExample(sql);
        if (Collections3.isEmpty(list)) {
            return Lists.newArrayList();
        }
        return list;
    }

    @Override
    public boolean deleteByPrimaryKey(Long id) {
        return memberSpecificTagMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public boolean deleteByMemberId(Long memberId) {
        MemberSpecificTagExample sql = new MemberSpecificTagExample();
        sql.or().andMemberIdEqualTo(memberId);
        return memberSpecificTagMapper.deleteByExample(sql) > 0;
    }

    @Override
    public MemberSpecificTag findByTagName(MemberSpecificTag memberSpecificTag) {
        return memberSpecificTagMapper.selectByTagName(memberSpecificTag);
    }


}
