package com.xyxy.platform.modules.service.member;

import com.xyxy.platform.modules.entity.member.MemberLoginLog;

/**
 * 简介: 会员登录日志
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-24 16:18
 */
public interface MemberLoginLogService {

    /**
     * 记录会员登录日志 异步
     * @param log
     */
    void createLoginLog(MemberLoginLog log);

}
