package com.xyxy.platform.modules.repository.mybatis.auth;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.auth.CardCheck;
import com.xyxy.platform.modules.entity.auth.CardCheckExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface CardCheckMapper {
    int countByExample(CardCheckExample example);

    int deleteByExample(CardCheckExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CardCheck record);

    int insertSelective(CardCheck record);

    List<CardCheck> selectByExample(CardCheckExample example);

    CardCheck selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CardCheck record, @Param("example") CardCheckExample example);

    int updateByExample(@Param("record") CardCheck record, @Param("example") CardCheckExample example);

    int updateByPrimaryKeySelective(CardCheck record);

    int updateByPrimaryKey(CardCheck record);
}