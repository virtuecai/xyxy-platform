package com.xyxy.platform.modules.service.finance;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.finance.AccountLog;
import com.xyxy.platform.modules.entity.finance.Payment;
import com.xyxy.platform.modules.entity.finance.UserAccount;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authc.Account;

import java.util.Date;
import java.util.List;

/**
 * FinanceService
 *
 * @author yangdianjun
 * @date 2015/12/21 0021
 */
public interface FinanceService {

    /**
     * 获取我的钱包信息
     * @param memberId 用户id
     * @return 我的钱包信息对象
     * @version 1.0
     */
    public AccountLog getMyAccount(Long memberId);

    /**
     * 获取我的收入、支出、提现记录
     * @param memberId  用户id
     * @param action    操作类型 1)充值 2)付款 3)退款
     * @param isPaid    是否已付款  0）否 1）是
     * @version 1.0
     * @return 我的收支信息对象
     */
    public PageInfo getIncomeOrPaylog(Long memberId, Integer action ,Integer isPaid,Integer pageNumber,Integer pageSize);

    /**
     * 获取所有支付方式
     * @return
     */
    public List<Payment> getPayment();

    /**
     * 添加一条支付记录
     * @param userAccount 支付记录对象
     * @return 添加的支付记录对象
     */
    public UserAccount insertUseraccount(UserAccount userAccount);

    /**
     * 更新支付记录
     * @param userAccount 更新的支付记录对象
     * @return 更新完毕的支付记录对象
     */
    public UserAccount updateUseraccount(UserAccount userAccount);


}
