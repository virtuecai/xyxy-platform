package com.xyxy.platform.modules.service.image;

import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.image.ImagePkg;
import com.xyxy.platform.modules.entity.image.ImageTypes;

import java.util.List;

/**
 * 简介: 图片资源服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-05 16:55
 */
public interface ImageItemService {

    /**
     * 根据 图片组ID+图片类型查询图片
     *
     * @param imagePkgId 图片组ID
     * @param imageType  图片类型
     * @return
     */
    List<ImageItem> findImageItemList(Long imagePkgId, ImageTypes imageType);

    /**
     * 根据 图片组ID 查询图片list
     *
     * @param imagePkgId 图片组ID
     * @return
     */
    List<ImageItem> findImageItemList(Long imagePkgId);

    /**
     * 简介: 新增 ImageItem
     *
     * @param imageItem
     * @return
     * @author zhaowei
     * @Date 2016年1月6日 下午3:00:09
     * @version 1.0
     */
    boolean insert(ImageItem imageItem);


    /**
     * 插入一条 pkgId 图片组
     * @param imagePkg
     * @return
     */
    Integer insertImagePkg(ImagePkg imagePkg);

    /**
     * 删除 图片数据, 同时删除图片文件
     * @param imageItem 图片信息对象
     * @return
     */
    Integer delete(ImageItem imageItem);

    /**
     * 删除 图片数据, 同时删除图片文件
     * @param imageItem 图片信息对象
     * @param deleteImageFile 删除数据信息的适合, 是否同时删除图片文件
     * @return
     */
    Integer delete(ImageItem imageItem, Boolean deleteImageFile);

    /**
     * 批量删除图片, 同时删除图片
     * @param imageItemList 图片信息集合
     * @return
     */
    Integer delete(List<ImageItem> imageItemList);

    /**
     * 批量删除图片, 同时删除图片
     * @param imageItemList 图片信息集合
     * @param deleteImageFile 删除数据信息的适合, 是否同时删除图片文件
     * @return
     */
    Integer delete(List<ImageItem> imageItemList, Boolean deleteImageFile);

    /**
     * 图片 url list, 转换成 uri list
     * @param urlList 完成路径图片url
     * @return
     */
    List<String> convertUrlToUri(List<String> urlList);

    /**
     * 更新imageItem uri
     * @param imageItem
     * @return
     */
    Integer updateUri(ImageItem imageItem);
}
