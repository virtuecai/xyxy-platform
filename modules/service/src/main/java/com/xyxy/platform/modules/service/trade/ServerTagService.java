package com.xyxy.platform.modules.service.trade;

import com.xyxy.platform.modules.entity.trade.ServerTags;

import java.util.List;

/**
 * 简介: 服务标签
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-16 23:04
 */
public interface ServerTagService {

    /**
     * 根据服务ID 查询该服务得所有标签
     * @param goodsId 服务ID
     * @return
     */
    List<ServerTags> findByGoodsId(Long goodsId);

    /**
     * 根据标签ID查询
     * @param serverTagId 标签ID
     * @return
     */
    ServerTags findById(Long serverTagId);

    /**
     * 根据 标签名查询标签
     * @param tagName 标签名
     * @return
     */
    ServerTags findByName(String tagName);

    /**
     * (服务,标签关系表保存)保存服务的标签, 并且删除原有的标签
     * @param serverTagsList
     * @param goodsId
     * @return
     */
    Integer save(List<ServerTags> serverTagsList, Long goodsId);

    /**
     * 新增服务标签(仅标签)
     * @param serverTags
     * @return
     */
    Integer insert(ServerTags serverTags);

}
