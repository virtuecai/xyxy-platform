package com.xyxy.platform.modules.service.image.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.file.SystemFileService;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.image.ImageItemExample;
import com.xyxy.platform.modules.entity.image.ImagePkg;
import com.xyxy.platform.modules.entity.image.ImageTypes;
import com.xyxy.platform.modules.repository.mybatis.image.ImageItemMapper;
import com.xyxy.platform.modules.repository.mybatis.image.ImagePkgMapper;
import com.xyxy.platform.modules.service.image.ImageItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 简介: 图片资源服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-05 16:57
 */
@Service
public class ImageItemServiceImpl implements ImageItemService {

    @Autowired
    private ImageItemMapper imageItemMapper;
    @Autowired
    private ImagePkgMapper imagePkgMapper;
    @Autowired
    private SystemFileService systemFileService;

    @Override
    public List<ImageItem> findImageItemList(Long imagePkgId, ImageTypes imageType) {
        if (null == imagePkgId || null == imageType) {
            throw new RuntimeException("imagePkgId or imageType is null!");
        }
        ImageItemExample sql = new ImageItemExample();
        sql.or().andImagePkgIdEqualTo(imagePkgId).andImageTypeIdEqualTo(imageType.getTypeId());
        sql.setOrderByClause("image_item.idx asc");
        List<ImageItem> imageItemList = imageItemMapper.selectByExample(sql);
        return Collections3.isEmpty(imageItemList) ? Lists.<ImageItem>newArrayList() : imageItemList;
    }

    @Override
    public List<ImageItem> findImageItemList(Long imagePkgId) {
        if (null == imagePkgId) {
            throw new RuntimeException("imagePkgId is null!");
        }
        ImageItemExample sql = new ImageItemExample();
        sql.or().andImagePkgIdEqualTo(imagePkgId);
        sql.setOrderByClause("image_item.idx asc");
        List<ImageItem> imageItemList = imageItemMapper.selectByExample(sql);
        return Collections3.isEmpty(imageItemList) ? Lists.<ImageItem>newArrayList() : imageItemList;
    }

    @Override
    public boolean insert(ImageItem imageItem) {
        if (null != imageItem && StringUtils.isNotEmpty(imageItem.getUri())) {
            systemFileService.changeFileStatus(imageItem.getUri());
            return imageItemMapper.insert(imageItem) > 0;
        }
        return false;
    }

    @Override
    public Integer insertImagePkg(ImagePkg imagePkg) {
        return imagePkgMapper.insert(imagePkg);
    }

    @Override
    public Integer delete(ImageItem imageItem) {
        systemFileService.deleteTmpFile(imageItem.getUri());
        return imageItemMapper.deleteByPrimaryKey(imageItem.getId());
    }

    @Override
    public Integer delete(ImageItem imageItem, Boolean deleteImageFile) {
        if (deleteImageFile) {
            systemFileService.deleteTmpFile(imageItem.getUri());
        }
        return imageItemMapper.deleteByPrimaryKey(imageItem.getId());
    }

    @Override
    public Integer delete(List<ImageItem> imageItemList) {
        if (Collections3.isEmpty(imageItemList)) return 0;
        Integer row = 0;
        for (ImageItem imageItem : imageItemList) {
            row += this.delete(imageItem);
        }
        return row;
    }

    @Override
    public Integer delete(List<ImageItem> imageItemList, Boolean deleteImageFile) {
        if (Collections3.isEmpty(imageItemList)) return 0;
        Integer row = 0;
        for (ImageItem imageItem : imageItemList) {
            row += this.delete(imageItem, deleteImageFile);
        }
        return row;
    }

    @Override
    public List<String> convertUrlToUri(List<String> imageUrlList) {
        List<String> result = Lists.newArrayList();
        if (Collections3.isEmpty(imageUrlList)) return result;
        for (String url : imageUrlList) {
            //if(StringUtils.isEmpty(url) || url.indexOf("http") == -1) continue;
            result.add(systemFileService.getUriByUrl(url));
        }
        return result;
    }

    @Override
    public Integer updateUri(ImageItem imageItem) {
        if (imageItem == null || imageItem.getId() == null) {
            return 0;
        }
        systemFileService.deleteTmpFile(imageItem.getUri());
        return imageItemMapper.updateByPrimaryKey(imageItem);
    }
}
