package com.xyxy.platform.modules.service.image;

import com.xyxy.platform.modules.entity.image.ImagePkg;

/**
 * 简介: 图片组
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-20 16:55
 */
public interface ImagePkgService {

    ImagePkg findById(Long imagePkgId);

    Integer insert(ImagePkg imagePkg);

}
