package com.xyxy.platform.modules.service.shop.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.repository.mybatis.member.MemberMapper;
import com.xyxy.platform.modules.service.member.MemberService;
import com.xyxy.platform.modules.service.member.vo.MemberDetail;
import com.xyxy.platform.modules.service.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-07
 */
@Service
@Transactional(readOnly = true)
public class ShopServiceImpl implements ShopService {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberMapper memberMapper;

    /**
     * 根据条件搜索星店列表
     * @param parMap{
     *
     * }  条件集合
     * @param pageNum 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @Override
    public PageInfo getShopList(String keyword, Integer pageNum, Integer pageSize , Map<String, Object> parMap, Integer type) {
        /*采用数据库方式*/
//        if(type==null||type==1)
//            return memberService.findForNewest(keyword, pageNum, pageSize, parMap);
//        else if(type==2)
//            return memberService.findForRecommend(keyword, pageNum, pageSize, parMap);
//        else
//            return null;
        /*采用solrj索引方式*/
        return memberService.findMemberInfoSolrj(keyword, String.valueOf(pageNum), String.valueOf(pageSize), parMap, type);
    }

    @Override
    public List getSpecificTags(long tagId,Integer number,boolean isToParent) {
        return memberMapper.findSpecificTags(tagId, number, isToParent);
    }

    @Override
    public Map getSpecificTag(long tagId) {
        return memberMapper.findSpecificTag(tagId);
    }
}
