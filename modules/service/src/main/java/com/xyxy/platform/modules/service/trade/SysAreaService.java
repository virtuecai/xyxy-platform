/**
 *
 * com.xyxy.platform.modules.service.trade
 * SysAreaService.java
 *
 * 2016-2016年1月13日-下午7:39:25
 */
package com.xyxy.platform.modules.service.trade;

import com.xyxy.platform.modules.entity.common.SysArea;

/**
 *
 * SysAreaService
 *
 * xiangzhipeng
 * xiangzhipeng
 * 2016年1月13日 下午7:39:25
 *
 * @version 1.0.0
 *
 */
public interface SysAreaService {
    SysArea getSysAreaById(Long long1);
}
