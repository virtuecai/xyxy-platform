package com.xyxy.platform.modules.service.member;

import com.xyxy.platform.modules.entity.member.MemberProfession;
import com.xyxy.platform.modules.entity.member.Profession;

import java.util.List;

/**
 * MemberProfessionService
 *
 * @author yangdianjun
 * @date 2016/1/13 0013
 */
public interface MemberProfessionService {
    /**
     * 单个创建
     * @return
     */
    MemberProfession create(Profession profession, Long memberId);

    /**
     * 批量创建(foreach 单个创建)
     * @param tagList
     * @return
     */
    List<MemberProfession> create(List<Profession> tagList, Long memberId);

    /**
     * 单个更新
     * @return
     */
    boolean update(MemberProfession memberProfession);

    /**
     * 批量更新(foreach 单个更新)
     * @param memberSpecificTagList
     */
    void update(List<MemberProfession> memberSpecificTagList);


    /**
     * 更新会员个性标签, 先删除所有, 然后新增
     * @param tagList
     * @param memberId
     * @return
     */
    List<MemberProfession> update(List<Profession> tagList, Long memberId);

    /**
     * 根据 memberId 查询
     * @param memberId 会员ID
     * @return
     */
    List<MemberProfession> findByMemberId(Long memberId);

    /**
     * 根据主键查询 个性标签
     * @param id
     * @return
     */
    boolean deleteByPrimaryKey(Long id);

    /**
     * 根据会员ID删除该会员的所有个性标签
     * @param memberId
     * @return
     */
    boolean deleteByMemberId(Long memberId);

    /**
     * 查询用户是否存在这个职业标签
     * @return
     */
    MemberProfession findByTagName(MemberProfession memberProfession);
}
