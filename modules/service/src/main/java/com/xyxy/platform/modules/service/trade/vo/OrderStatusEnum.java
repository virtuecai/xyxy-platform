package com.xyxy.platform.modules.service.trade.vo;
/**
 * 订单状态枚举
 * @author zhaowei
 */
public enum OrderStatusEnum {
	/**
	 * 无效
	 */
	INVALID(0,"无效"),
	/**
	 * 已确定
	 */
	CONFIRM(1,"已确定"),
	/**
	 * 待付款
	 */
	OBLIGATION(2,"待付款"),
	/**
	 * 已支付
	 */
	PAYMENT(3,"已支付"),
	/**
	 * 退款中
	 */
	REFUND(4,"退款中"),
	/**
	 * 交易成功
	 */
	SUCCESS(5,"交易成功"),
	/**
	 * 退货
	 */
	RETURN(6,"退货"),
	/**
	 * 关闭
	 */
	CLOSE(7,"关闭");
	private int value;
	private String msg;
	
	private OrderStatusEnum(int vlaue,String msg){
		this.value = vlaue;
		this.msg = msg;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
