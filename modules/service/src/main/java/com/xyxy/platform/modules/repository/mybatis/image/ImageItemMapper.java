package com.xyxy.platform.modules.repository.mybatis.image;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.image.ImageItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface ImageItemMapper {
    int countByExample(ImageItemExample example);

    int deleteByExample(ImageItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ImageItem record);

    int insertSelective(ImageItem record);

    List<ImageItem> selectByExample(ImageItemExample example);

    ImageItem selectByPrimaryKey(Long id);

    List<ImageItem> selectByImagePkgId(Long imagePkgId);

    int updateByExampleSelective(@Param("record") ImageItem record, @Param("example") ImageItemExample example);

    int updateByExample(@Param("record") ImageItem record, @Param("example") ImageItemExample example);

    int updateByPrimaryKeySelective(ImageItem record);

    int updateByPrimaryKey(ImageItem record);

}