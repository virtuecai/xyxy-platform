package com.xyxy.platform.modules.service.trade;

import java.util.List;

import com.xyxy.platform.modules.entity.trade.OrderInfo;
import com.xyxy.platform.modules.repository.mybatis.trade.OrderInfoDto;
import com.xyxy.platform.modules.service.trade.vo.Order;
import com.xyxy.platform.modules.service.trade.vo.OrderCommentDto;



/** 
 *
 * @author vic E-mail: 251361939@qq.com
 * @version 创建时间：2015年12月21日 下午2:41:50 
 */
public interface OrderService {
	/**
	 * 简介:    获取订单列表
	 * @author zhaowei
	 * @Date 2015年12月21日 下午2:48:18
	 * @version 1.0
	 *
	 * @param option    操作类型    1)购买订单列表  2)售出订单列表
	 * @param memberId  会员id
	 * @param type		订单状态    0 无效  1 已确定  2 待付款   3 已支付  4 退款中  5 交易成功  6 退货   7 已关闭
	 * @param page		页数
	 * @param rows		每页条数
	 * @return
	 */
	List<OrderInfo> findOrderInfoList(int option ,String memberId,String type,int page,int rows);
	
	/**
	 * 简介:    获取订单封装对象列表
	 * @author zhaowei
	 * @Date 2015年12月21日 下午2:48:18
	 * @version 1.0
	 *
	 * @param option    操作类型    1)购买订单列表  2)售出订单列表
	 * @param memberId  会员id
	 * @param type		订单状态    0 无效  1 已确定  2 待付款   3 已支付  4 退款中  5 交易成功  6 退货   7 已关闭
	 * @param page		页数
	 * @param rows		每页条数
	 * @return
	 */
	List<OrderInfoDto> findOrderInfoDtoList(int option,String memberId,String type,int page,int rows);
	
	/**
	 * 简介:根据会员id、订单号查询买家订单
	 * @author zhaowei
	 * @Date 2015年12月22日 上午11:00:51
	 * @version 1.0
	 * --------update 增加订单来源----------xiangzhipeng
	 * @param memberid
	 * @param orderSn
	 * @return
	 */
	List<OrderInfo> getMemberOrder(long memberid ,String orderSn,String referer);
	
	/**
	 * 简介:根据会员id、订单号查询卖家订单
	 * @author zhaowei
	 * @Date 2015年12月22日 下午2:47:34
	 * @version 1.0
	 *
	 * @param memberid
	 * @param orderSn
	 * @return
	 */
	OrderInfo getSallOrder(long memberid,String orderSn);
	
	/**
	 * 简介:删除买家订单
	 * @author zhaowei
	 * @Date 2015年12月22日 上午11:51:37
	 * @version 1.0
	 *
	 * @param memberid   会员id
	 * @param orderSn	   订单号
	 * @return
	 */
	boolean   removeMemberOrder(long memberid,String orderSn);
	
	
	/**
	 * 简介:删除卖家订单
	 * @author zhaowei
	 * @Date 2015年12月22日 上午11:51:50
	 * @version 1.0
	 *
	 * @param sallId	 商家id
	 * @param orderSn	 订单号
	 * @return
	 */
	boolean   removeSallOrder(long sallId ,String orderSn);
	
	/**
	 * 简介: 取消买家订单
	 * @author zhaowei
	 * @Date 2015年12月22日 下午3:39:20
	 * @version 1.0
	 *
	 * @param memberId   买家id
	 * @param orderSn	   订单号
	 * @return
	 */
	boolean   cancelMemberOrder(long memberId,String orderSn);
	
	/**
	 * 简介: 取消卖家订单
	 * @author zhaowei
	 * @Date 2015年12月22日 下午3:42:39
	 * @version 1.0
	 *
	 * @param sallId	卖家id
	 * @param orderSn	订单号
	 * @return
	 */
	boolean   cancelSallOrder(long sallId,String orderSn);
	
	/**
	 * 简介: 提交订单
	 * @author zhaowei
	 * @Date 2015年12月22日 下午4:41:43
	 * @version 1.0
	 *
	 * @param order
	 * @return
	 */
	boolean   submitOrderInfo(Order order);
	
	/**
	 * 简介: 新增订单评论
	 * @author zhaowei
	 * @Date 2015年12月23日 下午3:27:17
	 * @version 1.0
	 *
	 * @param orderComment
	 * @return
	 */
	boolean   saveOrderComment(OrderCommentDto orderComment);
	
	int   updateOrderInfo(OrderInfo orderInfo);

	
}
