package com.xyxy.platform.modules.service.member;

import com.xyxy.platform.modules.entity.member.MemberSpecificTag;
import com.xyxy.platform.modules.entity.member.SpecificTag;

import java.util.List;

/**
 * 简介: 会员个性标签服务类
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-21 11:41
 */
public interface MemberSpecificTagService {

    /**
     * 单个创建
     * @param specificTag
     * @return
     */
    MemberSpecificTag create(SpecificTag specificTag, Long memberId);

    /**
     * 批量创建(foreach 单个创建)
     * @param tagList
     * @return
     */
    List<MemberSpecificTag> create(List<SpecificTag> tagList, Long memberId);

    /**
     * 单个更新
     * @param memberSpecificTag
     * @return
     */
    boolean update(MemberSpecificTag memberSpecificTag);

    /**
     * 批量更新(foreach 单个更新)
     * @param memberSpecificTagList
     */
    void update(List<MemberSpecificTag> memberSpecificTagList);


    /**
     * 更新会员个性标签, 先删除所有, 然后新增
     * @param tagList
     * @param memberId
     * @return
     */
    List<MemberSpecificTag> update(List<SpecificTag> tagList, Long memberId);

    /**
     * 根据 memberId 查询
     * @param memberId 会员ID
     * @return
     */
    List<MemberSpecificTag> findByMemberId(Long memberId);

    /**
     * 根据主键查询 个性标签
     * @param id
     * @return
     */
    boolean deleteByPrimaryKey(Long id);

    /**
     * 根据会员ID删除该会员的所有个性标签
     * @param memberId
     * @return
     */
    boolean deleteByMemberId(Long memberId);

    /**
     * 查询用户是否存在这个个性标签
     * @return
     */
    MemberSpecificTag findByTagName(MemberSpecificTag memberSpecificTag);

}
