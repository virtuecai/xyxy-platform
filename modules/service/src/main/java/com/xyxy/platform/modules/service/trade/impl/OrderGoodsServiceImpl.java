package com.xyxy.platform.modules.service.trade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyxy.platform.modules.entity.trade.OrderGoods;
import com.xyxy.platform.modules.entity.trade.OrderGoodsExample;
import com.xyxy.platform.modules.repository.mybatis.trade.OrderGoodsMapper;
import com.xyxy.platform.modules.service.trade.OrderGoodsService;

@Service
public class OrderGoodsServiceImpl implements OrderGoodsService{

	@Autowired
	private OrderGoodsMapper  orderGoodsMapper;
	
	@Override
	public List<OrderGoods> selectOrderGoodsByOrderId(long orderId) {
		OrderGoodsExample orderGoodsExample = new OrderGoodsExample();
		orderGoodsExample.or().andGoodsIdEqualTo(orderId);
		return orderGoodsMapper.selectByExample(orderGoodsExample);
	}

}
