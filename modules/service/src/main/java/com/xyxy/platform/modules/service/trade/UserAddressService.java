/**
 *
 * com.xyxy.platform.modules.service.trade
 * UserAddressService.java
 *
 * 2016-2016年1月7日-下午2:43:09
 */
package com.xyxy.platform.modules.service.trade;

import java.util.List;

import com.xyxy.platform.modules.entity.trade.Cart;
import com.xyxy.platform.modules.entity.trade.UserAddress;

/**
 *
 * UserAddressService
 *
 * xiangzhipeng
 * xiangzhipeng
 * 2016年1月7日 下午2:43:09
 *
 * @version 1.0.0
 *
 */
public interface UserAddressService {
	List<UserAddress> selectAddressById(long id);
	/**
	 * 简介: 编辑联系人
	 * @author zhaowei
	 * @Date 2016年1月10日 下午9:20:15
	 * @version 1.0
	 * @param userAddress
	 * @return
	 */
	boolean editUserAddress(UserAddress userAddress);
	/**
	 * 简介: 删除联系人
	 * @author zhaowei
	 * @Date 2016年1月10日 下午9:20:48
	 * @version 1.0
	 * @param id
	 * @return
	 */
	boolean removeUserAddress(long id,long memberId);

	int insertUserAddress(UserAddress userAddress);

	int deleteByPrimaryKey(Long addressId);

	UserAddress selectByPrimaryKey(Long addressId);
	/**
	 * 简介: 新增联系人
	 * @author zhaowei
	 * @Date 2016年1月11日 下午2:42:09
	 * @version 1.0
	 * @param userAddress
	 * @return
	 */
	boolean saveUserAddress(UserAddress userAddress);
}
