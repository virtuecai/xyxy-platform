package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.SpecificTag;
import com.xyxy.platform.modules.entity.member.SpecificTagExample;

import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface SpecificTagMapper {
    int countByExample(SpecificTagExample example);

    int deleteByExample(SpecificTagExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SpecificTag record);

    int insertSelective(SpecificTag record);

    List<SpecificTag> selectByExample(SpecificTagExample example);

    SpecificTag selectByPrimaryKey(Long id);

    List<SpecificTag> selectByMemberId(Long memberId);

    int updateByExampleSelective(@Param("record") SpecificTag record, @Param("example") SpecificTagExample example);

    int updateByExample(@Param("record") SpecificTag record, @Param("example") SpecificTagExample example);

    int updateByPrimaryKeySelective(SpecificTag record);

    int updateByPrimaryKey(SpecificTag record);


    /**
     * 根据会员ID获取标签信息
     * @param memberId 会员
     * @return
     */
    List<SpecificTag> findSpecificTagByMemberId(long memberId);

    /**
     * 查询个人标签
     * @param memberId
     * @return
     */
    List<HashMap> findSpecificTags(long memberId);

}