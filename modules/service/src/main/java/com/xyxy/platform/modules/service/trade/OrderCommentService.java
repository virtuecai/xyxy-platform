package com.xyxy.platform.modules.service.trade;

import java.util.List;
import java.util.Map;

import com.xyxy.platform.modules.entity.trade.OrderComment;
import com.xyxy.platform.modules.service.trade.vo.OrderCommentDto;

/**
 * 订单评价服务
 * @author Administrator
 *
 */
public interface OrderCommentService {
	/**
	 * 简介:订单评价分页列表
	 * @author zhaowei
	 * @Date 2015年12月23日 下午7:55:15
	 * @version 1.0
	 *
	 * @param parMap
	 * @param page
	 * @param rows
	 * @return
	 */
	List<OrderCommentDto> findOrderCommentList(OrderCommentDto orderCommentDto , int page , int rows);
}
