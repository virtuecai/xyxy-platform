package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.Profession;
import com.xyxy.platform.modules.entity.member.ProfessionExample;

import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface ProfessionMapper {
    int countByExample(ProfessionExample example);

    int deleteByExample(ProfessionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Profession record);

    int insertSelective(Profession record);

    List<Profession> selectByExample(ProfessionExample example);

    Profession selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Profession record, @Param("example") ProfessionExample example);

    int updateByExample(@Param("record") Profession record, @Param("example") ProfessionExample example);

    int updateByPrimaryKeySelective(Profession record);

    int updateByPrimaryKey(Profession record);

    /**
     * 查询职业标签
     * @param memberId
     * @return
     */
    List<HashMap> findProfessionTags(long memberId);
}