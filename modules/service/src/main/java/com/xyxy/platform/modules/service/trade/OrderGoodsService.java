package com.xyxy.platform.modules.service.trade;

import java.util.List;

import com.xyxy.platform.modules.entity.trade.OrderGoods;

public interface OrderGoodsService {
	/**
	* 简介: 根据订单id获取 订单商品对象列表
	* @author zhaowei
	* @Date 2016年1月12日 下午7:29:22
	* @version 1.0
	* @param orderId
	* @return
	 */
	List<OrderGoods> selectOrderGoodsByOrderId(long orderId);
}
