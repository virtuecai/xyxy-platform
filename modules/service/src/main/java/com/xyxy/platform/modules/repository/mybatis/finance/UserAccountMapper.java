package com.xyxy.platform.modules.repository.mybatis.finance;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.finance.UserAccount;
import com.xyxy.platform.modules.entity.finance.UserAccountExample;
import java.util.List;
import java.util.Map;

import com.xyxy.platform.modules.entity.finance.selectAllvo;
import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface UserAccountMapper {
    int countByExample(UserAccountExample example);

    int deleteByExample(UserAccountExample example);

    int deleteByPrimaryKey(Long accountId);

    int insert(UserAccount record);

    int insertSelective(UserAccount record);

    List<UserAccount> selectByExample(UserAccountExample example);

    UserAccount selectByPrimaryKey(Long accountId);

    int updateByExampleSelective(@Param("record") UserAccount record, @Param("example") UserAccountExample example);

    int updateByExample(@Param("record") UserAccount record, @Param("example") UserAccountExample example);

    int updateByPrimaryKeySelective(UserAccount record);

    int updateByPrimaryKey(UserAccount record);

    List<selectAllvo> getIncomeOrPaylog(Map<Object,Object> map);

}