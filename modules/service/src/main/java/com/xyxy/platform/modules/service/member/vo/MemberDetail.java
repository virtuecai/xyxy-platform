package com.xyxy.platform.modules.service.member.vo;

import java.io.Serializable;
import java.util.List;

import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.entity.member.SpecificTag;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;

/**
 * 简介: 用于用户列表详情
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-03 22:56
 */
public class MemberDetail implements Serializable {

	private static final long serialVersionUID = -3220335824681740854L;

	//member对象信息
    private Member member;

    //关注人数
    private Integer countFollow;

    //是否认证
    private boolean isAuthentication;

    //图片: 头像,等
    private List<ImageItem> imageItemList;

    //标签列表
    private List<SpecificTag> tagList;

    //显示地址
	private List<SysArea>  address;
	
	//服务列表
	private List<GoodsDto> goods; 
	
	//资料完整度
	private int completeNo;


    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Integer getCountFollow() {
        return countFollow;
    }

    public void setCountFollow(Integer countFollow) {
        this.countFollow = countFollow;
    }

    public boolean isAuthentication() {
        return isAuthentication;
    }

    public void setIsAuthentication(boolean isAuthentication) {
        this.isAuthentication = isAuthentication;
    }

    public List<ImageItem> getImageItemList() {
        return imageItemList;
    }

    public void setImageItemList(List<ImageItem> imageItemList) {
        this.imageItemList = imageItemList;
    }

    public List<SpecificTag> getTagList() {
        return tagList;
    }

    public void setTagList(List<SpecificTag> tagList) {
        this.tagList = tagList;
    }

	public List<SysArea> getAddress() {
		return address;
	}

	public void setAddress(List<SysArea> address) {
		this.address = address;
	}

	public List<GoodsDto> getGoods() {
		return goods;
	}

	public void setGoods(List<GoodsDto> goods) {
		this.goods = goods;
	}

	public void setAuthentication(boolean isAuthentication) {
		this.isAuthentication = isAuthentication;
	}
	
	public int getCompleteNo() {
		return completeNo;
	}

	public void setCompleteNo(int completeNo) {
		this.completeNo = completeNo;
	}

	@Override
	public String toString() {
		return "MemberDetail [member=" + member + ", countFollow="
				+ countFollow + ", isAuthentication=" + isAuthentication
				+ ", imageItemList=" + imageItemList + ", tagList=" + tagList
				+ ", address=" + address + ", goods=" + goods + "]";
	}
    
}
