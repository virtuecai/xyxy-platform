package com.xyxy.platform.modules.repository.mybatis.sns;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.FeedBack;
import com.xyxy.platform.modules.entity.sns.FeedBackExample;

import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface FeedBackMapper {
    int countByExample(FeedBackExample example);

    int deleteByExample(FeedBackExample example);

    int deleteByPrimaryKey(Long msgId);

    int insert(FeedBack record);

    int insertSelective(FeedBack record);

    List<FeedBack> selectByExampleWithBLOBs(FeedBackExample example);

    List<FeedBack> selectByExample(FeedBackExample example);

    FeedBack selectByPrimaryKey(Long msgId);

    int updateByExampleSelective(@Param("record") FeedBack record, @Param("example") FeedBackExample example);

    int updateByExampleWithBLOBs(@Param("record") FeedBack record, @Param("example") FeedBackExample example);

    int updateByExample(@Param("record") FeedBack record, @Param("example") FeedBackExample example);

    int updateByPrimaryKeySelective(FeedBack record);

    int updateByPrimaryKeyWithBLOBs(FeedBack record);

    int updateByPrimaryKey(FeedBack record);

    /*自定义查询接口*/

    /**
     * 根据会员ID查询留言消息信息
     * @param memberId 会员ID
     * @return
     */
    List<HashMap> selectFeedBackPage(Long memberId);
}