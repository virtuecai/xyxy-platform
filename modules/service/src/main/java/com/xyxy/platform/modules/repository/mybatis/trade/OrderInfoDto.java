package com.xyxy.platform.modules.repository.mybatis.trade;

import java.util.List;

import com.xyxy.platform.modules.entity.trade.OrderInfo;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;
/**
 * 订单封装对象
 * @author zhaowei
 *
 */
public class OrderInfoDto {
	
	private OrderInfo orderInfo;//订单信息
	private List<GoodsDto> goodsDtoList; //订单商品信息
	
	public OrderInfo getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}
	public List<GoodsDto> getGoodsDtoList() {
		return goodsDtoList;
	}
	public void setGoodsDtoList(List<GoodsDto> goodsDtoList) {
		this.goodsDtoList = goodsDtoList;
	}
	
	@Override
	public String toString() {
		return "OrderInfoDto [orderInfo=" + orderInfo + ", goodsDtoList=" + goodsDtoList + "]";
	}
	
	
}
