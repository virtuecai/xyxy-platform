package com.xyxy.platform.modules.repository.mybatis.sns;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.MemberCollectionLog;
import com.xyxy.platform.modules.entity.sns.MemberCollectionLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberCollectionLogMapper {
    int countByExample(MemberCollectionLogExample example);

    int deleteByExample(MemberCollectionLogExample example);

    int deleteByPrimaryKey(Long collectionLog);

    int insert(MemberCollectionLog record);

    int insertSelective(MemberCollectionLog record);

    List<MemberCollectionLog> selectByExample(MemberCollectionLogExample example);

    MemberCollectionLog selectByPrimaryKey(Long collectionLog);

    int updateByExampleSelective(@Param("record") MemberCollectionLog record, @Param("example") MemberCollectionLogExample example);

    int updateByExample(@Param("record") MemberCollectionLog record, @Param("example") MemberCollectionLogExample example);

    int updateByPrimaryKeySelective(MemberCollectionLog record);

    int updateByPrimaryKey(MemberCollectionLog record);
}