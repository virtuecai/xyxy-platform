package com.xyxy.platform.modules.service.trade;

import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.service.trade.vo.CategoryTree;

import java.util.List;

/**
 * 简介: 商品分类
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-07 19:13
 */
public interface CategoryService {

    /**
     * 根据父级ID查询分类一级: 0
     * @param parentId 父级ID
     * @return
     */
    List<Category> findByParentId(Long parentId);

    Category findById(Long id);

    /**
     * 查询所有服务分类信息
     * @return
     */
    List<CategoryTree> findAllWithTree();
}
