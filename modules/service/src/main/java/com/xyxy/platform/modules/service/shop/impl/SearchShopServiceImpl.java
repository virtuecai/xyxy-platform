package com.xyxy.platform.modules.service.shop.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.service.shop.SearchShopService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-07
 */
@Service
@Transactional(readOnly = true)
public class SearchShopServiceImpl implements SearchShopService {


    /**
     * 根据关键词搜索星店列表
     * @param map{
     *
     * }  条件集合
     * @param keyword 关键词
     * @param pageNum 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @Override
    public PageInfo searchShopList(Map map, String keyword, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        // List list = memberCollectionMapper.selectGoodsPage(memberId);
        List list = new ArrayList();
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
}
