package com.xyxy.platform.modules.service.shop;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.member.Member;

import java.util.List;
import java.util.Map;

/**
 * 获取会员信息、获取个人主页会员介绍、获取会员头像、获取会员分页留言信息、获取个人主页推荐服务
 * 个人主页提交留言、粉丝与关注的人统计、职业标签和个人标签信息、关注人那些人也关注了他
 * @author liushun
 * @version 1.0
 * @Date 2016-01-05
 */
public interface ShopHomeService {


    /**
     * 获取会员信息
     * @param memberId 会员ID
     * @return
     */
    Member getMemberInfo(long memberId);


    /**
     * 获取个人主页会员介绍
     * @param articleId 文章ID
     * @return
     */
    String getMemberDesc(long articleId);

    /**
     * 获取个人主页会员头像
     * @param imageId 图片ID
     * @return
     */
    String getMemberImg(long imageId);

    /**
     * 获取个人主页分页留言信息
     * @param memberId 会员ID
     * @param pageNum 页数
     * @param pageSize 每页大小
     * @return
     */
    PageInfo getMessagePage(long memberId, Integer pageNum, Integer pageSize);

    /**
     * 获取个人主页推荐服务
     * @param memberId 会员ID
     * @return
     */
    List getGoodsList(long memberId);

    /**
     * 个人主页提交留言
     * @param toMemberId 浏览人ID
     * @param content 留言内容
     * @return
     */
    int submitMessage(long toMemberId, String content);

    /**
     * 个人主页统计粉丝和我关注的人
     * @param memberId 会员ID
     * @param toMemberId 被关会员ID
     * @return
     */
    Map getWithCount(long memberId, long toMemberId);

    /**
     * 个人主页获取职业标签和个人标签列表信息
     * @param memberId 会员ID
     * @return
     */
    Map getTagList(long memberId);

    /**
     * 个人主页获取我关注的人中 有那些人人也关注了他
     * 我们相同关注的人
     * @param memberId 会员ID
     * @param toMemberId 浏览人ID
     * @return
     */
    Map getMemberWithList(long memberId, long toMemberId);

}
