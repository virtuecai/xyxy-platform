package com.xyxy.platform.modules.service.sns;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.FeedBack;

import java.util.List;

/**
 * 提交留言、删除留言、获取留言列表
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */

public interface FeedBackService {

    /**
     * 提交留言
     * @param feedBack 留言实体类
     * @return
     */
    int subimtFeedBack(FeedBack feedBack);

    /**
     * 删除留言
     * @param msgId 留言ID
     * @return
     */
    int deleteFeedBack(long msgId);

    /**
     * 获取留言列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    PageInfo getFeedbackList(long memberId, Integer pageNumber, Integer pageSize);
}
