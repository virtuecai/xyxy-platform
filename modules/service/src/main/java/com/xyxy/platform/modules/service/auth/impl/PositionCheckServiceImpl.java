package com.xyxy.platform.modules.service.auth.impl;

import com.xyxy.platform.modules.service.auth.PositionCheckService;
import org.springframework.stereotype.Service;

/**
 * 简介: 职业认证
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-25 15:09
 */
@Service
public class PositionCheckServiceImpl implements PositionCheckService {
}
