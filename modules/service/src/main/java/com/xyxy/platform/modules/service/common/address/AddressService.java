package com.xyxy.platform.modules.service.common.address;

import java.util.List;

/**
 * @author liushun
 * @version 1.0
 * @Date 2015-12-30
 */
public interface AddressService {

    /**
     * 获取所有的省
     * @return
     */
    List getListProvince();

    /**
     * 根据省ID获取市
     * @param provinceId 省ID
     * @return
     */
    List getListCity(Integer provinceId);

    /**
     * 根据市ID获取区域
     * @param cityId 市ID
     * @return
     */
    List getListArea(Integer cityId);
    
    /**
     * 简介: 根据子节点id获取全路径集合 例如  list{  "一级地址" , "二级地址" , "查询子节点地址" }
     * @author zhaowei
     * @Date 2016年1月6日 下午2:25:41
     * @version 1.0
     *
     * @param noteId
     * @return
     */
    List getPathArea(Integer noteId );

    /**
     * 拼接地址字符串
     * @param addressId 地址ID
     * @param type 1、只显示省 2、只显示省市 3、只显示省市区
     * @param str 拼接字符
     * @return
     */
    String getAddressStr(Integer addressId, int type, String str);
}
