package com.xyxy.platform.modules.repository.mybatis.sns;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.MemberFollowLog;
import com.xyxy.platform.modules.entity.sns.MemberFollowLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberFollowLogMapper {
    int countByExample(MemberFollowLogExample example);

    int deleteByExample(MemberFollowLogExample example);

    int deleteByPrimaryKey(Long followLogId);

    int insert(MemberFollowLog record);

    int insertSelective(MemberFollowLog record);

    List<MemberFollowLog> selectByExample(MemberFollowLogExample example);

    MemberFollowLog selectByPrimaryKey(Long followLogId);

    int updateByExampleSelective(@Param("record") MemberFollowLog record, @Param("example") MemberFollowLogExample example);

    int updateByExample(@Param("record") MemberFollowLog record, @Param("example") MemberFollowLogExample example);

    int updateByPrimaryKeySelective(MemberFollowLog record);

    int updateByPrimaryKey(MemberFollowLog record);
}