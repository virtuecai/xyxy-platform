package com.xyxy.platform.modules.service.sns;
/**
 * 服务关注服务类
 * @author zhaowei
 */
public interface GoodFollowService {
	/**
	 * 简介:根据商品id获取关注数量
	 * @author zhaowei
	 * @Date 2016年1月6日 下午4:42:01
	 * @version 1.0
	 *
	 * @param memberId
	 * @return
	 */
	int countGoodFollowByGoodId(long goodId);
}
