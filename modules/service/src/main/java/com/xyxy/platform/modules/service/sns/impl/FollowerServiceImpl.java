package com.xyxy.platform.modules.service.sns.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.sns.MemberFollow;
import com.xyxy.platform.modules.entity.sns.MemberFollowExample;
import com.xyxy.platform.modules.repository.mybatis.member.MemberMapper;
import com.xyxy.platform.modules.repository.mybatis.sns.MemberFollowMapper;
import com.xyxy.platform.modules.service.sns.FollowerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 查询粉丝列表、我的关注列表、添加或取消关注
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */

@Service
@Transactional(readOnly = true)
public class FollowerServiceImpl implements FollowerService {

    @Autowired
    private MemberFollowMapper memberFollowMapper;

    @Override
    public int countFollower(long memberId) {
        MemberFollowExample memberFollowExample = new MemberFollowExample();
        memberFollowExample.or().andToMemberIdEqualTo(memberId);
        return memberFollowMapper.countByExample(memberFollowExample);
    }

    /**
     * 获取粉丝列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @Override
    public PageInfo getFollowerPage(long memberId, Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List list = memberFollowMapper.selectFollowPage(memberId);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    /**
     * 查询我关注的用户列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @Override
    public PageInfo getWithPage(long memberId, Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List list = memberFollowMapper.selectWithPage(memberId);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    /**
     * 添加关注
     * @param memberFollow 实体
     * @return
     */
    @Transactional(readOnly = false)
    @Override
    public int addWith(MemberFollow memberFollow) {
        /*根据被关注会员查询是否已存在关注*/
        int flag = memberFollowMapper.selectIsToMemberFollow(memberFollow.getMemberId(), memberFollow.getToMemberId());
        if(flag>0)
            return 0;
        else {
            int result = memberFollowMapper.insert(memberFollow);
            //TODO 调用 DAO 把操作添加到会员关注日志表
            return result;
        }
    }


    /**
     * 取消关注
     * @param followId 关注ID
     * @return
     */
    @Transactional(readOnly = false)
    @Override
    public int delWith(long followId, long memberId) {
        int result = memberFollowMapper.deleteByPrimaryKey(followId);
        //TODO 调用 DAO 把操作添加到会员关注日志表
        return result;
    }

    @Override
    public boolean isFollow(long memberId, long toMemberId) {
        Integer result = memberFollowMapper.getIsFollow(memberId, toMemberId);
        if(result!=null&&result>0)
            return true;
        return false;
    }

    @Override
    public int fansNewCount(long memberId) {
        //fansNewCount
        String currDate = "";
        Calendar c = Calendar.getInstance();
        currDate += c.get(Calendar.YEAR) + "-";
        currDate += (c.get(Calendar.MONTH)+1)+"-";
        currDate += c.get(Calendar.DAY_OF_WEEK);
        currDate += " 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = null;
        try {
            startDate = sdf.parse(currDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = new Date(startDate.getTime() + (24*60*60*1000-1));

        MemberFollowExample followExample = new MemberFollowExample();
        followExample.or().andToMemberIdEqualTo(memberId)
                .andCreateTimeBetween(startDate, endDate);

        return memberFollowMapper.countByExample(followExample);
    }


}
