package com.xyxy.platform.modules.service.finance.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.xyxy.platform.modules.core.utils.BeanUtilsExt;
import com.xyxy.platform.modules.entity.finance.AccountLog;
import com.xyxy.platform.modules.entity.finance.Payment;
import com.xyxy.platform.modules.entity.finance.UserAccount;
import com.xyxy.platform.modules.entity.finance.selectAllvo;
import com.xyxy.platform.modules.repository.mybatis.finance.AccountLogMapper;
import com.xyxy.platform.modules.repository.mybatis.finance.PaymentMapper;
import com.xyxy.platform.modules.repository.mybatis.finance.UserAccountMapper;
import com.xyxy.platform.modules.service.finance.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * FinanceServiceImpl
 *
 * @author yangdianjun
 * @date 2015/12/21 0021
 */
@Service("financeService")
public class FinanceServiceImpl implements FinanceService {

    @Autowired
    private AccountLogMapper accountDao;
    @Autowired
    private PaymentMapper paymentDao;
    @Autowired
    private UserAccountMapper userAccountDao;



    @Override
    public AccountLog getMyAccount(Long memberId) {
        return accountDao.selectByMemberId(memberId);
    }

    @Override
    public PageInfo getIncomeOrPaylog(Long memberId,  Integer action, Integer isPaid,Integer pageNumber,Integer pageSize) {
        Map<Object,Object> map= Maps.newHashMap();
        map.put("memberId",memberId); map.put("action",action); map.put("isPaid", isPaid);
        List<selectAllvo> list = userAccountDao.getIncomeOrPaylog(map);
        PageHelper.startPage(pageNumber, pageSize);
        for(selectAllvo u:list){
            if(u.getPaymentId()==2){//这条记录使用的是余额支付。
                u.setIsYue(0);
            }else{
                u.setIsYue(1);
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return  pageInfo;
    }

    @Override
    public List<Payment> getPayment() {
        return paymentDao.selectAll();
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserAccount insertUseraccount(UserAccount userAccount) {
        userAccount.setCreateTime(new Date());
        //获取当前用户的余额。
        BigDecimal sum=accountDao.selectMaxMoney(userAccount.getMemberId());
        if(sum==null) sum=BigDecimal.valueOf(0);
        AccountLog source=accountDao.selectByMemberId(userAccount.getMemberId());
        AccountLog target=new AccountLog();
        target.setCreateTime(userAccount.getCreateTime());
        target.setMemberId(userAccount.getMemberId());
        target.setNote(userAccount.getAdminNote());
        target.setAction(userAccount.getAction());
        if(userAccount.getIsYue()==1&&(sum.compareTo(BigDecimal.ZERO)>0)){//余额支付
            //账户余额计算
            BigDecimal usermoney=source.getUserMoney().subtract(userAccount.getAmount());
            if(usermoney.compareTo(BigDecimal.valueOf(0))==-1){//传过来的金额大于余额 跳出操作
                return null;
            }
            if(userAccount.getAction()==0){
                //没有付款,只添加当前操作的款数,不进行余额的加减
                target.setMoney(userAccount.getAmount());
                target.setUserMoney(sum);
            }else if(userAccount.getAction()==1){
                //已付款,进余额计算
                target.setMoney(userAccount.getAmount().subtract(new BigDecimal(-1)));
                target.setUserMoney(sum.subtract(userAccount.getAmount()));
            }
        }else if(userAccount.getIsYue()==1){//其他方式支付
            if(userAccount.getAction()==0){
                target.setMoney(userAccount.getAmount());
                target.setUserMoney(sum);
                }else if(userAccount.getAction()==1){
                target.setMoney(userAccount.getAmount().subtract(new BigDecimal(-1)));
                target.setUserMoney(sum);
            }
        }
        //操作类型1)充值    2)付款    3)退款
        if(userAccount.getAction()==1){
            target.setType(0);
        }else{
            target.setType(1);
        }
        userAccountDao.insert(userAccount);
        if(userAccount.getIsPaid()==1 && userAccount.getPaymentId()==2)
        accountDao.insert(target);
        return userAccount;
    }

    @Override
    public UserAccount updateUseraccount(UserAccount userAccount) {
        if(userAccount==null) return  null;
        UserAccount entity = userAccountDao.selectByPrimaryKey(userAccount.getAccountId());
        if(entity==null) return null;
        BeanUtils.copyProperties(userAccount, entity, BeanUtilsExt.getNullPropertyNames(userAccount));
        userAccount.setUpdateTime(new Date());
        userAccount.setIsPaid(1);
        userAccountDao.updateByPrimaryKeySelective(userAccount);
        return userAccount;
    }

}
