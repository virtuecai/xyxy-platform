package com.xyxy.platform.modules.service.sns.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.sns.FeedBack;
import com.xyxy.platform.modules.repository.mybatis.sns.FeedBackMapper;
import com.xyxy.platform.modules.service.sns.FeedBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 提交留言、删除留言、获取留言列表
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */

@Service
@Transactional(readOnly = true)
public class FeedBackServiceImpl implements FeedBackService {

    @Autowired
    private FeedBackMapper feedBackMapper;
    /**
     * 提交留言
     * @param feedBack 留言实体类
     * @return
     */
    @Transactional(readOnly = false)
    @Override
    public int subimtFeedBack(FeedBack feedBack) {
        int result = feedBackMapper.insert(feedBack);
        //TODO 预留日志消息
        return result;
    }

    /**
     * 删除留言
     * @param msgId 留言消息ID
     * @return
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteFeedBack(long msgId) {
        int result = feedBackMapper.deleteByPrimaryKey(msgId);
        //TODO 预留日志消息
        return result;
    }

    /**
     * 获取留言列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    @Override
    public PageInfo getFeedbackList(long memberId, Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List list = feedBackMapper.selectFeedBackPage(memberId);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
}
