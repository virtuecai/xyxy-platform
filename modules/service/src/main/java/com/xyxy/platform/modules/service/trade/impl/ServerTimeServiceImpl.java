package com.xyxy.platform.modules.service.trade.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyxy.platform.modules.entity.trade.Goods;
import com.xyxy.platform.modules.entity.trade.ServerTime;
import com.xyxy.platform.modules.entity.trade.ServerTimeExample;
import com.xyxy.platform.modules.repository.mybatis.trade.ServerTimeMapper;
import com.xyxy.platform.modules.service.trade.GoodsService;
import com.xyxy.platform.modules.service.trade.ServerTimeService;

/**
 * 服务时段服务实现类
 * 
 * @author zhaowei
 */
@Service
public class ServerTimeServiceImpl implements ServerTimeService {

	@Autowired
	private ServerTimeMapper serverTimeMapper;
	@Autowired
	private GoodsService goodsService;

	@Override
	public String selectAvailable(String goodsSn, Date startDate, Date endDate) {
		Goods goods = goodsService.selectGoodsByGoodsId(goodsSn);
		if(goods == null){
			return "";
		}
		// 获取数据库配置
		List<String> resultList = new ArrayList<String>();
		Date tempDate = startDate;
		Calendar c = Calendar.getInstance();
		boolean isFirstSunday = false;
		int weekDay = 0;
		while (tempDate.getTime() < endDate.getTime()) {
			// 根据商品id 和 星期几获取服务时段
			c.setTime(tempDate);
			isFirstSunday = (c.getFirstDayOfWeek() == Calendar.SUNDAY);
			// 获取周几
			weekDay = c.get(Calendar.DAY_OF_WEEK);
			if (isFirstSunday) {
				weekDay = weekDay - 1;
				if (weekDay == 0) {
					weekDay = 7;
				}
			}
			resultList.add(selectServerTimeSetting(goods.getGoodsId(), weekDay));
			tempDate.setTime(tempDate.getTime() + 24 * 60 * 60 * 1000);
		}
		
		//拼装可预约时间下标串  例如： 1,5,18
		int count = resultList.size();
		int index = 0 ;
	    String str = null ;
	    StringBuffer strBuf = new StringBuffer();
		for(int i=0;i<5;i++){
			for(int y=0;y<count ;y++){
				str = String.valueOf(resultList.get(y).charAt(i));
				if("1".equals(str)){
					strBuf.append(index).append(",");
				}
				index ++ ;
			}
		}
		if(strBuf.length() > 0){
			if(strBuf.indexOf(",") != -1){
				return strBuf.substring(0, strBuf.length() -1);
			}
		}
		return "";
	}

	@Override
	public String selectServerTimeSetting(long goodsId, int weekDay) {
		ServerTimeExample serverTimeExample = new ServerTimeExample();
		serverTimeExample.or().andGoodsIdEqualTo(goodsId);
		List<ServerTime> resultList = serverTimeMapper.selectByExample(serverTimeExample);
		if (resultList == null || resultList.size() == 0) {
			return null;
		}
		ServerTime serverTime = resultList.get(0);
		switch (weekDay) {
		case 1:
			return serverTime.getTime1();
		case 2:
			return serverTime.getTime2();
		case 3:
			return serverTime.getTime3();
		case 4:
			return serverTime.getTime4();
		case 5:
			return serverTime.getTime5();
		case 6:
			return serverTime.getTime6();
		case 7:
			return serverTime.getTime7();
		default:
			return null;
		}
	}

	
}
