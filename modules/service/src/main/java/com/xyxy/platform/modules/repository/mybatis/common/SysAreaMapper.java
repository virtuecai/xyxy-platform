package com.xyxy.platform.modules.repository.mybatis.common;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.entity.common.SysAreaExample;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface SysAreaMapper {
    int countByExample(SysAreaExample example);

    int deleteByExample(SysAreaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysArea record);

    int insertSelective(SysArea record);

    List<SysArea> selectByExample(SysAreaExample example);

    SysArea selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysArea record, @Param("example") SysAreaExample example);

    int updateByExample(@Param("record") SysArea record, @Param("example") SysAreaExample example);

    int updateByPrimaryKeySelective(SysArea record);

    int updateByPrimaryKey(SysArea record);

    /**
     * 获取全部省份
     * @return
     */
    List<HashMap> selectAllProvince();

    /**
     * 根据省份ID获取城市列表
     * @param provinceId 省份ID
     * @return
     */
    List<HashMap> selectCityList(Integer provinceId);

    /**
     * 根据城市ID获取城市列表
     * @param cityId 城市ID
     * @return
     */
    List<HashMap> selectAreaList(Integer cityId);

    Map findAddressStr(int addressId);
}