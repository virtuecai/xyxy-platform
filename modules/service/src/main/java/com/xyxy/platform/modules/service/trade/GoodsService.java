package com.xyxy.platform.modules.service.trade;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.entity.trade.Goods;
import com.xyxy.platform.modules.service.trade.vo.GoodsDto;

import java.util.List;
import java.util.Map;

/**
 * 商品服务类
 * @author zhaowei
 */
public interface GoodsService {

	/**
	 * 根据 主键 ID 查询
	 * @param id
	 * @return
	 */
	Goods selectByPrimaryId(Long id);

	/**
	 * 简介: 根据产品编号获取产品信息
	 * @author zhaowei
	 * @Date 2015年12月22日 下午4:24:48
	 * @version 1.0
	 *
	 * @param goodsSn
	 * @return
	 */
	Goods selectGoodsByGoodsId(String goodsSn);

	/**
	* 简介: 根据产品编号获取产品基本信息
	* @author zhaowei
	* @Date 2016年1月8日 下午4:43:41
	* @version 1.0
	* @param goodsSn
	* @return
	 */
	GoodsDto selectGoodsDtoBySn(String goodsSn);

	/**
	* 简介: 根据产品编号获取产品详细信息
	* @author zhaowei
	* @Date 2016年1月8日 下午4:43:41
	* @version 1.0
	* @param goodsSn
	* @return
	 */
	GoodsDto selectGoodsDtoInfoBySn(String goodsSn);

	/**
	 * 简介:发布服务
	 * @author zhaowei
	 * @Date 2016年1月4日 下午6:46:01
	 * @version 1.0
	 *
	 * @param goodsDto
	 * @return
	 */
	boolean saveGoods(GoodsDto goodsDto)  throws RuntimeException;

	/**
	 * 更新服务信息
	 * @author caizhengda
	 * @param goodsDto
	 * @return
	 * @throws RuntimeException
	 */
	boolean updateGoods(GoodsDto goodsDto) throws RuntimeException;

	/**
	 * 简介:根据id获取商品分类
	 * @author zhaowei
	 * @Date 2016年1月4日 下午7:07:29
	 * @version 1.0
	 *
	 * @param id
	 * @return
	 */
	Category selectCategoryById(long id);

	/**
	 * 简介: 根据条件获取推荐服务列表
	 * @author zhaowei
	 * @Date 2016年1月5日 下午6:41:01
	 * @version 1.0
	 * @updator: caizhengda
	 * @param params {
	 * 					categoryId:服务分类id  ,
	 * 					cityId:城市id ,
	 * 					serverType:服务类型id 1）上门服务 2）约定地点服务3）线上服务4）邮寄 ,
	 * 					pageNum:页数 ,
	 * 					pageSize:每页条数
	 * 				 }
	 * @return
	 */
	PageInfo<GoodsDto> selectBestGoodsByMap(Map<String, Object> params);

	/**
	 * 简介: 根据条件获取最热服务列表
	 * @author zhaowei
	 * @Date 2016年1月5日 下午6:41:01
	 * @version 1.0
	 * @updator: caizhengda
	 * @param params {
	 * 					categoryId:服务分类id  ,
	 * 					cityId:城市id ,
	 * 					serverType:服务类型id 1）上门服务 2）约定地点服务3）线上服务4）邮寄 ,
	 * 					page:页数 ,
	 * 					rows:每页条数
	 * 				 }
	 * @return
	 */
	PageInfo<GoodsDto> selectHotGoodsByMap(Map<String, Object> params);

	/**
	 * 简介: 根据条件获取最新服务列表
	 * @author zhaowei
	 * @Date 2016年1月5日 下午6:41:01
	 * @version 1.0
	 * @updator: caizhengda
	 * @param params {
	 * 					categoryId:服务分类id  ,
	 * 					cityId:城市id ,
	 * 					serverType:服务类型id 1）上门服务 2）约定地点服务3）线上服务4）邮寄 ,
	 * 					pageNum:页数 ,
	 * 					pageSize:每页条数,
	 * 					memberId:会员id
	 * 				 }
	 * @return
	 */
	PageInfo<GoodsDto> selectNewGoodsByMap(Map<String, Object> params);

	/**
	 * 根据会员id获取服务列表
	 * 简介:
	 * @author zhaowei
	 * @Date 2016年1月6日 下午5:00:42
	 * @version 1.0
	 *
	 * @param memberId
	 * @return
	 */
	PageInfo<GoodsDto> selectGoodsByMemberId(long memberId);

	/**
	 * selectByGoodsId
	 * @param id
	 * @return
	 *List<Goods>
	 * @exception
	 * @since  1.0.0
	*/
	PageInfo<Goods> selectByGoodsId(long id);

	/**
	* 简介: 根据商品序列号获取 售出数量
	* @author zhaowei
	* @Date 2016年1月8日 下午5:23:22
	* @version 1.0
	* @param goodsSn
	* @return
	 */
	int selectSallGoodsCount(String goodsSn);

	/**
	 * 获取发布的服务次数
	 * @return
	 */
	int countPublishGoods();

	/**
	* 简介: 根据订单id获取商品列表
	* @author zhaowei
	* @Date 2016年1月12日 下午7:23:25
	* @version 1.0
	* @param orderId
	* @return
	 */
	List<GoodsDto> selectGoodsDtoByOrderId(long orderId);

	/**
	 * 根据每页条数获取总页数
	 * @param pageSize
	 * @return
	 */
	Integer countGoodsPage();

	/**
	 * 分页查询, 目前实现
	 * @param pageNum
	 * @param pageSize
	 * @param params
	 * @return
	 */
	List<Goods> selectByGoods(Integer pageNum, Integer pageSize, Goods params);

	/**
	 * 更新服务基本信息 根据服务ID, 用户ID
	 * @param goodsId 服务ID
	 * @param memberId 用户ID
	 * @return
	 */
	boolean update(Long goodsId, Long memberId, Goods params);

	/**
	 * 根据 服务ID , 会员ID 判断该服务是否存在
	 * @param goodsId 服务ID
	 * @param memberId 会员ID
	 * @return
	 */
	boolean isExist(Long goodsId, Long memberId);

	/**
	 * 根据服务ID和会员ID 查询服务
	 * @param goodsId 服务ID
	 * @param memberId 会员ID
	 * @return
	 */
	Goods findByIdAndMemberId(Long goodsId, Long memberId);
}
