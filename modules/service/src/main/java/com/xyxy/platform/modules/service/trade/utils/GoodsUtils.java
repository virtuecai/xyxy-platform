package com.xyxy.platform.modules.service.trade.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 商品工具类
 * 
 * @author zaowei
 */
public class GoodsUtils {

	final static String sn = "gd";

	/**
	 * 简介: 生成订单流水号 ，暂时用最简单的方案
	 * 
	 * @author zhaowei
	 * @Date 2016年1月5日 上午8:29:20
	 * @version 1.0
	 *
	 */
	public static synchronized String getGoodsSn(){
		String str = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		str += sn;
		str += sdf.format(new Date());
		int[] array = {0,1,2,3,4,5,6,7,8,9};
		Random rand = new Random();
		for (int i = 10; i > 1; i--) {
		    int index = rand.nextInt(i);
		    int tmp = array[index];
		    array[index] = array[i - 1];
		    array[i - 1] = tmp;
		}
		int result = 0;
		for(int i = 0; i < 7; i++)
		    result = result * 10 + array[i];
		
		str += String.format("%07d", result);
		return str;
	}

	
}
