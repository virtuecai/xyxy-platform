package com.xyxy.platform.modules.service.trade;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.trade.GoodsComment;

/**
 * 服务评价服务
 * @author zhaowei
 */
public interface GoodsCommentService {
	/**
	* 简介: 根据商品id获取评价数量 
	* @author zhaowei
	* @Date 2016年1月9日 上午11:42:43
	* @version 1.0
	* @param goodsId
	* @return
	 */
	int selectCountByGoodsId(long goodsId);
	
	/**
	* 简介: 根据商品id获取最新评论列表
	* @author zhaowei
	* @Date 2016年1月11日 下午7:48:59
	* @version 1.0
	* @param goodsId
	* @return
	 */
	PageInfo<GoodsComment> selectNewListGoodsId(long goodsId,int pageNum,int pageSize);
}
