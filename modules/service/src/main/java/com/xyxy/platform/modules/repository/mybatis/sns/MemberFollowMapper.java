package com.xyxy.platform.modules.repository.mybatis.sns;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.MemberFollow;
import com.xyxy.platform.modules.entity.sns.MemberFollowExample;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;


@MyBatisRepository
public interface MemberFollowMapper {
    int countByExample(MemberFollowExample example);

    int deleteByExample(MemberFollowExample example);

    int deleteByPrimaryKey(Long followId);

    int insert(MemberFollow record);

    int insertSelective(MemberFollow record);

    List<MemberFollow> selectByExample(MemberFollowExample example);

    MemberFollow selectByPrimaryKey(Long followId);

    int updateByExampleSelective(@Param("record") MemberFollow record, @Param("example") MemberFollowExample example);

    int updateByExample(@Param("record") MemberFollow record, @Param("example") MemberFollowExample example);

    int updateByPrimaryKeySelective(MemberFollow record);

    int updateByPrimaryKey(MemberFollow record);


    /**
     * 判断会员id是否被关注
     * @param memberId 会员ID
     * @param toMemberId 被关注会员ID
     * @return
     */
    int selectIsToMemberFollow(@Param("memeberId") long memberId, @Param("toMemberId") long toMemberId);

    /**
     * 根据会员ID查询粉丝消息
     * @param toMemberId 会员ID
     * @return
     */
    List<HashMap> selectFollowPage(Long toMemberId);

    /**
     * 根据会员ID查询我关注粉丝列表
     * @param memberId 会员ID
     * @return
     */
    List<HashMap> selectWithPage(Long memberId);

    /**
     * 根据会员ID 查询该会员被关注数量
     * @param toMemberId 被关注的会员ID
     * @return
     */
    Integer countByToMemberId(Long toMemberId);

    /**
     * 判断是否已经关注
     * @param memberId 会员ID
     * @param toMemberId 关注会员ID
     * @return
     */
    Integer getIsFollow(@Param("memberId") long memberId, @Param("toMemberId") Long toMemberId);
}