package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.entity.member.MemberSpecificTag;
import com.xyxy.platform.modules.entity.member.MemberSpecificTagExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberSpecificTagMapper {
    int countByExample(MemberSpecificTagExample example);

    int deleteByExample(MemberSpecificTagExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MemberSpecificTag record);

    int insertSelective(MemberSpecificTag record);

    List<MemberSpecificTag> selectByExample(MemberSpecificTagExample example);

    MemberSpecificTag selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MemberSpecificTag record, @Param("example") MemberSpecificTagExample example);

    int updateByExample(@Param("record") MemberSpecificTag record, @Param("example") MemberSpecificTagExample example);

    int updateByPrimaryKeySelective(MemberSpecificTag record);

    int updateByPrimaryKey(MemberSpecificTag record);

    MemberSpecificTag selectByTagName(MemberSpecificTag memberSpecificTag);
}