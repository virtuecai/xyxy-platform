package com.xyxy.platform.modules.service.trade.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.IsDelete;
import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.entity.trade.CategoryExample;
import com.xyxy.platform.modules.repository.mybatis.trade.CategoryMapper;
import com.xyxy.platform.modules.service.trade.CategoryService;
import com.xyxy.platform.modules.service.trade.vo.CategoryTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 简介: 商品分类 服务实现
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-07 19:20
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> findByParentId(Long parentId) {
        if (null == parentId) parentId = 0l;
        CategoryExample sql = new CategoryExample();
        sql.or()
                .andPidEqualTo(parentId)
                .andIsDelEqualTo(IsDelete.NO.ordinal());
        List<Category> categoryList = categoryMapper.selectByExample(sql);
        return Collections3.isNotEmpty(categoryList) ? categoryList : Lists.<Category>newArrayList();
    }

    @Override
    public Category findById(Long id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CategoryTree> findAllWithTree() {
        List<CategoryTree> list = categoryMapper.findTreeAll();
        return Collections3.isEmpty(list) ? Lists.<CategoryTree>newArrayList() : list;
    }
}
