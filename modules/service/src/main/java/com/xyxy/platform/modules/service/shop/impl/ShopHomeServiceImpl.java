package com.xyxy.platform.modules.service.shop.impl;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.content.Article;
import com.xyxy.platform.modules.entity.image.ImageItem;
import com.xyxy.platform.modules.entity.image.ImageTypes;
import com.xyxy.platform.modules.entity.member.Member;
import com.xyxy.platform.modules.entity.sns.FeedBack;
import com.xyxy.platform.modules.repository.mybatis.content.ArticleMapper;
import com.xyxy.platform.modules.repository.mybatis.member.MemberMapper;
import com.xyxy.platform.modules.repository.mybatis.member.ProfessionMapper;
import com.xyxy.platform.modules.repository.mybatis.member.SpecificTagMapper;
import com.xyxy.platform.modules.service.common.address.AddressService;
import com.xyxy.platform.modules.service.content.ArticleService;
import com.xyxy.platform.modules.service.content.impl.ArticleServiceImpl;
import com.xyxy.platform.modules.service.image.ImageItemService;
import com.xyxy.platform.modules.service.member.MemberService;
import com.xyxy.platform.modules.service.shop.ShopHomeService;
import com.xyxy.platform.modules.service.sns.FeedBackService;
import com.xyxy.platform.modules.service.sns.FollowerService;
import com.xyxy.platform.modules.service.trade.GoodsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-01-05
 */
@Service
@Transactional(readOnly = true)
public class ShopHomeServiceImpl implements ShopHomeService {

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private ImageItemService imageItemService;

    @Autowired
    private FeedBackService feedBackService;

    @Autowired
    private FollowerService followService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private SpecificTagMapper specificTagMapper;

    @Autowired
    private ProfessionMapper professionMapper;

    @Autowired
    private AddressService addressService;

    /**
     * 获取会员信息
     * @param memberId 会员ID
     * @return
     */
    @Override
    public Member getMemberInfo(long memberId) {
        Member member = memberMapper.selectByPrimaryKey(memberId);
        if (member != null) {
            if (StringUtils.isEmpty(member.getAddress()))
                return member;
            else {
                member.setAddress(addressService.getAddressStr(Integer.parseInt(member.getAddress()), 3, "-"));
                return member;
            }
        }else
            return null;
    }

    /**
     * 获取个人主页会员介绍
     * @param articleId 文章ID
     * @return
     */
    @Override
    public String getMemberDesc(long articleId) {
        Article article = articleService.selectById(articleId);
        return article == null?"":article.getContent();
    }

    /**
     * 获取个人主页会员头像
     * @param imageId 图片ID
     * @return
     */
    @Override
    public String getMemberImg(long imageId) {
        List<ImageItem> imageItemList = imageItemService.findImageItemList(imageId, ImageTypes.USER_ICON);
        if(Collections3.isNotEmpty(imageItemList))
            return imageItemList.get(0).getUri();
        else
            return "";
    }

    /**
     * 获取个人主页分页留言信息
     * @param memberId 会员ID
     * @param pageNum 页数
     * @param pageSize 每页大小
     * @return
     */
    @Override
    public PageInfo getMessagePage(long memberId, Integer pageNum, Integer pageSize) {
        return feedBackService.getFeedbackList(memberId, pageNum, pageSize);
    }

    /**
     * 获取个人主页推荐服务
     * @param memberId 会员ID
     * @return
     */
    @Override
    public List getGoodsList(long memberId) {
        return goodsService.selectGoodsByMemberId(memberId).getList();
    }

    /**
     * 个人主页提交留言
     * @param toMemberId 浏览人ID
     * @param content 留言内容
     * @return
     */
    @Override
    public int submitMessage(long toMemberId, String content) {
        //TODO 提交留言
        FeedBack feedBack = new FeedBack();
//        feedBack.setMemberId();
//        feedBack.setMsgContent(content);
//        feedBack.setMsgType();
        return feedBackService.subimtFeedBack(feedBack);
    }

    /**
     * 个人主页统计粉丝和我关注的人
     * @param memberId 会员ID
     * @param toMemberId 被关会员ID
     * @return
     */
    @Override
    public Map getWithCount(long memberId, long toMemberId) {
        Map map = new HashMap();
        //TODO 判断是否已经关注 Boolean
        followService.isFollow(memberId, toMemberId);
        map.put("isGz", true);
        //TODO 获取粉丝数统计 Int
        map.put("fsCount",58);
        //TODO 获取关注数统计 Int
        map.put("gzCount",10);
        return map;
    }

    /**
     * 个人主页获取职业标签和个人标签列表信息
     * @param memberId 会员ID
     * @return
     */
    @Override
    public Map getTagList(long memberId) {
        Map map = new HashMap();
        //TODO 职业标签 List
        List zy_list = new ArrayList();
        map.put("zyTag",professionMapper.findProfessionTags(memberId));
        //TODO 个人标签 List
        List gr_list = new ArrayList();
        map.put("grTag",specificTagMapper.findSpecificTags(memberId));
        return map;
    }

    /**
     * 个人主页获取我关注的人中 有那些人人也关注了他
     * @param memberId 会员ID
     * @param toMemberId 浏览人ID
     * @return
     */
    @Override
    public Map getMemberWithList(long memberId, long toMemberId) {
        Map map = new HashMap();
        //TODO 我关注的人多少人关注了他
        List qt_list = new ArrayList();
        map.put("qtGZList",qt_list);
        //TODO 共同关注的人
        List gt_list = new ArrayList();
        map.put("gtGzList", gt_list);
        return map;
    }
}
