package com.xyxy.platform.modules.service.shop;

import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * 获取星店分页列表、获取职业标签列表、根据标签ID查询标签
 * @author liushun
 * @version 1.0
 * @Date 2016-01-07
 */
public interface ShopService {

    /**
     * 根据条件搜索星店
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param parMap 条件集合
     * @param type 1、推荐 2、最新
     * @return
     */
    PageInfo getShopList(String keyword, Integer pageNum, Integer pageSize , Map<String, Object> parMap, Integer type);

    /**
     * 获取职业标签列表
     * @param tagId 标签ID
     * @param number 取多少条记录
     * @param isToParent 判断是否为顶级标签
     * @return
     */
    List getSpecificTags(long tagId,Integer number,boolean isToParent);

    /**
     * 根据标签ID获取标签map{
     *  tagId: 标签ID
     *  tagNam: 标签名称
     * }
     * @param tagId 标签ID
     * @return
     */
    Map getSpecificTag(long tagId);
}
