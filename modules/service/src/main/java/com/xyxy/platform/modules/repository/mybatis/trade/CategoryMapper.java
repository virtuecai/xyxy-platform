package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.Category;
import com.xyxy.platform.modules.entity.trade.CategoryExample;

import java.util.List;

import com.xyxy.platform.modules.service.trade.vo.CategoryTree;
import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface CategoryMapper {
    int countByExample(CategoryExample example);

    int deleteByExample(CategoryExample example);

    int deleteByPrimaryKey(Long categoryId);

    int insert(Category record);

    int insertSelective(Category record);

    List<Category> selectByExample(CategoryExample example);

    Category selectByPrimaryKey(Long categoryId);

    int updateByExampleSelective(@Param("record") Category record, @Param("example") CategoryExample example);

    int updateByExample(@Param("record") Category record, @Param("example") CategoryExample example);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);

    List<CategoryTree> findTreeAll();
}