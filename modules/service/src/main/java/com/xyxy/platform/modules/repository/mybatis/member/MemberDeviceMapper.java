package com.xyxy.platform.modules.repository.mybatis.member;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.member.MemberDevice;
import com.xyxy.platform.modules.entity.member.MemberDeviceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface MemberDeviceMapper {
    int countByExample(MemberDeviceExample example);

    int deleteByExample(MemberDeviceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MemberDevice record);

    int insertSelective(MemberDevice record);

    List<MemberDevice> selectByExample(MemberDeviceExample example);

    MemberDevice selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MemberDevice record, @Param("example") MemberDeviceExample example);

    int updateByExample(@Param("record") MemberDevice record, @Param("example") MemberDeviceExample example);

    int updateByPrimaryKeySelective(MemberDevice record);

    int updateByPrimaryKey(MemberDevice record);
}