package com.xyxy.platform.modules.service.trade.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.trade.*;
import com.xyxy.platform.modules.repository.mybatis.trade.GoodsServerTagMapper;
import com.xyxy.platform.modules.repository.mybatis.trade.ServerTagsMapper;
import com.xyxy.platform.modules.service.trade.ServerTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 简介: 服务标签实现
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-16 23:05
 */
@Service
@Transactional(readOnly = true)
public class ServerTagServiceImpl implements ServerTagService {

    /**
     * 服务,标签 关联
     */
    @Autowired
    private GoodsServerTagMapper goodsServerTagMapper;

    /**
     * 服务标签表
     */
    @Autowired
    private ServerTagsMapper serverTagsMapper;

    @Override
    public List<ServerTags> findByGoodsId(Long goodsId) {
        //服务标签管理表查询
        GoodsServerTagExample serverTagExample = new GoodsServerTagExample();
        serverTagExample.or().andGoodsIdEqualTo(goodsId);
        List<GoodsServerTag> goodsServerTagList = goodsServerTagMapper.selectByExample(serverTagExample);

        //查询具体服务
        List<ServerTags> result = Lists.newArrayList();
        if (Collections3.isNotEmpty(goodsServerTagList)) {
            for (GoodsServerTag goodsServerTag : goodsServerTagList) {
                ServerTags tag = this.findById(goodsServerTag.getServerTagId1());
                if (null != tag) result.add(tag);
            }
        }
        return result;
    }

    @Override
    public ServerTags findById(Long serverTagId) {
        ServerTagsExample sql = new ServerTagsExample();
        sql.or().andServerTagIdEqualTo(serverTagId);
        return serverTagsMapper.selectByPrimaryKey(serverTagId);
    }

    @Override
    public ServerTags findByName(String tagName) {
        ServerTagsExample sql = new ServerTagsExample();
        sql.or().andTagNameEqualTo(tagName);
        List<ServerTags> serverTagList = serverTagsMapper.selectByExample(sql);
        return Collections3.getFirst(serverTagList);
    }

    @Override
    public Integer save(List<ServerTags> serverTagsList, Long goodsId) {
        Integer result = 0;
        if (Collections3.isEmpty(serverTagsList) || null == goodsId) {
            return result;
        }
        Date date = new Date();
        //先删除原有
        GoodsServerTagExample sql = new GoodsServerTagExample();
        sql.or().andGoodsIdEqualTo(goodsId);
        goodsServerTagMapper.deleteByExample(sql);
        for (ServerTags tag : serverTagsList) {
            GoodsServerTag goodsServerTag = new GoodsServerTag();
            goodsServerTag.setCreateTime(date);
            goodsServerTag.setGoodsId(goodsId);
            goodsServerTag.setServerTagId1(tag.getServerTagId());
            result += goodsServerTagMapper.insert(goodsServerTag);
        }
        return null;
    }

    @Override
    public Integer insert(ServerTags serverTags) {
        return serverTagsMapper.insert(serverTags);
    }
}
