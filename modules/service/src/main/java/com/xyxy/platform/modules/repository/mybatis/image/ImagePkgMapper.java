package com.xyxy.platform.modules.repository.mybatis.image;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.image.ImagePkg;
import com.xyxy.platform.modules.entity.image.ImagePkgExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface ImagePkgMapper {
    int countByExample(ImagePkgExample example);

    int deleteByExample(ImagePkgExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ImagePkg record);

    int insertSelective(ImagePkg record);

    List<ImagePkg> selectByExample(ImagePkgExample example);

    int updateByExampleSelective(@Param("record") ImagePkg record, @Param("example") ImagePkgExample example);

    int updateByExample(@Param("record") ImagePkg record, @Param("example") ImagePkgExample example);
}