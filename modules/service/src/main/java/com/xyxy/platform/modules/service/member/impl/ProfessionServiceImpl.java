package com.xyxy.platform.modules.service.member.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.cache.memcached.SpyMemcachedClient;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.member.Profession;
import com.xyxy.platform.modules.entity.member.ProfessionExample;
import com.xyxy.platform.modules.repository.mybatis.member.ProfessionMapper;
import com.xyxy.platform.modules.service.member.ProfessionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ProfessionServiceImpl
 *
 * @author yangdianjun
 * @date 2016/1/13 0013
 */
@Service
public class ProfessionServiceImpl implements ProfessionService{
    @Autowired
    private ProfessionMapper mapper;

    private final String cachePrefix = "Profession_";

    @Autowired
    private SpyMemcachedClient memcachedClient;
    @Override
    public Profession create(Profession profession) {
        if (null == profession) {
            return null;
        }
        mapper.insertSelective(profession);
        memcachedClient.set(getCacheKey(profession.getId()), 0, profession);
        return profession;
    }

    @Override
    public List<Profession> create(List<Profession> tagList) {
        List<Profession> result = Lists.newArrayList();
        if (Collections3.isEmpty(tagList)) {
            return Lists.newArrayList();
        }
        for (Profession tag : tagList) {
            result.add(this.create(tag));
        }
        return result;
    }

    @Override
    public boolean update(Profession profession) {
        int row = mapper.updateByPrimaryKeySelective(profession);
        if (row > 0) {
            memcachedClient.set(getCacheKey(profession.getId()), 0, profession);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void update(List<Profession> professionList) {
        if (Collections3.isEmpty(professionList)) {
            return;
        }
        for (Profession tag : professionList) {
            this.update(tag);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Profession findById(Long id) {
        Profession tag = memcachedClient.get(getCacheKey(id));
        if (null == tag) {
            tag = mapper.selectByPrimaryKey(id);
            memcachedClient.set(getCacheKey(id), 0, tag);
        }
        return tag;
    }

    @Override
    public Profession findByName(String name) {
        ProfessionExample sql = new ProfessionExample();
        sql.or().andNameEqualTo(name);
        List<Profession> tagList = mapper.selectByExample(sql);
        if (Collections3.isNotEmpty(tagList)) {
            return tagList.get(0);
        }
        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        memcachedClient.delete(getCacheKey(id));
        return mapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public List<Profession> findByIdList(List<Long> professionIdList) {
        List<Profession> result = Lists.newArrayList();
        if (null != professionIdList && Collections3.isNotEmpty(professionIdList)) {
            for (Long id : professionIdList) {
                Profession tmp = this.findById(id);
                if (null == tmp) continue;
                result.add(tmp);
            }
        }
        return result;
    }

    @Override
    public List<Profession> checkNotInDb(List<Profession> tagList) {
        List<Profession> result = Lists.newArrayList();
        if (null == tagList || Collections3.isEmpty(tagList)) {
            return Lists.newArrayList();
        }
        for (Profession tag : tagList) {
            if (null != tag && null != tag.getId() && null != this.findById(tag.getId())) {
                result.add(tag);
                continue;
            }
            if (StringUtils.isNotEmpty(tag.getName())) {
                tag = this.findByName(tag.getName());
            }
            if(null == tag.getId()){
                tag = this.create(tag);
            }
            result.add(tag);
        }
        return result;
    }


    /**
     * 拼接缓存key
     *
     * @param id 标签ID 主键
     * @return
     */
    private String getCacheKey(Long id) {
        return cachePrefix + id;
    }
}
