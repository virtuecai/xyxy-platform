/**
 *
 * com.xyxy.platform.modules.service.trade.impl
 * UserAddressServiceImpl.java
 *
 * 2016-2016年1月7日-下午2:44:53
 */
package com.xyxy.platform.modules.service.trade.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.entity.trade.UserAddress;
import com.xyxy.platform.modules.repository.mybatis.common.SysAreaMapper;
import com.xyxy.platform.modules.repository.mybatis.trade.UserAddressMapper;
import com.xyxy.platform.modules.service.trade.UserAddressService;

/**
 *
 * UserAddressServiceImpl
 *
 * xiangzhipeng
 * xiangzhipeng
 * 2016年1月7日 下午2:44:53
 *
 * @version 1.0.0
 *
 */
@Service
public class UserAddressServiceImpl implements UserAddressService {

	@Autowired
	private UserAddressMapper  userAddressMapper;
	@Autowired
	private SysAreaMapper sysAreaMapper;
	@Override
	public List<UserAddress> selectAddressById(long id) {
		// TODO Auto-generated method stub
		return userAddressMapper.selectAddressById(id);
	}

	@Override
	public boolean editUserAddress(UserAddress userAddress) {
		/*UserAddress tempAddress = userAddressMapper.selectByPrimaryKey(userAddress.getAddressId());
		tempAddress.setConsignee(userAddress.getConsignee());
		tempAddress.setAddress(userAddress.getAddress());
		tempAddress.setDistrict(userAddress.getDistrict());
		//处理市
		SysArea sysArea = sysAreaMapper.selectByPrimaryKey(userAddress.getAddressId().intValue());
		sysArea = sysAreaMapper.selectByPrimaryKey(sysArea.getParentid());
		tempAddress.setCity(userAddress.getCity());
		//处理省份
		sysArea = sysAreaMapper.selectByPrimaryKey(sysArea.getParentid());
		tempAddress.setProvince(suserAddress.get);
		tempAddress.setUpdateTime(new Date());*/
		return userAddressMapper.updateByPrimaryKey(userAddress)>0;
	}

	@Override
	public boolean removeUserAddress(long id,long memberId) {
		UserAddress userAddress = userAddressMapper.selectByPrimaryKey(id);
		if(userAddress == null || userAddress.getMemberId().longValue() != memberId){
			return false;
		}
		return userAddressMapper.deleteByPrimaryKey(id)>0;
	}

	@Override
	public int insertUserAddress(UserAddress userAddress) {
		return userAddressMapper.insert(userAddress);
	}

	@Override
	public int deleteByPrimaryKey(Long addressId) {
		// TODO Auto-generated method stub
		return userAddressMapper.deleteByPrimaryKey(addressId);
	}

	@Override
	public UserAddress selectByPrimaryKey(Long addressId) {
		// TODO Auto-generated method stub
		return userAddressMapper.selectByPrimaryKey(addressId);
	}

	@Override
	public boolean saveUserAddress(UserAddress userAddress) {
		userAddress.setCreateTime(new Date());
		Long districe = userAddress.getDistrict();
		if(districe != null){
			//处理市
			SysArea sysArea = sysAreaMapper.selectByPrimaryKey(userAddress.getAddressId().intValue());
			sysArea = sysAreaMapper.selectByPrimaryKey(sysArea.getParentid());
			userAddress.setCity(sysArea.getId().longValue());
			//处理省份
			sysArea = sysAreaMapper.selectByPrimaryKey(sysArea.getParentid());
			userAddress.setProvince(sysArea.getId().longValue());
		}
		return userAddressMapper.insert(userAddress)>0;
	}

}
