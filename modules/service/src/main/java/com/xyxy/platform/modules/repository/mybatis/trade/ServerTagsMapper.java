package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.ServerTags;
import com.xyxy.platform.modules.entity.trade.ServerTagsExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface ServerTagsMapper {
    int countByExample(ServerTagsExample example);

    int deleteByExample(ServerTagsExample example);

    int deleteByPrimaryKey(Long serverTagId);

    int insert(ServerTags record);

    int insertSelective(ServerTags record);

    List<ServerTags> selectByExample(ServerTagsExample example);

    ServerTags selectByPrimaryKey(Long serverTagId);

    int updateByExampleSelective(@Param("record") ServerTags record, @Param("example") ServerTagsExample example);

    int updateByExample(@Param("record") ServerTags record, @Param("example") ServerTagsExample example);

    int updateByPrimaryKeySelective(ServerTags record);

    int updateByPrimaryKey(ServerTags record);
    
    
    /**
     * 简介: 根据商品id 获取商品标签数据集
     * @author zhaowei
     * @Date 2016年1月5日 下午7:52:06
     * @version 1.0
     *
     * @param goodId
     * @return
     */
    List<ServerTags> selectServerTagsByGoodId(long goodId);
}