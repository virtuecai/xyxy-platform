/**
 * com.xyxy.platform.modules.service.trade.impl
 * CartServiceImpl.java
 * <p/>
 * 2016-2016年1月6日-下午8:16:47
 */
package com.xyxy.platform.modules.service.trade.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.utils.Collections3;
import com.xyxy.platform.modules.entity.trade.Cart;
import com.xyxy.platform.modules.entity.trade.CartExample;
import com.xyxy.platform.modules.repository.mybatis.trade.CartMapper;
import com.xyxy.platform.modules.service.trade.CartService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * CartServiceImpl
 * <p/>
 * xiangzhipeng
 * xiangzhipeng
 * 2016年1月6日 下午8:16:47
 *
 * @version 1.0.0
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartMapper cartMapper;

    @Override
    public List<Cart> selectCartByMemberId(long memId) {
        return cartMapper.selectByMemberId(memId);
    }

    @Override
    public int deletebyId(long id) {
        // TODO Auto-generated method stub
        return cartMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Cart selectCartByCartId(long id) {
        // TODO Auto-generated method stub
        return cartMapper.selectByPrimaryKey(id);
    }

    @Override
    public Boolean insert(Cart cart) {
        //不得为空
        if (null == cart || StringUtils.isEmpty(cart.getGoodscontent())) {
            return false;
        }

        //查询已存在的
        CartExample querySql = new CartExample();
        querySql.or().andMemberIdEqualTo(cart.getMemberId()).andGoodsIdEqualTo(cart.getGoodsId());
        List<Cart> cartList = cartMapper.selectByExample(querySql);

        //再次根据json内容进行过滤
        cartList = this.filterCartList(cartList, cart);

        //为空添加新
        if (Collections3.isEmpty(cartList)) {
            cart.setCreateTime(new Date());
            cart.setUpdateTime(new Date());
            cart.setNumber(1);
            cartMapper.insertSelective(cart);
        } else {
            Cart entity = Collections3.getFirst(cartList);
            Cart setValues = new Cart();
            setValues.setNumber(entity.getNumber() + 1);
            CartExample sql = new CartExample();
            sql.or().andCartIdEqualTo(entity.getCartId());
            cartMapper.updateByExampleSelective(setValues, sql);
        }

        return true;
    }

    /**
     * 相同 memberId, goodsId, carList, 比较json内容
     *
     * @param cartList 根据 memberId, goodsId 查询出来的 购物车
     * @param cart     购物车对象
     * @return
     */
    private List<Cart> filterCartList(List<Cart> cartList, Cart cart) {
        if (Collections3.isEmpty(cartList)) return null;
        List<Cart> result = Lists.newArrayList();
        for (Cart cart1 : cartList) {
            JSONObject json1 = JSON.parseObject(cart1.getGoodscontent());
            JSONObject json2 = JSON.parseObject(cart.getGoodscontent());
            if (null == json1 || json1.isEmpty() || null == json2 || json2.isEmpty()) continue;
            // serverDate: 服务时间时间戳, serverTimeIdx: 服务时间段 定义idx
            Long serverDate = json1.getLong("serverDate");
            Integer serverTimeIdx = json1.getInteger("serverTimeIdx");
            Long serverDate2 = json2.getLong("serverDate");
            Integer serverTimeIdx2 = json2.getInteger("serverTimeIdx");
            if ((null != serverDate && null != serverDate2 && serverDate.equals(serverDate2)) &&
                    (null != serverTimeIdx && null != serverTimeIdx2 && serverTimeIdx.equals(serverTimeIdx2))) {
                result.add(cart1);
            }
        }
        return result;
    }

}
