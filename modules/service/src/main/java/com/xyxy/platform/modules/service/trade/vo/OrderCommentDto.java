package com.xyxy.platform.modules.service.trade.vo;

import java.io.Serializable;
import java.util.List;

/** 
 * 订单评论dto
 * @author vic E-mail: 251361939@qq.com
 * @version 创建时间：2015年12月21日 下午2:04:50 
 */
public class OrderCommentDto implements Serializable {
	
	private static final long serialVersionUID = 6340830188573611579L;
	private String memberId;	//会员id
	private String imgurl;		//用户头像
	private String alias;		//用户昵称
	private int gender;			//用户性别
	private String tags;		//印象字符串列表 ,号分割
	private String detailed;	//详细评价
	private List<String> imgs ;	//图片数组
	private String time;		//评价时间
	private int isAuth;			//是否已认证
	private String orderSn;		//订单号
	private int describe;       //真实描述
	private int quality ;	    //真实品质
	private int attitude ;      //服务态度
	private String imgsIds;		//图片id串
	
	
	/**
	 * 会员id
	 */
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	/**
	 * 用户头像
	 */
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 用户昵称
	 */
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 用户性别
	 */
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	/**
	 * 印象字符串列表 ,号分割
	 */
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	 * 详细评价
	 */
	public String getDetailed() {
		return detailed;
	}
	public void setDetailed(String detailed) {
		this.detailed = detailed;
	}
	/**
	 * 图片数组
	 */
	public List<String> getImgs() {
		return imgs;
	}
	public void setImgs(List<String> imgs) {
		this.imgs = imgs;
	}
	/**
	 * 评价时间
	 */
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * 是否已认证
	 */
	public int getIsAuth() {
		return isAuth;
	}
	public void setIsAuth(int isAuth) {
		this.isAuth = isAuth;
	}
	/**
	 * 订单号
	 * @return
	 */
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	/**
	 * 真实描述
	 * @return
	 */
	public int getDescribe() {
		return describe;
	}
	public void setDescribe(int describe) {
		this.describe = describe;
	}
	/**
	 * 真实品质
	 * @return
	 */
	public int getQuality() {
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}
	/**
	 * 服务态度
	 * @return
	 */
	public int getAttitude() {
		return attitude;
	}
	public void setAttitude(int attitude) {
		this.attitude = attitude;
	}
	
	/**
	 * 简介:评论图片id串
	 * @author zhaowei
	 * @Date 2015年12月29日 上午9:46:53
	 * @version 1.0
	 * @return
	 */
	public String getImgsIds() {
		return imgsIds;
	}
	public void setImgsIds(String imgsIds) {
		this.imgsIds = imgsIds;
	}
	@Override
	public String toString() {
		return "OrderCommentDto [memberId=" + memberId + ", imgurl=" + imgurl
				+ ", alias=" + alias + ", gender=" + gender + ", tags=" + tags
				+ ", detailed=" + detailed + ", imgs=" + imgs + ", time="
				+ time + ", isAuth=" + isAuth + ", orderSn=" + orderSn
				+ ", describe=" + describe + ", quality=" + quality
				+ ", attitude=" + attitude + ", imgsIds=" + imgsIds + "]";
	}
	
	
}
