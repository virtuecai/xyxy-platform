package com.xyxy.platform.modules.service.sns;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.MemberCollection;

import java.util.List;
import java.util.Map;

/**
 * 获取会员服务收藏列表、添加或取消收藏
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */
public interface CollectionService {

    /**
     * 获取会员服务收藏列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    PageInfo getCollectionPage(Long memberId, Integer pageNumber, Integer pageSize);

    /**
     * 添加商品收藏
     * @param memberCollection 会员收藏夹实体类
     * @return
     */
    int createCollection(MemberCollection memberCollection);

    /**
     * 取消商品收藏
     * @param collectionId 收藏夹ID
     * @param memberId 会员ID
     * @return
     */
    int deleteCollection(Long collectionId, Long memberId);
    
    /**
    * 简介: 根据商品id获取收藏数
    * @author zhaowei
    * @Date 2016年1月9日 上午10:54:18
    * @version 1.0
    * @param goodsId
    * @return
     */
    int selectCountByGoodsId(long goodsId);

    /**
     * 根据 会员ID, 服务ID, 查询该会员是否已收藏了该服务
     * @author caizhengda
     * @param memberId 会员ID
     * @param goodsId 服务ID
     * @return
     */
    boolean isCollection(Long memberId, Long goodsId);
}

