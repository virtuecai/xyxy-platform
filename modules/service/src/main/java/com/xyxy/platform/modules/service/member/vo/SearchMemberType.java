package com.xyxy.platform.modules.service.member.vo;

/**
 * 简介: 星会员搜索类型
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-25 10:46
 */
public enum SearchMemberType {


    /**
     * 推荐
     */
    RECOMMEND(0),
    /**
     * 最新的
     */
    NEWEST(1),
    /**
     * 附近
     */
    NEAR(2);

    private SearchMemberType(int val) {
        this.val = val;
    }

    private int val;

    public int getVal() {
        return val;
    }

    public static SearchMemberType get(int val) {
        for (SearchMemberType e : values()) {
            if(e.getVal() == val) {
                return e;
            }
        }
        return null;
    }

}
