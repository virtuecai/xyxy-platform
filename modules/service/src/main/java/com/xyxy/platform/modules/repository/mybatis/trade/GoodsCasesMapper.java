package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.GoodsCases;
import com.xyxy.platform.modules.entity.trade.GoodsCasesExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface GoodsCasesMapper {
    int countByExample(GoodsCasesExample example);

    int deleteByExample(GoodsCasesExample example);

    int deleteByPrimaryKey(Long caseId);

    int insert(GoodsCases record);

    int insertSelective(GoodsCases record);

    List<GoodsCases> selectByExample(GoodsCasesExample example);

    GoodsCases selectByPrimaryKey(Long caseId);

    int updateByExampleSelective(@Param("record") GoodsCases record, @Param("example") GoodsCasesExample example);

    int updateByExample(@Param("record") GoodsCases record, @Param("example") GoodsCasesExample example);

    int updateByPrimaryKeySelective(GoodsCases record);

    int updateByPrimaryKey(GoodsCases record);
}