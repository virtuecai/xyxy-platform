/**
 *
 * com.xyxy.platform.modules.service.trade.impl
 * SysAreaServiceImpl.java
 *
 * 2016-2016年1月13日-下午7:40:24
 */
package com.xyxy.platform.modules.service.trade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyxy.platform.modules.entity.common.SysArea;
import com.xyxy.platform.modules.repository.mybatis.common.SysAreaMapper;
import com.xyxy.platform.modules.service.trade.SysAreaService;

/**
 *
 * SysAreaServiceImpl
 *
 * xiangzhipeng
 * xiangzhipeng
 * 2016年1月13日 下午7:40:24
 *
 * @version 1.0.0
 *
 */
@Service
public class SysAreaServiceImpl implements SysAreaService {

    @Autowired
    private SysAreaMapper  sysAreaMapper;
    @Override
    public SysArea getSysAreaById(Long id) {
        // TODO Auto-generated method stub
        return sysAreaMapper.selectByPrimaryKey(id.intValue());
    }

}
