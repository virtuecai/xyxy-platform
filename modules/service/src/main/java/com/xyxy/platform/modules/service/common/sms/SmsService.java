package com.xyxy.platform.modules.service.common.sms;

import com.xyxy.platform.modules.service.common.exception.SmsException;
import com.xyxy.platform.modules.service.common.sms.vo.SmsTemplateType;

/**
 * 简介: 短信验证码服务
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-24 14:38
 */
public interface SmsService {

    /**
     * 发送 注册 验证码
     * 一个ip可以使用三个号码
     * 一个号码一天可以获取五次
     * @param mobileNumber 手机号
     * @param ip           请求ip地址
     * @return
     */
    void sendRegisterCode(String mobileNumber, String ip) throws SmsException;

    /**
     * 获得短信验证码
     *
     * @param mobileNumber 手机号码
     * @return
     */
    Long generateSmsCode(String mobileNumber, SmsTemplateType smsTemplateType);

    /**
     * 发送短信
     *
     * @param mobileNumber 手机号
     * @param content      发送内容(根据模版生成)
     */
    void sendSms(String mobileNumber, String content);

    /**
     * 判断验证码是否还在有效期内
     * 存在则不能重复发送
     *
     * @param mobileNumber
     * @return
     */
    boolean exist(String mobileNumber);

    /**
     * 判断验证码是否正确
     *
     * @param mobileNumber 手机号码
     * @param code         输入的验证码
     * @return
     */
    boolean verifyCode(String mobileNumber, Long code);

    /**
     * 获得模版内容
     *
     * @param smsTemplateType
     * @param arguments
     * @return
     */
    String generateContent(SmsTemplateType smsTemplateType, Object... arguments);

}
