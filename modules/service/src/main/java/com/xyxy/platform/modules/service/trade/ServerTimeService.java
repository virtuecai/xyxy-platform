package com.xyxy.platform.modules.service.trade;

import java.util.Date;

/**
 * 服务时间服务接口
 * @author zhaowei
 */
public interface ServerTimeService {
	/**
	* 简介:  获取服务时段串 
	* @author zhaowei
	* @Date 2016年1月10日 上午10:58:14
	* @version 1.0
	* @param goodsSn
	* @param startDate
	* @param endDate
	* @return
	 */
	public String selectAvailable(String goodsSn,Date startDate,Date endDate);
	/**
	* 简介: 根据商品 
	* @author zhaowei
	* @Date 2016年1月10日 下午8:06:08
	* @version 1.0
	* @param goodsId   商品id
	* @param weekDay   星期几
	* @return
	 */
	public String selectServerTimeSetting(long goodsId,int weekDay);
	
}
