package com.xyxy.platform.modules.service.sns;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.sns.MemberFollow;

import java.util.List;

/**
 * 查询粉丝列表、我的关注列表、添加或取消关注
 * @author liushun
 * @version 1.0
 * @Date 2015-12-21
 */
public interface FollowerService {

	/**
	 * 简介: 今日新增粉丝数
	 * @author zhaowei
	 * @Date 2016年1月7日 下午4:47:14
	 * @version 1.0
	 *
	 * @param memberId
	 * @return
	 */
	int fansNewCount(long memberId);
	
	/**
	 * 简介:根据会员id获取粉丝关注数
	 * @author zhaowei
	 * @Date 2016年1月6日 下午4:53:32
	 * @version 1.0
	 *
	 * @param memberId
	 * @return
	 */
	int countFollower(long memberId);
	
    /**
     * 获取粉丝列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */

    PageInfo getFollowerPage(long memberId, Integer pageNumber, Integer pageSize);

    /**
     * 查询我关注的用户列表
     * @param memberId 会员ID
     * @param pageNumber 当前页数
     * @param pageSize 每页大小
     * @return
     */
    PageInfo getWithPage(long memberId, Integer pageNumber, Integer pageSize);

    /**
     * 添加关注
     * @param memberFollow 实体
     * @return
     */
    int addWith(MemberFollow memberFollow);

    /**
     * 取消关注
     * @param followId 关注ID
     * @param memberId 会员ID
     * @return
     */
    int delWith(long followId, long memberId);


    boolean isFollow(long memberId, long toMemberId);
}
