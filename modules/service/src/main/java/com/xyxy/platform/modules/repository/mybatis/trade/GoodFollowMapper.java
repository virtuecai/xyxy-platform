package com.xyxy.platform.modules.repository.mybatis.trade;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.GoodFollow;
import com.xyxy.platform.modules.entity.trade.GoodFollowExample;
@MyBatisRepository
public interface GoodFollowMapper {
    int countByExample(GoodFollowExample example);

    int deleteByExample(GoodFollowExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GoodFollow record);

    int insertSelective(GoodFollow record);

    List<GoodFollow> selectByExampleWithRowbounds(GoodFollowExample example, RowBounds rowBounds);

    List<GoodFollow> selectByExample(GoodFollowExample example);

    GoodFollow selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GoodFollow record, @Param("example") GoodFollowExample example);

    int updateByExample(@Param("record") GoodFollow record, @Param("example") GoodFollowExample example);

    int updateByPrimaryKeySelective(GoodFollow record);

    int updateByPrimaryKey(GoodFollow record);
}