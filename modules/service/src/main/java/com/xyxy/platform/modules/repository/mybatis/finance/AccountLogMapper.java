package com.xyxy.platform.modules.repository.mybatis.finance;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.finance.AccountLog;
import com.xyxy.platform.modules.entity.finance.AccountLogExample;

import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface AccountLogMapper {
    int countByExample(AccountLogExample example);

    int deleteByExample(AccountLogExample example);

    int deleteByPrimaryKey(Long logId);

    int insert(AccountLog record);

    int insertSelective(AccountLog record);

    List<AccountLog> selectByExample(AccountLogExample example);

    AccountLog selectByPrimaryKey(Long logId);

    int updateByExampleSelective(@Param("record") AccountLog record, @Param("example") AccountLogExample example);

    int updateByExample(@Param("record") AccountLog record, @Param("example") AccountLogExample example);

    int updateByPrimaryKeySelective(AccountLog record);

    int updateByPrimaryKey(AccountLog record);

    AccountLog selectByMemberId(@Param("memberId")Long memberId);

    BigDecimal selectMaxMoney(@Param("memberId") Long memberId);
}