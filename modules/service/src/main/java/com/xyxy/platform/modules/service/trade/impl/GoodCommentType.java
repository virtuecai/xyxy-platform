package com.xyxy.platform.modules.service.trade.impl;
/**
 * 商品评价类型枚举
 * @author zhaowei
 */
public enum GoodCommentType {
	
	DEFAULT(1,"普通商品评价");
	
	private int value;
	private String msg;
	private GoodCommentType(int value,String msg){
		this.value = value;
		this.msg = msg;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
