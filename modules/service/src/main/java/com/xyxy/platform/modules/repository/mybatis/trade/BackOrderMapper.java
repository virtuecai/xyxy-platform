package com.xyxy.platform.modules.repository.mybatis.trade;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.trade.BackOrder;
import com.xyxy.platform.modules.entity.trade.BackOrderExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface BackOrderMapper {
    int countByExample(BackOrderExample example);

    int deleteByExample(BackOrderExample example);

    int deleteByPrimaryKey(Long backId);

    int insert(BackOrder record);

    int insertSelective(BackOrder record);

    List<BackOrder> selectByExample(BackOrderExample example);

    BackOrder selectByPrimaryKey(Long backId);

    int updateByExampleSelective(@Param("record") BackOrder record, @Param("example") BackOrderExample example);

    int updateByExample(@Param("record") BackOrder record, @Param("example") BackOrderExample example);

    int updateByPrimaryKeySelective(BackOrder record);

    int updateByPrimaryKey(BackOrder record);
}