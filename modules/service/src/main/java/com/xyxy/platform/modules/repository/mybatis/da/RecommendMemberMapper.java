package com.xyxy.platform.modules.repository.mybatis.da;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.da.RecommendMember;
import com.xyxy.platform.modules.entity.da.RecommendMemberExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
@MyBatisRepository
public interface RecommendMemberMapper {
    int countByExample(RecommendMemberExample example);

    int deleteByExample(RecommendMemberExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RecommendMember record);

    int insertSelective(RecommendMember record);

    List<RecommendMember> selectByExample(RecommendMemberExample example);

    RecommendMember selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RecommendMember record, @Param("example") RecommendMemberExample example);

    int updateByExample(@Param("record") RecommendMember record, @Param("example") RecommendMemberExample example);

    int updateByPrimaryKeySelective(RecommendMember record);

    int updateByPrimaryKey(RecommendMember record);
}