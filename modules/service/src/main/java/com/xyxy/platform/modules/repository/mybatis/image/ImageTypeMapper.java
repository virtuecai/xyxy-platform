package com.xyxy.platform.modules.repository.mybatis.image;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.image.ImageType;
import com.xyxy.platform.modules.entity.image.ImageTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface ImageTypeMapper {
    int countByExample(ImageTypeExample example);

    int deleteByExample(ImageTypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ImageType record);

    int insertSelective(ImageType record);

    List<ImageType> selectByExample(ImageTypeExample example);

    ImageType selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ImageType record, @Param("example") ImageTypeExample example);

    int updateByExample(@Param("record") ImageType record, @Param("example") ImageTypeExample example);

    int updateByPrimaryKeySelective(ImageType record);

    int updateByPrimaryKey(ImageType record);
}