package com.xyxy.platform.modules.repository.mybatis.auth;

import com.xyxy.platform.modules.core.repository.mybatis.annotation.MyBatisRepository;
import com.xyxy.platform.modules.entity.auth.PositionCheck;
import com.xyxy.platform.modules.entity.auth.PositionCheckExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MyBatisRepository
public interface PositionCheckMapper {
    int countByExample(PositionCheckExample example);

    int deleteByExample(PositionCheckExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PositionCheck record);

    int insertSelective(PositionCheck record);

    List<PositionCheck> selectByExample(PositionCheckExample example);

    PositionCheck selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PositionCheck record, @Param("example") PositionCheckExample example);

    int updateByExample(@Param("record") PositionCheck record, @Param("example") PositionCheckExample example);

    int updateByPrimaryKeySelective(PositionCheck record);

    int updateByPrimaryKey(PositionCheck record);
}