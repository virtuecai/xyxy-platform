package com.xyxy.platform.modules.service.trade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.entity.trade.GoodsComment;
import com.xyxy.platform.modules.entity.trade.GoodsCommentExample;
import com.xyxy.platform.modules.repository.mybatis.trade.GoodsCommentMapper;
import com.xyxy.platform.modules.service.trade.GoodsCommentService;
@Service
public class GoodsCommentServiceImpl implements GoodsCommentService {

	@Autowired
	private GoodsCommentMapper goodsCommentMapper;
	
	@Override
	public int selectCountByGoodsId(long goodsId) {
		GoodsCommentExample  goodsCommentExample = new GoodsCommentExample();
		goodsCommentExample.or().andCommentTypeEqualTo(GoodCommentType.DEFAULT.getValue())
		.andCommentValueEqualTo(goodsId).andIsDelEqualTo(0);
		return goodsCommentMapper.countByExample(goodsCommentExample);
	}

	@Override
	public PageInfo<GoodsComment> selectNewListGoodsId(long goodsId, int pageNum, int pageSize) {
		GoodsCommentExample goodsCommentExample = new GoodsCommentExample();
		goodsCommentExample.or().andCommentTypeEqualTo(1).andCommentValueEqualTo(goodsId);
		goodsCommentExample.setOrderByClause(" create_time desc ");
		
		
		
		return null;
	}
	
}
