package com.xyxy.platform.modules.core.solr;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * 简介: solr client 调用哦示例
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-01-27 15:40
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-config.xml"})
public class SolrClientServiceTest {

    @Autowired
    SolrClientService client;

    @Test
    public void testCore0Query() throws Exception {
        SolrSearchParams params = new SolrSearchParams();
        params.setPageNum(0);
        params.setPageSize(10);
        params.setQ("tagList:null AND isBest:true AND (alias:* OR tagList:*)");
        params.setSort("createTime desc");
        QueryResponse response = client.query("core1", params);
        System.out.println(response);
    }

}