package com.xyxy.platform.modules.core.file;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-02-25 17:31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-config.xml"})
public class SystemFileServiceTest {

    @Autowired
    private SystemFileService systemFileService;

    @Test
    public void testGetFilePath() throws Exception {
        System.out.println(systemFileService.getFilePath());
    }

    @Test
    public void testGetTmpFilePath() throws Exception {
        System.out.println(systemFileService.getTmpFilePath());
    }

    @Test
    public void testSaveTmpFile() throws Exception {
        FileInputStream fi = new FileInputStream(new File("/Users/czd/Pictures/身份证1.JPG"));
        String fileUrl = systemFileService.saveTmpFile(fi, "身份证1.JPG");
        System.out.println(fileUrl);
    }

    @Test
    public void testDeleteTmpFile() throws Exception {
        boolean success = systemFileService.deleteTmpFile("d97fae22-349b-4381-a26d-d358160e478c.jpg");
        System.out.println(success);
    }

    @Test
    public void testChangeFileStatus() throws Exception {
        systemFileService.changeFileStatus("d97fae22-349b-4381-a26d-d358160e478c.jpg");
    }

    @Test
    public void testChangeImageFileStatusFromHtml() throws Exception {
        String html = systemFileService.changeImageFileStatusFromHtml("<img src=\"http://10.1.1.241/filestmp/8eeb86bf-00db-455f-9dfb-638c45cd78a2.JPG\"><img src=\"http://10.1.1.241/filestmp/a39ab3b9-8184-4866-bc4d-ce0ed731ff22.JPG\">");
        System.out.println(html);
    }

    @Test
    public void testGetImageFileNameListFromHtml() throws Exception {
        List<String> fileNames = systemFileService.getImageFileNameListFromHtml("<img src=\"http://10.1.1.241/filestmp/8eeb86bf-00db-455f-9dfb-638c45cd78a2.JPG\"><img src=\"http://10.1.1.241/filestmp/a39ab3b9-8184-4866-bc4d-ce0ed731ff22.JPG\">");
        System.out.println(fileNames);
    }

    @Test
    public void testGetFileNameFromUrl() throws Exception {
        String fileName = systemFileService.getFileNameFromUrl("http://10.1.1.241/filestmp/8eeb86bf-00db-455f-9dfb-638c45cd78a2.JPG");
        System.out.println(fileName);
    }

    @Test
    public void testGetUriByUrl() throws Exception {
        String uri = systemFileService.getUriByUrl("http://10.1.1.241/filestmp/8eeb86bf-00db-455f-9dfb-638c45cd78a2.JPG");
        System.out.println(uri);
    }

    @Test
    public void testGetFileUrlByUri() throws Exception {
        String url = systemFileService.getFileUrlByUri("8eeb86bf-00db-455f-9dfb-638c45cd78a2.JPG");
        System.out.println(url);
    }

    @Test
    public void testGetTmpFileUrlByUri() throws Exception {
        String url = systemFileService.getTmpFileUrlByUri("8eeb86bf-00db-455f-9dfb-638c45cd78a2.JPG");
        System.out.println(url);
    }
}