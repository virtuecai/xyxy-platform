package com.xyxy.platform.modules.core.utils.collections;

/**
 * 条件接口
 */
public interface Predicate<T> {
    /**
     * Use the specified parameter to perform a test that returns true or false.
     *
     * @param object the object to evaluate, should not be changed
     * @return true or false
     * @throws ClassCastException                              (runtime) if the input is the wrong class
     * @throws IllegalArgumentException                        (runtime) if the input is invalid
     * @throws org.apache.commons.collections.FunctorException (runtime) if the predicate encounters a problem
     */
    public boolean evaluate(T object);
}
