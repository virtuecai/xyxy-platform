package com.xyxy.platform.modules.core;

public class Constants {

    /**
     * 当前登录用户
     */
    public static final String CURRENT_USER = "user";

    public static final String SESSION_FORCE_LOGOUT_KEY = "session.force.logout";

    public static final String ENCODING = "UTF-8";

    public static final String CONTEXT_PATH = "ctx";

    public static final String FILE_UPLOAD_PROGRESS = "upload_ps";

}
