package com.xyxy.platform.modules.core.sitemesh3;

import org.sitemesh.SiteMeshContext;
import org.sitemesh.content.ContentProperty;
import org.sitemesh.content.tagrules.TagRuleBundle;
import org.sitemesh.content.tagrules.html.ExportTagToContentRule;
import org.sitemesh.tagprocessor.State;

/**
 * 前端规范, 网页加载由上而下.
 * 用于讲自己被装饰页面的js放在 layout js的下面, js由上而下依赖关系.
 * Created by VirtueCai on 15/12/11.
 */
public class ExtHtmlTagRuleBundle implements TagRuleBundle {

    @Override
    public void install(State defaultState, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {
        defaultState.addRule("body-child-1", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("body-child-1"), false));
        defaultState.addRule("body-child-2", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("body-child-2"), false));
        defaultState.addRule("body-child-3", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("body-child-3"), false));
        defaultState.addRule("javascript-list", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("javascript-list"), false));
    }

    @Override
    public void cleanUp(State defaultState, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {

    }

}