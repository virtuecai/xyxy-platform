package com.xyxy.platform.modules.core.utils.converter;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * 将字符串转换为整数
 * Created by juju on 14-5-15.
 */
public class LongTransformer extends StringTransformer<Long> {

    public Long transform(String from) {
        return transform(from, 0l);
    }

    public Long transform(String from, Long defaultVal) {
        return NumberUtils.toLong(from, defaultVal);
    }
}
