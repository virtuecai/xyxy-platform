package com.xyxy.platform.modules.core.vo;

/**
 * Created by liushun on 2015/11/21 0021.
 */
public class Page {
    private int pageIndex;
    private int pageSize;
    //搜索条件
    private String searchKey;
    //搜索词
    private String searchValue;
    //排序字段
    private String orderFiled;
    //排序方式
    private String orderWay;

    //-----------------拼接字符串-------------
    private Object orderTerms;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderWay() {
        return orderWay;
    }

    public void setOrderWay(String orderWay) {
        this.orderWay = orderWay;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getOrderFiled() {
        return orderFiled;
    }

    public void setOrderFiled(String orderFiled) {
        this.orderFiled = orderFiled;
    }

    public String getOrderTerms() {
        if(this.getOrderFiled()==null||this.getOrderWay()==null)
            return null;
        else
            return this.getOrderFiled() + " " + this.getOrderWay();
    }

}
