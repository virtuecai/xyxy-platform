package com.xyxy.platform.modules.core.utils.converter;

import com.xyxy.platform.modules.core.utils.DateFormats;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * 将字符串转换为整数
 * Created by juju on 14-5-15.
 */
public class DateTransformer extends StringTransformer<Date> {

    private String dateFmt = DateFormats.FULL_FMT;

    public DateTransformer() {
        super();
    }

    public DateTransformer(String dateFmt) {
        this.dateFmt = dateFmt;
    }

    public Date transform(String from) {
        return transform(from, null);
    }

    public Date transform(String from, Date defaultVal) {
        try {
            return DateUtils.parseDate(from, dateFmt, DateFormats.FULL_FMT, DateFormats.DATE_FMT);
        } catch (ParseException ex) {
            return defaultVal;
        }
    }
}
