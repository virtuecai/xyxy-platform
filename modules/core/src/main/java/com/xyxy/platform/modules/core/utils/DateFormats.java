package com.xyxy.platform.modules.core.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 日期格式化工具类
 */
public class DateFormats extends DateFormatUtils {
    public static final String FULL_FMT = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_FMT = "yyyy-MM-dd";

    public static final String TIME_FMT = "HH:mm:ss";
}
