package com.xyxy.platform.modules.core.repository;


import com.xyxy.platform.modules.core.vo.Page;

import java.util.List;

/**
 * 所有自定义Dao的接口
 * Model : 代表数据库中的表 映射的Java对象类型
 * PK :代表对象的主键类型
 */
public interface BaseDao<Model, PK>{

    /**
     * 插入对象
     * @param model
     * @return
     */
    int insert(Model model);

    /**
     * 插入对象,只插入不为null的字段,不会影响有默认值的字段
     * @param model
     * @return
     */
    int insertSelective(Model model);

    /**
     * 根据主键进行更新,只会更新不是null的数据
     * @param model
     * @return
     */
    int updateByPrimaryKeySelective(Model model);

    /**
     * 根据主键进行更新
     * @param model
     * @return
     */
    int updateByPrimaryKey(Model model);

    /**
     * 根据主键删除对象
     * @param id
     * @return
     */
    int deleteByPrimaryKey(PK id);

    /**
     * 根据主键查询对象
     * @param id
     * @return
     */
    Model selectByPrimaryKey(PK id);

    /**
     * 查询所有记录
     * @return
     */
    List<Model> selectAll();

    /**
     * 基本分页查询
     */
    List selectPage(Page page);
}
