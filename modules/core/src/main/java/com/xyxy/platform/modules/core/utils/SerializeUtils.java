package com.xyxy.platform.modules.core.utils;

import java.io.*;

/**
 * java对象序列化工具类
 * http://git.oschina.net/ld/J2Cache/issues/3
 */
public class SerializeUtils {

    public static byte[] serialize(Object obj) {
        ObjectOutputStream oos = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
            oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            return baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (oos != null)
                try {
                    oos.close();
                } catch (IOException e) {
                }
        }
    }

    public static <T> T deserialize(byte[] bits) {
        ObjectInputStream ois = null;
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(bits);
            ois = new ObjectInputStream(bais);
            return (T) ois.readObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (ois != null)
                try {
                    ois.close();
                } catch (IOException e) {
                }
        }
    }

    //---------------------第三方序列化方案--------------------
//    public static byte[] fserialize(Serializable obj) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
//        fserialize(obj, byteArrayOutputStream);
//        return byteArrayOutputStream.toByteArray();
//    }
//
//    /**
//     * @param obj  the object to serialize to bytes, may be null
//     * @param byteArrayOutputStream  the stream to write to, must not be null
//     * @throws IllegalArgumentException if <code>outputStream</code> is <code>null</code>
//     * @throws org.apache.commons.lang.SerializationException (runtime) if the serialization fails
//     */
//    private static void fserialize(Serializable obj, ByteArrayOutputStream byteArrayOutputStream) {
//        if (byteArrayOutputStream == null) {
//            throw new IllegalArgumentException("The OutputStream must not be null");
//        }
//        FSTObjectOutput out = null;
//        try {
//            // stream closed in the finally
//            out = new FSTObjectOutput(byteArrayOutputStream);
//            out.writeObject(obj);
//            out.flush();
//        } catch (IOException ex) {
//            throw new RuntimeException(ex);
//        } finally {
//            try {
//                if (out != null) {
//                    out.close();
//                    out=null;
//                }
//            } catch (IOException ex) {
//                // ignore close exception
//            }
//        }
//    }
//    // Deserialize
//    //-----------------------------------------------------------------------
//    /**
//     * <p>Deserializes a single <code>Object</code> from an array of bytes.</p>
//     *
//     * @param objectData  the serialized object, must not be null
//     * @return the deserialized object
//     * @throws IllegalArgumentException if <code>objectData</code> is <code>null</code>
//     */
//    public static Object fdeserialize(byte[] objectData) {
//        if (objectData == null) {
//            throw new IllegalArgumentException("The byte[] must not be null");
//        }
//        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(objectData);
//        return fdeserialize(byteArrayInputStream);
//    }
//    /**
//     * <p>Deserializes an <code>Object</code> from the specified stream.</p>
//     *
//     * <p>The stream will be closed once the object is written. This
//     * avoids the need for a finally clause, and maybe also exception
//     * handling, in the application code.</p>
//     *
//     * <p>The stream passed in is not buffered internally within this method.
//     * This is the responsibility of your application if desired.</p>
//     *
//     * @param byteArrayInputStream  the serialized object input stream, must not be null
//     * @return the deserialized object
//     * @throws IllegalArgumentException if <code>inputStream</code> is <code>null</code>
//     */
//    private static Object fdeserialize(ByteArrayInputStream byteArrayInputStream) {
//        if (byteArrayInputStream == null) {
//            throw new IllegalArgumentException("The InputStream must not be null");
//        }
//        FSTObjectInput in = null;
//        try {
//            // stream closed in the finally
//            in = new FSTObjectInput(byteArrayInputStream);
//            return in.readObject();
//        } catch (ClassNotFoundException ex) {
//            throw new RuntimeException(ex);
//        } catch (IOException ex) {
//            throw new RuntimeException(ex);
//        } finally {
//            try {
//                if (in != null) {
//                    in.close();
//                    in=null;
//                }
//            } catch (IOException ex) {
//                // ignore close exception
//            }
//        }
//    }

    //---------------------第三方序列化方案--------------------
}
