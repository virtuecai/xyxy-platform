package com.xyxy.platform.modules.core.web.extensions.ajax;

import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * ajax 辅助判断工具
 * Created by VirtueCai on 15/11/20.
 */
public class AjaxUtils {

	/**
	 * 判断是否是 ajax 请求
	 * @param webRequest
	 * @return
	 */
	public static boolean isAjaxRequest(WebRequest webRequest) {
		String requestedWith = webRequest.getHeader("X-Requested-With");
		return requestedWith != null ? "XMLHttpRequest".equalsIgnoreCase(requestedWith) : false;
	}
	/**
	 * 判断是否是 ajax 请求
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		String requestedWith = request.getHeader("X-Requested-With");
		return requestedWith != null ? "XMLHttpRequest".equalsIgnoreCase(requestedWith) : false;
	}

	/**
	 * 判断是否是 ajax 文件上传
	 * @param webRequest
	 * @return
	 */
	public static boolean isAjaxUploadRequest(WebRequest webRequest) {
		return webRequest.getParameter("ajaxUpload") != null;
	}

	/**
	 * 判断是否是 ajax 文件上传
	 * @param request
	 * @return
	 */
	public static boolean isAjaxUploadRequest(HttpServletRequest request) {
		return request.getParameter("ajaxUpload") != null;
	}
	
	private AjaxUtils() {}

}
