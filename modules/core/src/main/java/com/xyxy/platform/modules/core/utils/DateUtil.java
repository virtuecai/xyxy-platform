/**
 *
 * com.xyxy.platform.modules.core.utils
 * DateUtil.java
 *
 * 2016-2016年1月9日-上午11:24:58
 */
package com.xyxy.platform.modules.core.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class DateUtil {

	protected static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

	public static final String FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_STANDARD_MIN = "yyyy-MM-dd HH:mm";
	public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String FORMAT_YYYYMMDD = "yyyyMMdd";
	public static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String FORMAT_YYYY_MM_DD_SECONDS = "yyyy-MM-dd HH:mm:ss";
	private static ThreadLocal<SimpleDateFormat> dateFormatLocal = new ThreadLocal();
	private static final Pattern YYYYMMDD_PATTERN = Pattern.compile("[1-9]{1}[0-9]{3}-((0?[1-9])|(1[0-2]))-((0?[1-9])|([1-2][0-9])|([3][0-1]))");
	private static final Pattern YYYYMMDDHHMMSS_PATTERN = Pattern.compile("[1-9]{1}[0-9]{3}-((0?[1-9])|(1[0-2]))-((0?[1-9])|([1-2][0-9])|([3][0-1]))\\s+(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9]):([0-5]?[0-9])");
	private static ThreadLocal<SimpleDateFormat> dateFormat = new ThreadLocal();

	private DateUtil() {
	}

	public static String getCurrentTime() {
		SimpleDateFormat standardFormate = getStandardFormate();
		return standardFormate.format(new Date());
	}

	public static Timestamp getCurrentDatetime() {
		return new Timestamp(System.currentTimeMillis());
	}

	public static String format(Date date) {
		if(date == null) {
			return null;
		} else {
			SimpleDateFormat standardFormate = getStandardFormate();
			return standardFormate.format(date);
		}
	}

	public static SimpleDateFormat getStandardFormate() {
		if(null == dateFormatLocal.get()) {
			dateFormatLocal.set(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		}

		return (SimpleDateFormat)dateFormatLocal.get();
	}

	private static SimpleDateFormat getDateFormate(String formate) {
		return new SimpleDateFormat(formate);
	}

	public static String format(Date date, String formatStr) {
		if(date == null) {
			return null;
		} else {
			SimpleDateFormat sf = new SimpleDateFormat(formatStr);
			return sf.format(date);
		}
	}

	public static Date formDate(String dateStr, String formater) {
		formater = null == formater?"yyyy-MM-dd HH:mm:ss":formater;
		SimpleDateFormat formatter = new SimpleDateFormat(formater);
		Date date = null;

		try {
			date = formatter.parse(dateStr);
		} catch (ParseException var5) {
			logger.error("ParseException error, Exception = " + var5);
		}

		return date;
	}

	public static int getWeekNumOfYear(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.setFirstDayOfWeek(2);
		return c.get(3);
	}


	public static boolean judgeDateFormat(String str, String dateFormat) {
		if(str != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			formatter.setLenient(false);

			try {
				formatter.format(formatter.parse(str));
				return true;
			} catch (Exception var4) {
				return false;
			}
		} else {
			return false;
		}
	}

	public static Date getOffsetNewDate(Date date, int offset) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		gc.add(5, offset);
		Date newDate = gc.getTime();
		return newDate;
	}

	public static Timestamp convertDate2TStamp(Date date) {
		return null == date?null:new Timestamp(date.getTime());
	}

	public static String addMinute(String time, int minite) {
		if(StringUtils.isEmpty(time)) {
			return "";
		} else {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = null;

			try {
				date = dateFormat.parse(time);
			} catch (ParseException var6) {
				return "";
			}

			calendar.setTimeInMillis(date.getTime());
			calendar.add(12, minite);
			return dateFormat.format(calendar.getTime());
		}
	}

	public static Date addMinute(Date requestTime, int minite) {
		if(null == requestTime) {
			return null;
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(requestTime.getTime());
			calendar.add(12, minite);
			return calendar.getTime();
		}
	}

	public static Date getBeforeDateInDay(int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(5, -days);
		return calendar.getTime();
	}

	public static String getCurrentTime(String formate) {
		SimpleDateFormat standardFormate = getDateFormate(formate);
		return standardFormate.format(new Date());
	}

	public static boolean validateYYYYMMDDHHMMSSDateStr(String dateStr) {
		return !StringUtils.isEmpty(dateStr) && YYYYMMDDHHMMSS_PATTERN.matcher(dateStr).matches();
	}

	public static Boolean isYYYYMMDD(String date) {
		return date == null?Boolean.valueOf(false):(YYYYMMDD_PATTERN.matcher(date).matches()?Boolean.valueOf(true):Boolean.valueOf(false));
	}

	public static boolean isBeforeNowTime(Date compareTime) {
		Date nowTime = new Date();
		return nowTime.before(compareTime);
	}

	public static Date parseNormalDate(String date, String formatStr) throws Exception {
		if(date == null) {
			return null;
		} else if(YYYYMMDD_PATTERN.matcher(date).matches()) {
			SimpleDateFormat sdf = getSimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			return sdf.parse(date);
		} else {
			throw new Exception("日期格式不正确");
		}
	}

	private static SimpleDateFormat getSimpleDateFormat(String format) {
		SimpleDateFormat sdf = (SimpleDateFormat)dateFormat.get();
		if(null != sdf) {
			if(StringUtils.isNotEmpty(format)) {
				sdf.applyPattern(format);
			}

			return sdf;
		} else {
			if(StringUtils.isEmpty(format)) {
				format = "yyyy-MM-dd HH:mm:ss";
			}

			sdf = new SimpleDateFormat(format);
			dateFormat.set(sdf);
			return sdf;
		}
	}

	public static boolean inDay(Date date) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(new Date());
		return c1.get(1) == c2.get(1) && c1.get(2) == c2.get(2) && c1.get(5) == c2.get(5);
	}

	public static List<Date> getCurrentMonthBetween() {
		ArrayList dateList = new ArrayList();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(gc.getTime());
		if(gc.get(5) < 15) {
			gc.set(2, gc.get(2) - 1);
		}

		gc.set(11, 0);
		gc.set(12, 0);
		gc.set(13, 0);
		gc.set(14, 0);
		gc.set(5, 1);
		dateList.add(gc.getTime());
		gc.set(5, gc.getActualMaximum(5));
		gc.set(11, gc.getActualMaximum(11));
		gc.set(12, gc.getActualMaximum(12));
		gc.set(13, gc.getActualMaximum(13));
		gc.set(14, gc.getActualMaximum(14));
		dateList.add(gc.getTime());
		return dateList;
	}

}
