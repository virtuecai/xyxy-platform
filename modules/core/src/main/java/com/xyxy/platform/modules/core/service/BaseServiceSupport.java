package com.xyxy.platform.modules.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyxy.platform.modules.core.repository.BaseDao;
import com.xyxy.platform.modules.core.vo.Page;

import java.util.List;

/**
 * Service的实现类, 其他的自定义 ServiceImpl, 继承自它,可以获得常用的增删查改操作
 */
public abstract class BaseServiceSupport<Model, PK> implements BaseService<Model, PK> {

    /**
     * 定义成抽象方法,由子类实现,完成dao的注入
     *
     * @return
     */
    public abstract BaseDao<Model, PK> getDao();

    public int insert(Model model) {
        return getDao().insert(model);
    }

    public int insertSelective(Model model) {
        return getDao().insertSelective(model);
    }

    public int updateByPrimaryKeySelective(Model model) {
        return getDao().updateByPrimaryKeySelective(model);
    }

    public int updateByPrimaryKey(Model model) {
        return getDao().updateByPrimaryKey(model);
    }

    public int deleteByPrimaryKey(PK id) {
        return getDao().deleteByPrimaryKey(id);
    }

    public Model selectByPrimaryKey(PK id) {
        return getDao().selectByPrimaryKey(id);
    }

    public List<Model> selectAll() {
        return getDao().selectAll();
    }

    public PageInfo getPage(Page page) {
        PageHelper.startPage(page.getPageIndex()+1,page.getPageSize(),page.getOrderTerms());
        PageInfo pi = new PageInfo(getDao().selectPage(page));
        return pi;
    }
}
