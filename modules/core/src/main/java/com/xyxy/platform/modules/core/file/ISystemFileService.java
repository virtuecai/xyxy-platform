package com.xyxy.platform.modules.core.file;

import org.springframework.scheduling.annotation.Async;

import java.io.InputStream;
import java.util.List;

/**
 * 简介:
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2016-02-25 15:04
 */
public interface ISystemFileService {

    //非临时文件存放文件夹(file.system.server.base.path子级)
    public static final String filesDir = "files";

    //临时文件存放文件夹(file.system.server.base.path子级)
    public static final String tmpFilesDir = "filestmp";

    /**
     * 非临时文件 存储 路径 (使用中的文件)
     */
    String getFilePath();

    /**
     * 临时文件 存储 路径
     */
    String getTmpFilePath();


    /**
     * 存储文件为临时文件
     *
     * @param inputStream 文件流
     * @param fileName    原名, 会在其基础上加入 uuid-
     */
    String saveTmpFile(InputStream inputStream, String fileName);

    /**
     * 删除临时文件
     *
     * @param primaryFileName 文件名
     * @return
     */
    @Async
    boolean deleteTmpFile(String primaryFileName);

    /**
     * 改变临时文件状态
     * 将临时文件拷贝至使用文件目录(非临时文件目录)
     *
     * @param primaryFileName 文件名
     */
    void changeFileStatus(String primaryFileName);

    /**
     * 改变临时文件状态
     * 解析 html 中得图片地址, 目前仅支持: <image src="imageUrl" />
     * 1. 根据 imageUrl 获得对于的 文件名(uuid+文件名)
     * 2. 将临时文件拷贝到非临时文件目录去.
     * 3. 修改 htmlContent 中得imageUrl. 非临时
     *
     * @param htmlContent html 内容
     * @return 修改过的html内容
     */
    String changeImageFileStatusFromHtml(String htmlContent);

    /**
     * 从html中获得文件名(uuid+filename) List
     *
     * @param htmlContent
     * @return
     */
    List<String> getImageFileNameListFromHtml(String htmlContent);

    /**
     * 根据 url 获得 唯一 FileName
     *
     * @param url
     * @return
     */
    String getFileNameFromUrl(String url);

    /**
     * 根据 url 获得 唯一 FileName
     *
     * @param url
     * @return
     */
    String getUriByUrl(String url);

    /**
     * 根据 响应获取文件访问路径, 如 image_item uri: 文件唯一名
     *
     * @param uri 数据所存放值, 实际就是文件名(包含后缀名)
     * @return
     */
    String getFileUrlByUri(String uri);

    /**
     * 如 image_item uri: 文件唯一名
     *
     * @param uri 数据所存放值, 实际就是文件名(包含后缀名)
     * @return
     */
    String getTmpFileUrlByUri(String uri);

}
