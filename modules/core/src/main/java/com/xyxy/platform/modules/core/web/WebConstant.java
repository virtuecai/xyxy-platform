package com.xyxy.platform.modules.core.web;

import java.util.HashMap;
import java.util.Map;

/**
 * @author caizhengda
 * @version [版本号, 2016-05-30]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class WebConstant {

    public static Map<String, Long> LAST_MODIFIED = new HashMap<String, Long>();

}
