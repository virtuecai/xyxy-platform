package com.xyxy.platform.modules.core.web.response;

/**
 *  数据绑定, 异常信息
 *  封装jsr数据验证错误提示信息
 * Created by VirtueCai on 15/11/20.
 */
public class BindingError {

    private String message;
    private String ObjectName;
    private String field;

    public BindingError() {
    }

    public BindingError(String message, String ObjectName, String field) {
        this.message = message;
        this.ObjectName = ObjectName;
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getObjectName() {
        return ObjectName;
    }

    public void setObjectName(String objectName) {
        ObjectName = objectName;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "BindingError{" +
                "message='" + message + '\'' +
                ", ObjectName='" + ObjectName + '\'' +
                ", field='" + field + '\'' +
                '}';
    }
}
