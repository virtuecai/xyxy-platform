package com.xyxy.platform.modules.core.mybatis.generator;


import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

import java.math.BigDecimal;
import java.sql.Types;

/**
 * 简单粗暴, 自定义jdbcType: tinyint -> javaType: Integer
 * 注意事项: mysql数据库中设计字段类型为 tinyint length =1, 本插件会判断为 BIT Byte
 * 所以为了使用生成代码插件出现问题, 数据库设计不要使用bit, 使用tinyint
 */
public class JavaTypeResolverCustomImpl extends JavaTypeResolverDefaultImpl {

    @Override
    public FullyQualifiedJavaType calculateJavaType(IntrospectedColumn introspectedColumn) {
        FullyQualifiedJavaType answer;
        JdbcTypeInformation jdbcTypeInformation = typeMap.get(introspectedColumn.getJdbcType());
        System.out.println("FullyQualifiedJavaType calculateJavaType --> introspectedColumn.getJdbcType(): " + introspectedColumn.getJdbcType());
        if (jdbcTypeInformation == null || introspectedColumn.getJdbcType() == Types.BIT || introspectedColumn.getJdbcType() == Types.TINYINT ) {
            switch (introspectedColumn.getJdbcType()) {
                case Types.DECIMAL:
                case Types.NUMERIC:
                    if (introspectedColumn.getScale() > 0
                            || introspectedColumn.getLength() > 18
                            || forceBigDecimals) {
                        answer = new FullyQualifiedJavaType(BigDecimal.class
                                .getName());
                    } else if (introspectedColumn.getLength() > 9) {
                        answer = new FullyQualifiedJavaType(Long.class.getName());
                    } else if (introspectedColumn.getLength() > 4) {
                        answer = new FullyQualifiedJavaType(Integer.class.getName());
                    } else {
                        answer = new FullyQualifiedJavaType(Short.class.getName());
                    }
                    break;
                case Types.BIT:
                    System.out.println("case Types.BIT:");
                    answer = new FullyQualifiedJavaType(Integer.class.getName());
                    break;
                case Types.TINYINT:
                    System.out.println("Types.TINYINT:");
                    answer = new FullyQualifiedJavaType(Integer.class.getName());
                    break;
                default:
                    answer = null;
                    break;
            }
        } else {
            answer = jdbcTypeInformation.getFullyQualifiedJavaType();
        }

        return answer;
    }

    @Override
    public String calculateJdbcTypeName(IntrospectedColumn introspectedColumn) {
        String answer;
        JdbcTypeInformation jdbcTypeInformation = typeMap.get(introspectedColumn.getJdbcType());

        System.out.println("String calculateJavaType --> introspectedColumn.getJdbcType(): " + introspectedColumn.getJdbcType());

        if (jdbcTypeInformation == null) {
            switch (introspectedColumn.getJdbcType()) {
                case Types.DECIMAL:
                    answer = "DECIMAL"; //$NON-NLS-1$
                    break;
                case Types.NUMERIC:
                    answer = "NUMERIC"; //$NON-NLS-1$
                    break;
                case Types.BIT:
                    answer = "INTEGER"; //$NON-NLS-1$
                    break;
                case Types.TINYINT:
                    answer = "INTEGER"; //$NON-NLS-1$
                    break;
                default:
                    answer = null;
                    break;
            }
        } else {
            answer = jdbcTypeInformation.getJdbcTypeName();
        }

        return answer;
    }


}
