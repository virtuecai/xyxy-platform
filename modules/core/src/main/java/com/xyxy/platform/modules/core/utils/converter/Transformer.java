package com.xyxy.platform.modules.core.utils.converter;

/**
 * 将对象从转换为另外一个对象
 * Created by juju on 14-5-15.
 */
public interface Transformer<TFrom, TTo> {
    TTo transform(TFrom from);

    TTo transform(TFrom from, TTo defaultVal);
}
