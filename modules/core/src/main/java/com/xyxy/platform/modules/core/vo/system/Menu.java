package com.xyxy.platform.modules.core.vo.system;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 组装 resource, 用于页面menu显示
 * Created by VirtueCai on 15/11/16.
 */
public class Menu {

    private Long id; //编号
    private String name; //资源名称
    private String url; //资源路径
    private Long parentId; //父编号
    private String parentIds; //父编号列表

    private List<Menu> childMenuList = new ArrayList<>(); //子级菜单

    public Menu() {
    }

    public Menu(Long id, String name, String url, Long parentId, String parentIds) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.parentId = parentId;
        this.parentIds = parentIds;
    }

    /**
     * 判断menu是否属于第二集菜单, 因为菜单中最外层是第二级
     *
     * @return
     */
    public boolean isSecondLevelMenu() {
        if (StringUtils.isNotBlank(this.parentIds)) {
            String ids = this.parentIds.lastIndexOf("/") == -1 ? this.parentIds : this.parentIds.substring(0, this.parentIds.length() - 1);
            return ids.split("/").length == 2 ? true : false;
        }
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    public List<Menu> getChildMenuList() {
        return childMenuList;
    }

    public void setChildMenuList(List<Menu> childMenuList) {
        this.childMenuList = childMenuList;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", parentId=" + parentId +
                ", parentIds='" + parentIds + '\'' +
                ", childMenuList=" + childMenuList +
                '}';
    }

}
