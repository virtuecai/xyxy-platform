package com.xyxy.platform.modules.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Time: 下午12:03
 * To change this template use File | Settings | File Templates.
 */
public class ItemUtils {

    /**
     * 从连接中提取出宝贝IID
     *
     * @param linkUrl 链接
     * @return
     */
    public static String queryIdFromLink(String linkUrl) {
        if (StringUtils.isBlank(linkUrl)) {
            return null;
        }
        Pattern p = Pattern.compile("id=[0-9]+");
        Matcher m = p.matcher(linkUrl);
        if (m.find()) {
            return m.group().replace("id=", "");
        }
        return linkUrl;
    }

    /**
     * 从URL中提取ID
     *
     * @param linkUrl 搜索关键字
     * @return 链接中的ID, 如果不是链接, 则返回NULL
     */
    public static Long getIdFromLink(String linkUrl) {
        if (StringUtils.isBlank(linkUrl)) {
            return null;
        }
        Pattern p = Pattern.compile("id=[0-9]+");
        Matcher m = p.matcher(linkUrl);
        Long result = null;
        if (m.find()) {
            String id = m.group().replace("id=", "");
            try {
                result = Long.parseLong(id);
            } catch (NumberFormatException nfe) {
                return null;
            }
        }
        return result;
    }

}
