package com.xyxy.platform.modules.core.utils;

import org.apache.shiro.codec.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by liushun on 2015/12/1 0001.
 * 先用MD5加密 在用SHA加密
 */
public class PasswordEncode {

    //必须16位字符串
    private static final String key = "http://xyxy.com/";
    /***
     * SHA加密 生成40位SHA码
     * @return 返回40位SHA码
     */
    public static String shaEncode(String md5) throws Exception {
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

        byte[] byteArray = md5.getBytes("UTF-8");
        byte[] md5Bytes = sha.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }


    public static  String  md5Encode(String tel)
    {
        BigInteger bigInteger=null;

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] inputData = tel.getBytes();
            md.update(inputData);
            bigInteger = new BigInteger(md.digest());
        } catch (Exception e) {e.printStackTrace();}
        return bigInteger.toString(16);
    }

    // 加密
    public static String EncString(String sSrc, String sKey){
        if (sKey == null) {
            System.out.print("Key为空null");
            return null;
        }
        // 判断Key是否为16位
        if (sKey.length() != 16) {
            System.out.print("Key长度不是16位");
            return null;
        }
        try{
            byte[] raw = sKey.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            AtomicReference<Cipher> cipher = new AtomicReference<>(Cipher.getInstance("AES/ECB/PKCS5Padding"));//"算法/模式/补码方式"
            cipher.get().init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.get().doFinal(sSrc.getBytes("utf-8"));
            return new Base64().encodeToString(encrypted);//此处使用BASE64做转码功能，同时能起到2次加密的作用。
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static String getENCPassword(String tel){
        try{
            String ants = EncString(tel, key);
            String sha = shaEncode(ants);
            return sha;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 测试主函数
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {
        System.out.println(getENCPassword("liushun"));
    }
}
