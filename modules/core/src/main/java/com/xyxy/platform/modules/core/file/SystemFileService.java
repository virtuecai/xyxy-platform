package com.xyxy.platform.modules.core.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.InputStream;
import java.util.List;

/**
 * 简介: 系统文件处理
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-29 11:23
 */
@Service
public class SystemFileService {

    protected static final Logger logger = LoggerFactory.getLogger(SystemFileService.class);

    @Value("${file.system.server.isLocal:false}")
    private boolean isLocal;

    // 文件本地存储实现
    @Resource(name = "SystemFileServiceLocalImpl")
    private ISystemFileService systemFileServiceLocalImpl;

    // 文件ftp存储实现
    @Resource(name = "SystemFileServiceFtpImpl")
    private ISystemFileService systemFileServiceFtpImpl;

    // 根据isLocal配置得到最终实现实例
    private ISystemFileService systemFileService;

    @PostConstruct
    public void init() {
        systemFileService = isLocal ? systemFileServiceLocalImpl : systemFileServiceFtpImpl;
    }

    public String getFilePath() {
        return systemFileService.getFilePath();
    }

    public String getTmpFilePath() {
        return systemFileService.getTmpFilePath();
    }

    public String saveTmpFile(InputStream inputStream, String fileName) {
        return systemFileService.saveTmpFile(inputStream, fileName);
    }

    public boolean deleteTmpFile(String primaryFileName) {
        return systemFileService.deleteTmpFile(primaryFileName);
    }

    public void changeFileStatus(String primaryFileName) {
        systemFileService.changeFileStatus(primaryFileName);
    }

    public String changeImageFileStatusFromHtml(String htmlContent) {
        return systemFileService.changeImageFileStatusFromHtml(htmlContent);
    }

    public List<String> getImageFileNameListFromHtml(String htmlContent) {
        return systemFileService.getImageFileNameListFromHtml(htmlContent);
    }

    public String getFileNameFromUrl(String url) {
        return systemFileService.getFileNameFromUrl(url);
    }

    public String getUriByUrl(String url) {
        return systemFileService.getUriByUrl(url);
    }

    public String getFileUrlByUri(String uri) {
        return systemFileService.getFileUrlByUri(uri);
    }

    public String getTmpFileUrlByUri(String uri) {
        return systemFileService.getTmpFileUrlByUri(uri);
    }
}
