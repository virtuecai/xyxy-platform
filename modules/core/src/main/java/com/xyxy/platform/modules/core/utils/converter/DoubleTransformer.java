package com.xyxy.platform.modules.core.utils.converter;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * 将字符串转换为整数
 * Created by juju on 14-5-15.
 */
public class DoubleTransformer extends StringTransformer<Double> {

    public Double transform(String from) {
        return transform(from, 0d);
    }

    public Double transform(String from, Double defaultVal) {
        return NumberUtils.toDouble(from, defaultVal);
    }
}
