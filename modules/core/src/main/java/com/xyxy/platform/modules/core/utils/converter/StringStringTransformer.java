package com.xyxy.platform.modules.core.utils.converter;

/**
 * 字符串到字符串, 即不转换
 * Created by juju on 14-5-15.
 */
public class StringStringTransformer extends StringTransformer<String> {

    public String transform(String from) {
        return from;
    }

    public String transform(String from, String defaultVal) {
        return from;
    }
}
