package com.xyxy.platform.modules.core.file.impl;

import com.google.common.collect.Lists;
import com.xyxy.platform.modules.core.file.ISystemFileService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * 简介: 系统文件处理
 * ftp 实现
 *
 * @author Caizhenegda
 * @version 1.0
 * @Date 2015-12-29 11:23
 */
@Service("SystemFileServiceFtpImpl")
public class SystemFileServiceFtpImpl implements ISystemFileService {

    protected static final Logger logger = LoggerFactory.getLogger(SystemFileServiceFtpImpl.class);

    /**
     * 文件服务端 ip
     */
    @Value("${file.system.server.ip:\"\"}")
    private String fileSystemServerIp;

    /**
     * 文件服务端 端口
     */
    @Value("${file.system.server.port:21}")
    private Integer fileSystemServerPort;

    /**
     * 文件服务端连接 用户名
     */
    @Value("${file.system.server.username:\"\"}")
    private String fileSystemServerUserName;

    /**
     * 文件服务端连接 密码
     */
    @Value("${file.system.server.password:\"\"}")
    private String fileSystemServerPassword;

    /**
     * 文件 存储 基本路径
     */
    @Value("${file.system.server.base.path:\"\"}")
    private String fileSystemServerBasePath;


    /**
     * 文件访问url(根据子级访问临时与非临时)
     */
    @Value("${file.system.server.access.url:\"\"}")
    private String fileAccessUrl;

    /**
     * 非临时文件 存储 路径 (使用中的文件)
     */
    @Override
    public String getFilePath() {
        return fileSystemServerBasePath + File.separator + "files";
    }

    /**
     * 临时文件 存储 路径
     */
    @Override
    public String getTmpFilePath() {
        return fileSystemServerBasePath + File.separator + "filestmp";
    }


    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        //创建文件夹路径
        FTPClient ftpClient = null;
        try {
            ftpClient = getFtpClient();
            ftpClient.mkd(getFilePath());
            ftpClient.mkd(getTmpFilePath());
        } catch (IOException e) {
            logger.error("获得ftpclient失败, e: {}", e.getMessage());
        } finally {
            try {
                ftpClient.disconnect();
            } catch (IOException e) {
                logger.error("关闭 FtpClient 连接失败, e: {}", e.getMessage());
            }
        }

    }

    /**
     * 获得ftp服务连接客户端
     *
     * @return
     */
    private FTPClient getFtpClient() throws IOException {
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(this.fileSystemServerIp, this.fileSystemServerPort);
        ftpClient.login(this.fileSystemServerUserName, this.fileSystemServerPassword);
        ftpClient.changeWorkingDirectory(getTmpFilePath());
        ftpClient.setControlEncoding("GBK");
        //设置文件类型（二进制）
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.setBufferSize(1024 * 2);
        return ftpClient;
    }

    /**
     * 存储文件为临时文件
     *
     * @param inputStream 文件流
     * @param fileName    原名, 会在其基础上加入 uuid-
     */
    @Override
    public String saveTmpFile(InputStream inputStream, String fileName) {
        fileName = UUID.randomUUID().toString() + fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        FTPClient ftpClient = null;
        try {
            ftpClient = getFtpClient();
            ftpClient.storeFile(new String(fileName.getBytes("GBK"), "iso-8859-1"), inputStream);
        } catch (IOException e) {
            logger.error("FTP客户端出错！e: {}", e.getMessage());
        } finally {
            IOUtils.closeQuietly(inputStream);
            try {
                ftpClient.disconnect();
            } catch (IOException e) {
                logger.error("关闭FTP连接发生异常！e: {}", e.getMessage());
            }
        }
        return new StringBuffer(this.fileAccessUrl).append("/").append(tmpFilesDir).append("/").append(fileName).toString();
    }

    /**
     * 删除临时文件
     *
     * @param primaryFileName 文件名
     * @return
     */
    @Override
    @Async
    public boolean deleteTmpFile(String primaryFileName) {
        FTPClient ftpClient = null;
        boolean result = false;
        try {
            ftpClient = getFtpClient();
            result = ftpClient.deleteFile(this.getTmpFilePath() + File.separator + primaryFileName);
        } catch (IOException e) {
            logger.error("删除删除临时文件失败！e: {}", e.getMessage());
        } finally {
            try {
                ftpClient.disconnect();
            } catch (IOException e) {
                logger.error("关闭FTP连接发生异常！e: {}", e.getMessage());
            }
        }
        return result;
    }

    /**
     * 改变临时文件状态
     * 将临时文件拷贝至使用文件目录(非临时文件目录)
     *
     * @param primaryFileName 文件名
     */
    @Override
    public void changeFileStatus(String primaryFileName) {
        ByteArrayInputStream in = null;
        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        FTPClient ftpClient;
        try {
            ftpClient = this.getFtpClient();
            // 将文件读到内存中
            ftpClient.retrieveFile(new String(primaryFileName.getBytes("GBK"), "iso-8859-1"), fos);
            in = new ByteArrayInputStream(fos.toByteArray());
            if (in != null) {
                ftpClient.changeWorkingDirectory(this.getFilePath());
                ftpClient.storeFile(new String(primaryFileName.getBytes("GBK"), "iso-8859-1"), in);
            }
        } catch (IOException e) {
            logger.error("changeFileStatus 失败, e: {}", e.getMessage());
        } finally {
            // 关闭流
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 改变临时文件状态
     * 解析 html 中得图片地址, 目前仅支持: <image src="imageUrl" />
     * 1. 根据 imageUrl 获得对于的 文件名(uuid+文件名)
     * 2. 将临时文件拷贝到非临时文件目录去.
     * 3. 修改 htmlContent 中得imageUrl. 非临时
     *
     * @param htmlContent html 内容
     * @return 修改过的html内容
     */
    @Override
    public String changeImageFileStatusFromHtml(String htmlContent) {
        //更新html内容
        Document document = Jsoup.parse(htmlContent);
        List<String> fileNameList = Lists.newArrayList();
        Elements images = document.select("img[src]");
        for (Element element : images) {
            String imgUrl = element.attr("src");
            if (imgUrl.indexOf(tmpFilesDir) == -1) continue;
            String fileName = imgUrl.substring(imgUrl.lastIndexOf("/") + 1);
            if (StringUtils.isNotEmpty(fileName)) {
                imgUrl = this.getFileUrlByUri(fileName);
                element.attr("src", imgUrl);
                fileNameList.add(fileName);
            }
        }
        //copy 文件 (临时文件不用删, 有 job 会完成)
        for (String fileName : fileNameList) {
            this.changeFileStatus(fileName);
        }
        return document.select("body").html().toString();
    }

    /**
     * 从html中获得文件名(uuid+filename) List
     *
     * @param htmlContent
     * @return
     */
    @Override
    public List<String> getImageFileNameListFromHtml(String htmlContent) {
        List<String> fileNameList = Lists.newArrayList();
        Document document = Jsoup.parse(htmlContent);
        Elements images = document.select("img[src]");
        for (Element element : images) {
            String imgUrl = element.attr("src");
            String fileName = imgUrl.substring(imgUrl.lastIndexOf("/") + 1);
            if (StringUtils.isNotEmpty(fileName)) {
                fileNameList.add(fileName);
            }
        }
        return fileNameList;
    }

    /**
     * 根据 url 获得 唯一 FileName
     *
     * @param url
     * @return
     */
    @Override
    public String getFileNameFromUrl(String url) {
        if (StringUtils.isNotEmpty(url) && url.indexOf("/") != -1) {
            return url.substring(url.lastIndexOf("/") + 1);
        }
        return null;
    }

    /**
     * 根据 url 获得 唯一 FileName
     *
     * @param url
     * @return
     */
    @Override
    public String getUriByUrl(String url) {
        if (StringUtils.isNotEmpty(url) && url.indexOf("/") != -1) {
            return url.substring(url.lastIndexOf("/") + 1);
        }
        return null;
    }

    /**
     * 根据 响应获取文件访问路径, 如 image_item uri: 文件唯一名
     *
     * @param uri
     * @return
     */
    @Override
    public String getFileUrlByUri(String uri) {
        if (StringUtils.isNotEmpty(uri)) {
            return new StringBuffer(this.fileAccessUrl).append("/").append(filesDir).append("/").append(uri).toString();
        }
        return "";
    }

    @Override
    public String getTmpFileUrlByUri(String uri) {
        if (StringUtils.isNotEmpty(uri)) {
            return new StringBuffer(this.fileAccessUrl).append("/").append(tmpFilesDir).append("/").append(uri).toString();
        }
        return "";
    }

}
