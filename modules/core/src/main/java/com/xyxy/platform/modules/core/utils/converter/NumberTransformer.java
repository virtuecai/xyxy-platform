package com.xyxy.platform.modules.core.utils.converter;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * 将字符串转换为整数
 * Created by juju on 14-5-15.
 */
public class NumberTransformer extends StringTransformer<Number> {

    public Number transform(String from) {
        return transform(from, null);
    }

    public Number transform(String from, Number defaultVal) {
        try {
            return NumberUtils.createNumber(from);
        } catch (Exception ex) {
            return defaultVal;
        }
    }
}
