package com.xyxy.platform.modules.core.utils.converter;


import com.xyxy.platform.modules.core.utils.Reflections;

/**
 * 将字符串从转换为另外一个基本对象
 * Created by juju on 14-5-15.
 */
public abstract class StringTransformer<TTo> implements Transformer<String, TTo> {

    protected Class<TTo> clazz;

    public StringTransformer() {
        if (this.getClass().getSimpleName().contains("$$EnhancerByCGLIB$$")) {
            return;
        }
        Class<TTo> clazz = Reflections.
                getClassGenricType(this.getClass());
        if (clazz.equals(Object.class)) {
            return;
        }
        this.clazz = clazz;
    }

    public boolean support(Class<TTo> clazz) {
        return this.clazz.equals(clazz);
    }

    public Class<TTo> getClazz() {
        return clazz;
    }
}
