package com.xyxy.platform.modules.core.utils.converter;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * 将字符串转换为整数
 * Created by juju on 14-5-15.
 */
public class IntegerTransformer extends StringTransformer<Integer> {

    public Integer transform(String from) {
        return transform(from, 0);
    }

    public Integer transform(String from, Integer defaultVal) {
        return NumberUtils.toInt(from, defaultVal);
    }
}
