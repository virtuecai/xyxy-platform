package com.xyxy.platform.modules.core.utils;

/**
 * CPU使用情况信息
 * Time: 下午3:13
 * To change this template use File | Settings | File Templates.
 */
public class OSComInfo {
    private float cpu;
    private float memory;

    public float getCpu() {
        return cpu;
    }

    public void setCpu(float cpu) {
        this.cpu = cpu;
    }

    public float getMemory() {
        return memory;
    }

    public void setMemory(float memory) {
        this.memory = memory;
    }
}
