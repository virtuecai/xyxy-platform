package com.xyxy.platform.modules.core.utils.converter;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * 将字符串转换为整数
 * Created by juju on 14-5-15.
 */
public class FloatTransformer extends StringTransformer<Float> {

    public Float transform(String from) {
        return transform(from, 0f);
    }

    public Float transform(String from, Float defaultVal) {
        return NumberUtils.toFloat(from, defaultVal);
    }
}
