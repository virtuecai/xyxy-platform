package com.xyxy.platform.modules.core.web.servlet;

import com.xyxy.platform.modules.core.web.WebConstant;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.util.Date;

/**
 * 用于初始化全局 变量
 * @author caizhengda
 * @version [版本号, 2016-05-30]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class InitServlet extends HttpServlet {

    private static final long serialVersionUID = -2240139285160469823L;

    @Override
    public void init() throws ServletException {
        super.init();

        ServletContext ctx = getServletContext();

        String contextPath = ctx.getContextPath();

        ctx.setAttribute("ctx", contextPath);

        ctx.setAttribute("version", System.currentTimeMillis());

        WebConstant.LAST_MODIFIED.put("lastModified", new Date().getTime());
    }
}
